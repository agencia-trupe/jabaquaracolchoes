-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: mysql
-- Generation Time: 03-Abr-2020 às 02:45
-- Versão do servidor: 5.7.22
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jabaquaracolchoes`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `banners`
--

CREATE TABLE `banners` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `a_partir_de` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ativo` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `banners`
--

INSERT INTO `banners` (`id`, `ordem`, `imagem`, `texto_1`, `texto_2`, `texto_3`, `a_partir_de`, `link`, `ativo`, `created_at`, `updated_at`) VALUES
(2, 9, 'menino-no-castorzinho-clean_20180927190305ivMdCeguQT.jpg', 'Vários Modelos', 'Mola ou Espuma', 'Colchões Castor', '', 'http://www.colchaostore.com.br/produtos/colchoes?marca=3', 0, '2018-09-27 22:03:06', '2020-03-19 16:09:42'),
(3, 8, '88_20181031191729c55yG2Os0B.jpeg', 'Últimos Lançamentos', 'Alta Tecnologia', 'Simmons', '', 'http://www.colchaostore.com.br/produtos/colchoes?marca=8', 0, '2018-09-27 22:14:43', '2020-03-19 16:09:35'),
(6, 6, 'mh-2650_20181101152820rcbsSa2UfU.jpg', 'Cabeçeiras Herval', 'Acabamento Primoroso', 'Tecidos de alta qualidade ', '', 'http://www.colchaostore.com.br/produtos/detalhe/cabeceira-mh-2650', 0, '2018-11-01 18:28:21', '2020-03-19 16:09:22'),
(7, 7, 'poltrona-castor-cinema-corano-1e576f2358121b42026b17bc9660d428_201811011533544iuIkp1GR4.jpg', 'Pronta entrega', 'Automáticas ou Manuais', 'Poltronas Reclináveis', '', 'http://www.colchaostore.com.br/produtos/detalhe/poltrona-cinema', 0, '2018-11-01 18:33:55', '2020-03-19 16:09:28'),
(9, 2, 'cover3_20181114113922gc1WWXFpc6.png', '', '', '', '', '', 0, '2018-11-14 13:39:23', '2020-03-19 16:08:50'),
(18, 5, 'herval-sleep_20190430172449YYN80eybCL.jpg', 'Malha Stretch Antimicrobiana', 'Látex Natural', 'Sensação Agradável, Seca e Fresca', '', 'http://www.colchaostore.com.br/produtos/detalhe/sleep', 0, '2019-04-30 20:24:49', '2020-03-19 16:09:15'),
(19, 4, 'latex_20190530203514XLSfSBVix2.jpg', 'Pillow Top de Látex', 'Travesseiros de Látex', 'Colchões  Com Látex', '', 'http://www.colchaostore.com.br/produtos/marcas/12', 0, '2019-05-30 23:35:14', '2020-03-19 16:09:08'),
(25, 3, 'sim-imgunica-b2c02-1_20190910135520OlG51WZETg.png', '', '', '', '', '', 0, '2019-06-11 17:31:47', '2020-03-19 16:08:58'),
(29, 1, 'black-oficial_20191028211523WYZ9ni4EXB.jpeg', '', '', '', '', '', 0, '2019-10-29 00:15:24', '2019-12-02 15:13:28'),
(30, 0, 'natal-simmons-flex-e-epeda-capa-site-2000x768px_20191204211630udNPSV4ROV.png', '', '', '', '', '', 0, '2019-12-05 00:16:30', '2019-12-26 15:08:19'),
(31, 0, 'b_20200131205446N0hBm5GErF.jpg', '', '', '', '', '', 0, '2020-01-31 23:54:46', '2020-03-19 16:08:40'),
(32, 0, 'covid-02_20200319130040j79c8F1UY5.jpg', '', '', '', '', '', 1, '2020-03-19 16:00:41', '2020-03-19 16:00:41');

-- --------------------------------------------------------

--
-- Estrutura da tabela `configuracoes`
--

CREATE TABLE `configuracoes` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `keywords` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem_de_compartilhamento` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `analytics` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `configuracoes`
--

INSERT INTO `configuracoes` (`id`, `title`, `description`, `keywords`, `imagem_de_compartilhamento`, `analytics`, `created_at`, `updated_at`) VALUES
(1, 'Jabaquara Colchões', 'Jabaquara Colchões', 'colchoes, camas, travesseiros, móveis, bicama, colchão, bases, box, box baú, colchões,store,cama,pillowtop,protetordecolchão,travesseiro,medidasespeciais,colchãosobmedida,colchoessobmedida,minicama,colchaodeberço,camabox,poltronareclinavel,', '', '', NULL, '2020-03-31 22:56:18');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contato`
--

CREATE TABLE `contato` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `whatsapp` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `endereco` text COLLATE utf8_unicode_ci NOT NULL,
  `google_maps` text COLLATE utf8_unicode_ci NOT NULL,
  `horario_de_atendimento` text COLLATE utf8_unicode_ci NOT NULL,
  `cnpj` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `instagram` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `g_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `g_telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `g_whatsapp` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `g_endereco` text COLLATE utf8_unicode_ci NOT NULL,
  `g_google_maps` text COLLATE utf8_unicode_ci NOT NULL,
  `g_horario_de_atendimento` text COLLATE utf8_unicode_ci NOT NULL,
  `g_cnpj` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `contato`
--

INSERT INTO `contato` (`id`, `email`, `telefone`, `whatsapp`, `endereco`, `google_maps`, `horario_de_atendimento`, `cnpj`, `facebook`, `instagram`, `created_at`, `updated_at`, `g_email`, `g_telefone`, `g_whatsapp`, `g_endereco`, `g_google_maps`, `g_horario_de_atendimento`, `g_cnpj`) VALUES
(1, 'contatoj@jabaquaracolchoes.com.br', '11 5581 5882, 11 2577 9063', '11 94357 7058', 'Av. Jabaquara 996 &middot; Mirand&oacute;polis &middot; 04046-100 &middot; S&atilde;o Paulo&nbsp;&middot; SP<br />\r\nEstacionamento conveniado: Rua Urutuba, 118', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3655.7601751917796!2d-46.64037578459539!3d-23.6129324846573!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce5a4eec0b0959%3A0xf6de39b5e3938385!2sAv.%20Jabaquara%2C%20996%20-%20Mirand%C3%B3polis%2C%20S%C3%A3o%20Paulo%20-%20SP%2C%2004046-100!5e0!3m2!1spt-BR!2sbr!4v1585881835302!5m2!1spt-BR!2sbr\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\" aria-hidden=\"false\" tabindex=\"0\"></iframe>', 'Segunda &agrave; Sexta das 09h &agrave;s 19h<br />\r\nS&aacute;bados das 09h &agrave;s 18h<br />\r\nDomingo: Fechado', '01.637.270/0001-70', 'https://www.facebook.com/colchaostore', 'https://www.instagram.com/jabaquaracolchoes/', NULL, '2020-04-03 02:44:02', 'contatog@jabaquaracolchoes.com.br', '11 5589 9967, 11 5581 2674, 11 5585 9663', '11 97191 0016', 'Rua Gravi 245&nbsp;&middot; Sa&uacute;de&nbsp;&middot; 04143-050&nbsp;&middot; S&atilde;o Paulo&nbsp;&middot; SP<br />\r\nEstacionamento conveniado: Rua Gravi, 205', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3655.745916198877!2d-46.63998098459566!3d-23.613443684657152!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce5a4e8b6312ed%3A0x491fe2081219589!2sR.%20Gravi%2C%20245%20-%20Vila%20da%20Sa%C3%BAde%2C%20S%C3%A3o%20Paulo%20-%20SP%2C%2004143-050!5e0!3m2!1spt-BR!2sbr!4v1585798042823!5m2!1spt-BR!2sbr\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\" aria-hidden=\"false\" tabindex=\"0\"></iframe>', 'Segunda &agrave; Sexta das 09h &agrave;s 19h<br />\r\nS&aacute;bados das 09h &agrave;s 18h<br />\r\nDomingo: Fechado', '54.596.275/0001-69');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contatos_recebidos`
--

CREATE TABLE `contatos_recebidos` (
  `id` int(10) UNSIGNED NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mensagem` text COLLATE utf8_unicode_ci NOT NULL,
  `lido` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `curriculos`
--

CREATE TABLE `curriculos` (
  `id` int(10) UNSIGNED NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cargo_pretendido` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `arquivo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lido` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `frete`
--

CREATE TABLE `frete` (
  `id` int(10) UNSIGNED NOT NULL,
  `estado` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cidade` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `de0_a_20` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `de20_a_50` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `de50_a_100` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `de100_a_150` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `de150_a_200` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mais_de_200` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ad` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pedagio` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `taxas_diversas` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gris` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cep_de` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cep_ate` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `valor_isento` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `coleta` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `divisao` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `icms` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `frete`
--

INSERT INTO `frete` (`id`, `estado`, `cidade`, `de0_a_20`, `de20_a_50`, `de50_a_100`, `de100_a_150`, `de150_a_200`, `mais_de_200`, `ad`, `pedagio`, `taxas_diversas`, `gris`, `cep_de`, `cep_ate`, `valor_isento`, `coleta`, `divisao`, `icms`, `created_at`, `updated_at`) VALUES
(1, 'AL', 'Maceió', '55.00', '85.00', '125.00', '140.00', '160.00', '0.90', '5.00', '120.00', '15.00', '0.10', '57000', '57099', '0.00', '120.00', '0.93', 'sim', NULL, '2019-06-08 22:35:19'),
(2, 'BA', 'Salvador', '55.00', '85.00', '125.00', '140.00', '160.00', '0.90', '5.00', '120.00', '15.00', '0.10', '40000', '44470', '0.00', '120.00', '0.93', 'sim', NULL, '2019-06-08 22:35:40'),
(3, 'CE', 'Fortaleza', '55.00', '85.00', '125.00', '140.00', '160.00', '0.90', '5.00', '120.00', '20.00', '0.10', '60000', '61900', '0.00', '120.00', '0.93', 'sim', NULL, '2019-06-08 22:35:56'),
(4, 'DF', 'Brasí­lia', '55.00', '85.00', '105.00', '140.00', '160.00', '0.90', '5.00', '120.00', '15.00', '0.10', '70000', '73699', '0.00', '120.00', '0.93', 'sim', NULL, '2019-06-08 22:36:15'),
(5, 'ES', 'Vitória', '55.00', '85.00', '105.00', '140.00', '160.00', '0.90', '5.00', '120.00', '15.00', '0.10', '29000', '29099', '0.00', '120.00', '0.93', 'sim', NULL, '2019-06-08 22:36:30'),
(6, 'GO', 'Goiânia', '155.00', '185.00', '205.00', '240.00', '260.00', '2.00', '2.00', '120.00', '15.00', '1.00', '72800', '74894', '0.00', '120.00', '0.93', 'sim', NULL, '2019-01-10 15:27:36'),
(7, 'MG', 'Belo Horizonte', '45.00', '70.00', '90.00', '130.00', '140.00', '0.65', '0.80', '120.00', '10.00', '0.10', '30000', '34999', '0.00', '120.00', '0.95', 'sim', NULL, '2019-06-08 22:36:49'),
(8, 'MG', 'Interior', '45.00', '78.00', '100.00', '130.00', '140.00', '0.70', '0.80', '120.00', '15.00', '0.10', '35000', '39999', '0.00', '120.00', '0.93', 'sim', NULL, '2019-06-08 22:37:11'),
(9, 'MT', 'Cuiabá', '55.00', '85.00', '105.00', '140.00', '160.00', '0.90', '5.00', '120.00', '15.00', '0.10', '78000', '78109', '0.00', '120.00', '0.93', 'sim', NULL, '2019-06-08 22:37:28'),
(10, 'PB', 'João Pessoa', '55.00', '85.00', '105.00', '140.00', '160.00', '0.90', '5.00', '120.00', '15.00', '0.10', '58000', '58099', '0.00', '120.00', '0.93', 'sim', NULL, '2019-06-08 22:37:51'),
(11, 'PE', 'Recife', '55.00', '85.00', '105.00', '140.00', '160.00', '0.90', '5.00', '120.00', '15.00', '0.10', '50000', '57999', '0.00', '120.00', '0.93', 'sim', NULL, '2019-06-08 22:38:09'),
(12, 'PR', 'Curitiba', '45.00', '70.00', '90.00', '130.00', '140.00', '0.65', '0.80', '120.00', '15.00', '0.10', '80000', '82999', '0.00', '120.00', '0.95', 'sim', NULL, '2019-06-08 22:38:34'),
(13, 'RJ', 'capital', '45.00', '100.00', '110.00', '130.00', '140.00', '0.65', '0.90', '120.00', '15.00', '0.10', '20000', '23799', '0.00', '120.00', '0.95', 'sim', NULL, '2019-11-21 19:28:38'),
(14, 'RJ', 'Interior', '90.00', '110.00', '130.00', '160.00', '180.00', '210.00', '2.00', '120.00', '15.00', '0.20', '26601', '28999', '0.00', '120.00', '0.95', 'sim', NULL, '2019-11-21 22:42:05'),
(15, 'RJ', 'metrop', '45.00', '78.00', '100.00', '130.00', '140.00', '0.77', '0.75', '30.00', '15.00', '0.10', '23800', '26600', '0.00', '120.00', '0.95', 'sim', NULL, '2019-01-10 15:29:51'),
(16, 'RS', 'Porto Alegre', '45.00', '70.00', '90.00', '130.00', '140.00', '0.75', '0.75', '120.00', '10.00', '0.10', '90000', '91999', '0.00', '120.00', '0.90', 'sim', NULL, '2019-06-08 22:39:32'),
(17, 'SC', 'Florianópolis', '45.00', '70.00', '90.00', '130.00', '140.00', '0.75', '0.70', '120.00', '10.00', '0.10', '88000', '88469', '0.00', '120.00', '0.95', 'sim', NULL, '2019-06-08 22:39:51'),
(18, 'SE', 'Aracaju', '55.00', '85.00', '105.00', '140.00', '160.00', '0.90', '5.00', '120.00', '15.00', '0.10', '49000', '49099', '0.00', '120.00', '0.93', 'sim', NULL, '2019-06-08 22:40:17'),
(19, 'SP', 'ABC', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '30.00', '5.00', '0.00', '09500', '09999', '500.00', '79.00', '0.00', 'nao', NULL, '2019-06-08 22:40:36'),
(20, 'SP', 'centro free', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '30.00', '0.00', '0.00', '01001', '02099', '200.00', '79.00', '0.00', 'nao', NULL, '2019-06-08 22:41:32'),
(21, 'SP', 'diadema', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '30.00', '0.00', '0.00', '09925', '09935', '500.00', '79.00', '0.00', 'nao', NULL, '2019-06-08 22:41:08'),
(22, 'SP', 'GSP 1', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '30.00', '0.00', '0.00', '09300', '09499', '0.00', '129.00', '0.00', 'nao', NULL, '2019-06-08 22:41:54'),
(23, 'SP', 'GSP 2', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '30.00', '0.00', '0.00', '06300', '06999', '0.00', '129.00', '0.00', 'nao', NULL, '2019-06-08 22:42:54'),
(24, 'SP', 'guarulhos', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '30.00', '0.00', '0.00', '07000', '07399', '0.00', '129.00', '0.00', 'nao', NULL, '2019-08-03 00:07:10'),
(25, 'SP', 'interior', '45.00', '80.00', '90.00', '130.00', '140.00', '0.75', '0.70', '120.00', '10.00', '0.10', '12000', '19999', '0.00', '120.00', '0.95', 'nao', NULL, '2019-06-08 22:34:03'),
(26, 'SP ', 'Itaquera', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '80.00', '0.00', '0.00', '08210', '08295', '0.00', '120.00', '0.00', 'sim', NULL, '2019-06-08 22:43:21'),
(27, 'SP', 'litoral', '40.00', '80.00', '105.00', '120.00', '140.00', '0.75', '0.75', '80.00', '10.00', '0.10', '11010', '11890', '0.00', '120.00', '0.95', 'sim', NULL, '2019-06-08 22:42:32'),
(28, 'SP', 'osasco', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '40.00', '0.00', '0.00', '06000', '06299', '0.00', '80.00', '0.00', 'sim', NULL, '2019-08-03 00:01:04'),
(29, 'SP', 'sto andre', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '80.00', '5.00', '0.00', '09000', '09299', '500.00', '79.00', '0.00', 'sim', NULL, '2019-06-08 22:44:06'),
(30, 'SP', 'Z L ', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '80.00', '0.00', '0.00', '03200', '03999', '500.00', '79.00', '0.00', 'sim', NULL, '2019-06-08 22:44:24'),
(31, 'SP', 'Z L free', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '80.00', '0.00', '0.00', '03000', '03199', '500.00', '79.00', '0.00', 'sim', NULL, '2019-06-08 22:44:59'),
(32, 'SP', 'Z Norte', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '80.00', '0.00', '0.00', '02100', '02999', '0.00', '89.00', '0.00', 'sim', NULL, '2019-06-08 22:45:17'),
(33, 'SP', 'Z S free', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '80.00', '0.00', '0.00', '04000', '04799', '300.00', '79.00', '0.00', 'sim', NULL, '2019-06-08 22:45:35'),
(34, 'SP', 'Z Sul', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '04800', '05899', '0.00', '89.00', '0.00', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2016_02_01_000000_create_users_table', 1),
('2016_03_01_000000_create_contato_table', 1),
('2016_03_01_000000_create_contatos_recebidos_table', 1),
('2017_09_01_163723_create_configuracoes_table', 1),
('2018_08_13_193614_create_paginas_table', 1),
('2018_08_13_202249_create_curriculos_table', 1),
('2018_08_14_150255_create_newsletter_table', 1),
('2018_08_15_023335_create_frete_table', 1),
('2018_08_17_171453_create_banners_table', 1),
('2018_08_17_175043_create_produtos_marcas_table', 1),
('2018_08_17_180352_create_produtos_propriedades_table', 1),
('2018_08_17_191024_create_produtos_categorias_table', 1),
('2018_08_17_203019_create_produtos_subcategorias_table', 1),
('2018_08_20_130112_create_produtos_table', 1),
('2018_08_20_130113_create_produtos_secoes_table', 1),
('2018_08_21_175128_create_produtos_destaques_table', 1),
('2018_08_21_175129_create_produtos_promocoes_table', 1),
('2018_08_21_191040_create_produto_propriedade_table', 1),
('2018_08_22_194813_create_produtos_medidas_table', 1),
('2018_08_31_171112_create_orcamentos_table', 1),
('2018_09_19_191451_add_conforto_to_produtos_table', 1),
('2018_09_20_185900_create_produtos_relacionados_table', 1),
('2018_10_26_140813_add_ordem_to_produtos_table', 1),
('2020_04_02_030553_alter_contato_table', 2),
('2020_04_02_033118_alter_produtos_table', 3),
('2020_04_03_010242_alter_orcamentos_table', 4);

-- --------------------------------------------------------

--
-- Estrutura da tabela `newsletter`
--

CREATE TABLE `newsletter` (
  `id` int(10) UNSIGNED NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `orcamentos`
--

CREATE TABLE `orcamentos` (
  `id` int(10) UNSIGNED NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cep` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mensagem` text COLLATE utf8_unicode_ci NOT NULL,
  `orcamento` text COLLATE utf8_unicode_ci NOT NULL,
  `lido` tinyint(1) NOT NULL DEFAULT '0',
  `loja` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tie` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `paginas`
--

CREATE TABLE `paginas` (
  `id` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `paginas`
--

INSERT INTO `paginas` (`id`, `titulo`, `slug`, `texto`, `created_at`, `updated_at`) VALUES
(1, 'Empresa', 'empresa', '<p>A Colch&atilde;o Store nasceu no in&iacute;cio de 2018 com o objetivo de ampliar a gama de produtos, buscando as novas inova&ccedil;&otilde;es e tecnologias em tudo que engloba o segmento de colch&otilde;es e conforto.</p>\r\n\r\n<p>Dispomos de um excelente Show Room com produtos em exposi&ccedil;&atilde;o que encantam a todos.</p>\r\n\r\n<p>Temos como diferencial entrega r&aacute;pida e eficiente.</p>\r\n\r\n<p>Contamos com colaboradores pr&oacute;prios e treinados.</p>\r\n\r\n<p>Despachamos para todo Brasil, via transportadoras credenciadas.</p>\r\n\r\n<p>Empresa do grupo Jabaquara Colch&otilde;es, que est&aacute; presente no mercado ddesde 1985, j&aacute; consolidada e de extremo prest&iacute;gio no mercado.</p>\r\n\r\n<p>Somos revendeores autorizados das melhores marcas de colch&otilde;es, tais como: Flex do Brasil, Epeda - Flex - Simmons - Aireloom, Colch&otilde;es Castor, MGA&nbsp;do Brasil - Eastman House, Colch&otilde;es Sankonfort, Mov&eacute;is e Colch&otilde;es Herval, entre outros.&nbsp;</p>\r\n', NULL, '2018-11-26 17:08:29'),
(2, 'Como comprar', 'como-comprar', '<p><span style=\"color:#e74c3c;\"><strong>Aten&ccedil;&atilde;o: Nossa loja n&atilde;o faz venda direta pelo site.</strong></span></p>\r\n\r\n<p>Aqui voc&ecirc;&nbsp;escolhe o produto que deseja, navegando pelas categorias: COLCH&Otilde;ES, BOX, BA&Uacute; &amp; GAVETAS, BICAMAS &amp; SOF&Aacute;-CAMA, POLTRONAS &amp; RECLIN&Aacute;VEIS, CABECEIRAS, TRAVESSEIROS, ACESS&Oacute;RIOS, etc.</p>\r\n\r\n<p>Voc&ecirc; tamb&eacute;m pode fazer uma &quot;busca&quot; pelo nome do produto ou fabricante.</p>\r\n\r\n<p>Escolha o produto, clique em solicitar or&ccedil;amento. Depois disso o produto selecionado ir&aacute; para o &quot;carrinho&quot;. Repita a opera&ccedil;&atilde;o para or&ccedil;ar mais produtos.&nbsp;</p>\r\n\r\n<p>Pronto, agora envie sua proposta. Responderemos prontamente!</p>\r\n\r\n<hr />\r\n<p><strong>Or&ccedil;amento</strong></p>\r\n\r\n<p>Se voce procurou e n&atilde;o achou algum produto em nosso site, solicite um or&ccedil;amento pelo FALE CONOSCO. Tamb&eacute;m fazemos medidas especias de colch&otilde;es de MOLEJO ou ESPUMA, e de todos tipos de Box (Ba&uacute;, Gavetas, Bicamas).</p>\r\n\r\n<hr />\r\n<p><strong>Regras de entrega de mercadorias</strong></p>\r\n\r\n<p>O acesso dos produtos no destino &eacute; de responsabilidade do cliente, portanto confira as dimens&otilde;es do produto e certifique-se de que passar&aacute; normalmente por elevadores, portas, escadas, e/ou corredores de sua moradia.</p>\r\n\r\n<p>N&atilde;o realizamos desmontagens e i&ccedil;amentos, eventuais despesas de dificuldade de entrega s&atilde;o de responsabilidade do cliente.</p>\r\n\r\n<p>Transportadoras terceirizadas n&atilde;o realizam &nbsp;montagens/desmontagens &nbsp;de produtos, e&nbsp;a entrega &eacute; feita no andar t&eacute;rreo&nbsp;da resid&ecirc;ncia ou apartamento, a fim de evitar riscos &agrave; seguran&ccedil;a do entregador e avarias no produto.</p>\r\n', NULL, '2018-11-01 22:22:32'),
(3, 'Formas de pagamento', 'formas-de-pagamento', '<p>&nbsp;</p>\r\n\r\n<p><strong>&Agrave; Vista</strong>: Boleto Banc&aacute;rio</p>\r\n\r\n<p><strong>Transfer&ecirc;ncia Banc&aacute;ria</strong>: <span style=\"color:#e74c3c;\">Favorecido: Jabaquara Colch&otilde;es Ltda</span></p>\r\n\r\n<p><span style=\"color:#e74c3c;\">CNPJ: 54.596.275/0001-69</span></p>\r\n\r\n<p><strong>Banco Bradesco: </strong>Ag&ecirc;ncia 0120</p>\r\n\r\n<p>Conta Corrente 99530-4</p>\r\n\r\n<p><strong>Banco Ita&uacute;: </strong>Ag&ecirc;ncia 0067</p>\r\n\r\n<p>Conta Corrente 02732-6</p>\r\n\r\n<p><strong>Banco Santander: </strong>Ag&ecirc;ncia 4771</p>\r\n\r\n<p>Conta Corrente 13000260-3<br />\r\n<br />\r\n<strong>Cart&atilde;o de Cr&eacute;dito</strong>: Apenas nas bandeiras VISA, ELO e&nbsp;MASTERCARD, nas formas de D&eacute;bito ou Cr&eacute;dito</p>\r\n\r\n<p><br />\r\n<strong>Parcelamento no Cart&atilde;o</strong>: Valor m&iacute;nimo de R$ 50,00 por parcela, em todas as categorias<br />\r\n&nbsp;</p>\r\n\r\n<p>Para pagamento com cart&atilde;o de Cr&eacute;dito ausente (aquele onde a valida&ccedil;&atilde;o da compra se d&aacute; sem a presen&ccedil;a do dono do cart&atilde;o), adotamos pol&iacute;ticas e condi&ccedil;&otilde;es que protegem tanto o consumidor quanto a loja contra fraudes de qualquer natureza.</p>\r\n\r\n<p>O cliente que optar pelo pagamento com cart&atilde;o de Cr&eacute;dito, &agrave;&nbsp;vista ou parcelado, ter&aacute; que autorizar por escrito, e tamb&eacute;m informar a bandeira, os n&uacute;meros do cart&atilde;o, validade e c&oacute;digo de seguran&ccedil;a.&nbsp;</p>\r\n\r\n<p>Dever&aacute; tamb&eacute;m enviar por meio eletr&ocirc;nico, c&oacute;pia do cart&atilde;o e comprovante de endere&ccedil;o.</p>\r\n', NULL, '2018-11-01 22:21:43');

-- --------------------------------------------------------

--
-- Estrutura da tabela `paginas_imagens`
--

CREATE TABLE `paginas_imagens` (
  `id` int(10) UNSIGNED NOT NULL,
  `pagina_id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `paginas_imagens`
--

INSERT INTO `paginas_imagens` (`id`, `pagina_id`, `ordem`, `imagem`, `created_at`, `updated_at`) VALUES
(3, 1, 0, 'fachada-3_20181101132335uWZJzAvKua.jpg', '2018-11-01 16:23:35', '2018-11-01 16:23:35'),
(5, 3, 0, 'bancos_20181101191010aTfYK8pUx1.png', '2018-11-01 22:10:10', '2018-11-01 22:10:10'),
(6, 3, 0, 'pagamentos_20181101191201P5dhw0jaV8.jpg', '2018-11-01 22:12:01', '2018-11-01 22:12:01'),
(7, 2, 0, 'como-comprar-na-loja_20181101201238SiZoeUOre4.png', '2018-11-01 23:12:38', '2018-11-01 23:12:38');

-- --------------------------------------------------------

--
-- Estrutura da tabela `produtos`
--

CREATE TABLE `produtos` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `secao_id` int(10) UNSIGNED NOT NULL,
  `categoria_id` int(10) UNSIGNED NOT NULL,
  `subcategoria_id` int(10) UNSIGNED DEFAULT NULL,
  `marca_id` int(10) UNSIGNED NOT NULL,
  `lojas` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'jabaquara,gravi',
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `capa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `material` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `linha` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `legenda` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `codigo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `conforto` int(11) NOT NULL,
  `resumo` text COLLATE utf8_unicode_ci NOT NULL,
  `descricao` text COLLATE utf8_unicode_ci NOT NULL,
  `dados_complementares` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `produtos`
--

INSERT INTO `produtos` (`id`, `ordem`, `secao_id`, `categoria_id`, `subcategoria_id`, `marca_id`, `lojas`, `slug`, `titulo`, `capa`, `material`, `linha`, `legenda`, `codigo`, `conforto`, `resumo`, `descricao`, `dados_complementares`, `created_at`, `updated_at`) VALUES
(3, 58, 1, 1, NULL, 23, 'jabaquara,gravi', 'levitate-splendido', 'Levitate Splendido ', 'levitate-splendido_20181026165854f9p5OvK1Yo.png', 'Conforto Firme', 'Espessura 29 cm', 'Molejo MIracoil ', '', 1, 'FABRICANTE MGA&nbsp;', '<p><span style=\"font-size:14px;\">Molejo MIracoil 432 2.2 (173 molas por&nbsp;m&sup2;)</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">PIlow-Top&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Tecido com malha de fibra natural de Bambu</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Aloe Vera</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Visco El&aacute;stico&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Air Flow&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Conforto D45</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Registro Inmetro&nbsp;005692/2017</span></p>\r\n', '<p><span style=\"font-size:14px;\">Colch&atilde;o com 29 cm de altura&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura do conjunto: 66 cm&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Suporte de peso de at&eacute; 120kg por pessoa</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Consulte valores para medidas especiais</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n', '2018-09-21 16:19:42', '2018-10-26 19:58:56'),
(4, 60, 1, 1, NULL, 23, 'jabaquara,gravi', 'posture-comfort', 'Posture Comfort ', 'posture-confort_20180925142343uzHW7mgDtd.jpg', 'Conforto Firme', 'Espessura 25 cm', 'Molejo Comfort Core™', '', 1, 'FABRICANTE MGA&nbsp;', '<p><span style=\"font-size:14px;\">Molejo Comfort Core&trade; 524 2.0 (210 molas por m&sup2;)&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Pillow-Top</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Tecido em Jacquard com fibras naturais de Bambu</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Aloe Vera&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Conforto D75</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Linha Itaflex</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Registro Inmetro&nbsp;005691/2017</span></p>\r\n', '<p><span style=\"font-size:14px;\">Colch&atilde;o com 25 cm de altura&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura do conjunto: 62&nbsp;cm</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Suporte de peso de at&eacute; 120kg por pessoa</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Consulte valores para medidas especiais</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n', '2018-09-25 17:14:30', '2018-10-25 20:20:30'),
(5, 59, 1, 1, NULL, 23, 'jabaquara,gravi', 'posture-blanc', 'Posture Blanc', 'posture-blanc_20180928183910OjyofBc9PR.png', 'Conforto Intermediário', 'Espessura 25 cm', 'Molejo Verticoil™', '', 2, 'FABRICANTE MGA&nbsp;', '<p><span style=\"font-size:14px;\">Molejo Verticoil&trade; 323 2.0 (129 molas por m&sup2;)</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Euro-Pillow</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Tampo em malha</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Visco El&aacute;stico</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Conforto D75</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Linha Itaflex</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Registro Inmetro&nbsp;005696/2017</span></p>\r\n', '<p><span style=\"font-size:14px;\">Colch&atilde;o com 25 cm de altura&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura do conjunto: 62&nbsp;cm</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Suporte de peso de at&eacute; 120kg por pessoa</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Consulte valores para medidas especiais</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n', '2018-09-25 21:52:55', '2018-10-25 20:21:07'),
(6, 57, 1, 1, NULL, 23, 'jabaquara,gravi', 'levitate-plush', 'Levitate Plush', 'levitate-plush_20180926164959qWXP7jVkWL.png', 'Conforto Macio', 'Espessura 30 cm', 'Molejo Comfort Core Extreme™', '', 3, 'FABRICANTE MGA&nbsp;', '<p><span style=\"font-size:14px;\">Molejo Comfort Core Extreme&trade; 532 2.0 (215 molas por m&sup2;)</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">PIlow-Top&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Tecido com malha de fibra natural de Bambu</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Aloe Vera</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Conforto Espuma HR</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Air Flow</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Registro Inmetro&nbsp;005691/2017</span></p>\r\n', '<p><span style=\"font-size:14px;\">Colch&atilde;o com 30 cm de altura&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura do conjunto:&nbsp;67 cm&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Suporte de peso de at&eacute; 120kg por pessoa</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Consulte valores para medidas especiais</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n', '2018-09-26 19:50:00', '2018-10-25 20:21:26'),
(7, 56, 1, 1, NULL, 23, 'jabaquara,gravi', 'sollievo-d-oro', 'Sollievo D\'oro', 'sollievo-doro_20180926171820WFjMSiaLrg.png', 'Conforto Macio', 'Espessura 34 cm', 'Molejo Comfort Core Extreme™', '', 3, 'FABRICANTE MGA&nbsp;', '<p style=\"margin:0cm 0cm 8pt\"><span style=\"font-size:14px;\">Molejo Comfort Core Extreme&trade; 532 2.0 (215 molas por m&sup2;)<br />\r\nVisco El&aacute;stico de 6 cm<br />\r\nEuro-Pillow<br />\r\nTecido com malha de fibra natural de Bambu<br />\r\nAir Flow<br />\r\nConforto D33</span></p>\r\n\r\n<p style=\"margin:0cm 0cm 8pt\"><br />\r\n<span style=\"font-size:14px;\">Registo Inmetro&nbsp;005691/2017</span><br />\r\n&nbsp;</p>\r\n', '<p><span style=\"font-size:14px;\">Colch&atilde;o com 34 cm de altura&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura do conjunto: 71 cm&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Suporte de peso de at&eacute; 120kg por pessoa</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Consulte valores para medidas especiais</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n', '2018-09-26 20:18:21', '2018-10-25 20:21:46'),
(8, 1, 1, 1, NULL, 8, 'jabaquara,gravi', 'platinum-bamboo-anniversary', 'Platinum Bamboo Anniversary', 'kmurduyr_20200226170330GqHBXiUdlh.png', 'Conforto Macio', 'Espessura 36 cm', 'Molas Pocketed', '', 3, 'FABRICANTE SIMMONS&nbsp;', '<p><span style=\"font-size:14px;\">Tecido da capa: Malha naturalmente hipoalerg&ecirc;nica com Viscose de Bambu e fios de carbono que proporciona um sono mais profundo e relaxante&nbsp;(84,8% Poli&eacute;ster/13% Viscose/2% Viscose Vambu e 0,19% Fio Intense)</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Tecnologias:&nbsp;L&aacute;tex (Special Comfort Latex que, al&eacute;m de confort&aacute;vel, permite maiores n&iacute;veis de ventila&ccedil;&atilde;o e suporte),&nbsp;MemoSense&reg; System (Sistema de espumas tecnol&oacute;gicas (Visco+L&aacute;tex) de alta densidade que oferece excelente suporte), Malha de Bamboo. (A verdadeira Malha Bamboo &eacute; hipoalerg&ecirc;nica, tem bactericida natural e um toque fresco e suave),&nbsp;Beautyrest&reg; (Tecnologia de molas exclusivas Beautyrest&reg;, ideal para quem dorme a dois), Molas Pocketed &ndash; Macio</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Registro Inmetro&nbsp;004311/2017</span></p>\r\n', '<p><span style=\"font-size:14px;\">Colch&atilde;o com 36 cm de altura&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura do conjunto:&nbsp;75 cm&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Suporte de peso de at&eacute; 120kg por pessoa</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n', '2018-09-27 15:35:06', '2020-04-02 03:43:17'),
(12, 54, 1, 1, NULL, 8, 'jabaquara,gravi', 'georgia-plush', 'Georgia Plush', 'georgia_20180927162827KqckN40z4i.jpeg', 'Conforto Macio', 'Espessura 31 cm', 'Molas Pocketed', '', 3, 'FABRICANTE SIMMONS', '<p><span style=\"font-size:14px;\">Tecido da capa:&nbsp;Malha de suave toque Bamboo Touch (87% Poli&eacute;ster/13% Viscose)</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Tecnologias:&nbsp;Beautyrest&reg; (Tecnologia de molas exclusivas Beautyrest&reg;, ideal para quem dorme a dois), Bamboo Touch (Malha Bamboo Touch com Viscose que d&aacute; suavidade extrema ao toque), Espuma de acolhida macia (Espuma Soft High Quality, que confere maciez na medida certa para um descanso restaurador), Molas Pocketed &ndash; Macio</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Registro Inmetro&nbsp;004311/2017</span></p>\r\n', '<p><span style=\"font-size:14px;\">Colch&atilde;o com 31&nbsp;cm de altura&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura do conjunto:&nbsp;70&nbsp;cm&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Suporte de peso de at&eacute; 120kg por pessoa</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n', '2018-09-27 19:28:29', '2020-02-26 20:47:01'),
(13, 53, 1, 1, NULL, 8, 'jabaquara,gravi', 'memphis-plush', 'Memphis Plush', '7899_20200226174253SMNeFPZ2ib.png', 'Conforto Macio', 'Espessura 31 cm', 'Molas Pocketed', '', 3, 'FABRICANTE SIMMONS', '<p><span style=\"font-size:14px;\">Tecido da capa:&nbsp;Malha de suave toque Bamboo Touch (87% Poli&eacute;ster/13% Viscose)</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Tecnologias:&nbsp;Beautyrest&reg; (Tecnologia de molas exclusivas Beautyrest&reg;, ideal para quem dorme a dois), Bamboo Touch&nbsp; (Malha Bamboo Touch com viscose que d&aacute; suavidade extrema ao toque), Espuma de acolhida macia (Espuma Soft High Quality, que confere maciez na medida certa para um descanso restaurador), Molas Pocketed&nbsp;&ndash; Macio</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Registro Inmetro&nbsp;004311/2017</span></p>\r\n', '<p><span style=\"font-size:14px;\">Colch&atilde;o com 31&nbsp;cm de altura&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura do conjunto:&nbsp;70&nbsp;cm&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Suporte de peso de at&eacute; 120kg por pessoa</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n', '2018-09-27 19:51:50', '2020-02-26 20:42:54'),
(14, 37, 1, 3, NULL, 3, 'jabaquara,gravi', 'baby-castorzinho', 'Baby Castorzinho', 'castorzinho-baby_20180927171933fAGQ3gMHhS.jpg', 'Conforto Macio', 'Espessura 10 cm', 'Espuma D18', '', 3, 'FABRICANTE CASTOR', '<p><span style=\"font-size:14px;\">ESTRUTURA INTERNA<br />\r\nEspuma: L&acirc;mina de espuma D18 com 9 cm</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">ESTRUTURA EXTERNA<br />\r\nFaixa Lateral do colch&atilde;o: Tecido Bordado em espuma D14 de Poliuretano de 6,0 mm</span><br />\r\n<span style=\"font-size:14px;\">&nbsp;<br />\r\nTamp&atilde;o superior bordado: Tecido em 61% Algod&atilde;o &ndash; 39% Viscose</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">01 espuma de Poliuretano D18 com 1,7 cm de espessura</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Bordado tipo &ndash; Cont&iacute;nuo &ndash; Colmeia</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Acabamento com Debrum de 35 mm</span></p>\r\n', '<p><span style=\"font-size:14px;\">Colch&atilde;o com 10 cm de altura&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Suporte de peso de at&eacute; 40kg por pessoa</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia:&nbsp;12 meses na espuma e 90 dias no tecido</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n', '2018-09-27 20:19:40', '2018-11-01 20:32:22'),
(15, 36, 1, 3, NULL, 3, 'jabaquara,gravi', 'baby-clean', 'Baby Clean', 'castorzinho-baby-clean_20180927173234maUJx3QTm9.jpg', 'Conforto Firme', 'Espessura 12 cm', 'Espuma D18', '', 1, 'FABRICANTE CASTOR', '<p><span style=\"font-size:14px;\">ESTRUTURA INTERNA<br />\r\nEspuma: L&acirc;mina de espuma D18 com 9 cm</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">ESTRUTURA EXTERNA<br />\r\nFaixa Lateral do colch&atilde;o: Tecido 100% Poli&eacute;ster bordado em espuma D20 de Poliuretano de 6,0 mm</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Tamp&atilde;o superior e inferior bordado: Tecido em 100% Poli&eacute;ster</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">01 espuma de Poliuretano D20 com 1,7 cm de espessura</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Bordado tipo &ndash; Cont&iacute;nuo &ndash; Colmeia</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Acabamento com Debrum de 40 mm</span></p>\r\n', '<p><span style=\"font-size:14px;\">Colch&atilde;o com 12&nbsp;cm de altura&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Suporte de peso de at&eacute; 40kg por pessoa</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia:&nbsp;12 meses na espuma e 90 dias no tecido</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n', '2018-09-27 20:32:41', '2018-11-01 20:31:49'),
(17, 52, 1, 1, NULL, 8, 'jabaquara,gravi', 'concept-goldsmith', 'Concept Goldsmith', 'mvaiiipg_20200226173458bs0AIZCofe.png', 'Conforto Macio', 'Espessura 40 cm', 'Molas Pocketed', '', 3, 'FABRICANTE SIMMONS&nbsp;', '<p><span style=\"font-size:14px;\">Tecido da capa:&nbsp;Nobre tecido em malha importada da B&eacute;lgica composta por fios de Microtencel de suave toque (72% Poli&eacute;ster/28% Microtencel)</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Tecnologias:&nbsp;L&aacute;tex (Special Comfort Latex que, al&eacute;m de confort&aacute;vel, permite maiores n&iacute;veis de ventila&ccedil;&atilde;o e suporte),&nbsp;Top Visco Inside (Colch&atilde;o que se molda ao corpo e distribui seu peso uniformemente), Visco (O Visco distribui o peso uniformemente, proporcionando uma melhor adapta&ccedil;&atilde;o ao seu conforto), Beautyrest&reg; (Tecnologia de molas exclusivas Beautyrest&reg;, ideal para quem dorme a dois), Molas Pocketed &ndash; Macio</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Registro Inmetro&nbsp;004311/2017</span></p>\r\n', '<p><span style=\"font-size:14px;\">Colch&atilde;o com 40&nbsp;cm de altura&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura do conjunto:&nbsp;79&nbsp;cm&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Suporte de peso de at&eacute; 120kg por pessoa</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n', '2018-09-27 21:53:04', '2020-02-26 20:34:59'),
(18, 51, 1, 1, NULL, 8, 'jabaquara,gravi', 'highline', 'Highline', 'higline_20200226171158kfBMWeUEsH.png', 'Conforto Macio', 'Espessura 36 cm', 'Molas Pocketed', '', 3, 'FABRICANTE SIMMONS&nbsp;', '<p><span style=\"font-size:14px;\">Tecido da capa:&nbsp;Nobre tecido em malha importada da B&eacute;lgica composta por fios de Microtencel de suave toque (72% Poli&eacute;ster/28% Microtencel)</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Tecido da faixa lateral:&nbsp;Suede Animale Anthylope com toque aveludado (100% Poli&eacute;ster)</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Tecnologias:&nbsp;L&aacute;tex (Special Comfort Latex que, al&eacute;m de confort&aacute;vel, permite maiores n&iacute;veis de ventila&ccedil;&atilde;o e suporte), Beautyrest (Tecnologia de molas exclusivas Beautyrest, ideal para quem dorme a dois), Visco Dry Gel (Melhor adequa&ccedil;&atilde;o ao frio e ao calor), Microtencel LAVA (Malha belga LAVA, composta por fios de Microtencel altamente requintados, com toque extremamente suave e fresco), Molas Pocketed &ndash; Macio</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Registro Inmetro&nbsp;004311/2017</span></p>\r\n', '<p><span style=\"font-size:14px;\">Colch&atilde;o com 36 cm de altura&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura do conjunto:&nbsp;75 cm&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Suporte de peso de at&eacute; 120kg por pessoa</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa&nbsp;</span></p>\r\n', '2018-09-28 15:36:00', '2020-02-26 20:11:59'),
(19, 50, 1, 1, NULL, 8, 'jabaquara,gravi', 'majesty-top-visco', 'Majesty Top Visco', 'majestty_20180928131016z9Vwf8e8ac.jpeg', 'Conforto Intermediário', 'Espessura 30 cm', 'Molas Pocketed', '', 2, 'FABRICANTE SIMMONS&nbsp;', '<p><span style=\"font-size:14px;\">Tecido da capa:&nbsp;Malha de suave toque Bamboo Touch (87% Poli&eacute;ster/13% Viscose)</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Tecnologias:&nbsp;Top Visco Inside (Colch&atilde;o que se molda ao corpo e distribui seu peso uniformemente), Beautyrest&reg; (Tecnologia de molas exclusivas Beautyrest&reg;, ideal para quem dorme a dois), Bamboo Touch&nbsp;(Malha Bamboo Touch com Viscose que d&aacute; suavidade extrema ao toque), Molas Pocketed &ndash; Intermedi&aacute;rio</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Registro Inmetro&nbsp;004311/2017</span></p>\r\n', '<p><span style=\"font-size:14px;\">Colch&atilde;o com 30 cm de altura&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura do conjunto: 69 cm&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Suporte de peso de at&eacute; 120kg por pessoa</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa&nbsp;</span></p>\r\n', '2018-09-28 16:10:18', '2020-02-26 20:19:41'),
(20, 49, 1, 1, NULL, 8, 'jabaquara,gravi', 'sunset-latex', 'Sunset Latex', 'sunset_20180928133319CQ39o9hIUu.jpeg', 'Conforto Macio', 'Espessura 33 cm', 'Molas Pocketed', '', 3, 'FABRICANTE SIMMONS&nbsp;', '<p><span style=\"font-size:14px;\">Tecido da capa:&nbsp;&nbsp;Malha de suave toque Bamboo Touch (87% Poli&eacute;ster/13% Viscose)</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Tecnologias:&nbsp;L&aacute;tex (Special Comfort Latex que, al&eacute;m de confort&aacute;vel, permite maiores n&iacute;veis de ventila&ccedil;&atilde;o e suporte), Beautyrest&reg; (Tecnologia de molas exclusivas Beautyrest&reg;, ideal para quem dorme a dois), Bamboo Touch (Malha Bamboo Touch com Viscose que d&aacute; suavidade extrema ao toque), Molas Pocketed &ndash; Macio</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Registro Inmetro&nbsp;004311/2017</span></p>\r\n', '<p><span style=\"font-size:14px;\">Colch&atilde;o com 33&nbsp;cm de altura&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura do conjunto:&nbsp;72 cm&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Suporte de peso de at&eacute; 120kg por pessoa</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n', '2018-09-28 16:33:23', '2020-02-26 20:27:34'),
(22, 48, 1, 1, NULL, 8, 'jabaquara,gravi', 'vegas-top-visco', 'Vegas Top Visco', 'vegas_20180928145536uXnRDh0zi5.jpeg', 'Conforto Intermediário', 'Espessura 28 cm', 'Molas Pocketed', '', 2, 'FABRICANTE SIMMONS&nbsp;', '<p><span style=\"font-size:14px;\">Tecido da capa:&nbsp;Malha de suave toque Bamboo Touch (87% Poli&eacute;ster/13% Viscose)</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Tecnologias:&nbsp;Top Visco Inside (Colch&atilde;o que se molda ao corpo e distribui seu peso uniformemente), Beautyrest&reg; (Tecnologia de molas exclusivas Beautyrest&reg;, ideal para quem dorme a dois), Bamboo Touch (Malha Bamboo Touch com Viscose que d&aacute; suavidade extrema ao toque), Molas Pocketed &ndash; Intermedi&aacute;rio</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Registro Inmetro&nbsp;004311/2017</span></p>\r\n', '<p><span style=\"font-size:14px;\">Colch&atilde;o com 28 cm de altura&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura do conjunto:&nbsp;67 cm&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Suporte de peso de at&eacute; 120kg por pessoa</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa&nbsp;</span></p>\r\n', '2018-09-28 17:55:40', '2020-02-26 20:22:46'),
(23, 15, 2, 12, NULL, 23, 'jabaquara,gravi', 'box-bau-solteiro', 'Box Baú Solteiro', 'bau-solt_20180929130525Dcv6v6KRwa.jpg', 'Estrutura em Madeira Eucalipto ', 'Espessura 34 cm', 'Pistão Nakata', '', 0, 'FABRICANTE MGA&nbsp;', '<p><span style=\"font-size:14px;\">O Box Ba&uacute; com abertura lateral tem uma grande &aacute;rea &uacute;til, ideal para guardar roupas de cama, cobertores e objetos em geral, sendo op&ccedil;&atilde;o excelente para quem busca aproveitamento de espa&ccedil;o.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">As medidas recomend&aacute;veis para essse produto s&atilde;o as de solteiro:&nbsp;078x188, 088x188, 096x203 cm.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Estrutura em Madeira Eucalipto</span><br />\r\n<span style=\"font-size: 14px;\">Acionamento por PIst&atilde;o Nakata</span><br />\r\n<span style=\"font-size:14px;\">Abertura lateral</span><br />\r\n<span style=\"font-size:14px;\">Acabamento interno do Ba&uacute; com chapa de Eucatex</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Opcional: Acionamento do B&aacute;u por molas</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Altura do Box de Pist&atilde;o: 34 cm &ndash; Altura dos p&eacute;s: 6,5 cm &ndash; Altura &uacute;til: 25 cm<br />\r\nAltura do Box de Mola: 34 cm &ndash; Altura dos p&eacute;s: 6,5 cm &ndash; Altura &uacute;til: 25 cm</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\"><strong>PRE&Ccedil;O REFERENTE AO TECIDO SARA BEGE, OUTROS REVESTIMENTOS CONSULTAR VALORES</strong></span></p>\r\n\r\n<p><span style=\"font-size:14px;\"><strong>(OP&Ccedil;&Atilde;O: TECIDO POLI&Eacute;STER, CORANO, CHENILLE E CAMUR&Ccedil;A)</strong></span></p>\r\n', '<p><span style=\"font-size:14px;\">Garantia: 01 ano<br />\r\nFoto meramente ilustrativa&nbsp;</span></p>\r\n', '2018-09-29 16:05:29', '2018-11-01 16:45:51'),
(24, 14, 2, 12, NULL, 23, 'jabaquara,gravi', 'box-bau-casal', 'Box Baú Casal', 'bau-af-mga2_20180929132658m3procWylC.jpg', 'Estrutura em Madeira Eucalipto ', 'Espessura 34 cm', 'Pistão Nakata', '', 0, 'FABRICANTE MGA', '<p><span style=\"font-size:14px;\">O Box Ba&uacute; com abertura frontal tem uma grande &aacute;rea &uacute;til, ideal para guardar roupas de cama, cobertores e objetos em geral, sendo op&ccedil;&atilde;o excelente para quem busca aproveitamento de espa&ccedil;o.</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">As medidas recomend&aacute;veis para esse produto s&atilde;o as de casal: 128x188 e 138x188 cm.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Estrutura em&nbsp;Madeira Eucalipto<br />\r\nAcionamento por Pist&atilde;o Nakata<br />\r\nAbertura frontal</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Acabamento interno do Ba&uacute; com chapa de Eucatex</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Opcional: Acionamento do B&aacute;u por molas</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Altura do Box de Pist&atilde;o: 34 cm &ndash; Altura dos p&eacute;s: 6,5 cm &ndash; Altura &uacute;til: 25 cm<br />\r\nAltura do Box de Mola: 34 cm &ndash; Altura dos p&eacute;s: 6,5 cm &ndash; Altura &uacute;til: 25 cm</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\"><strong>PRE&Ccedil;O REFERENTE AO TECIDO SARA BEGE, OUTROS REVESTIMENTOS CONSULTAR VALORES<br />\r\n(OP&Ccedil;&Atilde;O: TECIDO POLI&Eacute;STER, CORANO, CHENILLE E CAMUR&Ccedil;A)</strong></span><br />\r\n&nbsp;</p>\r\n', '<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa&nbsp;</span></p>\r\n', '2018-09-29 16:27:03', '2018-10-26 20:01:37'),
(25, 13, 2, 12, NULL, 23, 'jabaquara,gravi', 'box-bau-queen', 'Box Baú Queen', 'bau-bp-mga2_201809291336598dKFeiqwNh.jpg', 'Estrutura em Madeira Eucalipto ', 'Espessura 34 cm', 'Pistão Nakata', '', 0, 'FABRICANTE MGA&nbsp;', '<p><span style=\"font-size:14px;\">O Box Ba&uacute; com abertura frontal tem uma grande &aacute;rea &uacute;til, ideal para guardar roupas de cama, cobertores e objetos em geral, sendo op&ccedil;&atilde;o excelente para quem busca aproveitamento de espa&ccedil;o.</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Este produto &eacute; composto por dois m&oacute;dulos, com a fun&ccedil;&atilde;o de facilitar o manuseio e o transporte, al&eacute;m de garantir maior resist&ecirc;ncia e suporte de peso ao conjunto.</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Medida Queen Size158x198 cm com dois m&oacute;dulos de 079x198 cm cada.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Estrutura em Madeira Eucalipto</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Acionamento por Pist&atilde;o Nakata</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Abertura frontal</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Acabamento interno do B&aacute;u&nbsp;com chapa de Eucatex</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Opcional: Acionamento do B&aacute;u por molas</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura do Box de Pist&atilde;o: 34 cm &ndash; Altura dos p&eacute;s: 6,5 cm &ndash; Altura &uacute;til: 25 cm<br />\r\nAltura do Box de Mola: 34 cm &ndash; Altura dos p&eacute;s: 6,5 cm &ndash; Altura &uacute;til: 25 cm</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\"><strong>PRE&Ccedil;O REFERENTE AO TECIDO SARA BEGE, OUTROS REVESTIMENTOS CONSULTAR VALORES<br />\r\n(OP&Ccedil;&Atilde;O: TECIDO POLI&Eacute;STER, CORANO, CHENILLE E CAMUR&Ccedil;A)</strong></span><br />\r\n&nbsp;</p>\r\n', '<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa&nbsp;</span></p>\r\n', '2018-09-29 16:37:05', '2018-11-01 16:45:41'),
(26, 12, 2, 12, NULL, 23, 'jabaquara,gravi', 'box-bau-king', 'Box Baú King', 'bau-bp-mga2_201809291613170nV4AH00Ck.jpg', 'Estrutura em Madeira Eucalipto ', 'Espessura 34 cm', 'Pistão Nakata', '', 0, 'FABRICANTE MGA', '<p><span style=\"font-size:14px;\">O Box Ba&uacute; com abertura frontal tem uma grande &aacute;rea &uacute;til, ideal para guardar roupas de cama, cobertores e objetos em geral, sendo op&ccedil;&atilde;o excelente para quem busca aproveitamento de espa&ccedil;o.</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Este produto &eacute; composto por dois m&oacute;dulos, com a fun&ccedil;&atilde;o de facilitar o manuseio e o transporte, al&eacute;m de garantir maior resist&ecirc;ncia e suporte de peso ao conjunto.</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Medida King&nbsp;Size 193x203 cm com dois m&oacute;dulos de 096x203 cm cada.</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Estrutura em Madeira Eucalipto</span><br />\r\n<span style=\"font-size: 14px;\">Acionamento por Pist&atilde;o Nakata</span><br />\r\n<span style=\"font-size:14px;\">Abertura lateral</span><br />\r\n<span style=\"font-size:14px;\">Acabamento interno do Ba&uacute; com chapa de Eucatex</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Opcional: Acionamento do B&aacute;u por molas</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Altura do Box de Pist&atilde;o: 34 cm &ndash; Altura dos p&eacute;s: 6,5 cm &ndash; Altura &uacute;til: 25 cm<br />\r\nAltura do Box de Mola: 34 cm &ndash; Altura dos p&eacute;s: 6,5 cm &ndash; Altura &uacute;til: 25 cm</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\"><strong>PRE&Ccedil;O REFERENTE AO TECIDO SARA BEGE, OUTROS REVESTIMENTOS CONSULTAR VALORES<br />\r\n(OP&Ccedil;&Atilde;O: TECIDO POLI&Eacute;STER, CORANO, CHENILLE E CAMUR&Ccedil;A)</strong></span></p>\r\n', '<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa&nbsp;</span></p>\r\n', '2018-09-29 19:13:23', '2018-10-26 20:02:20'),
(27, 47, 1, 1, NULL, 10, 'jabaquara,gravi', 'acordes', 'Acordes', 'acordes_20181017174303QdTSqII1h9.jpeg', 'Conforto Intermediário', 'Espessura 23 cm', 'Molas Ensacadas Individualmente', '', 2, 'FABRICANTE EPEDA', '<p><span style=\"font-size:14px;\">Tecido da capa:&nbsp;Tecido de suave toque com fios de Viscose (85% Poli&eacute;ster/15% Viscose)</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Tecido da faixa lateral:&nbsp;Tecido Chenille nas cores Lyon (69% Poli&eacute;ster/31% Polipropileno)</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Tecnologias:&nbsp;Belgian Design (O design belga do tecido aumente em at&eacute; quatro vezes a circula&ccedil;&atilde;o de ar devido ao seu alto poder de absor&ccedil;&atilde;o e evapora&ccedil;&atilde;o de umidade), Molas ensacadas (Se voc&ecirc; ou seu parceiro se mexem muito durante a noite, voc&ecirc;s precisam de um colch&atilde;o com essa tecnologia), Molas ensacadas individualmente</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Registro Inmetro&nbsp;004311/2017</span></p>\r\n', '<p><span style=\"font-size:14px;\">Colch&atilde;o com 23 cm de altura&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura do conjunto:&nbsp;62 cm&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Suporte de peso de at&eacute; 120kg por pessoa</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa&nbsp;</span></p>\r\n', '2018-10-01 15:27:45', '2018-11-01 20:06:08'),
(28, 45, 1, 1, NULL, 9, 'jabaquara,gravi', 'tenerife', 'Tenerife', 'teneriffe_20181001181722tbhxBpWNaZ.jpeg', 'Conforto Firme', 'Espessura 28 cm', 'Molas LFK 2.2 - Extra Support', '', 1, 'FABRICANTE FLEX DO BRASIL', '<p><span style=\"font-size:14px;\">Tecido da capa: Nobre tecido em Bamboo Touch Stretch com fibras naturais de Bambu de suave toque (87% Poli&eacute;ster/13% Viscose de Bambu)</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Tecido da faixa lateral: Tecido Chenille nas cores Lyon (69% Poli&eacute;ster/31% Polipropileno)</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Tecnologias: Bamboo Touch Stretch (Malha com Viscose de Bambu, que d&aacute; suavidade ao toque), Espuma de alta densidade (Espuma de alta densidade que garante o suporte ideal para quem busca um colch&atilde;o firme), LFK 2.2 - Extra support (Tecnologia de molas exclusivas LFK, mais firme, resistente e est&aacute;vel), Molas Flex LFK 2.2 - Extra Support</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Registro Inmetro&nbsp;004181/2017</span></p>\r\n', '<p><span style=\"font-size:14px;\">Colch&atilde;o com 28 cm de altura&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura do conjunto:&nbsp;67 cm&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Suporte de peso de at&eacute; 120kg por pessoa</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa&nbsp;</span></p>\r\n', '2018-10-01 21:17:26', '2018-11-16 16:53:45'),
(29, 46, 1, 1, NULL, 9, 'jabaquara,gravi', 'avila', 'Ávila', 'avilla_20181001182615NpGVAto1GE.jpeg', 'Conforto Intermediário', 'Espessura 26 cm', 'Molas LFK 2.2', '', 2, 'FABRICANTE FLEX DO BRASIL', '<p><span style=\"font-size:14px;\">Tecido da capa: Tecido de suave toque com fios de Viscose (85% Poli&eacute;ster/15% Viscose)</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Tecido da faixa lateral: Tecido Chenille nas cores Lyon (69% Poli&eacute;ster/31% Polipropileno)</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Tecnologias: Belgian Design (O design belga do tecido aumenta em at&eacute; quatro vezes a circula&ccedil;&atilde;o de ar devido ao seu alto poder de absor&ccedil;&atilde;o e evapora&ccedil;&atilde;o de umidade), Molas Flex LFK 2.2 (Tecnologia LFK em a&ccedil;o carbono com bitola de 2.2mm, 74% mais firme que o molejo Bonnell Tradicional, estabilidade m&aacute;xima, durabilidade garantida e firmeza com m&aacute;ximo conforto)</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Registro Inmetro&nbsp;004181/2017</span></p>\r\n', '<p><span style=\"font-size:14px;\">Colch&atilde;o com 26&nbsp;cm de altura&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura do conjunto:&nbsp;65&nbsp;cm&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Suporte de peso de at&eacute; 120kg por pessoa</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa&nbsp;</span></p>\r\n', '2018-10-01 21:26:17', '2018-11-01 20:03:15'),
(31, 0, 4, 17, NULL, 6, 'jabaquara,gravi', 'poltrona-mh-1216', 'Poltrona MH 1216', 'poltrona-1216_20181002130406drKyXp0MMt.jpg', 'Reclinável ', '', 'Estrutura em Madeira Eucalipto ', '', 3, 'FABRICANTE HERVAL', '<p><span style=\"font-size:14px;\">Poltrona reclin&aacute;vel para descanso, tipo papai. Tem destaque principal ao mecanismo reclin&aacute;vel.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Estrutura Interna da&nbsp;Madeira:&nbsp;Madeira de Eucalipto de reflorestamento e chapas multilaminadas revestidas com espuma e tecido. Com estrutura (mecanismo) de fabrica&ccedil;&atilde;o pr&oacute;pria, com acabamento em pintura Ep&oacute;xi</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Composi&ccedil;&atilde;o Interna da Poltrona: Espuma:&nbsp;Encosto desmont&aacute;vel com precintas el&aacute;sticas e espuma de alta densidade</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Almofadas encosto preenchidas em fibras de Poli&eacute;ster</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Assento fixo percintas el&aacute;sticas e Espuma Soft D30</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Todas as espumas s&atilde;o de fabrica&ccedil;&atilde;o pr&oacute;pria e ecologicamente corretas</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Revestimentos: Liberado toda Cartela Herval inclusive Couro. Composi&ccedil;&atilde;o afixada em etiqueta que acompanha o produto</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Dimens&otilde;es: L: 89&nbsp;cm (Largura)</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">P: 93 cm (Comprimento)<br />\r\nA: 103 cm (Altura)</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Este produto aberto possui 1,49 m de profundidade</span></p>\r\n', '<p><span style=\"font-size:14px;\">Outras op&ccedil;&otilde;es de revestimento: CONSULTAR VALORES</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Suporte de peso de at&eacute; 110kg&nbsp;por pessoa&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 90 dias&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n', '2018-10-02 16:04:08', '2018-11-01 21:47:29'),
(32, 0, 4, 17, NULL, 6, 'jabaquara,gravi', 'poltrona-mh-1237', 'Poltrona MH 1237', 'poltrona-1237_20181003123313s2LaD0MYkQ.jpg', 'Reclinável ', '', 'Estrutura em Madeira Eucalipto ', '', 2, 'FABRICANTE HERVAL', '<p><span style=\"font-size:14px;\">Poltrona estofada com mecanismo reclin&aacute;vel.</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Estrutura Interna da&nbsp;Madeira:&nbsp;Estrutura de madeira de eucalipto de reflorestamento. O esqueleto &eacute; montado com grampos de a&ccedil;o e fixo com cola PVA (pr&oacute;pria para madeira), proporcionando assim grande resist&ecirc;ncia e evitando rangidos no produto.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Composi&ccedil;&atilde;o Interna da Poltrona</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Espuma:&nbsp;Encosto desmont&aacute;vel em fibra de Poli&eacute;ster e fibra siliconada</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Assento com percintas el&aacute;sticas e Espuma Soft D30</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Todas as espumas s&atilde;o de fabrica&ccedil;&atilde;o pr&oacute;pria e ecologicamente corretas</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Revestimentos: Toda Cartela Herval de Tecidos inclusive Couro. Composi&ccedil;&atilde;o afixada em etiqueta que acompanha o produto</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Dimens&otilde;es: L: 70 cm (Largura)<br />\r\nC: 92 cm (Comprimento)<br />\r\nA: 100 cm (Altura)<br />\r\nEste produto aberto possui 1,47 m de profundidade</span></p>\r\n', '<p><span style=\"font-size:14px;\">Outras op&ccedil;&otilde;es de revestimento: CONSULTAR VALORES</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Opcional:&nbsp;O produto possui op&ccedil;&atilde;o de p&eacute;s em madeira nas cores Am&ecirc;ndoa e Imbuia</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Suporta at&eacute; 100kg&nbsp;por pessoa&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 90 dias&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n', '2018-10-02 21:13:58', '2018-11-01 21:45:21'),
(33, 0, 4, 19, NULL, 6, 'jabaquara,gravi', 'sofa-cama-mh-1456', 'Sofá-cama MH 1456', 'sofa-1456_20181003142843lOAsq0vBwY.jpg', '', '', 'Estrutura em Madeira Eucalipto ', '', 2, 'FABRICANTE HERVAL&nbsp;', '<p><span style=\"font-size:14px;\">Estrutura Interna da Madeira: Estrutura&nbsp;de Madeira de Eucalipto de reflorestamento. O esqueleto &eacute; montado com grampos de a&ccedil;o e fixo com cola PVA (pr&oacute;pria para madeira), proporcionando assim grande resist&ecirc;ncia eevitando rangidos no produto.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Assento e encosto:&nbsp;Assento com almofadas soltas preenchidas com espumas de alta densidade envoltas de uma manta de Espuma Eco Soft, sobre a estrutura met&aacute;lica dobr&aacute;vel da cama (ferragem tubular de fabrica&ccedil;&atilde;o pr&oacute;pria), acompanhada por um colch&atilde;o, tamb&eacute;m de fabrica&ccedil;&atilde;o pr&oacute;pria, medindo 128x183x90 cm (Casal) ou 78x183x 90 cm&nbsp;(Solteiro).</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Encosto preenchido com flocos de espuma.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Todas as espumas s&atilde;o de fabrica&ccedil;&atilde;o pr&oacute;pria e ecologicamente corretas.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Revestimentos:&nbsp;Toda Cartela Herval de Tecidos inclusive Couro. Composi&ccedil;&atilde;o afixada em etiqueta que acompanha o produto.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Dimens&otilde;es do Sof&aacute;-Cama de 02 lugares<br />\r\nL:&nbsp;155&nbsp;cm (Largura)<br />\r\nC:&nbsp;95 cm (Comprimento)<br />\r\nA:&nbsp;91&nbsp;cm (Altura)</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Este produto aberto possui 2,20 m de profundidade</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Dimens&otilde;es do Sof&aacute;-Cama de 01 lugar</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">L:&nbsp;105&nbsp;cm (Largura)<br />\r\nC: 95&nbsp;cm (Comprimento)<br />\r\nA:&nbsp;91&nbsp;cm (Altura)</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Este produto aberto possui 2,20 m de profundidade</span></p>\r\n', '<p><span style=\"font-size:14px;\">Outras op&ccedil;&otilde;es de revestimento: CONSULTAR VALORES</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Suporte de peso de at&eacute; 100kg&nbsp;por pessoa&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 90 dias&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n', '2018-10-03 17:28:44', '2018-11-01 20:52:33'),
(34, 0, 4, 19, NULL, 6, 'jabaquara,gravi', 'sofa-cama-mh-1500', 'Sofá-cama MH 1500', 'sofa-1500_20181003145034xya4MXUB8U.jpg', '', '', 'Estrutura em Madeira Eucalipto ', '', 2, 'FABRICANTE HERVAL&nbsp;', '<p><span style=\"font-size:14px;\">Este &eacute; um produto que traz toda praticidade de um Sof&aacute;-Cama sem deixar de lado a quest&atilde;o do conforto e design de um produto de linha alta. O sistema de movimenta&ccedil;&atilde;o do encosto permite transformar o sof&aacute; em cama com um simples movimento, deixando o pr&aacute;tico e funcional.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Os p&eacute;s s&atilde;o em madeira maci&ccedil;a de Tauari, garantindo assim resist&ecirc;ncia necess&aacute;ria e agregando valor ao produto atrav&eacute;s da nobreza da madeira natural.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Estrutura Interna da Madeira:&nbsp;Estrutura de Madeira de Eucalipto de reflorestamento e chapas MDF</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">O esqueleto &eacute; montado com grampos de a&ccedil;o e fixo com cola PVA (pr&oacute;pria para madeira), proporcionando assim grande resist&ecirc;ncia e evitando rangidos no produto.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Assento e enconsto:&nbsp;Assento fixo e encosto articul&aacute;vel com almofadas soltas, preenchidos com espuma D28 envolto com uma manta de Espuma Eco Soft</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Almofada encosto preenchidas com fibra Poli&eacute;ster.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Todas as espumas s&atilde;o de fabrica&ccedil;&atilde;o pr&oacute;pria e ecologicamente corretas.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Revestmentos: Liberado Cartela Herval de tecidos, grupo &ldquo;B&rdquo; para cima, menos Couro. Composi&ccedil;&atilde;o afixada em etiqueta que acompanha o produto.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Dimens&otilde;es: FECHADO: L: 180 cm (Largura)&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">C: 98 cm (Comprimento)</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">A: 90 cm (Altura)</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">ABERTO:&nbsp;L: 180 cm (Largura)&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">C: 1,43&nbsp;cm (Comprimento)</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">A: 44&nbsp;cm (Altura)</span></p>\r\n', '<p><span style=\"font-size:14px;\">Outras op&ccedil;&otilde;es de revestimento: CONSULTAR VALORES&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Opcional:&nbsp;P&eacute;s com op&ccedil;&atilde;o nas cores Preto, Imbuia ou Am&ecirc;ndoa</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Suporta de peso de at&eacute; 100kg por pessoa&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 90 dias&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n', '2018-10-03 17:50:35', '2018-11-01 20:55:05'),
(35, 43, 1, 1, NULL, 15, 'jabaquara,gravi', 'eclipse-elegance-firm', 'Eclipse Elegance Firm', 'eclipse-elegance_20181003161027vMi4fHXZDR.jpg', 'Conforto Firme', 'Espessura 29 cm', 'Molejo MIracoil ', '', 1, 'FABRICANTE EASTMAN HOUSE', '<p><span style=\"font-size:14px;\">Molejo Miracoil:&nbsp;Sua estrutura &eacute; feita com fios cont&iacute;nuos garantindo maior resili&ecirc;ncia e durabilidade. Possui uma alta contagem de 173 molas p/ m&sup2;,&nbsp;aliado a uma excelente distribui&ccedil;&atilde;o de peso para melhor conformidade do corpo.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Espuma de Poliuretano de densidade D45, proporciona um excelente suporte para pessoas com at&eacute; 150Kg, dando ao colch&atilde;o a caracter&iacute;stica de conforto extra-firme.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Tecido com tecnologia Intense. Fio que neutraliza as cargas negativas do corpo proporcionando uma sensa&ccedil;&atilde;o maior de relaxamento.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Malha com fibras naturais de Bambu. A fibra de Viscose de Bambu tem uma estrutura altamente absorvente, com lacunas microsc&oacute;picas que mant&eacute;m o tecido sempre fresco e arejado al&eacute;m de oferecer um toque macio e sedoso.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Sistema Air Flow: Feito com tecido especial que permite a circula&ccedil;&atilde;o de ar na zona de conforto do colch&atilde;o, criando um microclima mais fresco e agrad&aacute;vel, evitando a prolifera&ccedil;&atilde;o de bact&eacute;rias.</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Life Edge&trade;: A tecnologia patenteada Lifedge Perimeter Support proporciona um &oacute;timo suporte em todo o per&iacute;metro do colch&atilde;o tanto para sentar quanto para deitar, permitindo que toda a superf&iacute;cie do colch&atilde;o seja utilizada com excelente conforto.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Matelass&ecirc;: Matelass&ecirc;&nbsp;feito com Malha de 340 gr/m&sup2; composto com Espuma Visco El&aacute;stica e Espuma Soft para receber e acolher perfeitamente as curvas do corpo, aliviando a tens&atilde;o&nbsp;e os pontos de press&atilde;o.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Registro Inmetro 005692/2017</span></p>\r\n', '<p><span style=\"font-size:14px;\">Colch&atilde;o com 30&nbsp;cm de altura&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura do conjunto: 72 cm&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Suporte de peso de at&eacute; 150kg por pessoa</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Consulte valores para medidas especiais</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n', '2018-10-03 19:10:28', '2018-10-25 19:59:19');
INSERT INTO `produtos` (`id`, `ordem`, `secao_id`, `categoria_id`, `subcategoria_id`, `marca_id`, `lojas`, `slug`, `titulo`, `capa`, `material`, `linha`, `legenda`, `codigo`, `conforto`, `resumo`, `descricao`, `dados_complementares`, `created_at`, `updated_at`) VALUES
(38, 39, 1, 1, NULL, 15, 'jabaquara,gravi', 'majestic', 'Majestic ', 'majestic_20181003181725n8S21MMkgs.jpg', 'Conforto Firme', 'Espessura 37 cm', 'Molejo Comfort Core MultiSuporte ', '', 1, 'FABRICANTE EASTMAN HOUSE', '<p><span style=\"font-size:14px;\">Molejo Comfort Core MultiSuporte: Molejo com 23cm de altura e uma alta contagem de 267 molas por m&sup2;, Ensacadas Individualmente, cada uma delas com dupla a&ccedil;&atilde;o, sendo 3 cm para recep&ccedil;&atilde;o do corpo e 20 cm para suporte do corpo.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Espuma de Poliuretano de densidade D45, proporciona um excelente suporte para pessoas com at&eacute; 150Kg, dando ao colch&atilde;o a caracter&iacute;stica de conforto extra-firme.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Malha com fibras de Tencel: Um produto ecol&oacute;gico 30% mais resistente que o Algod&atilde;o, tem um toque suave e diferenciado das malhas convencionais.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Allergy Free:&nbsp;Design patenteado pela Eastman House, com dupla fun&ccedil;&atilde;o, que elimina &aacute;caros e outros micro-organismos combinando o uso do L&aacute;tex com pequenos canais de ventila&ccedil;&atilde;o em conjunto com um tecido antial&eacute;rgico com tecnologia Actipro&trade;, criando um ambiente limpo, fresco e livre de bact&eacute;ria.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Pulse Oxygel:&nbsp;A exclusiva tecnologia SonoCore, patenteada pela LATEXCO, oferece a mais dur&aacute;vel, melhor ventilada, mais forte, mais resistente e mais suave Espuma de L&aacute;tex de todos os tempos, projetada para garantir um perfeito sono repousante e revitalizante. A adi&ccedil;&atilde;o da tecnologia Oxygel (part&iacute;culas de gel infundido) garante a redu&ccedil;&atilde;o de temperatura, contribuindo para a cria&ccedil;&atilde;o de um microclima fresco e agrad&aacute;vel, ideal para dormir.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Tencel: O fio Tencel tem origem a partir da &aacute;rvore de Eucalipto, &eacute; 30% mais resistente que o Algod&atilde;o, e seu ciclo produtivo n&atilde;o agride a natureza.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Life Edge&trade;: A tecnologia patenteada Lifedge Perimeter Support proporciona um &oacute;timo suporte em todo o per&iacute;metro do colch&atilde;o tanto para sentar quanto para deitar, permitindo que toda a superf&iacute;cie do colch&atilde;o seja utilizada com excelente conforto.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Registro Inmetro&nbsp;005691/2017</span></p>\r\n', '<p><span style=\"font-size:14px;\">Colch&atilde;o com 37&nbsp;cm de altura&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura do conjunto: 74&nbsp;cm&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Suporte de peso de at&eacute; 120kg por pessoa</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Consulte valores para medidas especiais</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n', '2018-10-03 21:17:26', '2018-10-25 20:13:26'),
(39, 35, 1, 1, NULL, 3, 'jabaquara,gravi', 'premium-amazon', 'Premium Amazon', 'amazon-pocket-conjunto138_20181004130624EBqyZdraTD.jpg', 'Conforto Macio', 'Espessura 32 cm', 'Molas Pocket', '', 3, 'FABRICANTE CASTOR', '<p><span style=\"font-size:14px;\">ESTRUTURA INTENA&nbsp;<br />\r\nAs molas tipo POCKET SYSTEM &ndash; Ensacadas individualmente com tela de Polipropileno<br />\r\nColagem das molas tipo HOT MELT &ndash; Proporciona maior individualidade das molas<br />\r\nBordas perimetrais em Espuma Resistente<br />\r\nRevestida por feltro 700 gr/m&sup2; ou espuma em toda a arma&ccedil;&atilde;o proporcionando maior estabilidade na arma&ccedil;&atilde;o</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Enchimento Interno: 01 l&acirc;mina de espuma D28 com 8,0 cm de espessura no enchimento superior<br />\r\n01 l&acirc;mina de espuma D20 com 3,0 cm de espessura no enchimento inferior</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">ESTRUTURA EXTERNA<br />\r\nFaixa Lateral do colch&atilde;o: Tecido Boucle malha 100% Poli&eacute;ster<br />\r\nEspuma D20 1,1 cm</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Euro PIllow</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Tamp&atilde;o superior: Tecido Malha 100% Poli&eacute;ster Amazonfair<br />\r\nManta de Poli&eacute;ster Ulsoft<br />\r\n01 l&acirc;mina de espuma D20 Gel com 2,0 cm de espessura<br />\r\n01 l&acirc;mina de espuma D20 com 2,0 cm de espessura</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Tratamento no tecido: Amazonfair: Esta tecnologia &eacute; feita &agrave; partir da polpa do fruto de cupua&ccedil;u e traz um toque de maciez extrema no tecido</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Fresh Comfort Dow: Desenvolvido com a exclusiva espuma Fresh Comfort Gel da Dow, com c&eacute;lulas mais abertas e part&iacute;culas de gel que propiciam a dissipa&ccedil;&atilde;o do calor, proporcionando conforto constante em todas as esta&ccedil;&otilde;es do ano</span></p>\r\n', '<p><span style=\"font-size:14px;\">Colch&atilde;o com 32 cm de altura</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura do conjunto: 72 cm</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Suporte de peso de at&eacute;&nbsp;120kg por pessoa&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 36 meses no molejo &ndash; 24 meses no estofamento &ndash; 90 dias no tecido</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Consulte valores para medidas especiais&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n', '2018-10-04 15:18:43', '2018-11-01 19:54:15'),
(41, 0, 4, 19, NULL, 6, 'jabaquara,gravi', 'sofa-cama-mh-1602', 'Sofá-cama MH 1602', 'sofa-1602-2_20181004182248aOXs0Ogw0B.jpg', '', '', 'Estrutura em Madeira Eucalipto ', '', 2, 'FABRICANTE HERVAL', '<p><span style=\"font-size:14px;\">Sof&aacute;-cama com almofadas soltas no assento para possibilitar a abertura da estrutura dobr&aacute;vel da cama, acompanha colch&atilde;o com medidas de casal.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Estrutura Interna da Madeira:&nbsp;Madeira de Eucalipto de reflorestamento.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">O esqueleto &eacute; montado com grampos de a&ccedil;o e fixo com cola PVA (pr&oacute;pria para madeira), proporcionando assim grande resist&ecirc;ncia e evitando rangidos no produto. O produto consta com ferragem tubular de fabrica&ccedil;&atilde;o pr&oacute;pria.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">P&eacute;s de madeira</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Assento e encosto: O assento &eacute; composto por almofadas fixas preenchidas com Espuma HS120 envolta com uma manta de Espuma Eco Soft, sobre a estrutura met&aacute;lica dobr&aacute;vel da cama (ferragem tubular de fabrica&ccedil;&atilde;o pr&oacute;pria), acompanhada por um colch&atilde;o de Molas Bonnel, tamb&eacute;m de fabrica&ccedil;&atilde;o pr&oacute;pria, medindo 128x183x90 cm (Casal)</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">O encosto possui almofadas fixas, preenchidas com Espuma HS71 envolta por uma manta de Espuma Eco Soft.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Todas as espumas s&atilde;o de fabrica&ccedil;&atilde;o pr&oacute;pria e ecologicamente corretas.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Revestimentos:&nbsp;Toda Cartela Herval de Tecidos inclusive listrado. N&atilde;o dispon&iacute;vel em&nbsp;Couro. Composi&ccedil;&atilde;o afixada em etiqueta que acompanha o produto</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">DImens&otilde;es: L: 180&nbsp;cm (Largura)<br />\r\nC: 101&nbsp;cm (Comprimento)<br />\r\nA: 100 cm (Altura)</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Este produto aberto possui 2,20 m de profundidade</span></p>\r\n', '<p><span style=\"font-size:14px;\">Outras op&ccedil;&otilde;es de revestimento: CONSULTAR VALORES&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Suporta de peso de at&eacute; 100kg por pessoa&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 90 dias&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n\r\n<p><br />\r\n&nbsp;</p>\r\n\r\n<p><br />\r\n&nbsp;</p>\r\n', '2018-10-04 21:23:00', '2018-11-01 20:53:51'),
(42, 38, 1, 5, NULL, 6, 'jabaquara,gravi', 'cama-articulada-mh-1430-dream', 'Cama Articulada  MH 1430 Dream', 'cama-1430-articulada-dream_20181004184841AL6t1wVD8Q.jpg', 'Conforto Macio', 'Opção de massageador ', 'Molas Ensacadas Individualmente', '', 3, 'FABRICANTE HERVAL', '<p><span style=\"font-size:14px;\">O grande diferencial deste produto &eacute; a Cama Ajust&aacute;vel. Com motor bivolt e um controle, essa cama permite eleva&ccedil;&atilde;o da cabe&ccedil;a, das costas e dos p&eacute;s proporcionando ao cliente um conforto muito grande. Produto tamb&eacute;m possui colch&atilde;o com op&ccedil;&atilde;o de com ou sem massageador.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Cama com estrutura em ferragem de a&ccedil;o carbono com pintura Ep&oacute;xi. A Ferragem conta com um sistema articul&aacute;vel &uacute;nico que al&eacute;m de poder fazer movimentos ajust&aacute;veis via controle, esta possui um sistema de molejo diferenciado que deixam o produto bastante confort&aacute;vel. Laterais em chapas multilaminadas revestidas com fibras Poli&eacute;ster e tecido.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Colch&atilde;o com Molas Ensacadas Individualmente, fabricado com fio de a&ccedil;o especial (alto teor de carbono) de 2mm que proporcionam grande amortecimento ao corpo. S&atilde;o 194 molas p/m&sup2;. A estrutura de Molas Ensacadas &eacute; envolta por espumas de v&aacute;rias densidades, divididas em camadas e diversas espessuras. Tamb&eacute;m conta com uma camada de feltro agulhado deixando o colch&atilde;o mais confort&aacute;vel e macio.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Revestimento: Box com revestimento em tecido Suede. Revestimento em tecido Malha que proporciona um toque super suave. Conta tamb&eacute;m com uma camada de manta de fibra Poli&eacute;ster proporcionando mais conforto e l&acirc;minas de espuma de alta performance D18</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Colch&atilde;o Pillow Top One Side, n&atilde;o &eacute; necess&aacute;rio virar o colch&atilde;o</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Composi&ccedil;&atilde;o do tecido: Tecido do tampo: 84% Poli&eacute;ster 16% Polipropileno<br />\r\nTecido da faixa: 84% Poli&eacute;ster e 16% Polipropileno</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Dimens&otilde;es: Medida do colch&atilde;o: 100x200 cm</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Medida da Cama: 101x203 cm</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura da Cama montada com p&eacute;s (sem colch&atilde;o): 35 cm</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura do colch&atilde;o: 28 cm</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura total: 63 cm</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">P&eacute;s fixos em alum&iacute;nio polido (7,5 cm)</span></p>\r\n', '<p><span style=\"font-size:14px;\">Garantia: Estrutura: 01 ano<br />\r\nDemais componentes: 180 dias&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Suporte&nbsp; de peso de at&eacute; 120kg por pessoa&nbsp;</span></p>\r\n', '2018-10-04 21:48:42', '2018-11-01 20:40:42'),
(43, 0, 3, 9, NULL, 6, 'jabaquara,gravi', 'cabeceira-mh-2680', 'Cabeceira MH 2680', 'cabeceir-mh-2680_20181011122840irH74QFIfv.jpg', 'Decoração', 'Sem mini frame ', 'LINHA VIVERE', '', 0, 'FABRICANTE HERVAL', '<p><span style=\"font-size:14px;\">Madeira de Eucalipto de reflorestamento e revestidas com espuma e tecido.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">O esqueleto &eacute; montado com grampos de a&ccedil;o e fixo com cola PVA (pr&oacute;pria para madeira), proporcionando assim grande resist&ecirc;ncia e evitando rangidos no produto.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Cabeceira com Espuma Soft D18 envolto com uma manta de Espuma Eco Soft. Todas as espumas s&atilde;o de fabrica&ccedil;&atilde;o pr&oacute;pria e ecologicamente corretas.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Revestimentos:&nbsp;Cartela Herval de tecidos menos Couro, tecidos listrados, florais e tecidos sint&eacute;ticos. Com composi&ccedil;&atilde;o afixada em etiqueta que acompanha o produto</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Dimens&otilde;es:&nbsp;Casal 138: 140x122x10 cm</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Queen 158: 160x122x10 cm</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">King 193: 195x122x10 cm</span></p>\r\n', '<p><span style=\"font-size:14px;\">Outras op&ccedil;&otilde;es de revestimento: CONSULTAR VALORES&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia de 06 meses</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n', '2018-10-04 22:09:21', '2018-11-14 17:35:24'),
(44, 0, 3, 9, NULL, 6, 'jabaquara,gravi', 'cabeceira-mh-2681', 'Cabeceira MH 2681', 'cabeceira-2681_20181011123000h9Vzb0Suep.jpg', 'Decoração', 'Sem mini frame ', 'LINHA VIVERE', '', 0, 'FABRICANTE HERVAL', '<p><span style=\"font-size: 14px;\">Madeira de Eucalipto de reflorestamento e revestidas com espuma e tecido.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size: 14px;\">O esqueleto &eacute; montado com grampos de a&ccedil;o e fixo com cola PVA (pr&oacute;pria para madeira), proporcionando assim grande resist&ecirc;ncia e evitando rangidos no produto.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size: 14px;\">Cabeceira com Espuma Soft D18 envolto com uma manta de Espuma Eco Soft. Todas as espumas s&atilde;o de fabrica&ccedil;&atilde;o pr&oacute;pria e ecologicamente corretas.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Revestimentos: Cartela Herval de tecidos menos Couro, tecidos listrados, florais e tecidos sint&eacute;ticos. Com composi&ccedil;&atilde;o afixada em etiqueta que acompanha o produto</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Dimens&otilde;es:&nbsp;Casal 138: 140x122x10 cm</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Queen 158: 160x122x10 cm</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">King 193: 195x122x10 cm</span></p>\r\n', '<p><span style=\"font-size:14px;\">Outras op&ccedil;&otilde;es de revestimento: CONSULTAR VALORES&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia de 06 meses</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n', '2018-10-04 22:45:54', '2018-11-14 17:34:19'),
(46, 0, 3, 9, NULL, 6, 'jabaquara,gravi', 'cabeceira-mh-2650', 'Cabeceira MH 2650', 'cabeceira-mh-2650_20181011123111VPip33C41P.jpg', 'Decoração', 'Sem mini frame ', 'LINHA VIVERE', '', 0, 'FABRICANTE HERVAL&nbsp;', '<p><span style=\"font-size:14px;\">Madeira de Eucalipto de reflorestamento e revestidas com espuma e tecido.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">O esqueleto &eacute; montado com grampos de a&ccedil;o e fixo com cola PVA (pr&oacute;pria para madeira), proporcionando assim grande resist&ecirc;ncia e evitando rangidos no produto.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Cabeceira com Espuma Soft D18 envolto com uma manta de Espuma Eco Soft. Todas as espumas s&atilde;o de fabrica&ccedil;&atilde;o pr&oacute;pria e ecologicamente corretas.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Revestimentos: Cartela Herval de tecidos tecidos listrados e sint&eacute;ticos. Com composi&ccedil;&atilde;o afixada em etiqueta que acompanha o produto</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Dimens&otilde;es:&nbsp;Solteiro 088: 90x122x10&nbsp;cm</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Casal 138: 140x122x10 cm</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Queen 158: 160x122x10 cm</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">King 193: 195x122x10 cm</span></p>\r\n', '<p><span style=\"font-size:14px;\">Outras op&ccedil;&otilde;es de revestimento: CONSULTAR VALORES&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia de 06 meses</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n', '2018-10-04 23:00:04', '2018-11-14 17:34:01'),
(47, 2, 2, 14, NULL, 23, 'jabaquara,gravi', 'box-bau-cama-auxiliar-de-molejo', 'Box Baú + Cama Auxiliar de Molejo', '3x1-d-002_201810191218035Yrrj9ZLom.png', 'Box 3x1 ', '', 'Estrutura em Madeira Eucalipto', '', 2, 'FABRICANTE MGA', '<p><span style=\"font-size:14px;\">O Box 3 em 1 da MGA &eacute; o produto ideal para quem procura total aproveitamento de espa&ccedil;o. Al&eacute;m de ser uma cama Box Solteiro ele possui&nbsp;um Ba&uacute;&nbsp;e uma cama auxiliar.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Estrutura emMadeira Eucalipto</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Acionamento por Pist&atilde;o Nakata<br />\r\nAbertura lateral<br />\r\nAcabamento interno do Ba&uacute; com chapa de Eucatex</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">O colch&atilde;o da bicama &eacute; de Molejo Microcoil 285 2.0mm (114 molas p/m&sup2;)</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura do Box: 34 cm &ndash; Altura dos p&eacute;s: 6,5 cm &ndash; Altura &uacute;til: 12&nbsp;cm &ndash;&nbsp;Altura da cama auxiliar: 20 cm</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Suporte de peso por pessoa da bicama &eacute; de 100kg<br />\r\n&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\"><strong>PRE&Ccedil;O REFERENTE AO TECIDO TURCPTLP, OUTROS REVESTIMENTOS CONSULTAR VALORES<br />\r\n(OP&Ccedil;&Atilde;O: TECIDO POLI&Eacute;STER, CORANO, CHENILLE E CAMUR&Ccedil;A)</strong></span></p>\r\n', '<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Consulte valores para medidas especiais&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n', '2018-10-05 15:23:35', '2018-10-26 20:06:50'),
(48, 11, 2, 20, NULL, 23, 'jabaquara,gravi', 'bicama-auxiliar-de-espuma', 'Bicama Auxiliar de Espuma', 'bicama-mga_20181005124421VrknD8jYxR.jpg', 'Box com Bicama', '', 'Estrutura em Madeira Eucalipto ', '', 1, 'FABRICANTE MGA', '<p><span style=\"font-size:14px;\">Box + bicama com colch&atilde;o de Espuma D28</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Estrutura em Madeira Eucalipto</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura do Box: 25 cm &ndash; Altura dos p&eacute;s: 12 cm&nbsp;&ndash; Altura da cama auxiliar: 10 cm</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Medida do Box: 078x188&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Medida da cama auxiliar: 070x171 cm</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Medida do Box: 088x188</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Medida da cama auxiliar: 078x171 cm</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Medida do Box: 096x203&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Medida da cama auxiliar: 088x188 cm</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Peso m&aacute;ximo recomendado por pessoa: 80kg</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\"><strong>PRE&Ccedil;O REFERENTE AO TECIDO SARA BRANCO</strong></span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Registro Inmetro&nbsp;002927/2017</span></p>\r\n', '<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Consulte valores para medidas especiais&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n', '2018-10-05 15:44:22', '2018-10-26 20:02:37'),
(49, 10, 2, 20, NULL, 23, 'jabaquara,gravi', 'bicama-auxiliar-de-molejo', 'Bicama Auxiliar de Molejo ', 'bicama-molejo-mga_20181005125643utdMYKcjgK.jpg', 'Box com Bicama', '', ' Estrutura em Madeira Eucalipto ', '', 3, 'FABRICANTE MGA', '<p><span style=\"font-size:14px;\">Box + bicama com colch&atilde;o de Molejo Microcoil&nbsp;</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Estrutura em Madeira Eucalipto</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura do Box: 30&nbsp;cm &ndash; Altura dos p&eacute;s: 12 cm &ndash; Altura da cama auxiliar: 20 cm</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Medida do Box: 078x188&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Medida da cama auxiliar: 070x171 cm</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Medida do Box: 088x188</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Medida da cama auxiliar: 078x171 cm</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Medida do Box: 096x203&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Medida da cama auxiliar: 088x188 cm</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Peso m&aacute;ximo recomendado por pessoa: 100kg</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\"><strong>PRE&Ccedil;O REFERENTE AO TECIDO SARA BRANCO</strong></span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Registro Inmetro&nbsp;005690/2017 e 005697/2017</span></p>\r\n', '<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Consulte valores para medidas especiais&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n', '2018-10-05 15:56:43', '2018-10-26 20:03:18'),
(50, 9, 2, 20, NULL, 23, 'jabaquara,gravi', 'bicama-molejo-versatil', 'Bicama Molejo Versátil ', 'bicama-molejo-versatil-mga_201810051323549fO5XjOIES.jpg', 'Linha Residencial', '', 'Colchão de Molejo Microcoil ', '', 2, 'FABRICANTE MGA', '<p><span style=\"font-size:14px;\">Bicama Molejo Vers&aacute;til, alinha a cama auxiliar com o colch&atilde;o, trazendo maior espa&ccedil;o e conforto para quem dorme&nbsp;</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Colch&atilde;o Atenas com 22 cm de altura que &eacute; conjunto do Bicama Molejo Vers&aacute;til</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Estrutura em Madeira Eucalipto</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura do Box: 30 cm &ndash; Altura dos p&eacute;s: 12 cm &ndash; Altura da cama auxiliar: 19 cm</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Medida do Box: 078x188&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Medida da cama auxiliar: 070x171 cm</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Medida do Box: 088x188</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Medida da cama auxiliar: 078x171 cm</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Medida do Box: 096x203&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Medida da cama auxiliar: 088x188 cm</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Peso m&aacute;ximo recomendado por pessoa: 100kg</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\"><strong>PRE&Ccedil;O REFERENTE AO TECIDO SARA BRANCO</strong></span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Registro Inmetro&nbsp;005690/2017 e 005697/2017</span></p>\r\n', '<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Consulte valores para medidas especiais&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n', '2018-10-05 16:23:54', '2018-10-26 20:03:38'),
(52, 7, 2, 15, NULL, 23, 'jabaquara,gravi', 'gavetao', 'Gavetão', 'gavetao-mga_201810051450374YxLEV0vuA.jpg', 'Box com 02 gavetas', '', 'Gavetas deslizantes', '', 0, 'FABRICANTE MGA', '<p><span style=\"font-size:14px;\">O Box Gavet&atilde;o 120 MGA possui&nbsp;uma &uacute;nica gaveta que se extende pela lateral do Box, ideal para guardar roupas de cama, cobertores e objetos em geral.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Execelente op&ccedil;&atilde;o para quem busca aproveitamento de espa&ccedil;o</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Estrutura em Madeira Eucalipto</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">GAVETEIRO H30</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura do Box: 30 cm &ndash; Altura dos p&eacute;s: 12 cm &ndash; Dimens&otilde;es das gavetas: 58x115x20 cm</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">MEDIDAS:&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">069x198, 079x198, 078x188, 088x188, 096x203 e 120x203&nbsp;cm: 01 gaveta (Apenas de um lado)</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">128x188 e 138x188 cm: 02 gavetas (Uma de cada lado)</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Opcional: Posi&ccedil;&atilde;o da gaveta pode ser alterada, dando espa&ccedil;o para criado-mudo</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\"><strong>PRE&Ccedil;O REFERENTE AO TECIDO SARA BRANCO, OUTROS REVESTIMENTOS CONSULTAR VALORES<br />\r\n(OP&Ccedil;&Atilde;O: TECIDO POLI&Eacute;STER, CORANO, CHENILLE E CAMUR&Ccedil;A)</strong></span></p>\r\n', '<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Consulte valores para medidas especiais&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n', '2018-10-05 17:50:38', '2018-11-01 20:44:54'),
(53, 6, 2, 15, NULL, 23, 'jabaquara,gravi', 'gaveteiro', 'Gaveteiro', 'gaveteiro-mga_20181005192407p39Zm6GYe9.jpg', 'Box com 04 gavetas', '', 'Gavetas deslizantes', '', 0, 'FABRICANTE MGA', '<p><span style=\"font-size:14px;\">O gaveteiro MGA possui 04 gavetas ideais para guardar roupas de cama, cobertores e objetos em geral.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Execelente op&ccedil;&atilde;o para quem busca aproveitamento de espa&ccedil;o</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Estrutura em Madeira Eucalipto</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">GAVETEIRO H30</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura do Box: 30 cm &ndash; Altura dos p&eacute;s: 12 cm &ndash; Dimens&otilde;es das gavetas: 58x50x20 cm</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">MEDIDAS:&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">069x198, 079x198, 078x188, 088x188, 096x203 e 120x203 cm: 02&nbsp;gavetas (Do mesmo lado)</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">128x188 e 138x188 cm: 04 gavetas (Duas&nbsp;de cada lado)</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\"><strong>PRE&Ccedil;O REFERENTE AO TECIDO SARA BRANCO, OUTROS REVESTIMENTOS CONSULTAR VALORES<br />\r\n(OP&Ccedil;&Atilde;O: TECIDO POLI&Eacute;STER, CORANO, CHENILLE E CAMUR&Ccedil;A)</strong></span></p>\r\n', '<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Consulte valores para medidas especiais&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n', '2018-10-05 22:24:07', '2018-11-01 20:45:14'),
(55, 34, 1, 3, NULL, 3, 'jabaquara,gravi', 'sleep-max-d20', 'Sleep Max D20', 'sleep-max-d20-solteiro_20181006123728YRessUo0oA.jpg', 'Conforto Firme', 'Espessura 12 cm', 'Espuma D20', '', 1, 'FABRICANTE CASTOR', '<p><span style=\"font-size:14px;\">ESTRUTURA INTERNA&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Espuma: L&acirc;mina de Espuma D20 com 12 cm</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">ESTRUTURA EXTERNA&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Faixa Lateral do colch&atilde;o: Tecido 100% Poli&eacute;ster<br />\r\nTecido Matelassado em manta de Poli&eacute;ster 80g</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Tamp&atilde;o superior e inferior: Tecido sem bordado 100% Poli&eacute;ster<br />\r\nAcabamento com Debrum de 40 mm</span></p>\r\n', '<p><span style=\"font-size:14px;\">Colch&atilde;o com 12&nbsp;cm de altura&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Suporte de peso de at&eacute; 40kg por pessoa</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia:&nbsp;12 meses na espuma e 90 dias no tecido</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n', '2018-10-06 15:37:29', '2018-11-01 20:31:16'),
(56, 33, 1, 3, NULL, 3, 'jabaquara,gravi', 'sleep-max-d23', 'Sleep Max D23', 'sleep-max-d23-solteiro_20181006124823N4NqS1V73a.jpg', 'Conforto Firme', 'Espessura 15 cm', 'Espuma D23', '', 1, 'FABRICANTE CASTOR&nbsp;', '<p><span style=\"font-size:14px;\">ESTRUTURA INTERNA</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Espuma: L&acirc;mina de Espuma D23 com 12,5 cm para colch&atilde;o de 15 cm Matelassado</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">ESTRUTURA EXTERNA&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Faixa Lateral do colch&atilde;o: Tecido 100% Poli&eacute;ster<br />\r\nTecido Matelassado em manta de Poli&eacute;ster 80g</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Tamp&atilde;o superior e inferior: Tecido em 100% Poli&eacute;ster&nbsp;<br />\r\n01 espuma de Poliuretano D20 com 1,35 cm de espessura<br />\r\nMatelassado tipo &ndash; Continuo<br />\r\nAcabamento com Debrum de 35 mm</span></p>\r\n', '<p><span style=\"font-size:14px;\">Colch&atilde;o com 15&nbsp;cm de altura&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Suporte de peso de at&eacute; 60kg por pessoa</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 12 meses na espuma e 90 dias no tecido</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n', '2018-10-06 15:48:24', '2018-11-01 20:30:46'),
(57, 32, 1, 3, NULL, 3, 'jabaquara,gravi', 'sleep-max-d28', 'Sleep Max D28', 'sleep-max-d28-solteiro_201810061256346UPNm64S8O.jpg', 'Conforto Firme', 'Espessuras 18 e 25 cm', 'Espuma D28', '', 1, 'FABRICANTE CASTOR', '<p><span style=\"font-size:14px;\">ESTRUTURA INTERNA</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Espuma: L&acirc;mina de espuma D28 com 12,5 cm para colch&atilde;o de 18 cm bordado<br />\r\nL&acirc;mina de espuma D28 com 19,5 cm para colch&atilde;o de 25 cm Matelassado</span><br />\r\n&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">ESTRUTURA EXTERNA<br />\r\nFaixa Lateral do colch&atilde;o: Tecido 100% Poli&eacute;ster<br />\r\nTecido Matelassado em manta de Poli&eacute;ster 80g</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Tamp&atilde;o superior e inferior: Tecido em 100% Poli&eacute;ster<br />\r\n02 espumas de Poliuretano D20 com 1,35 cm de espessura<br />\r\nBordado tipo &ndash; Cont&iacute;nuo<br />\r\nAcabamento com debrum de 35 mm</span></p>\r\n', '<p><span style=\"font-size:14px;\">Colch&atilde;o com 18 e&nbsp;25 cm de altura&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Suporte de peso de at&eacute; 80kg&nbsp;por pessoa</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 12 meses na espuma e 90 dias no tecido</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n', '2018-10-06 15:56:34', '2018-11-01 20:30:08'),
(58, 31, 1, 3, NULL, 3, 'jabaquara,gravi', 'sleep-max-d33', 'Sleep Max D33', 'sleep-max-d33-solteiro_20181006130719yRuHJTt2q0.jpg', 'Conforto Firme', 'Espessura 18 e 25 cm', 'Espuma D33', '', 1, 'FABRICANTE CASTOR', '<p><br />\r\n<span style=\"font-size:14px;\">ESTRUTURA INTERNA</span><br />\r\n<span style=\"font-size:14px;\">L&acirc;mina de Espuma D33 com 12,5 cm para colch&atilde;o de 18 cm Matelassado<br />\r\nL&acirc;mina de Espuma D33 com 19,5 cm para colch&atilde;o de 25 cm Matelassado</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">ESTRUTURA EXTERNA<br />\r\nFaixa Lateral do colch&atilde;o: Tecido 100% Poli&eacute;ster</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Tecido Matelassado em manta de Poli&eacute;ster 80g</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">&nbsp;<br />\r\n<strong>Faixinha Euro-Pillow nos colch&otilde;es com 25 cm: </strong>Tecido 100% Poli&eacute;ster<br />\r\nTecido Matelassado em manta de Poli&eacute;ster 80g</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Tamp&atilde;o superior e inferior: Tecido em 100% Poli&eacute;ster<br />\r\n02 espumas de Poliuretano D20 com 1,35 cm de espessura<br />\r\nBordado tipo &ndash; Cont&iacute;nuo<br />\r\nAcabamento com Debrum de 35 mm</span></p>\r\n', '<p><span style=\"font-size:14px;\">Colch&atilde;o com 18 e&nbsp;25&nbsp;cm de altura&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Suporte de peso de at&eacute; 100kg por pessoa</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 24 meses na espuma e 90 dias no tecido</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n', '2018-10-06 16:07:19', '2018-11-01 20:29:23'),
(59, 30, 1, 3, NULL, 3, 'jabaquara,gravi', 'sleep-max-d45', 'Sleep Max D45', 'sleep-max-d45-solteiro-2014_20181006134216xOjGAP5kyl.jpg', 'Conforto Firme', 'Espessura 18 e 25 cm', 'Espuma D45', '', 1, '', '<p><span style=\"font-size:14px;\">ESTRUTURA INTERNA<br />\r\nEspuma: L&acirc;mina de Espuma D45 com 12,5 cm para colch&atilde;o de 18 cm Matelassado<br />\r\nL&acirc;mina de Espuma D45 com 19,5 cm para colch&atilde;o de 25 cm Matelassado</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">ESTRUTURA EXTERNA<br />\r\nFaixa Lateral do colch&atilde;o: Tecido 100% Poli&eacute;ster<br />\r\nTecido Matelassado em manta Poli&eacute;ster 80g<br />\r\n&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\"><strong>Faixinha Euro-Pillow nos colch&otilde;es com 25 cm:</strong> Tecido 100% Poli&eacute;ster<br />\r\nTecido Matelassado em manta Poli&eacute;ster 80g</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Tamp&atilde;o superior e inferior: Tecido em 100% Poli&eacute;ster<br />\r\n02 espumas de Poliuretano D20 com 1,35 cm de espessura<br />\r\nBordado tipo &ndash; Cont&iacute;nuo<br />\r\nAcabamento com Debrum de 35 mm</span></p>\r\n', '<p><span style=\"font-size:14px;\">Colch&atilde;o com 18 e&nbsp;25&nbsp;cm de altura&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Suporte de peso de at&eacute; 150kg por pessoa</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 24 meses na espuma e 90 dias no tecido</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n', '2018-10-06 16:42:17', '2018-11-01 20:27:57'),
(60, 29, 1, 11, NULL, 3, 'jabaquara,gravi', 'sleep-ortopedico-anatomico', 'Sleep Ortopédico Anatômico', 'sleep-ortopedico-anatomico-estampado_20181006140046rBhuHXTjti.jpg', 'Conforto Firme', 'Espessura 22 cm', 'Ortopédico com Madeira', '', 1, 'FABRICANTE CASTOR&nbsp;', '<p><span style=\"font-size:14px;\">ESTRUTURA INTERNA<br />\r\nEspuma: L&acirc;mina de Espuma D28 5 cm<br />\r\nArma&ccedil;&atilde;o de Madeira Ortop&eacute;dica de 8,0 cm<br />\r\nL&acirc;mina de Espuma AG 5,0 cm<br />\r\n(Uma face firme e a outra extra firme)<br />\r\n&nbsp;<br />\r\nESTRUTURA EXTERNA<br />\r\nFaixa Lateral do colch&atilde;o: Tecido 100% Poli&eacute;ster<br />\r\nTecido Matelassado em Espuma de Poliuretano D14 0,6 cm<br />\r\n&nbsp;<br />\r\nTamp&atilde;o superior e inferior colch&atilde;o: Tecido em 100% Poli&eacute;ster<br />\r\n01 espuma de Poliuretano D20 com 1,7 cm de espessura<br />\r\nBordado tipo &ndash; Cont&iacute;nuo &ndash; Colmeia<br />\r\nAcabamento com Debrum de 35 mm</span></p>\r\n', '<p><span style=\"font-size:14px;\">Colch&atilde;o com 22 cm de altura&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Suporte de peso de at&eacute; 120kg por pessoa</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 24 meses na espuma e 90 dias no tecido</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n', '2018-10-06 17:00:47', '2018-11-01 19:46:18'),
(63, 34, 7, 21, NULL, 13, 'jabaquara,gravi', 'latexlux-cervical-100-latex-natural', 'Latexlux Cervical 100% Látex Natural ', 'lateluxx_20181008131106PBtlBiQp00.png', 'Conforto Macio', '', 'Látex', '', 3, 'FABRICANTE THEVA&nbsp;', '<p><span style=\"font-size:14px;\">Mantem a coluna alinhada e reduz o ronco</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Possui gomos massageadores&nbsp;</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Capa em Viscose de Bambu bactericida, que&nbsp;mantem a temperatura sempre fresca, proporcionando toque suave&nbsp;</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Anti&aacute;caros e lav&aacute;vel</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura: 10/12 cm</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Dimens&atilde;o: 50x70 cm</span></p>\r\n', '<p><span style=\"font-size:14px;\">CUIDADOS: TRAVESSEIROS E ACESS&Oacute;RIOS DE L&Aacute;TEX</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Lavar &agrave; m&atilde;o com &aacute;gua fria e sab&atilde;o neutro<br />\r\nTorcer pressionando entre duas toalhas<br />\r\nSecar &agrave; sombra na posi&ccedil;&atilde;o horizontal<br />\r\nN&atilde;o expor produtos de L&aacute;tex ao sol ou calor<br />\r\nBolhas, marcas de inje&ccedil;&atilde;o de mat&eacute;ria prima e aera&ccedil;&otilde;es n&atilde;o alteram a performance do produto e n&atilde;o s&atilde;o considerados defeitos de fabrica&ccedil;&atilde;o<br />\r\nA n&atilde;o adapta&ccedil;&atilde;o ao cheiro do L&aacute;tex n&atilde;o caracteriza desvio de qualidade</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Refor&ccedil;amos a aten&ccedil;&atilde;o para pessoas al&eacute;rgicas ou hipersens&iacute;veis</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 90 dias</span></p>\r\n', '2018-10-08 16:11:07', '2018-10-26 22:04:45'),
(64, 33, 7, 21, NULL, 13, 'jabaquara,gravi', 'theva-kapok', 'Theva Kapok', 'theva-kapok_20181008132202J1mMMVL8fx.png', 'Conforto Macio', '', '100% Capoque ', '', 3, 'FABRICANTE THEVA', '<p><span style=\"font-size:14px;\">O Travesseiro Kapok utiliza a fibra do fruto da &aacute;rvore Kapok Tree (similar a Paineira), considerada pelos povos ind&iacute;genas como a &aacute;rvore Rainha da Floresta, sua fibra oferece um toque de conforto inigual&aacute;vel</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Produto 100% natural, livre de produtos qu&iacute;micos</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Proporciona alta absor&ccedil;&atilde;o da umidade mantendo a temperatura sempre fresca</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Possui propriedades bactericidas, antimicrobianas e anti&aacute;caros</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Suporte ideal para cabe&ccedil;a e pesco&ccedil;o</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Tecido Percal 230 fios proporcionando&nbsp;toque extra macio</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Dimens&atilde;o: 50x70 cm</span></p>\r\n', '<p><span style=\"font-size:14px;\">Garantia: 90 dias</span></p>\r\n', '2018-10-08 16:22:03', '2018-10-26 22:59:03'),
(66, 31, 7, 21, NULL, 13, 'jabaquara,gravi', 'plushpillo-premium', 'Plushpillo Premium ', 'theva-plushpillo-premium_2018100813382677mPxk8KHq.png', 'Conforto Macio', '', 'Látex', '', 3, '', '<p><span style=\"font-size:14px;\">Oferece a maciez da fibra siliconizada em p&eacute;rolas com o suporte do L&aacute;tex flocado</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Proporciona posi&ccedil;&atilde;o correta para cabe&ccedil;a e pesco&ccedil;o</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Mant&eacute;m a temperatura sempre fresca</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Anti&aacute;caros</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Capa 100% Algod&atilde;o 220 fios toque de Seda, com acabamento em vivo luxuoso</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Pr&oacute;prio para fronhas de 50x70cm</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura: 15 cm</span></p>\r\n', '<p><span style=\"font-size:14px;\">CUIDADOS: TRAVESSEIROS E ACESS&Oacute;RIOS DE L&Aacute;TEX</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Lavar &agrave; m&atilde;o com &aacute;gua fria e sab&atilde;o neutro<br />\r\nTorcer pressionando entre duas toalhas<br />\r\nSecar &agrave; sombra na posi&ccedil;&atilde;o horizontal<br />\r\nN&atilde;o expor produtos de l&aacute;tex ao sol ou calor<br />\r\nBolhas, marcas de inje&ccedil;&atilde;o de mat&eacute;ria prima e aera&ccedil;&otilde;es n&atilde;o alteram a performance do produto e n&atilde;o s&atilde;o considerados defeitos de fabrica&ccedil;&atilde;o<br />\r\nA n&atilde;o adapta&ccedil;&atilde;o ao cheiro do L&aacute;tex n&atilde;o caracteriza desvio de qualidade</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Refor&ccedil;amos a aten&ccedil;&atilde;o para pessoas al&eacute;rgicas ou hipersens&iacute;veis</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 90 dias</span></p>\r\n', '2018-10-08 16:38:27', '2018-10-26 22:06:06'),
(67, 30, 7, 21, NULL, 13, 'jabaquara,gravi', 'memogel-pillow', 'Memogel Pillow', 'theva-memogel-pillow_201810081345285IbU3v3OUx.png', 'Conforto Macio', '', 'Espuma Viscoelástica', '', 3, '', '<p><span style=\"font-size:14px;\">Este travesseiro utiliza um revolucion&aacute;rio material que combina o conforto da Espuma Viscoel&aacute;stica com o frescor das Micropart&iacute;culas de Gel</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Reduz o calor do corpo oferecendo um sono saud&aacute;vel e reparador</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Promove o al&iacute;vio dos pontos de press&atilde;o</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Capa remov&iacute;vel e lav&aacute;vel com tecido Liocel proporciona toque extra suave e absor&ccedil;&atilde;o da umidade</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Pr&oacute;prio para fronhas de 50x70 cm</span></p>\r\n', '<p><span style=\"font-size:14px;\">CUIDADOS: TRAVESSEIROS&nbsp;DE ESPUMA DE POLIURETANO VISCOEL&Aacute;STICA</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">A espuma de Poliuretano Viscoel&aacute;stica &eacute; termosens&iacute;vel, ou seja, o calor natural do corpo faz com que o travesseiro amolde-se ao usu&aacute;rio<br />\r\nN&atilde;o molhar e n&atilde;o lavar</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 90 dias</span></p>\r\n', '2018-10-08 16:45:30', '2018-10-26 22:06:23'),
(68, 29, 7, 21, NULL, 13, 'jabaquara,gravi', 'bestpluma', 'Bestpluma', 'bestpluma-1_20181008135012ZORtjREana.png', 'Conforto Macio', '', 'Pluma Sintética', '', 3, 'FABRICANTE THEVA', '<p><span style=\"font-size:14px;\">Produzido com Pluma Sint&eacute;tica oferece conforto inigual&aacute;vel (N&atilde;o maltrata os animais)</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">N&atilde;o tem cheiro</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Lava e seca na m&aacute;quina</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Tecido Algod&atilde;o Percal 220 fios </span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Capa com acabamento em vivo luxuoso&nbsp;</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Dimens&atilde;o: 50x70 cm</span></p>\r\n', '<p><span style=\"font-size:14px;\">CUIDADOS:&nbsp;TRAVESSEIRO DE PLUMA SINT&Eacute;TICA</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">&Eacute; lav&aacute;vel e sec&aacute;vel em m&aacute;quina</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 90 dias</span></p>\r\n', '2018-10-08 16:50:14', '2018-10-26 22:06:40'),
(69, 28, 7, 26, NULL, 13, 'jabaquara,gravi', 'barra-de-apoio-stand-up', 'Barra de apoio Stand Up', 'barra-standup-encarte_201810081435481GU0ibpXKA.png', '', '', '51x61 cm', '', 0, 'FABRICANTE THEVA', '<p><span style=\"font-size:14px;\">Independ&ecirc;ncia para pessoas com dificuldade de locomo&ccedil;&atilde;o, possibilitando o acesso a cama com mais seguran&ccedil;a</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Port&aacute;til e resistente at&eacute; 140kg para movimento de alavanca</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Oferece grip antiderrapante</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Possui bolso para guardar objetos pessoais</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">F&aacute;cil instala&ccedil;&atilde;o e montagem</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Dimens&atilde;o: 51x61 cm</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura: 45 cm</span></p>\r\n', '<p><span style=\"font-size:14px;\">N&atilde;o recomendado o uso como grade de prote&ccedil;&atilde;o</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">N&atilde;o exponha a barra de apoio Stand Up ao sol sob o risco de ressecamento</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">N&atilde;o utilizar em camas ajust&aacute;veis</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 90 dias</span></p>\r\n', '2018-10-08 17:35:50', '2018-11-03 18:22:12'),
(70, 27, 7, 26, NULL, 13, 'jabaquara,gravi', 'grade-de-cama-senior-sleep', 'Grade de cama Senior Sleep', 'gradecama-embalado-2_201810091222179uSVqUSX2V.png', '', '', '60x94 cm', '', 0, 'FABRICANTE THEVA', '<p><span style=\"font-size:14px;\">Previne quedas enquanto deitado, oferecendo seguran&ccedil;a para quem dorme e para quem cuida</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Ajust&aacute;vel a camas box e convencionais</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Port&aacute;til e suporta at&eacute; 140 kg de apoio</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Possui bolso para guardar objetos pessoais</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">F&aacute;cil instala&ccedil;&atilde;o e montagem</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Dimens&atilde;o: 60x94&nbsp;cm</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura: 54&nbsp;cm</span></p>\r\n', '<p><span style=\"font-size:14px;\">Garantia: 90 dias</span></p>\r\n', '2018-10-09 15:22:18', '2020-02-26 19:26:03');
INSERT INTO `produtos` (`id`, `ordem`, `secao_id`, `categoria_id`, `subcategoria_id`, `marca_id`, `lojas`, `slug`, `titulo`, `capa`, `material`, `linha`, `legenda`, `codigo`, `conforto`, `resumo`, `descricao`, `dados_complementares`, `created_at`, `updated_at`) VALUES
(71, 26, 7, 27, NULL, 13, 'jabaquara,gravi', 'suporte-terapeutico-antirrefluxo-adulto', 'Suporte Terapêutico Antirrefluxo Adulto ', 'suporte-terapeutico-antirefluxo-adulto_201810091233260DoE0nimTj.png', '', '', '70x82 cm', '', 1, 'FABRICANTE THEVA', '<p><span style=\"font-size:14px;\">Alivia os sintomas do refluxo gastroesof&aacute;gico, previnindo o retorno do conte&uacute;do estomacal para o es&ocirc;fago, laringe e faringe</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Evita tosse, pigarro, rouquid&atilde;o e dores de gargante decorrentes do refluxo</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Reduz o ronco</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Facilita o descanso na gravidez</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Melhora a respira&ccedil;&atilde;o em casos leves de obstru&ccedil;&atilde;o nasal</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Bipartida, facilita o transporte e a armazenagem</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Capa remov&iacute;vel e lav&aacute;vel em tecido 100% Algod&atilde;o que mant&eacute;m a temperatura fresca</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Superf&iacute;cie perfilada massageia e proporciona relaxamento</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Dimens&atilde;o: 70x82&nbsp;cm</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura: 14 cm</span></p>\r\n', '<p><span style=\"font-size:14px;\">Possui registro na Anvisa, registrada sob o n&uacute;mero 80730110001</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 06 meses</span></p>\r\n', '2018-10-09 15:33:27', '2018-10-26 22:08:12'),
(72, 25, 7, 28, NULL, 13, 'jabaquara,gravi', 'almofada-sapo', 'Almofada Sapo', 'sapo_20181009125545NxCEaxrzx6.png', '', '', 'Látex Flocado e Fibra Siliconada', '', 3, 'FABRICANTE THEVA', '<p><span style=\"font-size:14px;\">Almofada de encosto que acomoda confortavelmente ombros, bra&ccedil;os e coluna</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Tecido inteligente oferece excelente ventila&ccedil;&atilde;o e mant&eacute;m a temperatura sempre fresca</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Combina&ccedil;&atilde;o de L&aacute;tex Flocado e Fibra Siliconada oferece um toque inigual&aacute;vel</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Anti&aacute;caro</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Dimens&atilde;o: 45x70 cm</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura: 55&nbsp;cm</span></p>\r\n', '<p><span style=\"font-size:14px;\">Garantia: 90 dias</span></p>\r\n', '2018-10-09 15:55:46', '2018-10-26 23:33:12'),
(73, 24, 7, 28, NULL, 13, 'jabaquara,gravi', 'almofada-triangular', 'Almofada Triangular', 'plushpillo_20181009130130TTKmDY8gLd.png', '', '', 'Látex Flocado e Fibra Siliconada', '', 3, 'FABRICANTE THEVA', '<p><span style=\"font-size:14px;\">O melhor apoio para o seu descanso</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Acomoda confortavelmente a coluna e as pernas&nbsp;</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">A combina&ccedil;&atilde;o do L&aacute;tex Flocado com a Fibra Siliconizada proporciona toque inigual&aacute;vel</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Anti&aacute;caros</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Tecido inteligente permite a passagem do ar</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Possui capa remov&iacute;vel e lav&aacute;vel</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Dimens&atilde;o: 26x52&nbsp;cm</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura: 58&nbsp;cm</span></p>\r\n', '<p><span style=\"font-size:14px;\">Garantia: 90 dias</span></p>\r\n', '2018-10-09 16:01:32', '2018-10-26 23:33:29'),
(74, 23, 7, 27, NULL, 13, 'jabaquara,gravi', 'suporte-terapeutico-antirrefluxo-infantil', 'Suporte Terapêutico Antirrefluxo Infantil', 'antirrefluxo-infantil_201810091308222tK9q8B4Xf.png', '', '', '60x88 cm', '', 1, '', '<p><span style=\"font-size:14px;\">Alivia os sintomas do refluxo gastroesof&aacute;gico</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Evita tosse e dores de garganta decorrentes do refluxo</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Melhora a respira&ccedil;&atilde;o em casos leves de obstru&ccedil;&atilde;o nasal</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Garante um sono tranquilo ao beb&ecirc;</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Espuma de alta qualidade</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Dobr&aacute;vel, f&aacute;cil de transportar</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Capa remov&iacute;vel e lav&aacute;vel em tecido 100% Algod&atilde;o mant&eacute;m a temperatura fresca</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Dimens&atilde;o: 60x88&nbsp;cm</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura: 12&nbsp;cm</span></p>\r\n', '<p><span style=\"font-size:14px;\">A &uacute;nica com registro na Anvisa, registrada sob o n&uacute;mero 80730110001</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 06 meses</span></p>\r\n', '2018-10-09 16:08:24', '2018-10-26 22:09:11'),
(75, 21, 7, 23, NULL, 12, 'jabaquara,gravi', 'gold-queen', 'Gold Queen', 'gold-queen_20181009132000o1sWU9O1x1.png', 'Conforto Intermediário', '', '100% Látex Natural', '', 2, 'FABRICANTE DUNLOPILLO', '<p><span style=\"font-size:14px;\">Travesseiro 100% L&aacute;tex Natural Dunlopillo Talalay</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Ideal para quem prefere travesseiros mais altos</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Possui capa remov&iacute;vel&nbsp;e lav&aacute;vel de Tecido 100% Algod&atilde;o Percal 230 fios que mant&eacute;m a temperatura sempre fresca</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Dimens&atilde;o: 50x70 cm</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura: 18 cm</span></p>\r\n', '<p><span style=\"font-size:14px;\">CUIDADOS: TRAVESSEIROS E ACESS&Oacute;RIOS DE L&Aacute;TEX</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Lavar &agrave; m&atilde;o com &aacute;gua fria e sab&atilde;o neutro<br />\r\nTorcer pressionando entre duas toalhas<br />\r\nSecar &agrave; sombra na posi&ccedil;&atilde;o horizontal<br />\r\nN&atilde;o expor produtos de l&aacute;tex ao sol ou calor<br />\r\nBolhas, marcas de inje&ccedil;&atilde;o de mat&eacute;ria prima e aera&ccedil;&otilde;es n&atilde;o alteram a performance do produto e n&atilde;o s&atilde;o considerados defeitos de fabrica&ccedil;&atilde;o<br />\r\nA n&atilde;o adapta&ccedil;&atilde;o ao cheiro do L&aacute;tex n&atilde;o caracteriza desvio de qualidade</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Refor&ccedil;amos a aten&ccedil;&atilde;o para pessoas al&eacute;rgicas ou hipersens&iacute;veis</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n', '2018-10-09 16:20:02', '2018-10-26 22:09:27'),
(76, 20, 7, 23, NULL, 12, 'jabaquara,gravi', 'basic-bambu', 'Basic Bambu', 'basic-bambu_20181009132842iTzOV1YT83.png', 'Conforto Intermediário', '', '100% Látex Natural', '', 2, 'FABRICANTE DUNLOPILLO', '<p><span style=\"font-size:14px;\">Travesseiro 100% L&aacute;tex Natural Dunlopillo Talalay</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Sua capa remov&iacute;vel e lav&aacute;vel &eacute; produzida em Viscose de Bambu, que possui propriedades naturalmente bactericidas</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Oferece toque super suave e mant&eacute;m a temperatura sempre fresca</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Dimens&atilde;o: 50x70 cm</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura: 15 cm</span></p>\r\n', '<p><span style=\"font-size:14px;\">CUIDADOS: TRAVESSEIROS E ACESS&Oacute;RIOS DE L&Aacute;TEX</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Lavar &agrave; m&atilde;o com &aacute;gua fria e sab&atilde;o neutro<br />\r\nTorcer pressionando entre duas toalhas<br />\r\nSecar &agrave; sombra na posi&ccedil;&atilde;o horizontal<br />\r\nN&atilde;o expor produtos de L&aacute;tex ao sol ou calor<br />\r\nBolhas, marcas de inje&ccedil;&atilde;o de mat&eacute;ria prima e aera&ccedil;&otilde;es n&atilde;o alteram a performance do produto e n&atilde;o s&atilde;o considerados defeitos de fabrica&ccedil;&atilde;o<br />\r\nA n&atilde;o adapta&ccedil;&atilde;o ao cheiro do L&aacute;tex n&atilde;o caracteriza desvio de qualidade</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Refor&ccedil;amos a aten&ccedil;&atilde;o para pessoas al&eacute;rgicas ou hipersens&iacute;veis</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n', '2018-10-09 16:28:43', '2018-10-26 22:09:43'),
(77, 19, 7, 23, NULL, 12, 'jabaquara,gravi', 'basic-soft', 'Basic Soft', 'basicsoft-det-03_20181009133418qdy1viyByV.png', 'Conforto Macio', '', '100% Látex Natural', '', 3, '', '<p><span style=\"font-size:14px;\">Travesseiro 100% L&aacute;tex Natural Dunlopillo Talalay</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Para quem prefere travesseiros mais macios</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Tecido 100% Algod&atilde;o garantindo frescor</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">L&aacute;tex proporciona conforto inigual&aacute;vel e alta durabilidade</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Lav&aacute;vel</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Dimens&atilde;o: 50x70 cm</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura: 15&nbsp;cm</span></p>\r\n', '<p><span style=\"font-size:14px;\">CUIDADOS: TRAVESSEIROS E ACESS&Oacute;RIOS DE L&Aacute;TEX</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Lavar &agrave; m&atilde;o com &aacute;gua fria e sab&atilde;o neutro<br />\r\nTorcer pressionando entre duas toalhas<br />\r\nSecar &agrave; sombra na posi&ccedil;&atilde;o horizontal<br />\r\nN&atilde;o expor produtos de L&aacute;tex ao sol ou calor<br />\r\nBolhas, marcas de inje&ccedil;&atilde;o de mat&eacute;ria prima e aera&ccedil;&otilde;es n&atilde;o alteram a performance do produto e n&atilde;o s&atilde;o considerados defeitos de fabrica&ccedil;&atilde;o<br />\r\nA n&atilde;o adapta&ccedil;&atilde;o ao cheiro do L&aacute;tex n&atilde;o caracteriza desvio de qualidade</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Refor&ccedil;amos a aten&ccedil;&atilde;o para pessoas al&eacute;rgicas ou hipersens&iacute;veis</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n', '2018-10-09 16:34:19', '2018-10-26 22:10:17'),
(78, 18, 7, 29, NULL, 12, 'jabaquara,gravi', 'top-pad', 'Top Pad', 'top-pad-2_20181009134900JNZVSGk4M2.png', 'Conforto Macio', '', '100% Látex Natural', '', 3, '', '<p><span style=\"font-size:14px;\">O Top Pad &eacute; composto por uma l&acirc;mina de L&aacute;tex Dunlopillo, revestida com capa remov&iacute;vel em tecido Viscose de Bambu.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Possui propriedades naturalmente bactericidas, e mant&eacute;m a temperatura sempre fresca.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Medidas dispon&iacute;veis: 088x188 cm, 138x188 cm,158x198 cm e 193x203 cm</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura: 03 cm</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Medidas especiais sob consulta</span></p>\r\n', '<p><span style=\"font-size:14px;\">CUIDADOS: TRAVESSEIROS E ACESS&Oacute;RIOS DE L&Aacute;TEX</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Lavar &agrave; m&atilde;o com &aacute;gua fria e sab&atilde;o neutro<br />\r\nTorcer pressionando entre duas toalhas<br />\r\nSecar &agrave; sombra na posi&ccedil;&atilde;o horizontal<br />\r\nN&atilde;o expor produtos de L&aacute;tex ao sol ou calor<br />\r\nBolhas, marcas de inje&ccedil;&atilde;o de mat&eacute;ria prima e aera&ccedil;&otilde;es n&atilde;o alteram a performance do produto e n&atilde;o s&atilde;o considerados defeitos de fabrica&ccedil;&atilde;o<br />\r\nA n&atilde;o adapta&ccedil;&atilde;o ao cheiro do L&aacute;tex n&atilde;o caracteriza desvio de qualidade</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Refor&ccedil;amos a aten&ccedil;&atilde;o para pessoas al&eacute;rgicas ou hipersens&iacute;veis</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n', '2018-10-09 16:45:23', '2018-11-01 21:53:13'),
(79, 17, 7, 29, NULL, 18, 'jabaquara,gravi', 'topper-visco-gel', 'Topper Visco Gel', 'topper-visco-gel-2_20181009135538xPYWM9ZIAI.jpg', 'Conforto Macio', '', 'Visco Gel', '', 0, '', '<p><span style=\"font-size:14px;\">O toque e o conforto exclusivo proporcionado pela Espuma Viscoel&aacute;stica &eacute; percept&iacute;vel logo ao se deitar, contudo os maiores benef&iacute;cios voc&ecirc; notar&aacute; durante o descanso.</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">A espuma Visco Gel F.A. Colch&otilde;es minimiza os pontos de press&atilde;o causados no contato do corpo com a superf&iacute;cie do colch&atilde;o, melhorando a circula&ccedil;&atilde;o sangu&iacute;nea e diminuindo a quantidade de vezes que nos mexemos durante a noite. Com isto, alcan&ccedil;amos um sono mais profundo, auxiliando no processo de recupera&ccedil;&atilde;o do corpo.</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Al&eacute;m disto, a tecnologia GelSense une os benef&iacute;cios da Espuma Viscoel&aacute;stica com o conforto t&eacute;rmico do gel, proporcionando maior frescor quando comparado com Espumas Viscoel&aacute;sticas comuns.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura: 04 cm de Espuma Visco Gel</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Modo de Utiliza&ccedil;&atilde;o: Ap&oacute;s retirar o produto da caixa, abra o pl&aacute;stico que envolve a manta de espuma<br />\r\nDescarte o pl&aacute;stico, preferencialmente, separando-o em lixo recicl&aacute;vel<br />\r\nAcomode a espuma sobre o seu colch&atilde;o<br />\r\nUtilize um protetor de colch&atilde;o sobre a manta Visco Gel para prolongar a vida &uacute;til do Topper<br />\r\nUtilize um len&ccedil;ol para fix&aacute;-la</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Obs: Se necess&aacute;rio, deixe a manta de espuma arejando em local seco, ventilado e &agrave; sombra</span></p>\r\n', '<p><span style=\"font-size:14px;\">CUIDADOS:</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Aspire regularmente. N&atilde;o molhe o produto<br />\r\nCaso ocorra algum derramamento de l&iacute;quidos, limpe imediatamente com pano seco e deixe secar naturalmente &agrave; sombra em local ventilado<br />\r\n<br />\r\nImportante: &Eacute; comum ocorrer amarelamento da espuma que &eacute; um processo natural devido a oxida&ccedil;&atilde;o do produto. Contudo, isto n&atilde;o interfere na qualidade da mesma e n&atilde;o caracteriza nenhum tipo de defeito</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 03 meses</span></p>\r\n', '2018-10-09 16:55:40', '2018-11-01 21:51:38'),
(80, 16, 7, 22, NULL, 14, 'jabaquara,gravi', 'nasa-alto', 'Nasa Alto', 'nasa-alto_20200229161629kBt3WMzQWp.jpg', 'Conforto Macio', '', 'Espuma Viscoelástica', 'NS1116', 3, 'FABRICANTE DUOFLEX', '<p><span style=\"font-size:14px;\">NASA &eacute; uma Espuma Viscoel&aacute;stica moldada com alta viscosidade e densidade. Automold&aacute;vel e termossens&iacute;vel, adapta-se ao contorno e a temperatura do corpo, exercendo menos press&atilde;o nas &aacute;reas mais quentes ou salientes, facilitando a circula&ccedil;&atilde;o sangu&iacute;nea</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Al&eacute;m de oferecer maior capacidade de absor&ccedil;&atilde;o do peso e distribui&ccedil;&atilde;o equalizada da press&atilde;o sobre o corpo, o travesseiro NASA possui a propriedade de retorno lento ou mem&oacute;ria, agindo como um verdadeiro amortecedor</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">A alta qualidade da Espuma Viscoel&aacute;stica Duoflex, permite ao travesseiro NASA retornar &agrave; sua forma original, mesmo ap&oacute;s compress&atilde;o durante o transporte</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Tudo isso torna o travesseiro NASA o ideal para a posi&ccedil;&atilde;o natural de descanso, prevenindo dores na cabe&ccedil;a, pesco&ccedil;o, costas, bra&ccedil;os e ombros, garantindo um sono restaurador, com total prote&ccedil;&atilde;o anti&aacute;caros, fungos e bact&eacute;rias</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Capa: Malha 100% Algod&atilde;o com z&iacute;per</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Dimens&atilde;o: 50x70 cm</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura: 17 cm</span></p>\r\n', '<p><span style=\"font-size:14px;\">Garantia: 02 anos&nbsp;</span></p>\r\n', '2018-10-09 17:08:28', '2020-02-29 19:16:30'),
(81, 15, 7, 22, NULL, 14, 'jabaquara,gravi', 'nasa-cervical', 'Nasa Cervical', 'nasa-cervical_20200229161322cHktrvNcYM.jpg', 'Conforto Macio', '', 'Espuma Viscoelástica', 'NN2100', 3, 'FABRICANTE DUOFLEX', '<p><span style=\"font-size:14px;\">NASA &eacute; uma Espuma Viscoel&aacute;stica moldada com alta viscosidade e densidade. Automold&aacute;vel e termossens&iacute;vel, adapta-se ao contorno e a temperatura do corpo, exercendo menos press&atilde;o nas &aacute;reas mais quentes ou salientes, facilitando a circula&ccedil;&atilde;o sangu&iacute;nea</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Al&eacute;m de oferecer maior capacidade de absor&ccedil;&atilde;o do peso e distribui&ccedil;&atilde;o equalizada da press&atilde;o sobre o corpo, o travesseiro NASA possui a propriedade de retorno lento ou mem&oacute;ria, agindo como um verdadeiro amortecedor</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">A alta qualidade da Espuma Viscoel&aacute;stica Duoflex, permite ao travesseiro NASA retornar &agrave; sua forma original, mesmo ap&oacute;s compress&atilde;o durante o transporte</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Tudo isso torna o travesseiro NASA o ideal para a posi&ccedil;&atilde;o natural de descanso, prevenindo dores na cabe&ccedil;a, pesco&ccedil;o, costas, bra&ccedil;os e ombros, garantindo um sono restaurador, com total prote&ccedil;&atilde;o anti&aacute;caros, fungos e bact&eacute;rias</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Capa Plush: &nbsp;Algod&atilde;o e Poli&eacute;ster com z&iacute;per</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Dimens&atilde;o: 50x70 cm</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura:&nbsp;12,5 cm/7,5 cm/11 cm</span></p>\r\n', '<p><span style=\"font-size:14px;\">Garantia: 03 anos</span></p>\r\n', '2018-10-09 17:14:48', '2020-02-29 19:13:23'),
(82, 14, 7, 22, NULL, 14, 'jabaquara,gravi', 'natural-latex-flocos', 'Natural Látex Flocos', 'natural-latex_20200229161133B4JBlkGvvm.jpg', 'Conforto Macio', '', '100% Látex ', 'FL1100', 3, 'FABRICANTE DUOFLEX', '<p><span style=\"font-size:14px;\">Produzido a partir da seiva da seringueira, o travesseiro Natural L&aacute;tex &eacute; o de maior aceita&ccedil;&atilde;o no mundo, pois adapta-se com perfei&ccedil;&atilde;o a todas as pessoas. Sua estrutura permite maior durabilidade e conforto, al&eacute;m do toque mais macio, aveludado e fresco</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Eles permitem total sustenta&ccedil;&atilde;o da cabe&ccedil;a em todas as posi&ccedil;&otilde;es garantindo um sono tranquilo e confort&aacute;vel a noite toda. N&atilde;o &eacute; preciso afofar o travesseiro ap&oacute;s o uso, porque sua mat&eacute;ria prima e sua estrutura celular &uacute;nica permitem retornar &agrave; sua forma original mesmo depois de uso intensivo e cont&iacute;nuo</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Composto por milh&otilde;es de c&eacute;lulas abertas, o travesseiro Natural L&aacute;tex &eacute; moldado com centenas de furos que formam canais internos de circula&ccedil;&atilde;o de ar que facilitam a respira&ccedil;&atilde;o e ventila&ccedil;&atilde;o, fazendo com que ele fique a todo o tempo numa temperatura levemente inferior &agrave; do corpo, al&eacute;m de permitir evapora&ccedil;&atilde;o r&aacute;pida de umidade e transpira&ccedil;&atilde;o, prevenindo odores e contamina&ccedil;&otilde;es</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Al&eacute;m de serem anti&aacute;caros, fungos e bact&eacute;rias, s&atilde;o esterilizados um a um na produ&ccedil;&atilde;o, garantindo total higieniza&ccedil;&atilde;o</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Por serem lav&aacute;veis, higi&ecirc;nicos, frescos e extremamente confort&aacute;veis s&atilde;o indicados para todas as esta&ccedil;&otilde;es e climas</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Capa: 100% Algod&atilde;o Percal 200 fios</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Dimens&atilde;o: 50x70 cm</span></p>\r\n', '<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n', '2018-10-09 17:23:44', '2020-02-29 19:11:34'),
(83, 13, 7, 22, NULL, 14, 'jabaquara,gravi', 'real-latex-alto', 'Real Látex Alto', 'real-latex-alto_20200229163250VRJMAVnWaI.jpg', 'Conforto Macio', '', 'Látex Natural ', 'LS1100', 3, 'FABRICANTE DUOFLEX', '<p><span style=\"font-size:14px;\">Produzido a partir da seiva da seringueira, o travesseiro Natural L&aacute;tex &eacute; o de maior aceita&ccedil;&atilde;o no mundo, pois adapta-se com perfei&ccedil;&atilde;o a todas as pessoas. Sua estrutura permite maior durabilidade e conforto, al&eacute;m do toque mais macio, aveludado e fresco</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Eles permitem total sustenta&ccedil;&atilde;o da cabe&ccedil;a em todas as posi&ccedil;&otilde;es garantindo um sono tranquilo e confort&aacute;vel a noite toda. N&atilde;o &eacute; preciso afofar o travesseiro ap&oacute;s o uso, porque sua mat&eacute;ria prima e sua estrutura celular &uacute;nica permitem retornar &agrave; sua forma original mesmo depois de uso intensivo e cont&iacute;nuo</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Composto por milh&otilde;es de c&eacute;lulas abertas, o travesseiro Natural L&aacute;tex &eacute; moldado com centenas de furos que formam canais internos de circula&ccedil;&atilde;o de ar que facilitam a respira&ccedil;&atilde;o e ventila&ccedil;&atilde;o, fazendo com que ele fique a todo o tempo numa temperatura levemente inferior &agrave; do corpo, al&eacute;m de permitir evapora&ccedil;&atilde;o r&aacute;pida de umidade e transpira&ccedil;&atilde;o, prevenindo odores e contamina&ccedil;&otilde;es</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Al&eacute;m de serem anti&aacute;caros, fungos e bact&eacute;rias, s&atilde;o esterilizados um a um na produ&ccedil;&atilde;o, garantindo total higieniza&ccedil;&atilde;o</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Por serem lav&aacute;veis, higi&ecirc;nicos, frescos e extremamente confort&aacute;veis s&atilde;o indicados para todas as esta&ccedil;&otilde;es e climas</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Capa Dry Fresh: 100% Algod&atilde;o&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Dimens&atilde;o: 50x70 cm</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura: 16 cm</span></p>\r\n', '<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n', '2018-10-09 17:33:20', '2020-02-29 19:32:51'),
(85, 12, 7, 22, NULL, 14, 'jabaquara,gravi', 'molas-cervical', 'Molas Cervical', 'molas-cervical_20200229160838pvb9rs0ppB.jpg', 'Conforto Intermediário', '', 'Molas Encasuladas', 'MN2101', 2, 'FABRICANTE DUOFLEX', '<p><span style=\"font-size:14px;\">A ideia de utilizar molas em travesseiros sempre chamou a aten&ccedil;&atilde;o de especialistas, por propiciarem alta durabilidade, alta ventila&ccedil;&atilde;o e elasticidade controlada</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">O travesseiro de Molas Duoflex foi desenvolvido com o mais avan&ccedil;ado e moderno sistema de molas independentes do mundo, o Spring Case, que consiste numa colmeia de espuma macia onde se alojam as molas, de forma totalmente separada e independente</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Os diversos casulos permitem alta ventila&ccedil;&atilde;o, frescor e higiene devido ao bombeamento de ar causado pela movimenta&ccedil;&atilde;o durante o sono, promovendo uma maior circula&ccedil;&atilde;o de ar</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">As molas com formato barril podem sofrer flex&atilde;o lateral dentro de seus casulos, permitindo que os travesseiros sejam dobrados at&eacute; 90 graus, sem nenhuma deforma&ccedil;&atilde;o. O usu&aacute;rio n&atilde;o percebe as molas, j&aacute; que s&atilde;o lev&iacute;ssimas e seu conjunto &eacute; o que proporciona a sustenta&ccedil;&atilde;o da cabe&ccedil;a</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Os travesseiros de Molas Cervical&nbsp;permitem uma adapta&ccedil;&atilde;o personalizada &agrave; anatomia da pessoa, proporcionando apoio, conforto e maciez com total prote&ccedil;&atilde;o e aus&ecirc;ncia de ru&iacute;dos</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Espuma Poliuretano (Aerada) </span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Capa: 100% Algod&atilde;o&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Dimens&atilde;o: 50x70&nbsp;cm</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura: 14&nbsp;cm/11 cm/14 cm</span></p>\r\n', '<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n', '2018-10-09 17:45:30', '2020-02-29 19:08:39'),
(86, 11, 7, 22, NULL, 14, 'jabaquara,gravi', 'altura-regulavel', 'Altura Regulável', 'altura-regulavel_20200229160730kViNKpPtmp.jpg', 'Conforto Firme', '', 'Espuma Poliuretano', 'RE1103', 1, 'FABRICANTE DUOFLEX', '<p><span style=\"font-size:14px;\">Os vers&aacute;teis travesseiros Altura Regul&aacute;vel foram desenvolvidos para que o usu&aacute;rio possa regular e acertar a sua altura e maciez exatamente ao seu gosto e necessidade</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">O travesseiro possui 3 camadas internas e o usu&aacute;rio pode a qualquer tempo escolher entre 4 alturas, bastando remov&ecirc;-las ou adicion&aacute;-las, de maneira f&aacute;cil, r&aacute;pida, pr&aacute;tica e higi&ecirc;nica</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Tudo isso permite uma posi&ccedil;&atilde;o natural de descanso, trazendo apoio e conforto totalmente personalizados a anatomia de cada pessoa na posi&ccedil;&atilde;o escolhida para o seu sono</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">O design exclusivo e patenteado &eacute; constitu&iacute;do por: 01 travesseiro com bolso interno e acesso lateral<br />\r\n03 camadas internas individuais e remov&iacute;veis</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">A Duoflex disponibiliza o travesseiro Altura Regul&aacute;vel tamb&eacute;m nas espumas especiais Nasa e L&aacute;tex, produzidas com total prote&ccedil;&atilde;o anti&aacute;caros, fungos e bact&eacute;rias</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Espuma Poliuretano</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Capa: 100% Algod&atilde;o Percal 200 fios</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Dimens&atilde;o: 50x70 cm</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura: de 10 a 20 cm</span></p>\r\n', '<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n', '2018-10-09 18:04:22', '2020-02-29 19:07:30'),
(88, 9, 7, 22, NULL, 14, 'jabaquara,gravi', 'lavavel-alto', 'Lavável Alto', 'lavavel-alto_20200229160608mzY6bFtTpX.jpg', 'Conforto Firme', '', 'Espuma Poliuretano', 'LV3100', 1, 'FABRICANTE DUOFLEX', '<p><span style=\"font-size:14px;\">O travesseiro LAV&Aacute;VEL &eacute; composto por uma Espuma especial de Poliuretano, produzida com c&eacute;lulas abertas, que proporcionam maior frescor e ventila&ccedil;&atilde;o</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Sua estrutura aerada permite a lavagem e a secagem completa do travesseiro em m&aacute;quinas de uso dom&eacute;stico, garantindo um cheirinho de novo por muito mais tempo e uma maior vida &uacute;til, em compara&ccedil;&atilde;o com os travesseiros tradicionais. Para tanto, &eacute; importante fazer a higieniza&ccedil;&atilde;o a cada 6 meses seguindo rigorosamente as instru&ccedil;&otilde;es de lavagem</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Adicionalmente, a capa do LAV&Aacute;VEL &eacute; confeccionada em Dry Fresh, tecido 100% Algod&atilde;o de alta performance, que tamb&eacute;m favorece a evapora&ccedil;&atilde;o mais r&aacute;pida da umidade, al&eacute;m de apresentar um toque extra macio e refrescante</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Produzido com total prote&ccedil;&atilde;o anti&aacute;caros, fungos e bact&eacute;rias, o travesseiro LAV&Aacute;VEL garante um sono muito mais saud&aacute;vel</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Dimens&atilde;o: 50x70 cm</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura: 15 cm</span></p>\r\n', '<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n', '2018-10-09 22:17:03', '2020-02-29 19:06:08'),
(90, 7, 7, 22, NULL, 14, 'jabaquara,gravi', 'fresh-cervical', 'Fresh Cervical', 'fresh-cervical_202002291604053kYK7LyZ4N.jpg', 'Conforto Firme', '', 'Espuma Poliuretano', 'AT2100', 1, 'FABRICANTE DUOFLEX', '<p><span style=\"font-size:14px;\">Fresh &eacute; uma Espuma especial de Poliuretano expandido, tamb&eacute;m conhecida como sempre fria, hipersoft ou supersoft. &Eacute; uma espuma produzida com c&eacute;lulas abertas para proporcionar frescor, ventila&ccedil;&atilde;o, toque e maciez superiores &agrave;s espumas comuns, usadas nos colch&otilde;es, que s&atilde;o duras e &aacute;speras, com pouca aera&ccedil;&atilde;o. Fresh &eacute; especialmente indicada para pessoas que transpiram muito a noite</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">A capa do travesseiro Fresh &eacute; confeccionada com o nov&iacute;ssimo Dry Fresh, tecido de alta performance que propicia evapora&ccedil;&atilde;o mais r&aacute;pida e toque suave</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Dry Fresh &eacute; um conceito utilizado para definir o tecido 100% Algod&atilde;o que, devido &agrave; estrutura de fabrica&ccedil;&atilde;o, proporciona alta respirabilidade e troca t&eacute;rmica constante, combinados a um toque refrescante</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">&Eacute; a evolu&ccedil;&atilde;o dos tecidos especiais tipo &ldquo;DRY&rdquo;, com a grande vantagem de ser 100% Algod&atilde;o, produto natural, muito mais saud&aacute;vel, que previne os odores desagrad&aacute;veis e o calor gerados pelos tecidos sint&eacute;ticos</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">E como a maioria das espumas Duoflex, Fresh &eacute; produzida com prote&ccedil;&atilde;o total anti&aacute;caros, fungos e bact&eacute;rias</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Dimens&atilde;o: 50x70 cm</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura: 13 cm/10 cm/13 cm</span></p>\r\n', '<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n', '2018-10-09 22:39:05', '2020-02-29 19:04:06'),
(91, 6, 7, 28, NULL, 14, 'jabaquara,gravi', 'recoste-bem', 'Recoste Bem', 'recoste-bem_20181009194508UUvZiC6gD4.jpg', '', '', 'Espuma Poliuretano', 'AM0006', 1, 'FABRICANTE DUOFLEX', '<p><span style=\"font-size:14px;\">Especialmente desenvolvido para ser usado como apoio para a coluna e para as pernas, apresenta um lado plano e outro anat&ocirc;mico e gomos massageadores, que geram maior conforto.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Dimens&atilde;o: 38x38 cm</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura: 53 cm</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Capa: 100% Poli&eacute;ster com z&iacute;per&nbsp;</span></p>\r\n\r\n<p>&nbsp;</p>\r\n', '<p><span style=\"font-size:14px;\">Garantia: 03 meses</span></p>\r\n', '2018-10-09 22:45:08', '2018-10-26 23:28:26'),
(92, 5, 7, 24, NULL, 21, 'jabaquara,gravi', 'penas-de-ganso', 'Penas de Ganso', 'daune-travesseiros-100-penas_20181010124252CiZYVyMjO7.jpg', 'Conforto Macio', '', '100% Penas de Ganso ', '', 3, 'FABRICANTE DAUNE', '<p><span style=\"font-size:14px;\">Composi&ccedil;&atilde;o do&nbsp;revestimento: 100% Algod&atilde;o 233 fios</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Composi&ccedil;&atilde;o do enchimento: 100% Penas de Ganso</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Protegido contra &aacute;caros, fungos e bact&eacute;rias</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Dimens&atilde;o: 50x70 cm</span></p>\r\n', '<p><span style=\"font-size:14px;\">CUIDADOS</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Use capas protetoras de travesseiros DAUNE para ajudar a manter a vida &uacute;til do produto</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Exponha seu&nbsp;travesseiro de Pena de Ganso DAUNE ao sol antes de utiliz&aacute;-lo&nbsp;pela primeira vez e nunca guarde em lugar &uacute;mido. Para manter a maciez, lembre-se apenas de sacudir suavemente ap&oacute;s o uso di&aacute;rio</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Produtos de Penas e Plumas de Ganso DAUNE s&atilde;o muito resistentes e retornam &agrave; sua forma original mesmo depois de receber uma press&atilde;o equivalente a 80.000 vezes o seu peso</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Lavar o&nbsp;travesseiro, sempre que for necess&aacute;rio, em lavanderias especializadas</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Por fim, para aqueles que sofrem de alergias ou doen&ccedil;as respirat&oacute;rias, poder&atilde;o adquirir qualquer travesseiro DAUNE, pois todos os produtos possuem propriedades antibacterianas</span></p>\r\n', '2018-10-10 15:42:53', '2018-11-01 21:56:18'),
(93, 4, 7, 24, NULL, 21, 'jabaquara,gravi', 'plumas-de-ganso', 'Plumas de Ganso', 'daune-travesseiros-100-plumas_20181010124701Z2EHeCAi3t.jpg', 'Conforto Macio', '', '100% Pluma de Ganso ', '', 3, 'FABRICANTE DAUNE', '<p><span style=\"font-size:14px;\">Composi&ccedil;&atilde;o do revestimento: 100% Algod&atilde;o</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Composi&ccedil;&atilde;o do enchimento: 100% Plumas&nbsp;de Ganso</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Protegido contra &aacute;caros, fungos e bact&eacute;rias</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Dimens&atilde;o: 50x70 cm</span></p>\r\n', '<p><span style=\"font-size:14px;\">CUIDADOS</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Use capas protetoras de travesseiros DAUNE para ajudar a manter a vida &uacute;til do produto</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Exponha seu&nbsp;travesseiro de Pluma&nbsp;de Ganso DAUNE ao sol antes de utiliz&aacute;-lo&nbsp;pela primeira vez e nunca guarde em lugar &uacute;mido. Para manter a maciez, lembre-se apenas de sacudir suavemente ap&oacute;s o uso di&aacute;rio</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Produtos de Penas e Plumas de Ganso DAUNE s&atilde;o muito resistentes e retornam &agrave; sua forma original mesmo depois de receber uma press&atilde;o equivalente a 80.000 vezes o seu peso</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Lavar o&nbsp;travesseiro, sempre que for necess&aacute;rio, em lavanderias especializadas</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Por fim, para aqueles que sofrem de alergias ou doen&ccedil;as respirat&oacute;rias, poder&atilde;o adquirir qualquer travesseiro DAUNE, pois todos os produtos possuem propriedades antibacterianas</span></p>\r\n', '2018-10-10 15:47:01', '2018-11-01 21:55:57'),
(94, 0, 3, 9, NULL, 6, 'jabaquara,gravi', 'cabeceira-mh-2691', 'Cabeceira MH 2691', '2691_20181011123220gh4ZT1dqEB.jpg', 'Decoração', 'Sem mini frame ', 'LINHA ART ', '', 0, 'FABRICANTE HERVAL', '<p><span style=\"font-size:14px;\">Estrutura de madeira de eucalipto de reflorestamento e chapas MDF, fixos com cola branca (pr&oacute;pria para madeira natural) e com grampos de a&ccedil;o.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Painel com espuma D23 e manta de Espuma Eco Soft. Todas as espumas s&atilde;o de fabrica&ccedil;&atilde;o pr&oacute;pria e ecologicamente corretas.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Revestimentos:&nbsp;Cartela de tecido Herval, com composi&ccedil;&atilde;o afixada em etiqueta que acompanha o produto. Restringem-se Couros, sint&eacute;ticos e listrados</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Dimens&otilde;es:&nbsp;Casal 138: 172x125x24&nbsp;cm</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Queen 158: 192x125x24&nbsp;cm</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">King 193: 227x125x24&nbsp;cm</span></p>\r\n', '<p><span style=\"font-size:14px;\">Outras op&ccedil;&otilde;es de revestimentos: CONSULTAR VALORES&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia de 06 meses</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n', '2018-10-10 16:50:24', '2018-11-14 17:33:51'),
(95, 0, 3, 9, NULL, 6, 'jabaquara,gravi', 'cabeceira-mh-2695', 'Cabeceira MH 2695', 'cabeceira-2695_20181011123253UnmGOfVuh3.jpg', 'Decoração', 'Sem mini frame ', 'LINHA VIVERE', '', 0, '', '<p><span style=\"font-size:14px;\"><strong>CABECEIRA MH&nbsp;2695</strong></span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Madeira de Eucalipto de reflorestamento. O esqueleto &eacute; montado com grampos de a&ccedil;o e fixo com cola PVA (pr&oacute;pria para madeira), proporcionando assim grande resist&ecirc;ncia e evitando rangidos no produto.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Estrutura revestida por Espuma Soft e manta de Espuma Ecosoft. Todas as espumas s&atilde;o de fabrica&ccedil;&atilde;o pr&oacute;pria e ecologicamente corretas.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Sapata pl&aacute;stica</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Revestimentos: Cartela de Tecidos Herval, exceto Couro. Composi&ccedil;&atilde;o afixada em etiqueta que acompanha o produto</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Dimens&otilde;es:&nbsp;Casal 138: 160x130x23&nbsp;cm</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Queen 158: 180x130x23&nbsp;cm</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">King 193: 215x130x23&nbsp;cm</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\"><strong>CAMA MH&nbsp;2696</strong></span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Madeira de Eucalipto de reflorestamento. O esqueleto &eacute; montado com grampos de a&ccedil;o e fixo com cola PVA (pr&oacute;pria para madeira), proporcionando assim grande resist&ecirc;ncia e evitando rangidos no produto.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Estrutura revestida por espuma de m&eacute;dia densidade. Todas as espumas s&atilde;o de fabrica&ccedil;&atilde;o pr&oacute;pria e ecologicamente corretas.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">P&eacute;s de madeira maci&ccedil;a</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Revestimentos: Toda Cartela Herval de Tecidos. Composi&ccedil;&atilde;o afixada em etiqueta que acompanha o produto</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Dimens&otilde;es:&nbsp;Casal 138: 150x197x38&nbsp;cm</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Queen 158: 170x207x38&nbsp;cm</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">King 193: 210x212x38&nbsp;cm</span></p>\r\n', '<p><span style=\"font-size:14px;\">Outras op&ccedil;&otilde;es de revestimento: CONSULTAR VALORES&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia de 06 meses</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n\r\n<p>&nbsp;</p>\r\n', '2018-10-10 17:01:59', '2018-11-14 17:33:43'),
(96, 5, 2, 13, NULL, 23, 'jabaquara,gravi', 'box-simples-solteiro', 'Box Simples Solteiro', '300735644-bau-af-branco-fechado-solteiro-002_20181016184605CJYfAXWx1I.jpg', 'Box com altura de 25 cm', '', 'Estrutura em Madeira Eucalipto', '', 0, 'FABRICANTE MGA', '<p><span style=\"font-size:14px;\">A linha de&nbsp;Box Simples (base para colch&atilde;o)&nbsp;tem por objetivo oferecer solu&ccedil;&otilde;es variadas para diferentes alturas de colch&otilde;es, afim de atender as necessidades e prefer&ecirc;ncias do cliente.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Estrutura em Madeira Eucalipto</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura do Box: 25 cm &ndash; Altura dos p&eacute;s: 12 cm &ndash; Altura total: 37 cm</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\"><strong>PRE&Ccedil;O REFERENTE AO CORANO, OUTROS REVESTIMENTOS CONSULTAR VALORES<br />\r\n(OP&Ccedil;&Atilde;O: TECIDO POLI&Eacute;STER, CHENILLE E CAMUR&Ccedil;A)</strong></span></p>\r\n', '<p><span style=\"font-size:14px;\"><strong>Existem modelos de 12, 19 e 30 cm de altura tamb&eacute;m. Para mais informa&ccedil;&otilde;es, consultar&nbsp;valores.</strong></span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n', '2018-10-10 20:47:57', '2018-10-26 20:05:18'),
(97, 8, 2, 20, NULL, 23, 'jabaquara,gravi', 'bicama-molejo-versatil-olympia', 'Bicama Molejo Versátil Olympia ', 'olympia-suspenso-002_20181010180824Fw99u33GF0.png', 'Linha Hotelaria', '', 'Colchão de Molejo Verticoil ', '', 2, 'FABRICANTE MGA', '<p><span style=\"font-size:14px;\">Bicama Molejo Vers&aacute;til, alinha a cama auxiliar com o colch&atilde;o, trazendo maior espa&ccedil;o e conforto para quem dorme&nbsp;</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Especifica&ccedil;&otilde;es do colch&atilde;o Olympia: Molejo Verticoil 323 2.0 (129 molas p/m&sup2;)</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Conforto D75</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Double Side</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Tecido Jacquard by Dohler</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">21 cm de altura</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">(Esse colch&atilde;o faz parte do conjunto da Bicama Molejo Vers&aacute;til)</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Estrutura em Madeira Eucalipto</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura do Box: 30 cm &ndash; Altura dos p&eacute;s: 12 cm &ndash; Altura da cama auxiliar: 19 cm&nbsp;</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Medida do Box: 078x188&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Medida da cama auxiliar: 070x171 cm</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Medida do Box: 088x188</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Medida da cama auxiliar: 078x171 cm</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Medida do Box: 096x203&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Medida da cama auxiliar: 088x188 cm</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Peso m&aacute;ximo recomendado por pessoa: 120kg</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\"><strong>PRE&Ccedil;O REFERENTE AO TECIDO FANTCZ</strong></span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Registro Inmetro&nbsp;005696/2017&nbsp;</span></p>\r\n', '<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Consulte valores para medidas especiais&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n', '2018-10-10 21:08:30', '2018-10-26 20:04:04'),
(98, 4, 2, 13, NULL, 23, 'jabaquara,gravi', 'box-simples-casal', 'Box Simples Casal', '300735644-bau-af-branco-fechado-002_20181016183333jVKw9bgQFn.jpg', 'Box com altura de 25 cm', '', 'Estrutura em Madeira Eucalipto', '', 0, 'FABRICANTE MGA', '<p><span style=\"font-size:14px;\">A linha de Box Simples (base para colch&atilde;o) tem por objetivo oferecer solu&ccedil;&otilde;es variadas para diferentes alturas de colch&otilde;es, afim de atender as necessidades e prefer&ecirc;ncias do cliente.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Estrutura em Madeira Eucalipto</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura do Box: 25 cm &ndash;&nbsp;Altura dos p&eacute;s: 12 cm&nbsp;&ndash;&nbsp;Altura total: 37 cm</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\"><strong>PRE&Ccedil;O REFERENTE AO CORANO, OUTROS REVESTIMENTOS CONSULTAR VALORES<br />\r\n(OP&Ccedil;&Atilde;O: TECIDO POLI&Eacute;STER, CHENILLE E CAMUR&Ccedil;A)</strong></span></p>\r\n', '<p><span style=\"font-size:14px;\"><strong>Existem modelos de 12, 19 e 30 cm de altura tamb&eacute;m. Para mais informa&ccedil;&otilde;es, consultar&nbsp;valores.</strong></span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n', '2018-10-16 21:33:36', '2018-11-01 16:44:55'),
(99, 3, 2, 13, NULL, 23, 'jabaquara,gravi', 'box-simples-queen-king', 'Box Simples Queen/King', 'box-bipartido-queen-branco-002_20181016184546hF9qB7YUY8.jpg', 'Box com altura de 25 cm', '', 'Estrutura em Madeira Eucalipto', '', 0, 'FABRICANTE MGA', '<p><span style=\"font-size:14px;\">A linha de Box Simples (base para colch&atilde;o) tem por objetivo oferecer solu&ccedil;&otilde;es variadas para diferentes alturas de colch&otilde;es, afim de atender as necessidades e prefer&ecirc;ncias do cliente.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Nas medidas Queen e King o Box vir&aacute; composto por dois m&oacute;dulos, com a fun&ccedil;&atilde;o de facilitar o manuseio e o transporte, al&eacute;m de garantir maior resist&ecirc;ncia e suporte de peso ao conjunto.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Duas pe&ccedil;as de 079x198 cm na medida Queen e duas pe&ccedil;as de 096x203 cm na medida King.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Estrutura em Madeira Eucalipto</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura do Box: 25 cm &ndash; Altura dos p&eacute;s: 12 cm &ndash;&nbsp;Altura total: 37 cm</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\"><strong>PRE&Ccedil;O REFERENTE AO CORANO, OUTROS REVESTIMENTOS CONSULTAR VALORES<br />\r\n(OP&Ccedil;&Atilde;O: TECIDO POLI&Eacute;STER, CHENILLE E CAMUR&Ccedil;A)</strong></span></p>\r\n', '<p><strong><span style=\"font-size:14px;\">Existem modelos de 12, 19 e 30 cm de altura tamb&eacute;m. Para mais informa&ccedil;&otilde;es, consultar&nbsp;valores.</span></strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n', '2018-10-16 21:38:51', '2018-10-26 20:05:53');
INSERT INTO `produtos` (`id`, `ordem`, `secao_id`, `categoria_id`, `subcategoria_id`, `marca_id`, `lojas`, `slug`, `titulo`, `capa`, `material`, `linha`, `legenda`, `codigo`, `conforto`, `resumo`, `descricao`, `dados_complementares`, `created_at`, `updated_at`) VALUES
(100, 1, 2, 14, NULL, 23, 'jabaquara,gravi', 'box-bau-cama-auxiliar-de-espuma', 'Box Baú + Cama Auxiliar de Espuma', '3x1_20181019122723QWxnONBFRA.jpg', 'Box 3x1', '', 'Estrutura em Madeira Eucalipto', '', 1, 'FABRICANTE MGA', '<p><span style=\"font-size:14px;\">O Box 3 em 1 da MGA &eacute; o produto ideal para quem procura total aproveitamento de espa&ccedil;o. Al&eacute;m de ser uma cama Box Solteiro ele possiu um B&aacute;u e uma cama auxiliar.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Estrutura em Madeira Eucalipto<br />\r\nAcionamento por Pist&atilde;o Nakata<br />\r\nAbertura lateral<br />\r\nAcabamento interno do Ba&uacute; com chapa de Eucatex</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">O colch&atilde;o da bicama &eacute; de Espuma D28</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura do Box: 34 cm &ndash; Altura dos p&eacute;s: 6,5 cm &ndash; Altura &uacute;til: 12 cm &ndash; Altura da cama auxiliar: 10 cm</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Suporte de peso da bicama &eacute; de 80kg</span><br />\r\n&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\"><strong>PRE&Ccedil;O REFERENTE AO TECIDO SARA BEGE, OUTROS REVESTIMENTOS CONSULTAR VALORES<br />\r\n(OP&Ccedil;&Atilde;O: TECIDO POLI&Eacute;STER, CORANO, CHENILLE E CAMUR&Ccedil;A)</strong></span></p>\r\n', '<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Consulte valores para medidas especiais&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n', '2018-10-19 15:27:24', '2018-10-26 20:07:16'),
(102, 28, 1, 1, NULL, 7, 'jabaquara,gravi', 'impact', 'Impact®', '895_20181019181821haqUtJje0f.jpg', 'Conforto Firme', 'Espessura 32 cm', 'Molas ensacadas 2X ACTION', '', 1, 'FABRICANTE SANKONFORT', '<p><span style=\"font-size:14px;\">Colch&atilde;o com com tecido malha (80% Poli&eacute;ster e 20% Viscose de Bambu) exclusivo, que proporciona toque suave e extremamente firme com uma camada extra de conforto.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Molas 2X ACTION 2.0: Molejo com duplo sistema e maior contagem de molas por m&sup2; oferecendo mais conforto e tecnologia avan&ccedil;ada. Segue o alto padr&atilde;o de exel&ecirc;ncia em fabrica&ccedil;&atilde;o, todo o processo &eacute; minunciosamente estudado e planejado para proporcionar as melhores solu&ccedil;&otilde;es para o descanso e qualidade do sono.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Est&aacute;gio de Individualidade tecnologia inovadora onde as Molas s&atilde;o Ensacadas uma a uma e trabalham Individualmente permitindo que os casais mesmo com pesos muito diferentes sejam suportados de maneira exclusiva.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n', '<p><span style=\"font-size:14px;\">Colch&atilde;o com 32 cm de altura</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Suporte de peso de at&eacute; 150kg por pessoa&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Consulte valores para medidas especiais&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n', '2018-10-19 21:18:22', '2018-10-25 20:26:53'),
(103, 19, 1, 1, NULL, 7, 'jabaquara,gravi', 'emotion', 'Emotion®', 'emotion-baixa_20181019185633dQ35iB0dbV.jpg', 'Conforto Intermediário', 'Espessura 25 cm', 'Molas ensacadas 2X ACTION', '', 2, 'FABRICANTE SANKONFORT', '<p><span style=\"font-size:14px;\">Colch&atilde;o com com tecido malha (77% Poli&eacute;ster e 23% Viscose de Bambu) exclusivo, que proporciona toque suave e extremamente firme com uma camada extra de conforto.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Molas 2X ACTION 2.0: Molejo com duplo sistema e maior contagem de molas por m&sup2; oferecendo mais conforto e tecnologia avan&ccedil;ada. Segue o alto padr&atilde;o de exel&ecirc;ncia em fabrica&ccedil;&atilde;o, todo o processo &eacute; minunciosamente estudado e planejado para proporcionar as melhores solu&ccedil;&otilde;es para o descanso e qualidade do sono.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Est&aacute;gio de Individualidade tecnologia inovadora onde as Molas s&atilde;o Ensacadas uma a uma e trabalham Individualmente permitindo que os casais mesmo com pesos muito diferentes sejam suportados de maneira exclusiva.</span></p>\r\n', '<p><span style=\"font-size:14px;\">Colch&atilde;o com 25&nbsp;cm de altura</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Suporte de peso de at&eacute; 120kg por pessoa&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Consulte valores para medidas especiais&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n', '2018-10-19 21:56:34', '2018-10-25 20:27:41'),
(107, 27, 1, 3, NULL, 7, 'jabaquara,gravi', 'toulon-d28', 'Toulon D28®', 'toulon-d28_20181023182241pEolsdEBjp.jpg', 'Conforto Firme', 'Espessura 14 e 17 cm', 'Espuma D28', '', 1, 'FABRICANTE SANKONFORT&nbsp;', '<p><span style=\"font-size:14px;\">O colch&atilde;o Toulon possiu uma camada de espuma de alt&iacute;ssima qualidade em sua configura&ccedil;&atilde;o. Espuma de Poliuretano desenvolvida pela SANKO Espumas que garante a sua qualidade atrav&eacute;s de testes em seu&nbsp;laborat&oacute;rio, seguindo a norma da ABNT.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Tecido&nbsp;malha (80% Poli&eacute;ster e 20% Algod&atilde;o)</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Dupla Face: Pode ser usado dos dois lados</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size: 14px;\">Alturas: 14 ou 17 cm&nbsp;</span></p>\r\n', '<p><span style=\"font-size:14px;\">Suporte de peso de at&eacute; 90kg por pessoa&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Consulte valores para medidas especiais&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n', '2018-10-23 21:22:47', '2018-11-01 20:26:18'),
(108, 26, 1, 3, NULL, 7, 'jabaquara,gravi', 'toulon-d33', 'Toulon D33®', 'toulon-d33_20181023185652n3Ms9LfLdp.jpg', 'Conforto Firme', 'Espessura 14 e 17 cm', 'Espuma D33', '', 1, 'FABRICANTE SANKONFORT&nbsp;', '<p><span style=\"font-size:14px;\">O colch&atilde;o Toulon possiu uma camada de espuma de alt&iacute;ssima qualidade em sua configura&ccedil;&atilde;o. Espuma de Poliuretano desenvolvida pela SANKO Espumas que garante a sua qualidade atrav&eacute;s de testes em seu laborat&oacute;rio, seguindo a norma da ABNT.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Tecido malha (79% Poli&eacute;ster e 21% Algod&atilde;o)</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Dupla Face: Pode ser usado dos dois lados</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Alturas: 14 ou 17 cm&nbsp;</span></p>\r\n', '<p><span style=\"font-size:14px;\">Suporte de peso de at&eacute; 110kg por pessoa&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Consulte valores para medidas especiais&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n', '2018-10-23 21:56:58', '2018-11-01 20:25:40'),
(109, 25, 1, 3, NULL, 7, 'jabaquara,gravi', 'toulon-d20', 'Toulon D20®', 'toulon-d20-baixa_20181023200802W8eJxQBlxf.jpg', 'Conforto Firme', 'Espessura 12 cm', 'Espuma D20', '', 1, 'FABRICANTE SANKONFORT', '<p><span style=\"font-size:14px;\">O colch&atilde;o Toulon possiu uma camada de espuma de alt&iacute;ssima qualidade em sua configura&ccedil;&atilde;o. Espuma de Poliuretano desenvolvida pela SANKO Espumas que garante a sua qualidade atrav&eacute;s de testes em seu laborat&oacute;rio, seguindo a norma da ABNT.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Tecido malha (79% Poli&eacute;ster e 21% Algod&atilde;o)</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Dupla Face: Pode ser usado dos dois lados</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Alturas: 12 cm</span></p>\r\n', '<p><span style=\"font-size:14px;\">Suporte de peso de at&eacute; 60kg por pessoa&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n', '2018-10-23 23:08:03', '2018-11-01 20:24:35'),
(110, 24, 1, 3, NULL, 7, 'jabaquara,gravi', 'mercure-d33', 'Mercure D33®', 'mercure_20181024163403iQuGk8pmp4.jpg', 'Conforto Firme', 'Espessura 20 e 25 cm', 'Espuma D33', '', 1, 'FABRICANTE SANKONFORT&nbsp;', '<p><span style=\"font-size:14px;\">O colch&atilde;o Mercure&nbsp;possiu uma camada de espuma de alt&iacute;ssima qualidade em sua configura&ccedil;&atilde;o. Espuma de Poliuretano desenvolvida pela SANKO Espumas que garante a sua qualidade atrav&eacute;s de testes em seu laborat&oacute;rio, seguindo a norma da ABNT.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Tecido: 40% Algod&atilde;o, 50% Polipropileno e 10% Poli&eacute;ster</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Dupla Face: Pode ser usado dos dois lados</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Alturas: 20&nbsp;ou 25 cm&nbsp;</span></p>\r\n', '<p><span style=\"font-size:14px;\">Suporte de peso de at&eacute; 120kg por pessoa&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Consulte valores para medidas especiais&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n', '2018-10-24 19:34:09', '2018-11-01 20:23:55'),
(111, 23, 1, 3, NULL, 7, 'jabaquara,gravi', 'bordeaux-d45', 'Bordeaux D45®', 'bordeaux_20181024171921dw5HgycEip.jpg', 'Conforto Firme', 'Espessura 20 cm', 'Espuma D45', '', 1, 'FABRICANTE SANKONFORT&nbsp;', '<p><span style=\"font-size:14px;\">O colch&atilde;o Bordeaux&nbsp;possiu uma camada de espuma de alt&iacute;ssima qualidade em sua configura&ccedil;&atilde;o. Espuma de Poliuretano desenvolvida pela SANKO Espumas que garante a sua qualidade atrav&eacute;s de testes em seu laborat&oacute;rio, seguindo a norma da ABNT.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Tecido: 40% Algod&atilde;o, 50% Polipropileno e 10% Poli&eacute;ster</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Dupla Face: Pode ser usado dos dois lados</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura: 20 cm</span></p>\r\n', '<p><span style=\"font-size:14px;\">Suporte de peso de at&eacute; 150kg por pessoa&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Consulte valores para medidas especiais&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n', '2018-10-24 20:19:28', '2018-11-01 20:23:11'),
(112, 22, 1, 11, NULL, 7, 'jabaquara,gravi', 'orthopedic-plus', 'Orthopedic Plus®', 'orthopedic-plus-60_201810241810162ruy9XaswC.jpg', 'Conforto Intermediário', 'Espessura 18 e 25 cm', 'Espuma D26/AG80/D26 = 120 kg', '', 2, 'FABRICANTE SANKONFORT&nbsp;', '<p><span style=\"font-size:14px;\">O colch&atilde;o Orthopedic Plus&nbsp;possiu uma camada de espuma de alt&iacute;ssima qualidade em sua configura&ccedil;&atilde;o. Espuma de Poliuretano desenvolvida pela SANKO Espumas que garante a sua qualidade atrav&eacute;s de testes em seu laborat&oacute;rio, seguindo a norma da ABNT.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Tecido: 49% Algod&atilde;o, 42% Polipropileno e 5% Poli&eacute;ster</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Dupla face: Pode ser usado dos dois lados</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Alturas: 18 e 25&nbsp;cm</span></p>\r\n', '<p><span style=\"font-size:14px;\">Suporte de peso de at&eacute; 120kg por pessoa&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Consulte valores para medidas especiais&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n', '2018-10-24 21:10:22', '2019-05-03 00:52:29'),
(113, 21, 1, 11, NULL, 7, 'jabaquara,gravi', 'protege', 'Protege®', 'protege-03_20181024183446N7IbKvZ5hC.jpg', 'Conforto Firme', 'Espessura 22 cm', 'Espuma D45', '', 1, 'FABRICANTE SANKONFORT&nbsp;', '<p><span style=\"font-size:14px;\">Elaborado para pessoas de at&eacute; 180kg, o colch&atilde;o de Espumas Dupla Face tem um lado firme e outro extra firme, altamente adapt&aacute;vel garantindo um excelente conforto e proporcionando agrad&aacute;vel sensa&ccedil;&atilde;o de relaxamento. Protege, a mais recente inova&ccedil;&atilde;o t&eacute;cnica para seu conforto. Durabilidade e sustenta&ccedil;&atilde;o adequados ao seu biotipo.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Tecido: 50% Algod&atilde;o, 40% Polipropileno e 10% Poli&eacute;ster</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura: 22 cm</span></p>\r\n', '<p><span style=\"font-size:14px;\">Suporte de peso de at&eacute; 180kg por pessoa&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Consulte valores para medidas especiais&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n', '2018-10-24 21:34:53', '2018-11-01 19:44:44'),
(115, 0, 5, 30, NULL, 7, 'jabaquara,gravi', 'articulado-solteiro-e-casal', 'Articulado Solteiro e Casal®', 'articulado-01-2_20181024194707yBl4YzN7cc.jpg', 'Conforto Intermediário', '', 'Espuma D23', '', 2, 'FABRICANTE SANKONFORT&nbsp;', '<p><span style=\"font-size:14px;\">O Puff Articulado garante conforto com a seguran&ccedil;a que voc&ecirc; precisa.&nbsp;</span><span style=\"font-size:14px;\">Possui Espuma flex&iacute;vel de Poliuretano com desempenho de D23 kg/m&sup3;.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Medidas: Solteiro: 65x80x40 cm&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Casal: 120x80x40 cm</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura aberto: 20 cm</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura fechado: 40 cm</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Cores: Marrom e Cinza&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Revestimento: 56% Olefenica e 44% Poli&eacute;ster</span></p>\r\n', '<p><span style=\"font-size:14px;\">Garantia: 03 meses</span></p>\r\n', '2018-10-24 22:47:11', '2018-11-09 20:37:25'),
(116, 0, 5, 30, NULL, 7, 'jabaquara,gravi', 'multiuso-dobravel-solteiro-e-casal', 'Multiuso Dobrável Solteiro e Casal®', 'cp-1815-pg110-prancha-eleganza-chapinha-tentativa-rosa-02-7_20181024200349DZbcPhz6qI.jpg', 'Conforto Intermediário', '', 'Espuma D20', '', 2, '', '<p><span style=\"font-size:14px;\">O Multiuso Dobr&aacute;vel&nbsp;&eacute; perfeito para quem quer praticidade e conforto.&nbsp;Espuma flex&iacute;vel de Poliuretano com desempenho de D20&nbsp;kg/m&sup3;.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Solteiro: Acompanha 01 almofada</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Casal: Acompanha 02 almofadas</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Medidas: Solteiro: 63x63x36 cm</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Casal: 120x63x36 cm&nbsp;</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura aberto: 12 cm</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura fechado: 36&nbsp;cm</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Revestimento: 56% Olefinica e 44% Poli&eacute;ster</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Cores: Marrom e Cinza</span></p>\r\n', '<p><span style=\"font-size:14px;\">Garantia: 03 meses</span></p>\r\n', '2018-10-24 23:03:50', '2018-11-09 20:37:41'),
(117, 0, 5, 31, NULL, 7, 'jabaquara,gravi', 'descanso-aerobica', 'Descanso Aeróbica', 'colchonete-aerobica_20181024201253hn0SuQbETP.jpg', '', '', 'Espuma', '', 0, 'FABRICANTE SANKONFORT&nbsp;', '<p><span style=\"font-size:14px;\">Tenha mais seguran&ccedil;a em suas atividades f&iacute;sicas com Descanso Aer&oacute;bica. Possui revestimento de Corino Preto, com micro Espumas Compactadas MEC 80&nbsp;que proporcionam mais firmeza e durabilidade ao produto.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Medidas: 65x100x1,5 cm<br />\r\n62x135x3&nbsp;cm</span></p>\r\n', '<p><span style=\"font-size:14px;\">Garantia: 03 meses</span></p>\r\n', '2018-10-24 23:12:54', '2018-11-01 16:49:45'),
(118, 0, 5, 31, NULL, 7, 'jabaquara,gravi', 'descanso-camping', 'Descanso Camping ', 'colchonete-camping-fechado_201810242019419JUw7OToBy.jpg', 'Conforto Intermediário', '', '100% Polipropileno', '', 2, 'FABRICANTE SANKONFORT', '<p><span style=\"font-size:14px;\">Seus momentos de lazer ser&atilde;o &uacute;nicos&nbsp;com o colch&atilde;o Descanso Camping.&nbsp;A Sankonfort Colch&otilde;es est&aacute; sempre preocupada em oferecer ao consumidor o que h&aacute; de melhor em qualidade e tecnologia mundial, por isso &eacute; a primeira ind&uacute;stria brasileira do setor a obter produtos aprovados pelo sistema INMETRO.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Medidas: 65x180x3&nbsp;cm<br />\r\nRevestimento: 100% Polipropileno</span></p>\r\n', '<p><span style=\"font-size:14px;\">Garantia: 03 meses</span></p>\r\n', '2018-10-24 23:19:42', '2018-10-26 22:57:40'),
(119, 3, 7, 32, NULL, 7, 'jabaquara,gravi', 'espuma-anti-escaras', 'Espuma Anti-Escaras', 'anti-escaras_20181024202703GQj0fuhsPh.jpg', '', '', 'Espuma D20', '', 0, 'FABRICANTE SANKONFORT', '<p><span style=\"font-size:14px;\">Com a superf&iacute;cie perfilada que proporciona conforto e auxilia na preven&ccedil;&atilde;o das escaras provenientes de pessoas que passam muito tempo acamadas.</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Cama de espuma ondulada que permite melhor circula&ccedil;&atilde;o de ar entre o corpo e o colch&atilde;o. Colchonete&nbsp;perfilado&nbsp;com tecnologia D20 kg/m&sup3;.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Medidas: Solteiro: 85x185x4,0 cm</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Casal: 135x185x4,0 cm</span></p>\r\n', '<p><span style=\"font-size:14px;\">Garantia: 03 meses</span></p>\r\n', '2018-10-24 23:27:04', '2018-11-01 21:50:34'),
(120, 2, 7, 28, NULL, 7, 'jabaquara,gravi', 'tri-relax', 'Tri-Relax®', 'tri-relax_20181024203205QDaNHdT7hG.png', '', '', 'Espuma Poliuretano', '', 0, 'FABRICANTE SANKONFORT', '<p><span style=\"font-size:14px;\">Produto revolucion&aacute;rio, adequado para proporcionar conforto e relaxamento. Ideal para ver televis&atilde;o, leitura, eleva&ccedil;&atilde;o do tronco, cabe&ccedil;a, pesco&ccedil;o, joelhos e pernas, al&eacute;m de&nbsp;ajuste de altura para proporcionar melhor qualidade do sono e descanso.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Dimens&atilde;o: 60x92&nbsp;cm</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura: 16 cm</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Composi&ccedil;&atilde;o: Algod&atilde;o e Poli&eacute;ster</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Espuma: 100% Poliuretano</span></p>\r\n', '<p><span style=\"font-size:14px;\">Garantia: 03 meses</span></p>\r\n\r\n<p>&nbsp;</p>\r\n', '2018-10-24 23:32:06', '2018-10-26 23:27:58'),
(122, 42, 1, 1, NULL, 15, 'jabaquara,gravi', 'las-vegas-boulevard', 'Las Vegas Boulevard ', '711-las-vegas-2_20181025165023RRHGbXmBag.jpg', 'Conforto Macio', 'Espessura 32 cm', 'Molejo Comfort Core Dual Action™', '', 3, 'FABRICANTE EASTMAN HOUSE&nbsp;', '<p><span style=\"font-size:14px;\">A tecnologia de Molejo Dual Action&trade; &eacute; composta por Molas Ensacadas Individualmente com 18 cm de altura, e com um grupo de molas 48% mais r&iacute;gidas no centro do colch&atilde;o, na regi&atilde;o compat&iacute;vel com o quadril, onde se concentram at&eacute; 46% do peso corporal.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Air Flow: Feito com tecido especial que permite a circula&ccedil;&atilde;o do ar na zona de conforto do colch&atilde;o, criando um microclima mais fresco e agrad&aacute;vel, evitando a prolifera&ccedil;&atilde;o de bact&eacute;rias.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Matelass&ecirc;: Feito em Malha com 420 gr/m&sup2; com fibras naturais de Tencel, um produto ecol&oacute;gico 30% mais resistente que o Algod&atilde;o, com com toque suave e diferenciado das malhas convencionais. Al&eacute;m disso, seu Matelass&ecirc; &eacute; composto por 100% de Espuma HR (High Resilience) de alta resili&ecirc;ncia proporcionando uma resposta r&aacute;pida aos movimentos do corpo.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Espuma HR (High Resilience): Possui estrutura celular diferente de espumas convencionais, resultando numa grande capacidade de recupera&ccedil;&atilde;o ao seu estado original ap&oacute;s ter sido submetida a uma carga de esfor&ccedil;o. Essa propriedade ajuda na circula&ccedil;&atilde;o sangu&iacute;nea, preven&ccedil;&atilde;o de dores musculares permitindo maior conforto durante o sono.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Edge&trade;: A borda Edge&trade; proporciona estabilidade e maior suporte para quem usa as extremidades do colch&atilde;o. A tecnologia consiste em um grupo de Molas Ensacadas Individualmente, tratadas com duplo revenimento, resultando em uma borda consistente, dur&aacute;vel e com alta resili&ecirc;ncia.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Registro Inmetro&nbsp;005691/2017</span></p>\r\n', '<p><span style=\"font-size:14px;\">Colch&atilde;o com 32&nbsp;cm de altura&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Suporte de peso de at&eacute; 120kg por pessoa</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Consulte valores para medidas especiais</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n', '2018-10-25 19:50:24', '2018-11-16 16:52:56'),
(123, 55, 1, 1, NULL, 23, 'jabaquara,gravi', 'sollievo-cotone', 'Sollievo Cotone ', 'cottone_20181026163852yLchSphkCs.png', 'Conforto Intermediário', 'Espessura 34 cm', 'Molejo Comfort Core Dual Action™', '', 2, 'FABRICANTE MGA', '<p><span style=\"font-size:14px;\">Molejo Comfort Core Dual Action&trade; 532 2.0 (215 molas por m&sup2;)<br />\r\nVisco El&aacute;stico de 6 cm<br />\r\nEuro-Pillow<br />\r\nBorda Edge&trade;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Tecido em malha com&nbsp;fibras de Algod&atilde;o org&acirc;nico</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Air Flow<br />\r\nConforto D33</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Registo Inmetro 005691/2017</span></p>\r\n', '<p><span style=\"font-size:14px;\">Colch&atilde;o com 34 cm de altura&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura do conjunto: 71 cm&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Suporte de peso de at&eacute; 120kg por pessoa</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Consulte valores para medidas especiais</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n', '2018-10-26 19:38:54', '2018-10-26 19:38:54'),
(124, 1, 7, 28, NULL, 7, 'jabaquara,gravi', 'descanso-triangular', 'Descanso Triangular®', 'descanso-triangular_20181026202004hBpbUen1k5.jpg', '', '', 'Espuma Poliuretano', '', 0, 'FABRICANTE SANKONFORT&nbsp;', '<p><span style=\"font-size:14px;\">Descanso Triangular &eacute; garantia de repouso tranquilo. Possui manta perfilada e inclina&ccedil;&atilde;o ideal para amamenta&ccedil;&atilde;o e leitura.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Dimens&atilde;o: 35x55 cm</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura: 45 cm<br />\r\nTecido: 70% Algod&atilde;o e 30% Poli&eacute;ster<br />\r\nCor: Azul&nbsp;<br />\r\nEspuma: 100% Poliuretano<br />\r\nDensidade: 14 kg/m&sup3;</span></p>\r\n', '<p><span style=\"font-size:14px;\">Garantia: 03 meses</span></p>\r\n', '2018-10-26 23:19:07', '2018-10-26 23:23:46'),
(125, 0, 3, 9, NULL, 27, 'jabaquara,gravi', 'cabeceira-alvorada', 'Cabeceira Alvorada', 'alvorada-casal_20181029182939BPw9VNsJuV.jpg', 'Decoração', 'Com mini frame ', 'Estofada ', '', 0, 'FABRICANTE MB M&Oacute;VEIS&nbsp;', '<p><span style=\"font-size:14px;\">Modelo dispon&iacute;vel em Cabeceira e Painel</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Altura da Cabeceira: 125 cm<br />\r\nAltura do Painel: 70 cm<br />\r\nEspessura: 10 cm</span><br />\r\n&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Tecidos: Corino, Couro Ecol&oacute;gico, Suede Liso, Suede Amassado, Facto, Floral, Dallas</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong><span style=\"font-size:14px;\">PRE&Ccedil;O REFENTE AO CORINO, PARA OUTROS ACABAMENTOS, CONSULTAR VALORES</span></strong></p>\r\n', '<p><span style=\"font-size:14px;\">Consultar valores para medidas especiais&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 03 meses</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">CUIDADOS:&nbsp;</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Utilizar sab&atilde;o com esponja umedecida, remover excesso com pano &uacute;mido</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Secagem em ambientes arejados e na sombra<br />\r\nN&atilde;o utilizar produtos qu&iacute;micos nas tonalidades claras, evitar contato com tecidos ou materiais que soltem tinta</span></p>\r\n', '2018-10-29 21:21:39', '2018-11-14 17:33:27'),
(126, 0, 3, 9, NULL, 27, 'jabaquara,gravi', 'cabeceira-atenas', 'Cabeceira Atenas', 'atenas-casal_20181029183218YyMtNhPnQ6.jpg', 'Decoração', 'Com mini frame ', 'Estofada ', '', 0, 'FABRICANTE MB M&Oacute;VEIS&nbsp;', '<p><span style=\"font-size:14px;\">Modelo dispon&iacute;vel em Cabeceira e Painel<br />\r\nAltura da Cabeceira: 125 cm<br />\r\nAltura do Painel: 70 cm<br />\r\nEspessura: 10 cm</span><br />\r\n&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Tecidos: Corino, Couro ecol&oacute;gico, Suede Liso, Suede Amassado, Facto</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\"><strong>PRE&Ccedil;O REFENTE AO CORINO, PARA OUTROS ACABAMENTOS, CONSULTAR VALORES</strong></span></p>\r\n', '<p><span style=\"font-size:14px;\">Consultar valores para medidas especiais&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 03 meses</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">CUIDADOS:&nbsp;</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Utilizar sab&atilde;o com esponja umedecida, remover excesso com pano &uacute;mido</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Secagem em ambientes arejados e na sombra<br />\r\nN&atilde;o utilizar produtos qu&iacute;micos nas tonalidades claras, evitar contato com tecidos ou materiais que soltem tinta</span></p>\r\n', '2018-10-29 21:32:18', '2018-11-14 17:33:17'),
(127, 0, 3, 9, NULL, 27, 'jabaquara,gravi', 'cabeceira-barcelona', 'Cabeceira Barcelona', 'barcelona-casal_20181029183744VBl3KkSr4O.jpg', 'Decoração', 'Com mini frame ', 'Estofada ', '', 0, 'FABRICANTE MB M&Oacute;VEIS&nbsp;', '<p><span style=\"font-size:14px;\">Modelo dispon&iacute;vel em Cabeceira e Painel</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Altura da Cabeceira: 125 cm<br />\r\nAltura do Painel: 70 cm<br />\r\nEspessura: 8 cm</span><br />\r\n&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Tecidos: Corino, Couro ecol&oacute;gico, Suede Liso, Suede Amassado, Facto</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\"><strong>PRE&Ccedil;O REFENTE AO CORINO, PARA OUTROS ACABAMENTOS, CONSULTAR VALORES</strong></span></p>\r\n', '<p><span style=\"font-size:14px;\">Consultar valores para medidas especiais&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 03 meses</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">CUIDADOS:&nbsp;</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Utilizar sab&atilde;o com esponja umedecida, remover excesso com pano &uacute;mido</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Secagem em ambientes arejados e na sombra<br />\r\nN&atilde;o utilizar produtos qu&iacute;micos nas tonalidades claras, evitar contato com tecidos ou materiais que soltem tinta</span></p>\r\n', '2018-10-29 21:37:45', '2018-11-14 17:32:58'),
(128, 0, 3, 9, NULL, 27, 'jabaquara,gravi', 'cabeceira-canada', 'Cabeceira Canadá', 'canada-casal_201810291845082rRzCOH3ZK.jpg', 'Decoração', 'Com mini frame ', 'Estofada ', '', 0, 'FABRICANTE MB M&Oacute;VEIS', '<p><span style=\"font-size:14px;\">Modelo dispon&iacute;vel em Cabeceira e Painel</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Altura da Cabeceira: 125 cm<br />\r\nAltura do Painel: 70 cm<br />\r\nEspessura: 10 cm</span><br />\r\n&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Tecidos: Corino, Couro ecol&oacute;gico, Suede Liso, Suede Amassado, Facto, Floral, Dallas</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\"><strong>PRE&Ccedil;O REFENTE AO CORINO, PARA OUTROS ACABAMENTOS, CONSULTAR VALORES</strong></span></p>\r\n', '<p><span style=\"font-size:14px;\">Consultar valores para medidas especiais&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 03 meses</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">CUIDADOS:&nbsp;</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Utilizar sab&atilde;o com esponja umedecida, remover excesso com pano &uacute;mido</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Secagem em ambientes arejados e na sombra<br />\r\nN&atilde;o utilizar produtos qu&iacute;micos nas tonalidades claras, evitar contato com tecidos ou materiais que soltem tinta</span></p>\r\n', '2018-10-29 21:45:08', '2018-11-14 17:34:59'),
(129, 0, 3, 9, NULL, 27, 'jabaquara,gravi', 'cabeceira-cancun', 'Cabeceira Cancun', 'cancun-casal_20181029184934825P19J7hT.jpg', 'Decoração', 'Com mini frame ', 'Estofada ', '', 0, 'FABRICANTE MB M&Oacute;VEIS&nbsp;', '<p><span style=\"font-size:14px;\">Modelo dispon&iacute;vel em Cabeceira e Painel</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Altura da Cabeceira: 125 cm<br />\r\nAltura do Painel: 70 cm<br />\r\nEspessura: 11 cm</span><br />\r\n&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Tecidos: Corino, Couro ecol&oacute;gico, Suede Liso, Suede Amassado, Facto</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\"><strong>PRE&Ccedil;O REFENTE AO CORINO, PARA OUTROS ACABAMENTOS, CONSULTAR VALORES</strong></span></p>\r\n', '<p><span style=\"font-size:14px;\">Consultar valores para medidas especiais&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 03 meses</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">CUIDADOS:&nbsp;</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Utilizar sab&atilde;o com esponja umedecida, remover excesso com pano &uacute;mido</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Secagem em ambientes arejados e na sombra<br />\r\nN&atilde;o utilizar produtos qu&iacute;micos nas tonalidades claras, evitar contato com tecidos ou materiais que soltem tinta</span></p>\r\n', '2018-10-29 21:49:34', '2018-11-14 17:32:43'),
(130, 0, 3, 9, NULL, 27, 'jabaquara,gravi', 'cabeceira-cannes', 'Cabeceira Cannes', 'cannes-casal_20181029185319TmiVosuPSV.jpg', 'Decoração', 'Com mini frame ', 'Estofada ', '', 0, 'FABRICANTE MB M&Oacute;VEIS', '<p><span style=\"font-size:14px;\">Modelo dispon&iacute;vel em Cabeceira e Painel</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Altura da Cabeceira: 125 cm<br />\r\nAltura do Painel: 70 cm<br />\r\nEspessura: 10 cm</span><br />\r\n&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Tecidos: Corino, Couro ecol&oacute;gico, Suede Liso, Suede Amassado, Facto, Floral, Dallas</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\"><strong>PRE&Ccedil;O REFENTE AO CORINO, PARA OUTROS ACABAMENTOS, CONSULTAR VALORES</strong></span></p>\r\n', '<p><span style=\"font-size:14px;\">Consultar valores para medidas especiais&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 03 meses</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">CUIDADOS:&nbsp;</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Utilizar sab&atilde;o com esponja umedecida, remover excesso com pano &uacute;mido</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Secagem em ambientes arejados e na sombra<br />\r\nN&atilde;o utilizar produtos qu&iacute;micos nas tonalidades claras, evitar contato com tecidos ou materiais que soltem tinta</span></p>\r\n', '2018-10-29 21:53:19', '2018-11-14 17:32:19'),
(131, 0, 3, 9, NULL, 27, 'jabaquara,gravi', 'cabeceira-dubai', 'Cabeceira Dubai', 'dubai-casal_20181029185723GlPXCRmEjM.jpg', 'Decoração', 'Com mini frame ', 'Estofada ', '', 0, 'FABRICANTE MB M&Oacute;VEIS', '<p><span style=\"font-size:14px;\">Modelo dispon&iacute;vel em Cabeceira e Painel</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Altura da Cabeceira: 125 cm<br />\r\nAltura do Painel: 70 cm<br />\r\nEspessura: 11 cm</span><br />\r\n&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Tecidos: Corino, Couro ecol&oacute;gico, Suede Liso, Suede Amassado, Facto, Floral, Dallas</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\"><strong>PRE&Ccedil;O REFENTE AO CORINO, PARA OUTROS ACABAMENTOS, CONSULTAR VALORES</strong></span></p>\r\n', '<p><span style=\"font-size:14px;\">Consultar valores para medidas especiais&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 03 meses</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">CUIDADOS:&nbsp;</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Utilizar sab&atilde;o com esponja umedecida, remover excesso com pano &uacute;mido</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Secagem em ambientes arejados e na sombra<br />\r\nN&atilde;o utilizar produtos qu&iacute;micos nas tonalidades claras, evitar contato com tecidos ou materiais que soltem tinta</span></p>\r\n', '2018-10-29 21:57:23', '2018-11-14 17:32:01'),
(132, 0, 3, 9, NULL, 27, 'jabaquara,gravi', 'cabeceira-florenca', 'Cabeceira Florença', 'florenca-casal_20181029190214qfR7voezXf.jpg', 'Decoração', 'Com mini frame ', 'Estofada ', '', 0, 'FABRICANTE MB M&Oacute;VEIS', '<p><span style=\"font-size:14px;\">Modelo dispon&iacute;vel em Cabeceira e Painel</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Altura da Cabeceira: 125 cm<br />\r\nAltura do Painel: 70 cm<br />\r\nEspessura: 10 cm</span><br />\r\n&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Tecidos: Corino, Couro ecol&oacute;gico, Suede Liso, Suede Amassado, Facto</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Detalhes em Strass</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\"><strong>PRE&Ccedil;O REFENTE AO CORINO, PARA OUTROS ACABAMENTOS, CONSULTAR VALORES</strong></span></p>\r\n', '<p><span style=\"font-size:14px;\">Consultar valores para medidas especiais&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 03 meses</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">CUIDADOS:&nbsp;</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Utilizar sab&atilde;o com esponja umedecida, remover excesso com pano &uacute;mido</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Secagem em ambientes arejados e na sombra<br />\r\nN&atilde;o utilizar produtos qu&iacute;micos nas tonalidades claras, evitar contato com tecidos ou materiais que soltem tinta</span></p>\r\n', '2018-10-29 22:02:15', '2018-11-14 17:31:24'),
(133, 0, 3, 9, NULL, 27, 'jabaquara,gravi', 'cabeceira-genebra', 'Cabeceira Genebra', 'genebra-casal_20181029190616TTcjbnWTBr.jpg', 'Decoração', 'Com mini frame ', 'Estofada ', '', 0, 'FABRICENTE MB M&Oacute;VEIS', '<p><span style=\"font-size:14px;\">Modelo dispon&iacute;vel em Cabeceira e Painel</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura da Cabeceira: 125 cm<br />\r\nAltura do Painel: 70 cm<br />\r\nEspessura: 8 cm</span><br />\r\n&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Tecidos: Corino, Couro ecol&oacute;gico, Suede Liso, Suede Amassado, Facto, Floral, Dallas</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\"><strong>PRE&Ccedil;O REFENTE AO CORINO, PARA OUTROS ACABAMENTOS, CONSULTAR VALORES</strong></span></p>\r\n', '<p><span style=\"font-size:14px;\">Consultar valores para medidas especiais&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 03 meses</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">CUIDADOS:&nbsp;</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Utilizar sab&atilde;o com esponja umedecida, remover excesso com pano &uacute;mido</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Secagem em ambientes arejados e na sombra<br />\r\nN&atilde;o utilizar produtos qu&iacute;micos nas tonalidades claras, evitar contato com tecidos ou materiais que soltem tinta</span></p>\r\n', '2018-10-29 22:06:17', '2018-11-14 17:31:15'),
(134, 0, 3, 9, NULL, 27, 'jabaquara,gravi', 'cabeceira-holanda', 'Cabeceira Holanda', 'holanda-casal_201810291912356suT2cBmW3.jpg', 'Decoração', 'Com mini frame ', 'Estofada ', '', 0, 'FABRICANTE MB M&Oacute;VEIS', '<p><span style=\"font-size:14px;\">Modelo dispon&iacute;vel em Cabeceira e Painel</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Altura da Cabeceira: 125 cm<br />\r\nAltura do Painel: 70 cm<br />\r\nEspessura: 10 cm</span><br />\r\n&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Tecidos: Corino, Couro ecol&oacute;gico, Suede Liso, Suede Amassado, Facto, Dallas</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Detalhes produzidos em fita escovada prata</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\"><strong>PRE&Ccedil;O REFENTE AO CORINO, PARA OUTROS ACABAMENTOS, CONSULTAR VALORES</strong></span></p>\r\n', '<p><span style=\"font-size:14px;\">Consultar valores para medidas especiais&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 03 meses</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">CUIDADOS:&nbsp;</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Utilizar sab&atilde;o com esponja umedecida, remover excesso com pano &uacute;mido</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Secagem em ambientes arejados e na sombra<br />\r\nN&atilde;o utilizar produtos qu&iacute;micos nas tonalidades claras, evitar contato com tecidos ou materiais que soltem tinta</span></p>\r\n', '2018-10-29 22:12:36', '2018-11-14 17:30:46'),
(135, 0, 3, 9, NULL, 27, 'jabaquara,gravi', 'cabeceira-lorena', 'Cabeceira Lorena', 'lorena-casal_20181029201933JDPwLYnClI.jpg', 'Decoração', 'Com mini frame ', 'Estofada ', '', 0, 'FABRICANTE MB M&Oacute;VEIS', '<p><span style=\"font-size:14px;\">Modelo dispon&iacute;vel em Cabeceira e Painel</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Altura da Cabeceira: 125 cm<br />\r\nAltura do Painel: 70 cm<br />\r\nEspessura: 9 cm</span><br />\r\n&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Tecidos: Corino, Couro ecol&oacute;gico, Suede Liso, Suede Amassado, Facto, Floral, Dallas</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\"><strong>PRE&Ccedil;O REFENTE AO CORINO, PARA OUTROS ACABAMENTOS, CONSULTAR VALORES</strong></span></p>\r\n', '<p><span style=\"font-size:14px;\">Consultar valores para medidas especiais&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 03 meses</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">CUIDADOS:&nbsp;</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Utilizar sab&atilde;o com esponja umedecida, remover excesso com pano &uacute;mido</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Secagem em ambientes arejados e na sombra<br />\r\nN&atilde;o utilizar produtos qu&iacute;micos nas tonalidades claras, evitar contato com tecidos ou materiais que soltem tinta</span></p>\r\n', '2018-10-29 23:19:33', '2018-11-14 17:30:33'),
(136, 0, 3, 9, NULL, 27, 'jabaquara,gravi', 'cabeceira-malu', 'Cabeceira Malu', 'malu-casal_20181029202322xsJXd1hctD.jpg', 'Decoração', 'Com mini frame ', 'Estofada ', '', 0, 'FABRICANTE MB M&Oacute;VEIS', '<p><span style=\"font-size:14px;\">Modelo dispon&iacute;vel em Cabeceira e Painel</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Altura da Cabeceira: 125 cm<br />\r\nAltura do Painel: 70 cm<br />\r\nEspessura: 8 cm</span><br />\r\n&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Tecidos: Corino, Couro ecol&oacute;gico, Suede Liso, Suede Amassado, Facto, Floral, Dallas</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\"><strong>PRE&Ccedil;O REFENTE AO CORINO, PARA OUTROS ACABAMENTOS, CONSULTAR VALORES</strong></span></p>\r\n', '<p><span style=\"font-size:14px;\">Consultar valores para medidas especiais&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 03 meses</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">CUIDADOS:&nbsp;</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Utilizar sab&atilde;o com esponja umedecida, remover excesso com pano &uacute;mido</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Secagem em ambientes arejados e na sombra<br />\r\nN&atilde;o utilizar produtos qu&iacute;micos nas tonalidades claras, evitar contato com tecidos ou materiais que soltem tinta</span></p>\r\n', '2018-10-29 23:23:22', '2018-11-14 17:30:23'),
(137, 0, 4, 17, NULL, 3, 'jabaquara,gravi', 'poltrona-cinema', 'Poltrona Cinema', 'poltrona-cinema_20181030180319VFfqWXPVI2.jpg', 'Reclinagem Automática', '', 'Corano', '', 2, 'FABRICANTE CASTOR&nbsp;', '<p><span style=\"font-size:14px;\">A almofada do assento &eacute; fixa na Espuma D28 e estrutura de madeira com mecanismo.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">O acionamento da Poltrona Reclin&aacute;vel &eacute;&nbsp;eletr&ocirc;nico na voltagem 110/220.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Dimens&otilde;es: L: 78&nbsp;cm (Largura)<br />\r\nC: 82 cm (Comprimento)<br />\r\nA: 100 cm (Altura)<br />\r\nP: 55 cm (Profundidade)</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Este produto aberto possui 155 cm</span></p>\r\n', '<p><span style=\"font-size:14px;\">Garantia: 06 meses</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n', '2018-10-30 21:03:20', '2018-11-01 21:44:36'),
(138, 0, 4, 17, NULL, 3, 'jabaquara,gravi', 'poltrona-comfort-luxo', 'Poltrona Comfort Luxo', 'poltrona-comfort-luxo_20181030182455xoRJ9qe4pb.jpg', 'Reclinável ', '', 'Com balanço', '', 3, 'FABRICANTE CASTOR', '<p><span style=\"font-size:14px;\">Assento: Almofadas fixas em esp. D28 reves. de fibra siliconada 150g<br />\r\nEstrutura de madeira com mec. e molas NOSAG reves. de esp. D15</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Encosto: Almofadas fixas com enchimento Softtrel<br />\r\nEstrutura de madeira e mola NOSAG reves. de esp. D15</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Bra&ccedil;o: Almofadado fixo em esp. D20 soft reves. de fibra siliconada 150g<br />\r\nEstrutura de madeira reves. na esp. D15 e Fios do Mecanismo</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Acabamento: Corano</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Obs: Reclinavel s/ balan&ccedil;o ou girat&oacute;rio c/ balan&ccedil;o</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Dimens&otilde;es: L: 85&nbsp;cm (Largura)<br />\r\nC: 82 cm (Comprimento)<br />\r\nA: 107&nbsp;cm (Altura)</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">P: 58 cm (Profundidade)</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Este produto aberto possui 177 cm</span></p>\r\n', '<p><span style=\"font-size:14px;\">Garantia: 06 meses</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n', '2018-10-30 21:24:55', '2018-11-01 21:44:16'),
(139, 0, 4, 17, NULL, 3, 'jabaquara,gravi', 'poltrona-monza', 'Poltrona Monza ', 'poltrona-monza_20181030184426XN0vOT95wA.jpg', 'Reclinável ', '', 'Com alavanca ', '', 2, 'FABRICANTE CASTOR', '<p><span style=\"font-size:14px;\">Almofada do assento &eacute; fixa na espuma D28 revestida em fibra siliconizada e estrutura de madeira com mecanismo.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Bra&ccedil;o com almofada fixa na espuma D20 revestido de fibra siliconizada e estrutura de madeira com fios de mecanismo reclin&aacute;vel.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Dimens&otilde;es: L: 100&nbsp;cm (Largura)</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">C: 111&nbsp;cm (Comprimento)<br />\r\nA: 105&nbsp;cm (Altura)</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">P: 56&nbsp;cm (Profundidade)</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Este produto aberto possui 170&nbsp;cm</span></p>\r\n', '<p><span style=\"font-size:14px;\">Garantia: 06 meses</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n', '2018-10-30 21:44:27', '2018-11-01 21:43:53'),
(140, 0, 4, 17, NULL, 3, 'jabaquara,gravi', 'poltrona-senior', 'Poltrona Sênior ', 'senior_2018103019044465RpJqYFK5.jpg', 'Reclinagem Automática', '', 'Revestimento Corano', '', 3, 'FABRICANTE CASTOR&nbsp;', '<p><span style=\"font-size:14px;\">Almofada do assento &eacute; fixa na espuma D28 Soft e estrutura de madeira com molas.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">&Eacute; reclin&aacute;vel com controle na voltagem 110/127.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Dimens&otilde;es: L: 82&nbsp;cm (Largura)</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">C: 86&nbsp;cm (Comprimento)<br />\r\nA: 100&nbsp;cm (Altura)</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">P: 57&nbsp;cm (Profundidade)</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Este produto aberto possui 174&nbsp;cm</span></p>\r\n', '<p><span style=\"font-size:14px;\">Garantia: 06 meses</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n', '2018-10-30 22:04:44', '2018-11-01 21:43:37');
INSERT INTO `produtos` (`id`, `ordem`, `secao_id`, `categoria_id`, `subcategoria_id`, `marca_id`, `lojas`, `slug`, `titulo`, `capa`, `material`, `linha`, `legenda`, `codigo`, `conforto`, `resumo`, `descricao`, `dados_complementares`, `created_at`, `updated_at`) VALUES
(141, 22, 7, 29, NULL, 13, 'jabaquara,gravi', 'viscotop', 'Viscotop', 'viscotopp_20181030202942FGpxwaG0sQ.png', 'Conforto Macio', '', 'Espuma Viscoelástica', '', 3, 'FABRICANTE THEVA&nbsp;', '<p><span style=\"font-size:14px;\">Alivia a press&atilde;o exercida pelo corpo</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Capa em tecido com fios de seda oferece toque extra macio</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">El&aacute;stico nos 4 cantos facilita a arruma&ccedil;&atilde;o da cama</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Medidas dispon&iacute;veis: 088x188 cm, 138x188 cm,158x198 cm e 193x203 cm</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura: 02&nbsp;cm</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Medidas especiais sob consulta</span></p>\r\n', '<p><span style=\"font-size:14px;\">Possui registro na Anvisa, registrada sob o n&uacute;mero&nbsp;80730110001</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">CUIDADOS: </span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">A Espuma de Poliuretano Viscoel&aacute;stica &eacute; termosens&iacute;vel, ou seja, o calor natural do corpo faz com que o Pillow-Top&nbsp;amolde-se ao usu&aacute;rio</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">N&atilde;o molhar e n&atilde;o lavar</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n', '2018-10-30 23:29:43', '2018-11-01 21:54:10'),
(142, 0, 3, 9, NULL, 27, 'jabaquara,gravi', 'cabeceira-marrocos', 'Cabeceira Marrocos', 'marrocos-casal_20181031164833pZ4xwaUE3m.jpg', 'Decoração', 'Com mini frame ', 'Estofada', '', 0, 'FABRICANTE MB M&Oacute;VEIS&nbsp;', '<p><span style=\"font-size:14px;\">Modelo dispon&iacute;vel em Cabeceira e Painel</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Altura da cabeceira: 125 cm<br />\r\nAltura do painel: 70 cm<br />\r\nEspessura: 5 cm</span><br />\r\n&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Tecidos: Corino, Couro ecol&oacute;gico, Suede Liso, Suede Amassado, Facto, Floral, Dallas&nbsp;</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\"><strong>PRE&Ccedil;O REFENTE AO CORINO, PARA OUTROS ACABAMENTOS, CONSULTAR VALORES</strong></span></p>\r\n', '<p><span style=\"font-size:14px;\">Consultar valores para medidas especiais&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 03 meses</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">CUIDADOS:&nbsp;</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Utilizar sab&atilde;o com esponja umedecida, remover excesso com pano &uacute;mido</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Secagem em ambientes arejados e na sombra<br />\r\nN&atilde;o utilizar produtos qu&iacute;micos nas tonalidades claras, evitar contato com tecidos ou materiais que soltem tinta</span></p>\r\n', '2018-10-31 19:48:34', '2018-11-14 17:30:08'),
(143, 0, 3, 9, NULL, 27, 'jabaquara,gravi', 'cabeceira-monte-carlo', 'Cabeceira Monte Carlo', 'monte-carlo-casal-2_201810311656251xnwjOTH1c.jpg', 'Decoração', 'Com mini frame ', 'Estofada ', '', 0, 'FABRICANTE MB M&Oacute;VEIS', '<p><span style=\"font-size:14px;\">Modelo dispon&iacute;vel em Cabeceira e Painel</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Altura da Cabeceira: 125 cm<br />\r\nAltura do Painel: 70 cm<br />\r\nEspessura: 13 cm</span><br />\r\n&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Tecidos: Corino, Couro ecol&oacute;gico, Suede Liso, Suede Amassado, Facto, Floral, Dallas</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\"><strong>PRE&Ccedil;O REFENTE AO CORINO, PARA OUTROS ACABAMENTOS, CONSULTAR VALORES</strong></span></p>\r\n', '<p><span style=\"font-size:14px;\">Consultar valores para medidas especiais&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 03 meses</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">CUIDADOS:&nbsp;</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Utilizar sab&atilde;o com esponja umedecida, remover excesso com pano &uacute;mido</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Secagem em ambientes arejados e na sombra<br />\r\nN&atilde;o utilizar produtos qu&iacute;micos nas tonalidades claras, evitar contato com tecidos ou materiais que soltem tinta</span></p>\r\n', '2018-10-31 19:56:26', '2018-11-14 17:29:59'),
(144, 0, 3, 9, NULL, 27, 'jabaquara,gravi', 'cabeceira-monte-verde', 'Cabeceira Monte Verde', 'monte-verde-casal_20181031170020fT4OUj6RjS.jpg', 'Decoração', 'Com mini frame ', 'Estofada ', '', 0, 'FABRICANTE MB M&Oacute;VEIS', '<p><span style=\"font-size:14px;\">Modelo dispon&iacute;vel em Cabeceira e Painel</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Altura da Cabeceira: 125 cm<br />\r\nAltura do Painel: 70 cm<br />\r\nEspessura: 10 cm</span><br />\r\n&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Tecidos: Corino, Couro ecol&oacute;gico, Suede Liso, Suede Amassado, Facto, Floral, Dallas</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\"><strong>PRE&Ccedil;O REFENTE AO CORINO, PARA OUTROS ACABAMENTOS, CONSULTAR VALORES</strong></span></p>\r\n', '<p><span style=\"font-size:14px;\">Consultar valores para medidas especiais&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 03 meses</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">CUIDADOS:&nbsp;</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Utilizar sab&atilde;o com esponja umedecida, remover excesso com pano &uacute;mido</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Secagem em ambientes arejados e na sombra<br />\r\nN&atilde;o utilizar produtos qu&iacute;micos nas tonalidades claras, evitar contato com tecidos ou materiais que soltem tinta</span></p>\r\n', '2018-10-31 20:00:21', '2018-11-14 17:29:49'),
(145, 0, 3, 9, NULL, 27, 'jabaquara,gravi', 'cabeceira-montreal', 'Cabeceira Montreal', 'montreal-casal_20181031170359B0hY0yzZEu.jpg', 'Decoração', 'Com mini frame ', 'Estofada ', '', 0, 'FABRICANTE MB M&Oacute;VEIS', '<p><span style=\"font-size:14px;\">Modelo dispon&iacute;vel em Cabeceira e Painel</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Altura da Cabeceira: 125 cm<br />\r\nAltura do Painel: 70 cm<br />\r\nEspessura: 9 cm</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Tecido: Corino, Couro ecol&oacute;gico, Suede Liso, Suede Amassado, Facto, Floral, Dallas</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\"><strong>PRE&Ccedil;O REFENTE AO CORINO, PARA OUTROS ACABAMENTOS, CONSULTAR VALORES</strong></span></p>\r\n', '<p><span style=\"font-size:14px;\">Consultar valores para medidas especiais&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 03 meses</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">CUIDADOS:&nbsp;</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Utilizar sab&atilde;o com esponja umedecida, remover excesso com pano &uacute;mido</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Secagem em ambientes arejados e na sombra<br />\r\nN&atilde;o utilizar produtos qu&iacute;micos nas tonalidades claras, evitar contato com tecidos ou materiais que soltem tinta</span></p>\r\n', '2018-10-31 20:04:00', '2018-11-14 17:29:37'),
(146, 0, 3, 9, NULL, 27, 'jabaquara,gravi', 'cabeceira-munique', 'Cabeceira Munique', 'munique-casal_20181031170715PTvWvlsEp9.jpg', 'Decoração', 'Com mini frame ', 'Estofada ', '', 0, 'FABRICANTE MB M&Oacute;VEIS', '<p><span style=\"font-size:14px;\">Modelo dispon&iacute;vel em Cabeceira e Painel</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Altura da Cabeceira: 125 cm<br />\r\nAltura do Painel: 70 cm<br />\r\nEspessura: 10 cm</span><br />\r\n&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Tecidos: Corino, Couro ecol&oacute;gico, Suede Liso, Suede Amassado, Facto, Floral, Dallas</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\"><strong>PRE&Ccedil;O REFENTE AO CORINO, PARA OUTROS ACABAMENTOS, CONSULTAR VALORES</strong></span></p>\r\n', '<p><span style=\"font-size:14px;\">Consultar valores para medidas especiais&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 03 meses</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">CUIDADOS:&nbsp;</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Utilizar sab&atilde;o com esponja umedecida, remover excesso com pano &uacute;mido</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Secagem em ambientes arejados e na sombra<br />\r\nN&atilde;o utilizar produtos qu&iacute;micos nas tonalidades claras, evitar contato com tecidos ou materiais que soltem tinta</span></p>\r\n', '2018-10-31 20:07:16', '2018-11-14 17:29:07'),
(147, 0, 3, 9, NULL, 27, 'jabaquara,gravi', 'cabeceira-paris', 'Cabeceira Paris', 'paris-casal_20181031171113rG47HYG1z5.jpg', 'Decoração', 'Com mini frame ', 'Estofada ', '', 0, 'FABRICANTE MB M&Oacute;VEIS', '<p><span style=\"font-size:14px;\">Modelo dispon&iacute;vel em Vabeceira e Painel<br />\r\n<br />\r\nAltura da Cabeceira: 125 cm<br />\r\nAltura do Painel: 70 cm<br />\r\nEspessura: 10 cmA</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Tecido: Corino, Couro ecol&oacute;gico, Suede Liso, Suede Amassado, Facto, Floral, Dallas</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\"><strong>PRE&Ccedil;O REFENTE AO CORINO, PARA OUTROS ACABAMENTOS, CONSULTAR VALORES</strong></span></p>\r\n', '<p><span style=\"font-size:14px;\">Consultar valores para medidas especiais&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 03 meses</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">CUIDADOS:&nbsp;</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Utilizar sab&atilde;o com esponja umedecida, remover excesso com pano &uacute;mido</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Secagem em ambientes arejados e na sombra<br />\r\nN&atilde;o utilizar produtos qu&iacute;micos nas tonalidades claras, evitar contato com tecidos ou materiais que soltem tinta</span></p>\r\n', '2018-10-31 20:11:14', '2018-11-14 17:28:52'),
(148, 0, 3, 9, NULL, 27, 'jabaquara,gravi', 'cabeceira-ravena', 'Cabeceira Ravena', 'ravena-casal_20181031171901jgAOAAIGKy.jpg', 'Decoração', 'Com mini frame ', 'Estofada ', '', 0, 'FABRICANTE MB M&Oacute;VEIS', '<p><span style=\"font-size:14px;\">Modelo dispon&iacute;vel em Cabeceira e Painel</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Altura da Cabeceira: 125 cm<br />\r\nAltura do Painel: 70 cm<br />\r\nEspessura: 17 cm</span><br />\r\n&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Tecidos: Corino, Couro ecol&oacute;gico, Suede Liso, Suede Amassado, Facto, Floral, Dallas</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\"><strong>PRE&Ccedil;O REFENTE AO CORINO, PARA OUTROS ACABAMENTOS, CONSULTAR VALORES</strong></span></p>\r\n', '<p><span style=\"font-size:14px;\">Consultar valores para medidas especiais&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 03 meses</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">CUIDADOS:&nbsp;</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Utilizar sab&atilde;o com esponja umedecida, remover excesso com pano &uacute;mido</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Secagem em ambientes arejados e na sombra<br />\r\nN&atilde;o utilizar produtos qu&iacute;micos nas tonalidades claras, evitar contato com tecidos ou materiais que soltem tinta</span></p>\r\n', '2018-10-31 20:19:02', '2018-11-14 17:28:42'),
(149, 0, 3, 9, NULL, 27, 'jabaquara,gravi', 'cabeceira-suica', 'Cabeceira Suíça', 'suica-casal_20181031172330I4Egdh7rsT.jpg', 'Decoração', 'Com mini frame ', 'Estofada ', '', 0, 'FABRICANTE MB M&Oacute;VEIS', '<p><span style=\"font-size:14px;\">Modelo dispon&iacute;vel em Cabeceira e Painel</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Altura da Cabeceira: 125 cm<br />\r\nAltura do Painel: 70 cm<br />\r\nEspessura: 11 cm</span><br />\r\n&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Tecidos: Suede Liso, Suede Amassado</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\"><strong>PRE&Ccedil;O REFENTE AO SUEDE&nbsp;</strong></span></p>\r\n', '<p><span style=\"font-size:14px;\">Consultar valores para medidas especiais&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 03 meses</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">CUIDADOS:&nbsp;</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Utilizar sab&atilde;o com esponja umedecida, remover excesso com pano &uacute;mido</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Secagem em ambientes arejados e na sombra<br />\r\nN&atilde;o utilizar produtos qu&iacute;micos nas tonalidades claras, evitar contato com tecidos ou materiais que soltem tinta</span></p>\r\n', '2018-10-31 20:23:30', '2018-11-14 17:28:01'),
(150, 0, 3, 9, NULL, 27, 'jabaquara,gravi', 'cabeceira-veranda-reta', 'Cabeceira Veranda Reta', 'veranda-reta-1_20181031172956l4ax3zJ76F.jpg', 'Decoração', 'Com mini frame ', 'Estofada ', '', 0, 'FABRICANTE MB M&Oacute;VEIS', '<p><span style=\"font-size:14px;\">Modelo dispon&iacute;vel em Cabeceira e Painel</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Altura da Cabeceira: 125 cm<br />\r\nAltura do Painel: 70 cm<br />\r\nEspessura: 19 cm</span><br />\r\n&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Tecidos: Corino, Couro ecol&oacute;gico, Suede Liso, Suede Amassado, Facto</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\"><strong>PRE&Ccedil;O REFENTE AO CORINO, PARA OUTROS ACABAMENTOS, CONSULTAR VALORES</strong></span></p>\r\n', '<p><span style=\"font-size:14px;\">Consultar valores para medidas especiais&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 03 meses</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">CUIDADOS:&nbsp;</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Utilizar sab&atilde;o com esponja umedecida, remover excesso com pano &uacute;mido</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Secagem em ambientes arejados e na sombra<br />\r\nN&atilde;o utilizar produtos qu&iacute;micos nas tonalidades claras, evitar contato com tecidos ou materiais que soltem tinta</span></p>\r\n', '2018-10-31 20:29:56', '2018-11-14 17:27:21'),
(151, 0, 3, 33, NULL, 27, 'jabaquara,gravi', 'cabeceira-alphaville', 'Cabeceira Alphaville', 'alphaville-casal_20181031173836c86JGyH1vc.jpg', 'Decoração', 'Com mini frame ', 'MDF', '', 0, 'FABRICANTE MB M&Oacute;VEIS', '<p><span style=\"font-size:14px;\">Modelo dispon&iacute;vel em Cabeceira e Painel</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Altura da Cabeceira: 125 cm<br />\r\nAltura do Painel: 70 cm<br />\r\nEspessura: 5 cm<br />\r\n<br />\r\nPadr&otilde;es em MDF: Tabaco, Ruggine, Louro Freij&oacute;, Cobre Cortein, Castanho, Branco</span></p>\r\n', '<p><span style=\"font-size:14px;\">Consultar valores para medidas especiais&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 03 meses</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">COMO LIMPAR:</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Lustra M&oacute;veis com pano seco</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">N&atilde;o resiste a solventes clorados, cetonas, hidrocarbonetos arom&aacute;ticos, &eacute;steres, aminas, &oacute;leos minerais, &aacute;cidos concentrados a quente e subst&acirc;ncias fortemente alcalinas</span></p>\r\n', '2018-10-31 20:38:36', '2018-11-14 17:27:11'),
(152, 0, 3, 33, NULL, 27, 'jabaquara,gravi', 'cabeceira-havana', 'Cabeceira Havana', 'havana-casal_20181031174240gmKFMKXR7R.jpg', 'Decoração', 'Com mini frame ', 'MDF', '', 0, 'FABRICANTE MB M&Oacute;VEIS', '<p><span style=\"font-size:14px;\">Modelo dispon&iacute;vel em Cabeceira e Painel</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Altura da Cabeceira: 125 cm<br />\r\nAltura do Painel: 70 cm<br />\r\nEspessura: 5 cm</span><br />\r\n&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Padr&otilde;es em MDF: Tabaco, Ruggine, Louro Freij&oacute;, Cobre Cortein, Castanho, Branco</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Detalhes em Strass</span></p>\r\n', '<p><span style=\"font-size:14px;\">Consultar valores para medidas especiais&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 03 meses</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">COMO LIMPAR:</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Lustra M&oacute;veis com pano seco</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">N&atilde;o resiste a solventes clorados, cetonas, hidrocarbonetos arom&aacute;ticos, &eacute;steres, aminas, &oacute;leos minerais, &aacute;cidos concentrados a quente e subst&acirc;ncias fortemente alcalinas</span></p>\r\n', '2018-10-31 20:42:41', '2018-11-14 17:26:57'),
(153, 0, 3, 33, NULL, 27, 'jabaquara,gravi', 'cabeceira-malaga', 'Cabeceira Málaga', 'malaga-casal_20181031174524MQ9qoXckjH.jpg', 'Decoração', 'Com mini frame ', 'MDF', '', 0, 'FABRICANTE MB M&Oacute;VEIS', '<p><span style=\"font-size:14px;\">Modelo dispon&iacute;vel em Cabeceira e Painel</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Altura da Cabeceira: 125 cm<br />\r\nAltura do Painel: 70 cm<br />\r\nEspessura: 5 cm</span><br />\r\n&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Padr&otilde;es em MDF: Tabaco, Ruggine, Louro Freij&oacute;, Cobre Cortein, Castanho, Branco</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Detalhes em Strass</span></p>\r\n', '<p><span style=\"font-size:14px;\">Consultar valores para medidas especiais&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 03 meses</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">COMO LIMPAR:</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Lustra M&oacute;veis com pano seco</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">N&atilde;o resiste a solventes clorados, cetonas, hidrocarbonetos arom&aacute;ticos, &eacute;steres, aminas, &oacute;leos minerais, &aacute;cidos concentrados a quente e subst&acirc;ncias fortemente alcalinas</span></p>\r\n', '2018-10-31 20:45:25', '2018-11-14 17:26:41'),
(154, 0, 3, 33, NULL, 27, 'jabaquara,gravi', 'cabeceira-malibu', 'Cabeceira Malibú', 'malibu-casal_20181031174733ZWqqZ0eE99.jpg', 'Decoração', 'Com mini frame ', 'MDF', '', 0, 'FABRICANTE MB M&Oacute;VEIS', '<p><span style=\"font-size:14px;\">Modelo dispon&iacute;vel em Cabeceira e Painel</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Altura da Cabeceira: 125 cm<br />\r\nAltura do Painel: 70 cm<br />\r\nEspessura: 5 cm</span><br />\r\n&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Padr&otilde;es em MDF: Tabaco, Ruggine, Louro Freij&oacute;, Cobre Cortein, Castanho, Branco</span></p>\r\n', '<p><span style=\"font-size:14px;\">Consultar valores para medidas especiais&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 03 meses</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">COMO LIMPAR:</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Lustra M&oacute;veis com pano seco</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">N&atilde;o resiste a solventes clorados, cetonas, hidrocarbonetos arom&aacute;ticos, &eacute;steres, aminas, &oacute;leos minerais, &aacute;cidos concentrados a quente e subst&acirc;ncias fortemente alcalinas</span></p>\r\n', '2018-10-31 20:47:34', '2018-11-14 17:26:33'),
(156, 0, 3, 33, NULL, 27, 'jabaquara,gravi', 'cabeceira-marselha', 'Cabeceira Marselha', 'marselha-casal_20181031175230gcZzcPxVna.jpg', 'Decoração', 'Com mini frame ', 'MDF', '', 0, 'FABRICANTE MB M&Oacute;VEIS', '<p><span style=\"font-size:14px;\">Modelo dispon&iacute;vel em Cabeceira e Painel</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Altura da Cabeceira: 125 cm<br />\r\nAltura do Painel: 70 cm<br />\r\nEspessura: 5 cm</span><br />\r\n&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Padr&otilde;es em MDF: Tabaco, Ruggine, Louro Freij&oacute;, Cobre Cortein, Castanho, Branco</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Detalhes em Strass</span></p>\r\n', '<p><span style=\"font-size:14px;\">Consultar valores para medidas especiais&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 03 meses</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">COMO LIMPAR:</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Lustra M&oacute;veis com pano seco</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">N&atilde;o resiste a solventes clorados, cetonas, hidrocarbonetos arom&aacute;ticos, &eacute;steres, aminas, &oacute;leos minerais, &aacute;cidos concentrados a quente e subst&acirc;ncias fortemente alcalinas</span></p>\r\n', '2018-10-31 20:52:31', '2018-11-14 17:26:22'),
(157, 0, 3, 33, NULL, 27, 'jabaquara,gravi', 'cabeceira-nice', 'Cabeceira Nice', 'nice-casal_20181031175548FFLZCYGvdI.jpg', 'Decoração', 'Com mini frame ', 'MDF', '', 0, 'FABRICANTE MB M&Oacute;VEIS', '<p><span style=\"font-size:14px;\">Modelo dispon&iacute;vel em Cabeceira e Painel</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Altura da Cabeceira: 125 cm<br />\r\nAltura do Painel: 70 cm<br />\r\nEspessura: 5 cm</span><br />\r\n&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Padr&otilde;es em MDF: Tabaco, Ruggine, Louro Freij&oacute;, Cobre Cortein, Castanho, Branco</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Detalhes em Strass</span></p>\r\n', '<p><span style=\"font-size:14px;\">Consultar valores para medidas especiais&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 03 meses</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">COMO LIMPAR:</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Lustra M&oacute;veis com pano seco</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">N&atilde;o resiste a solventes clorados, cetonas, hidrocarbonetos arom&aacute;ticos, &eacute;steres, aminas, &oacute;leos minerais, &aacute;cidos concentrados a quente e subst&acirc;ncias fortemente alcalinas</span></p>\r\n', '2018-10-31 20:55:49', '2018-11-14 17:26:14'),
(158, 0, 3, 33, NULL, 27, 'jabaquara,gravi', 'cabeceira-valencia', 'Cabeceira Valência', 'valencia-casal_20181031180034vklxvVYcl5.jpg', 'Decoração', 'Com mini frame ', 'MDF', '', 0, 'FABRICANTE MB M&Oacute;VEIS', '<p><span style=\"font-size:14px;\">Modelo dispon&iacute;vel em Cabeceira e Painel</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Altura da Cabeceira: 125 cm<br />\r\nAltura do Painel: 70 cm<br />\r\nEspessura: 5 cm</span><br />\r\n&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Padr&otilde;es em MDF: Tabaco, Ruggine, Louro Freij&oacute;, Cobre Cortein, Castanho, Branco</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Padr&otilde;es em Tecidos: Corino, Couro ecol&oacute;gico, Suede Liso, Suede Amassado, Facto, Floral, Dallas</span></p>\r\n', '<p><span style=\"font-size:14px;\">Consultar valores para medidas especiais&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 03 meses</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">COMO LIMPAR O MDF:</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Lustra M&oacute;veis com pano seco</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">N&atilde;o resiste a solventes clorados, cetonas, hidrocarbonetos arom&aacute;ticos, &eacute;steres, aminas, &oacute;leos minerais, &aacute;cidos concentrados a quente e subst&acirc;ncias fortemente alcalinas</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">CUIDADOS DO ESTOFADO:&nbsp;</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Utilizar sab&atilde;o com esponja umedecida, remover excesso com pano &uacute;mido</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Secagem em ambientes arejados e na sombra<br />\r\nN&atilde;o utilizar produtos qu&iacute;micos nas tonalidades claras, evitar contato com tecidos ou materiais que soltem tinta</span></p>\r\n', '2018-10-31 21:00:35', '2018-11-14 17:26:05'),
(159, 0, 3, 33, NULL, 27, 'jabaquara,gravi', 'cabeceira-viena', 'Cabeceira Viena', 'viena-casal_20181031180348lUDO2dK5Kg.jpg', 'DECORAÇÃO', 'Com mini frame ', 'MDF', '', 0, 'FABRICANTE MB M&Oacute;VEIS', '<p><span style=\"font-size:14px;\">Modelo dispon&iacute;vel em Cabeceira e Painel</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Altura da cabeceira: 125 cm<br />\r\nAltura do painel: 70 cm<br />\r\nEspessura: 5 cm</span><br />\r\n&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Padr&otilde;es em MDF: Tabaco, Ruggine, Louro Freij&oacute;, Cobre Cortein, Castanho, Branco</span></p>\r\n', '<p><span style=\"font-size:14px;\">Consultar valores para medidas especiais&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 03 meses</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">CUIDADOS:&nbsp;</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Utilizar sab&atilde;o com esponja umedecida, remover excesso com pano &uacute;mido</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Secagem em ambientes arejados e na sombra<br />\r\nN&atilde;o utilizar produtos qu&iacute;micos nas tonalidades claras, evitar contato com tecidos ou materiais que soltem tinta</span></p>\r\n', '2018-10-31 21:03:49', '2018-11-14 17:08:27'),
(160, 44, 1, 1, NULL, 9, 'jabaquara,gravi', 'marrakech', 'Marrakech', '8vzo3bca_20181101163216Vt3nUMV6Xo.jpeg', 'Conforto Firme', 'Espessura 32 cm', 'Molas LFK 2.2', '', 1, 'FABRICANTE FLEX DO BRASIL', '<p><span style=\"font-size:14px;\">Tecido da capa: Nobre malha desenvolvida na B&eacute;lgica, composta com fios de Algod&atilde;o org&acirc;nico de suave toque (59% Poli&eacute;ster/41% Algod&atilde;o org&acirc;nico)</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Tecido da faixa lateral: Tecido lateral em Original Suede (51% Poli&eacute;ster/27% Poliuretano/22% Polipropileno)</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Tecnologias: Top Visco inside (Colch&atilde;o que se molda ao corpo e distribui seu peso uniformemente), Organic Cotton (Nobre malha desenvolvida na B&eacute;lgica, composta com fios de algod&atilde;o org&acirc;nico de suave toque), LFK 2.2 - Extra support (Tecnologia LFK em a&ccedil;o carbono com bitola de 2.2mm, 74% mais firme que o molejo Bonnell Tradicional, estabilidade m&aacute;xima, durabilidade garantida e firmeza com m&aacute;ximo conforto), Molas Flex LFK 2.2</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Registro Inmetro&nbsp;004181/2017</span></p>\r\n', '<p><span style=\"font-size:14px;\">Colch&atilde;o com 32 cm de altura&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura do conjunto:&nbsp;71 cm&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Suporte de peso de at&eacute; 130kg por pessoa</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa&nbsp;</span></p>\r\n', '2018-11-01 19:32:17', '2018-11-01 19:43:19'),
(164, 0, 6, 35, NULL, 20, 'jabaquara,gravi', 'poltrona-mr-2000-1', 'Poltrona MR 2000', '88_20181103151812FhG4r81qBP.png', 'Fixa', '', 'Compacta', '', 3, 'FABRICANTE RINC&Atilde;O', '<p><span style=\"font-size:14px;\">Assento: A sustenta&ccedil;&atilde;o se d&aacute; atrav&eacute;s de Percinta El&aacute;stica e Molas Ensacadas Individualmente, Espuma D23 Soft e com uma camada de manta termobonding</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Quantidade de mola 1 lugar: 42 molas</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Encosto: Almofada fixa, preenchimento de fibra siliconizada e flocos</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura dos p&eacute;s: 15 cm</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Estrutura: Produzido com Madeira de Eucalipto com cola e grampos industriais</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Dimens&otilde;es: L: 73 cm (Largura)</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">A: 93 cm (Altura)</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">P: 84&nbsp;cm (Profundidade)</span></p>\r\n', '<p><span style=\"font-size:14px;\">Garantia: 06 meses</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n', '2018-11-03 18:18:13', '2018-11-03 18:18:13'),
(165, 0, 6, 35, NULL, 20, 'jabaquara,gravi', 'poltrona-mr-1500', 'Poltrona MR 1500', 'poltrona-1500_20181103151926p60YBBzAN9.png', 'Fixa', '', 'Compacta', '', 3, 'FABRICANTE RINC&Atilde;O', '<p><span style=\"font-size:14px;\">Assento: A sustenta&ccedil;&atilde;o se d&aacute; atrav&eacute;s de Percinta El&aacute;stica, Espuma D24, almofada fixa com enchimento de flocos e fibra siliconizada<br />\r\n&nbsp;<br />\r\nEncosto: Espuma D20, almofada fixa com enchimento de flocos e fibra siliconizada<br />\r\n&nbsp;<br />\r\nAltura da base: 25 cm<br />\r\n&nbsp;<br />\r\nEstrutura: Produzido com Madeira de Eucalipto com cola e grampos industriais</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Dimens&otilde;es: L: 70&nbsp;cm (Largura)</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">A: 92&nbsp;cm (Altura)</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">P: 78 cm (Profundidade)</span></p>\r\n', '<p><span style=\"font-size:14px;\">Garantia: 06 meses</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n', '2018-11-03 18:19:27', '2018-11-03 18:19:27'),
(166, 0, 6, 35, NULL, 20, 'jabaquara,gravi', 'poltrona-mr-1200-1', 'Poltrona MR 1200', 'poltrona-1200_20181103152034wG2dslxuyH.png', 'Fixa', '', 'Compacta', '', 3, 'FABRICANTE RINC&Atilde;O', '<p><span style=\"font-size:14px;\">Assento: A sustenta&ccedil;&atilde;o se d&aacute; atrav&eacute;s de Persintas El&aacute;sticas e Espumas. As espumas t&ecirc;m a primeira camada com 4 cm de espessura D24, a segunda camada tem 2 cm de espessura D23 Soft</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Encosto: A sustenta&ccedil;&atilde;o se d&aacute; atrav&eacute;s de Persintas El&aacute;sticas e Espumas</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Estrutura: Produzido com Madeira de Eucalipto. Nas jun&ccedil;&otilde;es &eacute; utilizado cola e grampos industriais</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Os p&eacute;s s&atilde;o de madeira com 27 cm de altura</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Dimens&otilde;es: L: 75&nbsp;cm (Largura)</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">A: 89 cm (Altura)</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">P: 72&nbsp;cm (Profundidade)</span></p>\r\n', '<p><span style=\"font-size:14px;\">Garantia: 06 meses</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n', '2018-11-03 18:20:35', '2018-11-03 18:20:35'),
(167, 20, 1, 1, NULL, 25, 'jabaquara,gravi', 'prime-coill', 'Prime Coill', 'havenna-verticoil_201811051622469L8Fy1gMol.png', 'Conforto Intermediário', 'Espessura 24 cm', 'Molejo miracoill', '', 2, '', '<p><span style=\"font-size:14px;\">Molas Miracoill</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Espuma D26</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Revestimento Superior: Tecido 80% Poli&eacute;ster e 20% Bamboo</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Revestimento Lateral: 100% Poli&eacute;ster</span></p>\r\n', '<p><span style=\"font-size:14px;\">Colch&atilde;o com 24 cm de altura</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura do conjunto: 62 cm</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Suporte de peso de at&eacute; 100kg por pessoa&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n', '2018-11-05 18:22:49', '2020-02-26 21:29:11'),
(168, 0, 5, 30, NULL, 18, 'jabaquara,gravi', 'puff-conforto', 'Puff Conforto ', 'puff-conforto-1_20200226183537binbJb3O7v.jpg', 'Conforto Intermediário', '', 'Espuma D20', '', 2, 'FABRICANTE FA', '<p><span style=\"font-size:14px;\">Colch&atilde;o e sof&aacute; em um &uacute;nico produto. Vers&aacute;til e confort&aacute;vel.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Solteiro: Acompanha 01 almofada</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Casal: Acompanha 02 almofadas</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Medidas: Solteiro:&nbsp;60x60x30 cm (Fechado)<br />\r\n60x180x10 cm (Aberto)</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Casal: 128x60x30 cm (Fechado)<br />\r\n128x180x10 cm (Aberto)</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Enchimento: 100% Espuma de Poliuretano D20 kg/m&sup3;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Tecido: Chenille Azul ou Marrom</span></p>\r\n\r\n<p>&nbsp;</p>\r\n', '<p><span style=\"font-size:14px;\">Garantia: 03 meses</span></p>\r\n', '2018-11-09 20:31:26', '2020-02-26 21:36:47'),
(169, 0, 7, 25, NULL, 17, 'jabaquara,gravi', 'travesseiro-memory-foam-dubai', 'Travesseiro Memory Foam Dubai ', 'harmonia-dubai-d40_20181114154433IlMYmXqCgp.jpg', 'Conforto Firme', '', 'Espuma Viscoelástica Especial', '', 1, 'FABRICANTE GENERAL INJECTION&nbsp;', '<p><span style=\"font-size:14px;\">Produto com design totalmente diferenciado e rico em detalhes. Sua espuma possui alto suporte e firmeza elevada, conferindo uma experi&ecirc;ncia completa para amantes de travesseiros firmes e confort&aacute;veis.</span><br />\r\n&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Densidade: D40</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Termosenss&iacute;vel</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Embalagem: PVC&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Capa: 100% Algod&atilde;o</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Espuma: 100% Poliuretano</span><br />\r\n&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Dimens&atilde;o: 40x60 cm<br />\r\nAlturas: Baixo: 11 cm<br />\r\nM&eacute;dio: 14 cm<br />\r\nAlto: 18 cm</span></p>\r\n', '<p><span style=\"font-size:14px;\">Garantia: 03 meses</span></p>\r\n', '2018-11-14 17:44:36', '2018-11-14 18:08:02'),
(170, 0, 7, 25, NULL, 17, 'jabaquara,gravi', 'travesseiro-lottus-morpheus', 'Travesseiro Lottus Morpheus ', 'mg-7348_20181114160521K0HLFGDn5z.jpg', 'Conforto Macio', '', '100% Poliuretano', '', 3, 'FABRICANTE GENERAL INJECTION', '<p><span style=\"font-size:14px;\">Trata-se de um produto &uacute;nico. desenvolvido com refer&ecirc;ncias encontradas no Oriente M&eacute;dio.Seus elementos come&ccedil;am com a uni&atilde;o do tecido sedoso e ultra fiber, fibra especial, ultra fina; que confere toque super sedoso e confort&aacute;vel.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Para dar o suporte ideal foi desenvolvido o Memory Foam de alto peso e super macio. Sua estrutra macia conforta a cabe&ccedil;a na altura ideal para qualquer pessoa.&nbsp;</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Embalagem: Silk PVC Maleta&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Capa: 100% Poli&eacute;ster</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Enchimento capa: Fibra siliconizada 100% Poli&eacute;ster</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Espuma: 100% Poliuretano</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Dimens&atilde;o: 50x70 cm</span></p>\r\n', '<p><span style=\"font-size:14px;\">Garantia: 03 meses</span></p>\r\n', '2018-11-14 18:05:22', '2018-11-14 18:31:40'),
(172, 0, 7, 25, NULL, 17, 'jabaquara,gravi', 'travesseiro-o-legitimo-visco-plumas-alternativas', 'Travesseiro O Legitimo Visco + Plumas Alternativas ', 'encarte-travesseiro-visco_20181114164237p24UXdM4d9.jpg', 'Conforto Firme', '', 'Visco + Plumas', '', 1, 'FABRICANTE GENERAL INJECTION', '<p><span style=\"font-size:14px;\">L&acirc;mina de Visco: Uma l&acirc;mina de Memory Foam de 01 cm super macia e firme que absorve o rosto, a&nbsp;transpira&ccedil;&atilde;o e ajuda no tratamento de pessoas com bruxismo, pois n&atilde;o preciona o maxilar</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Plumas Alternativas: S&atilde;o t&eacute;rmicas e ocas e se modificam de acordo com a temperatura ambiente, enchendo no inverno e esvaziando nas temperaturas altas, deixam o travesseiro sempre super agrad&aacute;vel</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Tecido: 100% Algod&atilde;o com Matelass&ecirc;&nbsp;</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Dimens&atilde;o: 50x70 cm</span></p>\r\n', '<p><span style=\"font-size:14px;\">Garantia: 03 meses</span></p>\r\n', '2018-11-14 18:42:39', '2018-11-14 18:42:39'),
(173, 0, 7, 25, NULL, 17, 'jabaquara,gravi', 'travesseiro-o-legitimo-latex-touch-plumas-alternativas', 'Travesseiro O Legitimo Latex Touch & Plumas Alternativas ', 'encarte-o-legitimo-latex_20181114165708K1PaWbYl03.jpg', 'Conforto Firme', '', 'Latex Touch + Plumas ', '', 1, 'FABRICANTE GENERAL INJECTION&nbsp;', '<p><span style=\"font-size:14px;\">Latex Touch: Uma l&acirc;mina de 01 cm de PU Latex Touch que permite colocar seu rosto sobre um travesseiro super macio, firme e de retorno r&aacute;pido. Isso significa que o travesseiro trata voc&ecirc; de acordo com o seu peso, se adaptando ao seu biotipo.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Plumas Alternativas: S&atilde;o t&eacute;rmicas e ocas e se modificam de acordo com a temperatura ambiente, enchendo no inverno e esvaziando nas temperaturas altas, deixam o travesseiro sempre super agrad&aacute;vel</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Tecido: 100% Algod&atilde;o com Matelass&ecirc;&nbsp;</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Dimens&atilde;o: 50x70 cm</span></p>\r\n', '<p><span style=\"font-size:14px;\">Garantia: 03 meses</span></p>\r\n', '2018-11-14 18:57:10', '2018-11-14 18:57:10'),
(176, 18, 1, 1, NULL, 7, 'jabaquara,gravi', 'affection', 'Affection®', 'affection_20181127154643ujuJET8ULn.png', 'Conforto Firme', 'Espessura 30 cm', 'Molas Ensacadas 2X ACTION', '', 1, 'FABRICANTE SANKONFORT', '<p><span style=\"font-size:14px;\">Colch&atilde;o desenvolvido para quem busca alto nivel de conforto e suporte adequado para o corpo respeitando sua curvatura adaptando-se aos contornos do seu biotipo.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Molas 2X ACTION 2.0: Molejo com duplo sistema e maior contagem de molas por m&sup2; oferecendo mais conforto e tecnologia avan&ccedil;ada. Segue o alto padr&atilde;o de exel&ecirc;ncia em fabrica&ccedil;&atilde;o, todo o processo &eacute; minunciosamente estudado e planejado para proporcionar as melhores solu&ccedil;&otilde;es para o descanso e qualidade do sono.</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Estagio de Conforto proporcionado pelo alto nivel de qualidade do molejo que se comprime na parte superior, sua performace oferece toque confort&aacute;vel.</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Est&aacute;gio de individualidade tecnologia inovadora onde as Molas s&atilde;o Ensacadas uma a uma e trabalham Individualmente permitindo que os casais mesmo com pesos muito diferentes sejam suportados de maneira exclusiva.</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Colch&atilde;o com sistema de espuma de borda. </span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong><span style=\"font-size:14px;\">O tampo &eacute; bordado com tecido Camomila com camada de Espuma Viscoel&aacute;stico MR D80. Os tecidos de acabamento &quot;Camomile&quot;, feitos a partir de extratos de Camomila, aliviam o stress e promovem um sono profundo.&nbsp;</span></strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Colch&atilde;o com com tecido malha (80% Poli&eacute;ster e 20% Viscose de Bambu) exclusivo, que proporciona toque suave e extremamente firme com uma camada extra de conforto.</span></p>\r\n', '<p><span style=\"font-size:14px;\">Colch&atilde;o com 30&nbsp;cm de altura</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura do conjunto: 56 cm</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Suporte de peso de at&eacute; 120kg por pessoa&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Consulte valores para medidas especiais&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n', '2018-11-27 17:46:45', '2018-11-27 18:25:19'),
(178, 40, 1, 1, NULL, 15, 'jabaquara,gravi', 'lincoln', 'Lincoln', '714-lin-100_20181130183138g7nrmQtg6e.jpg', 'Conforto Firme', 'Espessura 32 cm', 'Molejo Comfort Core Extreme®', '', 1, 'FABRICANTE EASTMAN HOUSE', '<p><span style=\"font-size:14px;\">A linha Comfort Core Extreme&reg; proporciona maior firmeza comprovada em molas ensacadas. A tecnologia com tripla tempera empregada na produ&ccedil;&atilde;o, mant&eacute;m as propriedades do molejo com maior firmeza, estabilidade e durabilidade.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Espuma de poliuretano de densidade D33, proporciona um excelente suporte para pessoas com 90 a 110Kg, e d&aacute; ao colch&atilde;o a caracter&iacute;stica de conforto firme.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Malha com fibras de Tencel. Um produto ecol&oacute;gico 30% mais resistente que o algod&atilde;o, tem um toque suave e diferenciado das malhas convencionais.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\"><strong>PRE&Ccedil;O DO BOX REFERENTE AO TECIDO CAMUR&Ccedil;A</strong></span></p>\r\n', '<p><span style=\"font-size:14px;\">Colch&atilde;o com 32 cm de altura&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura do conjunto: 69 cm&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Suporte de peso de at&eacute; 110kg por pessoa</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Consulte valores para medidas especiais</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n', '2018-11-30 20:31:39', '2018-11-30 20:44:36'),
(179, 41, 1, 1, NULL, 15, 'jabaquara,gravi', 'washington', 'Washington ', '715-wash-1000_201811301844213IOdX7F52r.jpg', 'Conforto Firme', 'Espessura 32 cm', 'Molejo Dual Action™', '', 1, 'FABRICANTE EASTMAN HOUSE&nbsp;', '<p><span style=\"font-size:14px;\">A tecnologia Dual Action&trade; &eacute; composta por molas ensacadas individualmente com 18cm de altura, e com um grupo de molas 48% mais r&iacute;gidas no centro do colch&atilde;o, na regi&atilde;o compat&iacute;vel com o quadril, onde se concentram at&eacute; 46% do peso corporal.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Espuma de poliuretano de densidade D45, proporciona um excelente suporte para pessoas com at&eacute; 150Kg, dando ao colch&atilde;o a caracter&iacute;stica de conforto extra-firme.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Malha com fibras de Tencel. Um produto ecol&oacute;gico 30% mais resistente que o algod&atilde;o, tem um toque suave e diferenciado das malhas convencionais.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\"><strong>PRE&Ccedil;O DO BOX REFERENTE AO TECIDO CAMUR&Ccedil;A</strong></span></p>\r\n', '<p><span style=\"font-size:14px;\">Colch&atilde;o com 32&nbsp;cm de altura&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura do conjunto: 69 cm&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Suporte de peso de at&eacute; 150kg por pessoa</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Consulte valores para medidas especiais</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n', '2018-11-30 20:44:21', '2018-11-30 20:51:00'),
(180, 0, 6, 36, NULL, 24, 'jabaquara,gravi', 'cama-new-revenus', 'Cama New Revenus ', 'canvas-1_20190119145803iMktdl6plT.jpg', 'Madeira Eucalipto ', '', 'Madeira Natural', '', 0, '', '<p><span style=\"font-size:14px;\">MADEIRA LYPTUS: Lyptus&reg; &eacute; uma madeira nobre, totalmente extra&iacute;da de florestas renov&aacute;veis a partir de &aacute;rvores plantadas, o que assegura um suprimento confi&aacute;vel e ambientalmente sustent&aacute;vel.&nbsp;</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Para cada &aacute;rvore derrubada, uma outra &eacute; replantada, preservando as matas e toda a sua biodiversidade. Al&eacute;m de ser ecologicamente correta, a madeira Lyptus&reg; &eacute; desenvolvida com o cruzamento de &aacute;rvores selecionadas, que lhe conferem mais versatilidade, durabilidade e beleza. </span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Assim voc&ecirc; pode optar por uma madeira nobre, sem correr o risco de agredir o meio ambiente e sem pagar mais por isso, porque, apesar de todo o estudo, o manejo e a dedica&ccedil;&atilde;o que o Lyptus&reg; exige, isso n&atilde;o se reflete no ser valor final para o consumidor, tendo um custo equivalente ao de outras madeiras nobres.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Acabamento da Madeira: Natural</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Bitola dos p&eacute;s: 6x9 cm</span></p>\r\n', '<p><span style=\"font-size:14px;\">Garantia: 06 meses</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Consulte valores para medidas especiais</span></p>\r\n', '2019-01-19 16:58:04', '2019-01-19 17:06:26');
INSERT INTO `produtos` (`id`, `ordem`, `secao_id`, `categoria_id`, `subcategoria_id`, `marca_id`, `lojas`, `slug`, `titulo`, `capa`, `material`, `linha`, `legenda`, `codigo`, `conforto`, `resumo`, `descricao`, `dados_complementares`, `created_at`, `updated_at`) VALUES
(181, 0, 6, 37, NULL, 24, 'jabaquara,gravi', 'beliche-new-revenus', 'Beliche New Revenus', 'canvas_201901191508553MI27b2gHi.jpg', 'Madeira Eucalipto ', '', 'Madeira Natural', '', 0, '', '<p><span style=\"font-size:14px;\">MADEIRA LYPTUS: Lyptus&reg; &eacute; uma madeira nobre, totalmente extra&iacute;da de florestas renov&aacute;veis a partir de &aacute;rvores plantadas, o que assegura um suprimento confi&aacute;vel e ambientalmente sustent&aacute;vel.&nbsp;</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Para cada &aacute;rvore derrubada, uma outra &eacute; replantada, preservando as matas e toda a sua biodiversidade. Al&eacute;m de ser ecologicamente correta, a madeira Lyptus&reg; &eacute; desenvolvida com o cruzamento de &aacute;rvores selecionadas, que lhe conferem mais versatilidade, durabilidade e beleza.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Assim voc&ecirc; pode optar por uma madeira nobre, sem correr o risco de agredir o meio ambiente e sem pagar mais por isso, porque, apesar de todo o estudo, o manejo e a dedica&ccedil;&atilde;o que o Lyptus&reg; exige, isso n&atilde;o se reflete no ser valor final para o consumidor, tendo um custo equivalente ao de outras madeiras nobres.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Acabamento da Madeira: Natural</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Bitola dos p&eacute;s: 6x9 cm</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Medida Interna: 0,80x1,90 cm<br />\r\n0,90x1,90 cm</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Medida Externa: 2,02 cm</span></p>\r\n', '<p><span style=\"font-size:14px;\">Garantia: 06 meses</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Consulte valores para medidas especiais</span></p>\r\n', '2019-01-19 17:08:56', '2019-01-19 17:11:09'),
(182, 17, 1, 1, NULL, 6, 'jabaquara,gravi', 'touch', 'Touch', 'colchao-touch_20190430161044eILSnVDqHz.jpg', 'Conforto Macio', 'Espessura 38 cm', 'Duplo Molejo MaxSpring + Ensacadas', '', 3, 'FABRICANTE HERVAL', '<p><span style=\"font-size:14px;\">Duplo Molejo MaxSpring + Ensacadas: A duplicidade de molejos repassa ao colch&atilde;o um diferencial muito especial, pois une o conforto e resili&ecirc;ncia das Molas Maxspring e com a suavidade das Molas Ensacadas, formando um conjunto &uacute;nico.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Pillow Top One Side: Camada de espuma localizada na parte superior do colch&atilde;o Herval, sobre o molejo, proporcionando excelente conforto ao usu&aacute;rio. Por sua concep&ccedil;&atilde;o, na sua manuten&ccedil;&atilde;o n&atilde;o h&aacute; necessidade de virar o colch&atilde;o, apenas girando-o de vez em quando.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Espuma HR: Espuma Herval de elevada capacidade de sustenta&ccedil;&atilde;o/suporte, recuperando sua forma original mais rapidamente do que as demais espumas. Seu toque suave e &ldquo;el&aacute;stico&rdquo;, representa mais conforto, suporte e uniformidade na distribui&ccedil;&atilde;o de peso.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Espuma L&aacute;tex Natural: Espuma nobre, importada da B&eacute;lgica, possui propriedades naturais anti-microbianas e bactericida. Possui excelente elasticidade e suporte corporal, n&atilde;o deformando com o uso devido a sua alta resili&ecirc;ncia. Proporciona sensa&ccedil;&atilde;o extremamente agrad&aacute;vel, seca e fresca.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Forro Antiderrapante: O Tecido antiderrapante impede que o colch&atilde;o se movimente sobre a base, mantendo maior integridade do conjunto, aumentando a sua durabilidade.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Composi&ccedil;&atilde;o do tecido:&nbsp;Tampo tecido Malha Degrade: (82% Poli&eacute;ster/18% Viscose)</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Faixa tecido Linho: (55% Algod&atilde;o/45% Poli&eacute;ste)</span></p>\r\n', '<p><span style=\"font-size:14px;\">Colch&atilde;o com 38 cm de altura</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Suporte de peso de at&eacute; 110kg por pessoa</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: Molejo: 1 ano<br />\r\nDemais componentes:&nbsp;180 dias</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n\r\n<p>&nbsp;</p>\r\n', '2019-04-30 19:10:45', '2019-04-30 19:55:20'),
(183, 16, 1, 1, NULL, 6, 'jabaquara,gravi', 'rest', 'Rest', 'colchao-rest_20190430161949UNtZ89HItj.jpg', 'Conforto Macio', 'Espessura 34 cm', 'Molas Ensacadas Individualmente', '', 3, 'FABRICANTE HERVAL', '<p><span style=\"font-size:14px;\">Molas Ensacadas: Molas fabricadas em a&ccedil;o temperado com alto teor de carbono, unidas em grupos e ensacadas individualmente. Sistema de conforto com &oacute;tima adapta&ccedil;&atilde;o ao bi&oacute;tipo do usu&aacute;rio. Proporciona conforto suave e duradouro para casais, com redu&ccedil;&atilde;o da percep&ccedil;&atilde;o dos movimentos individuais, de um ou do outro.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Pillow Top One Side: Camada de espuma localizada na parte superior do colch&atilde;o Herval, sobre o molejo, proporcionando excelente conforto ao usu&aacute;rio. Por sua concep&ccedil;&atilde;o, na sua manuten&ccedil;&atilde;o n&atilde;o h&aacute; necessidade de virar o colch&atilde;o, apenas girando-o de vez em quando.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Malha: As malhas utilizadas pela Herval em seus colch&otilde;es caracterizam-se por seu toque macio, produzindo uma sensa&ccedil;&atilde;o maior de conforto, liberdade de movimentos durante o sono, al&eacute;m de &oacute;timo acabamento, beleza e sofistica&ccedil;&atilde;o ao colch&atilde;o.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Forro Antiderrapante: O Tecido antiderrapante impede que o colch&atilde;o se movimente sobre a base, mantendo maior integridade do conjunto, aumentando a sua durabilidade.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Composi&ccedil;&atilde;o do tecido:&nbsp;Tampo tecido Malha Stretch e Antimicrobiana: (80% Poli&eacute;ster/20% Viscose)</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Faixa tecido Linho: (55% Algod&atilde;o/45% Poli&eacute;ster)</span></p>\r\n', '<p><span style=\"font-size:14px;\">Colch&atilde;o com 34&nbsp;cm de altura</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Suporte de peso de at&eacute; 110kg por pessoa</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: Molejo: 1 ano<br />\r\nDemais componentes:&nbsp;180 dias</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n', '2019-04-30 19:19:50', '2019-04-30 19:54:45'),
(184, 15, 1, 1, NULL, 6, 'jabaquara,gravi', 'dreams', 'Dreams', 'colchao-dreams_20190430164437hoFlrHwHE8.jpg', 'Conforto Macio', 'Espessura 36 cm', 'Molas Ensacadas Individualmente', '', 3, 'FABRICANTE HERVAL', '<p><span style=\"font-size:14px;\">Molas Ensacadas: Molas fabricadas em a&ccedil;o temperado com alto teor de carbono, unidas em grupos e ensacadas individualmente. Sistema de conforto com &oacute;tima adapta&ccedil;&atilde;o ao bi&oacute;tipo do usu&aacute;rio. Proporciona conforto suave e duradouro para casais, com redu&ccedil;&atilde;o da percep&ccedil;&atilde;o dos movimentos individuais, de um ou do outro.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Pillow Top One Side: Camada de espuma localizada na parte superior do colch&atilde;o Herval, sobre o molejo, proporcionando excelente conforto ao usu&aacute;rio. Por sua concep&ccedil;&atilde;o, na sua manuten&ccedil;&atilde;o n&atilde;o h&aacute; necessidade de virar o colch&atilde;o, apenas girando-o de vez em quando.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Espuma HR Special: Espuma Herval de maior resili&ecirc;ncia, maciez e capacidade de sustenta&ccedil;&atilde;o. Por isso, seu nome &eacute; HR Special. Especial em tudo: na sustenta&ccedil;&atilde;o, no retorno a sua forma original (resili&ecirc;ncia), toque suave, macio e extremo conforto.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Espuma L&aacute;tex Natural: Espuma nobre, importada da B&eacute;lgica, possui propriedades naturais anti-microbianas e bactericida. Possui excelente elasticidade e suporte corporal, n&atilde;o deformando com o uso devido a sua alta resili&ecirc;ncia. Proporciona sensa&ccedil;&atilde;o extremamente agrad&aacute;vel, seca e fresca.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Forro Antiderrapante: O Tecido antiderrapante impede que o colch&atilde;o se movimente sobre a base, mantendo maior integridade do conjunto, aumentando a sua durabilidade.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Composi&ccedil;&atilde;o do tecido:&nbsp;Tampo tecido Malha Degrade: (82% Poli&eacute;ster/18% Viscose)</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Faixa tecido Linho: (55% Algod&atilde;o/ 45% Poli&eacute;ster)</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Faixinha tecido Malha Antimicrobiana: (83% Poli&eacute;ster /17% Viscose)</span></p>\r\n', '<p><span style=\"font-size:14px;\">Colch&atilde;o com 36&nbsp;cm de altura</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Suporte de peso de at&eacute; 120kg por pessoa</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: Molejo: 1 ano<br />\r\nDemais componentes:&nbsp;180 dias</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n', '2019-04-30 19:44:38', '2019-04-30 19:54:06'),
(185, 14, 1, 1, NULL, 6, 'jabaquara,gravi', 'sleep', 'Sleep', 'colchao-sleep_20190430165056COkbHjmNMc.jpg', 'Conforto Intermediário', 'Espessura 34,5 cm', 'Molas Maxspring', '', 2, 'FABRICANTE HERVAL', '<p><span style=\"font-size:14px;\">Molas Maxspring: Maxspring &eacute; marca pr&oacute;pria Herval de sistema de molejo cont&iacute;nuo. Um corpo &uacute;nico, monobloco, de molas entrela&ccedil;adas, em a&ccedil;o, submetido a processo de al&iacute;vio de tens&atilde;o. Tecnologia Europeia, que resulta em resili&ecirc;ncia uniforme e perene. Benef&iacute;cio: alto suporte ao colch&atilde;o e conforto cont&iacute;nuo a seus usu&aacute;rios, mesmo com o passar do tempo.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Pillow Top One Side: Camada de espuma localizada na parte superior do colch&atilde;o Herval, sobre o molejo, proporcionando excelente conforto ao usu&aacute;rio. Por sua concep&ccedil;&atilde;o, na sua manuten&ccedil;&atilde;o n&atilde;o h&aacute; necessidade de virar o colch&atilde;o, apenas girando-o de vez em quando.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Malha: As malhas utilizadas pela Herval em seus colch&otilde;es caracterizam-se por seu toque macio, produzindo uma sensa&ccedil;&atilde;o maior de conforto, liberdade de movimentos durante o sono, al&eacute;m de &oacute;timo acabamento, beleza e sofistica&ccedil;&atilde;o ao colch&atilde;o.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Espuma L&aacute;tex Natural: Espuma nobre, importada da B&eacute;lgica, possui propriedades naturais anti-microbianas e bactericida. Possui excelente elasticidade e suporte corporal, n&atilde;o deformando com o uso devido a sua alta resili&ecirc;ncia. Proporciona sensa&ccedil;&atilde;o extremamente agrad&aacute;vel, seca e fresca.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Forro Antiderrapante: O Tecido antiderrapante impede que o colch&atilde;o se movimente sobre a base, mantendo maior integridade do conjunto, aumentando a sua durabilidade.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Composi&ccedil;&atilde;o do tecido:&nbsp;Tampo tecido Malha Stretch e Antimicrobiana: (80% Poli&eacute;ster/ 20% Viscose)</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Faixa tecido Linho: (55% Algod&atilde;o/ 45% Poli&eacute;ster)</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Faixinha tecido Jacquard: (100% Poli&eacute;ster)</span></p>\r\n', '<p><span style=\"font-size:14px;\">Colch&atilde;o com 34,5&nbsp;cm de altura</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Suporte de peso de at&eacute; 110kg por pessoa</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: Molejo: 1 ano<br />\r\nDemais componentes:&nbsp;180 dias</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n', '2019-04-30 19:50:57', '2019-04-30 20:22:58'),
(186, 13, 1, 1, NULL, 23, 'jabaquara,gravi', 'duo-soft', 'Duo Soft ', 'duo-soft-002_20190502182848zQC9MgBm7X.png', 'Conforto Macio', 'Espessura 30 cm', 'Molejo Comfort Core Dual Action™', '', 3, 'FABRICANTE MGA&nbsp;', '<p><span style=\"font-size:14px;\">Molejo Comfort Core Dual Action&trade; 532 2.0 (282 molas p/ m&sup2;)</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Espuma Motion Fit&trade;</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Pillow-Top</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Borda Edge&trade;</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Tecido em malha com fibras de bambu</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Registro Inmetro 005691/2017</span></p>\r\n', '<p><span style=\"font-size:14px;\">Colch&atilde;o com 30 cm de altura&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura do conjunto: 67 cm&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Suporte de peso de at&eacute; 120kg por pessoa</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Consulte valores para medidas especiais</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Consulte valores para colch&atilde;o + box</span></p>\r\n', '2019-05-02 21:28:50', '2019-05-02 21:44:45'),
(187, 12, 1, 1, NULL, 23, 'jabaquara,gravi', 'duo-firm', 'Duo Firm', 'duo-firm_20190502183743eZZzMxiD2R.png', 'Conforto Firme', 'Espessura 30 cm', 'Molejo Comfort Core Dual Action™', '', 1, 'FABRICANTE MGA&nbsp;', '<p><span style=\"font-size:14px;\">Molejo Comfort Core Dual Action&trade; 532 2.0 (282 molas p/ m&sup2;)</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Espuma D45</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Pillow-Top</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Borda Edge&trade;</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Tecido em malha com fibras de bambu</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Registro Inmetro 005691/2017</span></p>\r\n', '<p><span style=\"font-size:14px;\">Colch&atilde;o com 30 cm de altura&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura do conjunto: 67 cm&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Suporte de peso de at&eacute; 120kg por pessoa</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Consulte valores para medidas especiais</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Consulte valores para colch&atilde;o + box</span></p>\r\n', '2019-05-02 21:37:45', '2019-05-02 21:44:27'),
(189, 11, 1, 38, NULL, 12, 'jabaquara,gravi', 'ferraro-latex', 'Ferraro - Látex', 'bestplush-det-01_20190530194400ao57mmUd9V.png', 'Conforto Macio', 'Espessura 34 cm', 'Molas Ensacadas Individualmente', '', 3, 'FABRICANTE DUNLOPILLO&nbsp;', '<p><span style=\"font-size:14px;\">&quot;O COLCH&Atilde;O FERRARO&nbsp;FOI DESENVOLVIDO PARA UNIR O QUE H&Aacute; DE MELHOR PARA SEU CONFORTO. UNIMOS O INCOMPAR&Aacute;VEL L&Aacute;TEX TALALAY COM MOLAS ENSACADAS, UM TOQUE EXTRA MACIO. SINTA ESSE ABRA&Ccedil;O.&quot;</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">L&Aacute;TEX DUNLOPILLO TALALAY: TRAZ O INCOMPAR&Aacute;VEL CONFORTO DO L&Aacute;TEX TALALAY.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">MOLAS ENSACADAS: EXCLUSIVO MOLEJO OFERECE SUPORTE INDEPENDENTE.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">COMPOSI&Ccedil;&Atilde;O DIFERENCIADA: OFERECE TOQUE EXTRA MACIO.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">ESPUMAS ESPECIAIS: ESPUMA VISCOEL&Aacute;STICA + HIPERSOFT OFERECEM CONFORTO DIFERENCIADO.</span></p>\r\n', '<p><span style=\"font-size:14px;\">Colch&atilde;o com 34 cm de altura&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Suporte de peso de at&eacute; 120kg por pessoa</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Consulte valores para medidas especiais</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Consulte valores para colch&atilde;o + box</span></p>\r\n', '2019-05-30 22:44:02', '2019-05-30 23:24:25'),
(190, 10, 1, 38, NULL, 12, 'jabaquara,gravi', 'dickens-latex', 'Dickens - Látex', 'theplace-det-01_20190530195343PjXcCiVLKH.png', 'Conforto Intermediário', 'Espessura 36 cm', 'Molejo Inteligente ', '', 2, 'FABRICANTE DUNLOPILLO', '<p><span style=\"font-size:14px;\">&quot;DICKENS &Eacute; FEITO COM UM MOLEJO INTELIGENTE, IDEAL PARA QUEM PROCURA SUPORTE E CONFORTO. ELE AINDA CONTA COM UM TECIDO QUE POSSUI AMETISTA NA SUA TRAMA, CONHECIDA POR SUAS PROPRIEDADES ANTI-STRESS.&quot;</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">L&Aacute;TEX DUNLOPILLO TALALAY: TRAZ O INCOMPAR&Aacute;VEL CONFORTO DO L&Aacute;TEX TALALAY.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">MOLEJO INTELIGENTE: DOIS MOLEJOS EM UM S&Oacute; CONJUNTO PROPORCIONAM SUPORTE E CONFORTO AO MESMO TEMPO.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">TECIDO AMETISTA: TRAMA DO COLCH&Atilde;O CONT&Eacute;M AMETISTA QUE AJUDA A ALIVIAR O STRESS.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">ESPUMAS ESPECIAIS: ESPUMA VISCOEL&Aacute;STICA + HIPERSOFT OFERECEM CONFORTO DIFERENCIADO.</span></p>\r\n', '<p><span style=\"font-size:14px;\">Colch&atilde;o com 36 cm de altura&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Suporte de peso de at&eacute; 120kg por pessoa</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Consulte valores para medidas especiais</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Consulte valores para colch&atilde;o + box</span></p>\r\n', '2019-05-30 22:53:45', '2019-05-30 23:24:35'),
(191, 9, 1, 38, NULL, 12, 'jabaquara,gravi', 'collins-latex', 'Collins - Látex', 'renovation-det-01_20190530200058RG7fJmabFj.png', 'Conforto Intermediário', 'Espessura 32 cm', 'Molas Ensacadas Individualmente', '', 2, 'FABRICANTE DUNLOPILLO', '<p><span style=\"font-size:14px;\">&quot;CONFORTO E FRESCOR. O COLCH&Atilde;O COLLINS ALIA O L&Aacute;TEX TALALAY COM UM TECIDO FEITO A PARTIR DE VISCOSE DE BAMBU QUE, AL&Eacute;M DE ANTIBACTERICIDA, AJUDA MANTER UMA TEMPERATURA FRESCA SEM ABRIR M&Atilde;O DE UM TOQUE SUPER MACIO.&quot;</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">L&Aacute;TEX DUNLOPILLO TALALAY: TRAZ O INCOMPAR&Aacute;VEL CONFORTO DO L&Aacute;TEX TALALAY.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">MOLAS ENSACADAS: EXCLUSIVO MOLEJO OFERECE SUPORTE INDEPENDENTE.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">TECIDO VISCOSE DE BAMBU: BACTERICIDA. AJUDA A MANTER A TEMPERATURA FRESCA E AINDA OFERECE UM TOQUE SUPER MACIO</span></p>\r\n', '<p><span style=\"font-size:14px;\">Colch&atilde;o com 32 cm de altura&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Suporte de peso de at&eacute; 120kg por pessoa</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Consulte valores para medidas especiais</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Consulte valores para colch&atilde;o + box</span></p>\r\n', '2019-05-30 23:01:00', '2019-05-30 23:24:51'),
(192, 8, 1, 38, NULL, 12, 'jabaquara,gravi', 'winstar-latex', 'Winstar - Látex', 'renascence-det-01_20190530201028xfdRGguicj.png', 'Conforto Firme', 'Espessura 36 cm', 'Molas Ensacadas Individualmente', '', 1, 'FABRICANTE DUNLOPILLO', '<p><span style=\"font-size:14px;\">&quot;IDEAL PARA QUEM BUSCA UM COLCH&Atilde;O MAIS FIRME, O WINSTAR&nbsp;CONTA COM 662 MOLAS ENSACADAS. AL&Eacute;M DO MOLEJO SUPERIOR, ELE AINDA POSSUI O BLUE L&Aacute;TEX, UMA CAMADA QUE CONTA COM MICROPART&Iacute;CULAS DE GEL PARA MANTER SUA TEMPERATURA FRESCA.&quot;</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">BLUE L&Aacute;TEX: CAMADA DE L&Aacute;TEX COM MICROPART&Iacute;CULAS DE GEL PARA MANTER A TEMPERATURA FRESCA.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">MOLEJO SUPERIOR: COLCH&Atilde;O COMPOSTO POR 662 MOLAS (NA MEDIDA 1,58M X 1,98M), PARA UM SUPORTE EXTRA REFOR&Ccedil;ADO.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">MOLAS ENSACADAS: EXCLUSIVO MOLEJO OFERECE SUPORTE INDEPENDENTE.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">DOUBLE SIDE: PODE SER USADO EM AMBOS OS LADOS PROLONGANDO ASSIM, A SUA VIDA &Uacute;TIL.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">TECIDO MALHA BELGA: PROPORCIONA SUAVIDADE AO DEITAR.</span></p>\r\n', '<p><span style=\"font-size:14px;\">Colch&atilde;o com 36 cm de altura&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Suporte de peso de at&eacute; 120kg por pessoa</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Consulte valores para medidas especiais</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Consulte valores para colch&atilde;o + box</span></p>\r\n', '2019-05-30 23:10:29', '2019-05-30 23:25:08'),
(193, 7, 1, 38, NULL, 12, 'jabaquara,gravi', 'vivaldi-latex', 'Vivaldi - Látex', 'memolatex-det-01_201905302018310wfkt56y2x.png', 'Conforto Intermediário', 'Espessura 31 cm', 'Molejo LFK ', '', 2, 'FABRICANTE DUNLOPILLO', '<p><span style=\"font-size:14px;\">&quot;A UNI&Atilde;O PARA O MELHOR CONFORTO. UNIMOS NO COLCH&Atilde;O VIVALDI AS TECNOLOGIAS DO MOLEJO LFK, ESPUMA VISCOEL&Aacute;STICA (NASA) E O INCOMPAR&Aacute;VEL L&Aacute;TEX TALALAY. UMA COMINA&Ccedil;&Atilde;O &Uacute;NICA.&quot;</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">L&Aacute;TEX DUNLOPILLO TALALAY: TR&Aacute;S O INCOMPAR&Aacute;VEL CONFORTO DO L&Aacute;TEX TALALAY.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">MOLEJO LFK: MUITO MAIS SUPORTE E CONFORTO.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">TECIDO COM FIOS DE SEDA: SUA CAMA COM UM TOQUE EXTRA MACIO.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">BORDADO THREE ZONE: CONCENTRA O BORDADO PARA &Aacute;REAS EM QUE MAIS PRECISAMOS DE SUPORTE.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">ESPUMA VISCOEL&Aacute;STICA: ALIVIA A PRESS&Atilde;O EXERCIDA PELO CORPO.</span></p>\r\n', '<p><span style=\"font-size:14px;\">Colch&atilde;o com 31 cm de altura&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Suporte de peso de at&eacute; 120kg por pessoa</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Consulte valores para medidas especiais</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Consulte valores para colch&atilde;o + box</span></p>\r\n', '2019-05-30 23:18:34', '2019-05-30 23:25:23'),
(194, 3, 1, 1, NULL, 8, 'jabaquara,gravi', 'platinum-bamboo-love-profusion', 'Platinum Bamboo Love Profusion', 'bamboo-love-profusion-baixa-158x198_20190912172901O6bcEbaa5J.png', 'Conforto Firme', 'Espessura 36 cm', 'Molas Pocketed', '', 1, 'FABRICANTE SIMMONS', '<p><span style=\"font-size:14px;\">Tecido da capa: Malha naturalmente hipoalerg&ecirc;nica com Viscose de Bambu e fios de carbono que proporciona um sono mais profundo e relaxante&nbsp;(84,8% Poli&eacute;ster/13% Viscose/2% Viscose Bambu e 0,19% Fio Intense)</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Tecido da faixa lateral: Admir&aacute;vel faixa lateral na cor cinza chumbo com detalhe prata entorno do colch&atilde;o (100% Poli&eacute;ster)</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Tecnologias: MemoSense&reg; System (Sistema de espumas tecnol&oacute;gicas (Visco+L&aacute;tex) de alta densidade que oferece excelente suporte), Malha de Bamboo. (A verdadeira&nbsp;malha bamboo &eacute; hipoalerg&ecirc;nica, tem bactericida natural e um toque fresco e suave), Beautyrest&reg; (Tecnologia de molas exclusivas Beautyrest&reg;, ideal para quem dorme a dois), Intense (Os fios de carbono liberam a energia est&aacute;tica do seu corpo proporcionando uma noite de sono mais relaxante), Molas Pocketed </span>&ndash;<span style=\"font-size:14px;\"> Firme</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Registro INMETRO: 004311/2017</span></p>\r\n', '<p><span style=\"font-size:14px;\">Colch&atilde;o com 36&nbsp;cm de altura&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura do conjunto: 75 cm&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Suporte de peso de at&eacute; 120kg por pessoa</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n', '2019-09-12 20:29:03', '2020-02-26 21:05:45'),
(195, 6, 1, 39, NULL, 8, 'jabaquara,gravi', 'hospitality-lfk', 'Hospitality LFK', 'hospitality_20200131161442hLLaK0ucfo.png', 'Conforto Intermediário', 'Espessura 25 cm', 'Molas LFK 2.2', '', 2, 'FABRICANTE SIMMONS', '<p><span style=\"font-size:14px;\">Tecido da capa: Tecido Jacquard (56% Poliéster, 44% Polipropileno) 130 g/m2m</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Tecnologias: Retardante de chamas (Tecido com tratamento retardante de chamas), Posturest (Tecnologia de molas exclusivas LFK, mais firme, resistente e est&aacute;vel), Molas LFK 2.2 &ndash; Extra Support (Tecnologia LFK em a&ccedil;o carbono com bitola de 2.2mm. 74% mais firme que o molejo Bonnell Tradicional. Estabilidade m&aacute;xima, durabilidade garantida, firmeza com m&aacute;ximo conforto)</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Colch&atilde;o dupla face:&nbsp;O seu colch&atilde;o DOUBLE SIDE (dois lados) foi desenvolvido para ser utilizado dos dois lados</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Registro INMETRO: 004181/2017</span></p>\r\n', '<p><span style=\"font-size:14px;\">Colch&atilde;o com 25 cm de altura&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura do conjunto: 59 cm&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Suporte de peso de at&eacute; 120kg por pessoa</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n', '2020-01-31 19:14:44', '2020-04-03 02:44:22'),
(196, 0, 7, 26, NULL, 13, 'jabaquara,gravi', 'suporte-lift', 'Suporte Lift', 'lift2_20200226193229X1Zf4WpSjm.png', 'Lançamento', '', 'Suporte para poltronas/cadeiras', '', 0, 'FABRICANTE THEVA', '<p><span style=\"font-size:14px;\">Independ&ecirc;ncia para idosos e pessoas com dificuldade para acessar e levantar de poltronas ou sof&aacute;s</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Possui altura, profundidade e largura ajust&aacute;veis:</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">PROFUNDIDADE: 50 cm a 62 cm</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">LARGURA: 46 cm a 64 cm</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">ALTURA DOS P&Eacute;S: 37 cm a 50 cm</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">ALTURA DOS BRA&Ccedil;OS: 21 cm a 28 cm</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">ALTURA TOTAL: 60 cm a 80 cm</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Suporta at&eacute; 120 Kg para apoio e movimentos de alavanca</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Possui grip emborrachado antiderrapante</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Port&aacute;til</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">F&aacute;cil de montar, n&atilde;o exige fura&ccedil;&atilde;o</span></p>\r\n', '<p><span style=\"font-size:14px;\">N&atilde;o expor o Suporte LIft ao sol sob risco de ressecamento do grip emborrachado</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 90 dias</span></p>\r\n', '2020-02-26 19:25:30', '2020-02-26 22:33:32'),
(197, 0, 7, 41, NULL, 13, 'jabaquara,gravi', 'almofada-duble-support', 'Almofada Duble Support', 'double-support_20200226163528VZk7o0Z5Hu.png', '', '', '10x20x40 cm', '', 0, 'FABRICANTE THEVA', '<p><span style=\"font-size:14px;\">Espuma viscoel&aacute;stica alivia os pontos de press&atilde;o </span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Tecido inteligente permite excelente passagem de ar </span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">El&aacute;stico facilita ajuste em encostos </span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Capa remov&iacute;vel e lav&aacute;vel</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Espuma viscoel&aacute;stica auxilia no relaxamento da coluna lombar</span></p>\r\n', '<p><span style=\"font-size:14px;\">Garantia: 90 dias</span></p>\r\n', '2020-02-26 19:35:29', '2020-02-26 19:35:29'),
(198, 0, 7, 41, NULL, 13, 'jabaquara,gravi', 'almofada-big', 'Almofada Big', 'big-lisa_20200226164140iyMNYULsq4.png', '', '', '13x40x48 cm', '', 0, 'FABRICANTE THEVA', '<p><span style=\"font-size:14px;\">Espuma viscoel&aacute;stica alivia os pontos de press&atilde;o</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Design anat&ocirc;mico apoia toda a coluna</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Proporciona postura correta ao sentar</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">El&aacute;stico facilita o ajuste em assentos</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Dispon&iacute;vel nas vers&otilde;es:<br />\r\nEstampada: Tecido viscose possui propriedades bactericidas, toque super suave e mantem a temperatura sempre fresca</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Lisa: Tecido inteligente permite a passagem de ar</span></p>\r\n', '<p><span style=\"font-size:14px;\">Garantia: 90 dias</span></p>\r\n', '2020-02-26 19:41:41', '2020-02-26 19:44:18'),
(199, 5, 1, 1, NULL, 8, 'jabaquara,gravi', 'aspen', 'Aspen ', 'wwk-dwfa_20200226175452qh4dO6dOV8.png', 'Conforto Firme', 'Espessura 27 cm', 'Molas Pocketed', '', 1, 'FABRICANTE SIMMONS', '<p><span style=\"font-size:14px;\">Tecido da capa: Malha de suave toque Bamboo Touch (87% Poli&eacute;ster/13% Viscose)</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Tecnologias: Beautyrest&reg; (ecnologia de molas exclusivas Beautyrest&reg;, ideal para quem dorme a dois), Bamboo Touch (Malha Bamboo Touch com Viscose que d&aacute; suavidade extrema ao toque), Espuma de alta densidade (Espuma de alta densidade que garante o suporte ideal para quem busca um colch&atilde;o firme), Molas Pocketed </span>&ndash;<span style=\"font-size:14px;\"> Firme</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Registro Inmetro 004311/2017</span></p>\r\n', '<p><span style=\"font-size:14px;\">Colch&atilde;o com 27 cm de altura&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura do conjunto: 66 cm&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Suporte de peso de at&eacute; 120kg por pessoa</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n', '2020-02-26 20:54:53', '2020-02-26 21:07:54'),
(200, 4, 1, 1, NULL, 8, 'jabaquara,gravi', 'platinum-bamboo-dreamer', 'Platinum Bamboo Dreamer', '8xwz-mda_20200226180508QgJNApVqHl.png', 'Conforto Firme', 'Espessura 36 cm', 'Molas Pocketed', '', 1, 'FABRICANTE SIMMONS', '<p><span style=\"font-size:14px;\">Tecido da capa: Malha naturalmente hipoalerg&ecirc;nica com Viscose de Bambu e fios de carbono que proporciona um sono mais profundo e relaxante. (84,8% Poli&eacute;ster/13% Viscose/2% Viscose Bambu e 0,19% Fio Intense)</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Tecnologias: MemoSense&reg; System (Sistema de espumas tecnol&oacute;gicas (Visco+L&aacute;tex) de alta densidade que oferece excelente suporte), Malha de Bamboo. (A verdadeira malha bamboo &eacute; hipoalerg&ecirc;nica, tem bactericida natural e um toque fresco e suave), Beautyrest&reg; (Tecnologia de molas exclusivas Beautyrest&reg;, ideal para quem dorme a dois), Intense (Os fios de carbono liberam a energia est&aacute;tica do seu corpo proporcionando uma noite de sono mais relaxante), Molas Pocketed &nbsp;&ndash;&nbsp;&nbsp;Firme</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Registro Inmetro 004311/2017</span></p>\r\n', '<p><span style=\"font-size:14px;\">Colch&atilde;o com 36 cm de altura&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura do conjunto: 75&nbsp;cm&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Suporte de peso de at&eacute; 120kg por pessoa</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa&nbsp;</span></p>\r\n', '2020-02-26 21:05:09', '2020-02-26 21:07:44'),
(201, 2, 1, 1, NULL, 8, 'jabaquara,gravi', 'platinum-bamboo-experience', 'Platinum Bamboo Experience', '8xwz-mda_2020022618130875pMEfiJ4I.png', 'Conforto Intermediário', 'Espessura 34 cm', ' Molas Pocketed', '', 2, 'FABRICANTE SIMMONS', '<p><span style=\"font-size:14px;\">Tecido da capa: Malha naturalmente hipoalerg&ecirc;nica com Viscose de Bambu e fios de carbono que proporciona um sono mais profundo e relaxante (84,8% Poli&eacute;ster/13% Viscose/2% Viscose Bambu e 0,19% Fio Intense)</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Tecnologias: L&aacute;tex (Special Comfort Latex que, al&eacute;m de confort&aacute;vel, permite maiores n&iacute;veis de ventila&ccedil;&atilde;o e suporte), Top Visco inside (Colch&atilde;o que se molda ao corpo e distribui seu peso uniformemente), Beautyrest&reg; (Tecnologia de molas exclusivas Beautyrest&reg;, ideal para quem dorme a dois), Visco Dry Gel (Melhor adequa&ccedil;&atilde;o ao frio e ao calor), Intense (Os fios de carbono liberam a energia est&aacute;tica do seu corpo proporcionando uma noite de sono mais relaxante),&nbsp;Molas Pocketed</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Registro INMETRO: 004311/2017</span></p>\r\n', '<p>Colch&atilde;o com 34 cm de altura&nbsp;</p>\r\n\r\n<p>Altura do conjunto: 73 cm&nbsp;</p>\r\n\r\n<p>Suporte de peso de at&eacute; 120kg por pessoa</p>\r\n\r\n<p>Garantia: 01 ano</p>\r\n\r\n<p>Foto meramente ilustrativa&nbsp;</p>\r\n', '2020-02-26 21:13:09', '2020-04-02 03:43:21'),
(202, 0, 5, 31, NULL, 18, 'jabaquara,gravi', 'mont-long', 'Mont Long', 'mont-long-1_20200226184113Xb4KCTioaj.jpg', 'Conforto Intermediário', '', 'Colchonete', '', 2, 'FABRICANTE FA', '<p><span style=\"font-size:14px;\">Enchimento: Fibras Diversas<br />\r\nTecido: Polipropileno</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Medida:<br />\r\nSolteiro: 060x190&nbsp;cm</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Casal: 130x190&nbsp;cm</span></p>\r\n', '<p><span style=\"font-size:14px;\">Garantia: 90 dias</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Foto meramente ilustrativa</span></p>\r\n', '2020-02-26 21:41:14', '2020-02-26 21:49:43'),
(203, 0, 5, 31, NULL, 18, 'jabaquara,gravi', 'pitangueiras', 'Pitangueiras', 'pitangueiras-1_202002261845343MKp3PDG2N.jpg', 'Conforto Intermediário', '', 'Colchonete+travesseiro', '', 2, 'FABRICANTE FA', '<p><span style=\"font-size:14px;\">Enchimento: Espuma Certificada</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Tecido Superior: Algod&atilde;o/&nbsp;Poli&eacute;ster</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Tecido Inferior: Courvin</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Medida: 058x178x4 cm</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">*Acompanha travasseiro*</span></p>\r\n', '<p><span style=\"font-size:14px;\">Garantia: 90 dias</span></p>\r\n', '2020-02-26 21:45:36', '2020-02-26 21:49:32'),
(204, 0, 5, 31, NULL, 18, 'jabaquara,gravi', 'kit-camping', 'Kit Camping', 'kit-camping-1_202002261848559pYbYPE0C3.jpg', 'Conforto Intermediário', '', 'Colchonete+travesseiro', '', 2, 'FABRICANTE FA', '<p><span style=\"font-size:14px;\">O Kit Camping &eacute; ideal para os aventureiros que gostam de acampar, seja no mato ou no quinta de casa</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Enchimento: Fibras Diversas</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Tecido: Polipropileno (TNT)</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Medida: 078x188 cm</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">*Acompanha travesseiro*</span></p>\r\n', '<p><span style=\"font-size:14px;\">Garantia: 90 dias</span></p>\r\n', '2020-02-26 21:48:57', '2020-02-26 21:49:11'),
(205, 0, 5, 31, NULL, 18, 'jabaquara,gravi', 'ginastica', 'Ginástica', 'ginastica-1_20200226185435E2e0uiTTlt.jpg', 'Conforto Firme', '', 'Colchonete para exercícios físicos', '', 1, 'FABRICANTE FA', '<p><span style=\"font-size:14px;\">Colchonete ideal para fazer exerc&iacute;cios f&iacute;sicos e pr&aacute;tico de limpar</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Espuma: Densidade 20</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Revestimento: Courvin Azul</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Medida: 066x094x3 cm&nbsp;</span></p>\r\n', '<p><span style=\"font-size:14px;\">Garantia: 90 dias</span></p>\r\n', '2020-02-26 21:54:37', '2020-02-26 22:01:16'),
(206, 0, 5, 31, NULL, 18, 'jabaquara,gravi', 'ginastica-pratico', 'Ginástica Pratico', 'ginastica-pratica-1_20200226190001ObHDrkCCCr.jpg', 'Conforto Firme', '', 'Colchonete para exercícios físicos', '', 1, 'FABRICANTE FA', '<p><span style=\"font-size:14px;\">Colchonete ideal para fazer exerc&iacute;cios f&iacute;sicos e pr&aacute;tico de limpar</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Enchimento: AG80</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Tecido: Courvin Preto&nbsp;</span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Medida: 045x085x3 cm</span></p>\r\n\r\n<p>&nbsp;</p>\r\n', '<p>Garantia: 90 dias</p>\r\n', '2020-02-26 22:00:03', '2020-02-26 22:00:03'),
(207, 0, 5, 31, NULL, 18, 'jabaquara,gravi', 'pratiksol', 'PratikSol', 'pratik-sol-1_20200226190449p9YE5tZRPJ.jpg', 'Conforto Intermediário', '', 'Dobrável e versátil', '', 2, 'FABRICANTE FA', '<p><span style=\"font-size:14px;\">Ideal para academia, camping e praia. Leve, pr&aacute;tico, resistente, com capa lav&aacute;vel, dobr&aacute;vel e com al&ccedil;a para transporte</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px;\">Medidas:<br />\r\nFechado: 046x055x10 cm<br />\r\nAberto: 055x141mx 3 cm</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Composi&ccedil;&atilde;o:<br />\r\nLado superior 95% Algod&atilde;o e 5% Poli&eacute;ster<br />\r\nLado inferior: 87% PVC e 13% Poli&eacute;ster<br />\r\nEnchimento: 100% Poliuretano</span></p>\r\n', '<p><span style=\"font-size:14px;\">Garantia: 90 dias</span></p>\r\n', '2020-02-26 22:04:50', '2020-02-26 22:04:50'),
(208, 0, 7, 22, NULL, 14, 'jabaquara,gravi', 'kids-nasa', 'Kids Nasa', 'kids-nasa_20200229162737UPYvtDUliI.jpg', 'Conforto Macio', '', 'Espuma Viscoelástica', 'BB3202', 3, 'FABRICANTE DUOFLEX', '<p><span style=\"font-size:14px;\">Nesse momento &eacute; indicado o uso de um travesseiro macio, similar ao de um adulto, por&eacute;m um pouco mais baixo, como &eacute; o caso do Kids Nasa Duoflex. A crian&ccedil;a deve ser orientada a dormir, preferencialmente, de lado e o travesseiro deve preencher completamente o espa&ccedil;o entre sua cabe&ccedil;a e o colch&atilde;o, alinhando assim sua coluna.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Capa: Malha 100% Algod&atilde;o com z&iacute;per</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Dimens&atilde;o: 45x65 cm</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura: 8 cm</span></p>\r\n', '<p><span style=\"font-size:14px;\">Garantia: 02 anos&nbsp;</span></p>\r\n', '2020-02-29 19:27:38', '2020-02-29 19:29:20'),
(209, 0, 7, 22, NULL, 14, 'jabaquara,gravi', 'classic-pillow', 'Classic Pillow', 'classic-pillow_20200229163724eZCMyWMmD3.jpg', 'Conforto Intermediário', '', 'Espuma Poliuretano', 'CL1100', 2, 'FABRICANTE DUOFLEX', '<p><span style=\"font-size:14px;\">A Duoflex tamb&eacute;m disponibiliza ao mercado, os travesseiros de espuma de poliuretano expandida, tamb&eacute;m conhecida como espuma fria.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">A linha de travesseiros de espuma fria oferece uma diversidade de modelos que atendem aos mais variados gostos e necessidades durante o sono.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">S&atilde;o travesseiros colados, flocados e/ou anat&ocirc;micos, extremamente confort&aacute;veis e produzidos com total prote&ccedil;&atilde;o anti&aacute;caros, fungos e bact&eacute;rias. Alguns modelos possuem tamb&eacute;m gomos massageadores que estimulam a circula&ccedil;&atilde;o.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Capa: Matelass&ecirc; - 100% Poli&eacute;ster</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Dimens&atilde;o: 50x70 cm</span></p>\r\n', '<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n', '2020-02-29 19:37:24', '2020-02-29 19:38:57'),
(210, 0, 7, 22, NULL, 14, 'jabaquara,gravi', 'gelflex', 'Gelflex', 'gelflex_20200229164237HDVaG0tsCE.jpg', 'Conforto Macio', '', 'Espuma Viscoelástica', 'GN1101', 3, 'FABRICANTE DUOFLEX', '<p><span style=\"font-size:14px;\">O GELflex NASA alia a suavidade e o conforto da espuma NASA ao frescor do GEL, proporcionando um toque ainda mais macio e extremamente gostoso.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Automold&aacute;vel e antipress&atilde;o, a Espuma Viscoel&aacute;stica Nasa adapta-se ao contorno do corpo, exercendo menor press&atilde;o e facilitando a circula&ccedil;&atilde;o sangu&iacute;nea.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">A inovadora manta de GEL, de consist&ecirc;ncia muito suave e conforto t&eacute;rmico &uacute;nico, proporciona um sono reparador com total relaxamento.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Gelflex Nasa &eacute; especialmente indicado para pessoas que apreciam o toque macio do NASA, por&eacute;m n&atilde;o abrem m&atilde;o de um travesseiro fresquinho, sendo produzido com total prote&ccedil;&atilde;o anti&aacute;caros, fungos e bact&eacute;rias.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Capa: Poliamida e Poli&eacute;ster</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Capa extra:Percal 200 fios 100/% Algod&atilde;o com z&iacute;per</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Dimens&atilde;o: 50x70 cm</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura: 14 cm</span></p>\r\n', '<p><span style=\"font-size:14px;\">Garantia: 02 anos</span></p>\r\n', '2020-02-29 19:42:38', '2020-02-29 19:43:33'),
(211, 0, 7, 22, NULL, 14, 'jabaquara,gravi', 'lavavel-baixo', 'Lavável Baixo', 'lavavel-baixo_20200229164805DPxhlplrqL.jpg', 'Conforto Intermediário', '', 'Espuma Poliuretano', 'LV3200', 2, 'FABRICANTE DUOFLEX', '<p><span style=\"font-size:14px;\">O travesseiro LAV&Aacute;VEL &eacute; composto por uma espuma especial de poliuretano, produzida com c&eacute;lulas abertas, que proporcionam maior frescor e ventila&ccedil;&atilde;o.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Sua estrutura aerada permite a lavagem e a secagem completa do travesseiro em m&aacute;quinas de uso dom&eacute;stico, garantindo um cheirinho de novo por muito mais tempo e uma maior vida &uacute;til, em compara&ccedil;&atilde;o com os travesseiros tradicionais.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Adicionalmente, a capa do LAV&Aacute;VEL &eacute; confeccionada em Dry Fresh, tecido 100% algod&atilde;o de alta performance, que tamb&eacute;m favorece a evapora&ccedil;&atilde;o mais r&aacute;pida da umidade, al&eacute;m de apresentar um toque extra macio e refrescante.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Produzido com total prote&ccedil;&atilde;o anti&aacute;caros, fungos e bact&eacute;rias, o travesseiro LAV&Aacute;VEL garante um sono muito mais saud&aacute;vel!</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Capa: Dry Fresh 100% Algod&atilde;o</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Dimens&atilde;o: 45x65 cm</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura: 8 cm</span></p>\r\n', '<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n', '2020-02-29 19:48:06', '2020-02-29 19:49:34'),
(212, 0, 7, 22, NULL, 14, 'jabaquara,gravi', 'nasa-x-alto', 'Nasa X Alto', 'nasa-x-alto_202002291653555n3pCJfeAw.jpg', 'Conforto Macio', '', 'Espuma Viscoelástica', 'NS3100', 3, 'FABRICANTE DUOFLEX', '<p><span style=\"font-size:14px;\">NASA &eacute; uma Espuma Viscoel&aacute;stica moldada com alta viscosidade e densidade. Automold&aacute;vel e termossens&iacute;vel, adapta-se ao contorno e a temperatura do corpo, exercendo menos press&atilde;o nas &aacute;reas mais quentes ou salientes, facilitando a circula&ccedil;&atilde;o sangu&iacute;nea.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Al&eacute;m de oferecer maior capacidade de absor&ccedil;&atilde;o do peso e distribui&ccedil;&atilde;o equalizada da press&atilde;o sobre o corpo, o travesseiro NASA possui a propriedade de retorno lento ou mem&oacute;ria, agindo como um verdadeiro amortecedor.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">A alta qualidade da espuma viscoel&aacute;stica Duoflex, permite ao travesseiro NASA retornar &agrave; sua forma original, mesmo ap&oacute;s compress&atilde;o durante o transporte.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Tudo isso torna o travesseiro NASA o ideal para a posi&ccedil;&atilde;o natural de descanso, prevenindo dores na cabe&ccedil;a, pesco&ccedil;o, costas, bra&ccedil;os e ombros, garantindo um sono restaurador, com total prote&ccedil;&atilde;o anti&aacute;caros, fungos e bact&eacute;rias.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Capa: Malha 100% Algod&atilde;o</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Dimens&atilde;o: 50x70 cm</span></p>\r\n\r\n<p>&nbsp;</p>\r\n', '<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n', '2020-02-29 19:53:56', '2020-02-29 19:53:56');
INSERT INTO `produtos` (`id`, `ordem`, `secao_id`, `categoria_id`, `subcategoria_id`, `marca_id`, `lojas`, `slug`, `titulo`, `capa`, `material`, `linha`, `legenda`, `codigo`, `conforto`, `resumo`, `descricao`, `dados_complementares`, `created_at`, `updated_at`) VALUES
(213, 0, 7, 22, NULL, 14, 'jabaquara,gravi', 'nasa-x', 'Nasa X ', 'nasa-x_202002291656560v11YtOz2w.jpg', 'Conforto Macio', '', 'Espuma Viscoelástica', 'NS3200', 3, 'FABRICANTE DUOFLEX', '<p><span style=\"font-size:14px;\">NASA &eacute; uma Espuma Viscoel&aacute;stica moldada com alta viscosidade e densidade. Automold&aacute;vel e termossens&iacute;vel, adapta-se ao contorno e a temperatura do corpo, exercendo menos press&atilde;o nas &aacute;reas mais quentes ou salientes, facilitando a circula&ccedil;&atilde;o sangu&iacute;nea.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Al&eacute;m de oferecer maior capacidade de absor&ccedil;&atilde;o do peso e distribui&ccedil;&atilde;o equalizada da press&atilde;o sobre o corpo, o travesseiro NASA possui a propriedade de retorno lento ou mem&oacute;ria, agindo como um verdadeiro amortecedor.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">A alta qualidade da espuma viscoel&aacute;stica Duoflex, permite ao travesseiro NASA retornar &agrave; sua forma original, mesmo ap&oacute;s compress&atilde;o durante o transporte.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Tudo isso torna o travesseiro NASA o ideal para a posi&ccedil;&atilde;o natural de descanso, prevenindo dores na cabe&ccedil;a, pesco&ccedil;o, costas, bra&ccedil;os e ombros, garantindo um sono restaurador, com total prote&ccedil;&atilde;o anti&aacute;caros, fungos e bact&eacute;rias.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Capa: Malha 100% Algod&atilde;o</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Dimens&atilde;o: 45x65&nbsp;cm</span></p>\r\n', '<p><span style=\"font-size:14px;\">Garantia: 01 ano</span></p>\r\n', '2020-02-29 19:56:57', '2020-02-29 19:56:57'),
(214, 0, 7, 22, NULL, 14, 'jabaquara,gravi', 'altura-regulavel-nasa', 'Altura Regulável Nasa', 'altura-regulavel-nasa_202002291700524ZrX4LfrtZ.jpg', 'Conforto Macio', '', 'Espuma Viscoelástica', 'RN1100', 3, 'FABRICANTE DUOFLEX', '<p><span style=\"font-size:14px;\">Os vers&aacute;teis travesseiros Altura Regul&aacute;vel foram desenvolvidos para que o usu&aacute;rio possa regular e acertar a sua altura e maciez exatamente ao seu gosto e necessidade.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">O travesseiro possui 3 camadas internas e o usu&aacute;rio pode a qualquer tempo escolher entre 4 alturas, bastando remov&ecirc;-las ou adicion&aacute;-las, de maneira f&aacute;cil, r&aacute;pida, pr&aacute;tica e higi&ecirc;nica.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Tudo isso permite uma posi&ccedil;&atilde;o natural de descanso, trazendo apoio e conforto totalmente personalizados a anatomia de cada pessoa na posi&ccedil;&atilde;o escolhida para o seu sono.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">O design exclusivo e patenteado &eacute; constitu&iacute;do por: 01 travesseiro com bolso interno e acesso lateral<br />\r\n03 camadas internas individuais e remov&iacute;veis</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">A Duoflex disponibiliza o travesseiro Altura Regul&aacute;vel tamb&eacute;m nas espumas especiais Nasa e L&aacute;tex, produzidas com total prote&ccedil;&atilde;o anti&aacute;caros, fungos e bact&eacute;rias.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Capa: 100% Algod&atilde;o Percal 200 fios</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Dimens&atilde;o: 50x70 cm</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura: de 10 a 20 cm</span></p>\r\n', '<p><span style=\"font-size:14px;\">Garantia: 02 anos</span></p>\r\n', '2020-02-29 20:00:53', '2020-02-29 20:01:52'),
(215, 0, 7, 22, NULL, 14, 'jabaquara,gravi', 'altura-regulavel-latex', 'Altura Regulável Látex', 'altura-reg-latex_20200229170722cVVAuTXtu1.jpg', 'Conforto Firme', '', 'Látex Natural', 'RL1103', 1, 'FABRICANTE DUOFLEX', '<p><span style=\"font-size:14px;\">Os vers&aacute;teis travesseiros Altura Regul&aacute;vel foram desenvolvidos para que o usu&aacute;rio possa regular e acertar a sua altura e maciez exatamente ao seu gosto e necessidade.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">O travesseiro possui 3 camadas internas e o usu&aacute;rio pode a qualquer tempo escolher entre 4 alturas, bastando remov&ecirc;-las ou adicion&aacute;-las, de maneira f&aacute;cil, r&aacute;pida, pr&aacute;tica e higi&ecirc;nica.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Tudo isso permite uma posi&ccedil;&atilde;o natural de descanso, trazendo apoio e conforto totalmente personalizados a anatomia de cada pessoa na posi&ccedil;&atilde;o escolhida para o seu sono.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">O design exclusivo e patenteado &eacute; constitu&iacute;do por: 01 travesseiro com bolso interno e acesso lateral<br />\r\n03 camadas internas individuais e remov&iacute;veis</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">A Duoflex disponibiliza o travesseiro Altura Regul&aacute;vel tamb&eacute;m nas espumas especiais Nasa e L&aacute;tex, produzidas com total prote&ccedil;&atilde;o anti&aacute;caros, fungos e bact&eacute;rias.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Interno: L&aacute;tex Poliuretano</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Externo: L&aacute;tex Natural</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px;\">Capa: 200 fios - 100% Algod&atilde;o com z&iacute;per</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Dimens&atilde;o: 50x70 cm</span></p>\r\n\r\n<p><span style=\"font-size:14px;\">Altura: de 10 a 20 cm</span></p>\r\n', '<p><span style=\"font-size:14px;\">Garantia: 02 anos</span></p>\r\n', '2020-02-29 20:07:23', '2020-02-29 20:07:23');

-- --------------------------------------------------------

--
-- Estrutura da tabela `produtos_categorias`
--

CREATE TABLE `produtos_categorias` (
  `id` int(10) UNSIGNED NOT NULL,
  `secao_id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `produtos_categorias`
--

INSERT INTO `produtos_categorias` (`id`, `secao_id`, `ordem`, `slug`, `titulo`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'colchao-com-molas', 'COLCHÃO COM MOLAS', '2018-08-28 18:05:15', '2019-06-01 18:18:03'),
(3, 1, 3, 'colchao-de-espuma', 'COLCHÃO DE ESPUMA', '2018-08-28 18:05:35', '2018-08-28 18:05:35'),
(4, 1, 4, 'colchonete', 'COLCHONETE', '2018-08-28 18:05:46', '2018-08-28 18:05:46'),
(5, 1, 5, 'cama-articulada', 'CAMA ARTICULADA', '2018-08-28 18:05:56', '2018-08-28 18:05:56'),
(9, 3, 0, 'cabeceira-estofada', 'CABECEIRA ESTOFADA', '2018-08-28 22:55:50', '2018-10-29 23:26:17'),
(11, 1, 0, 'colchao-ortopedico', 'COLCHÃO ORTOPÉDICO', '2018-09-03 21:02:32', '2019-06-01 18:19:20'),
(12, 2, 2, 'box-bau', 'BOX BAÚ', '2018-09-29 15:38:36', '2018-09-29 15:39:41'),
(13, 2, 1, 'box-simples', 'BOX SIMPLES', '2018-09-29 15:38:43', '2018-10-05 17:35:07'),
(14, 2, 4, '3x1', '3x1 ', '2018-09-29 15:38:58', '2018-09-29 15:38:58'),
(15, 2, 3, 'box-com-gavetas', 'BOX COM GAVETAS', '2018-09-29 15:39:07', '2018-10-05 17:34:33'),
(17, 4, 0, 'poltronas-reclinaveis', 'POLTRONAS RECLINÁVEIS', '2018-10-02 19:57:48', '2018-10-02 21:11:36'),
(19, 4, 0, 'sofas-cama', 'SOFÁS-CAMA', '2018-10-03 15:36:06', '2018-10-03 15:39:59'),
(20, 2, 0, 'box-com-bicama', 'BOX COM BICAMA', '2018-10-05 15:38:45', '2018-10-05 17:34:58'),
(21, 7, 0, 'travesseiro-theva', 'TRAVESSEIRO THEVA', '2018-10-08 15:52:00', '2018-10-09 16:23:14'),
(22, 7, 0, 'travesseiro-duoflex', 'TRAVESSEIRO DUOFLEX', '2018-10-08 16:03:55', '2018-10-09 16:23:25'),
(23, 7, 0, 'travesseiro-dunlopillo', 'TRAVESSEIRO DUNLOPILLO', '2018-10-08 16:04:10', '2018-10-09 16:23:35'),
(24, 7, 0, 'travesseiro-daune', 'TRAVESSEIRO DAUNE', '2018-10-08 16:05:16', '2018-10-09 16:23:44'),
(25, 7, 0, 'travesseiro-harmonia', 'TRAVESSEIRO HARMONIA', '2018-10-08 16:05:44', '2018-10-09 16:23:54'),
(26, 7, 0, 'acessibilidade', 'ACESSIBILIDADE', '2018-10-08 16:57:31', '2020-02-26 19:18:45'),
(27, 7, 0, 'almofada-antirrefluxo', 'ALMOFADA ANTIRREFLUXO', '2018-10-09 15:28:14', '2018-10-09 15:28:14'),
(28, 7, 0, 'encosto-de-leitura', 'ENCOSTO DE LEITURA', '2018-10-09 15:50:18', '2018-10-09 15:50:18'),
(29, 7, 0, 'pillow-top', 'PILLOW-TOP', '2018-10-09 16:38:01', '2018-10-09 16:38:01'),
(30, 5, 0, 'bicama', 'BICAMA', '2018-10-24 22:37:01', '2018-10-24 22:37:01'),
(31, 5, 0, 'colchonete-camping', 'COLCHONETE CAMPING', '2018-10-24 23:10:06', '2018-11-01 21:49:15'),
(32, 7, 0, 'anti-escaras', 'ANTI-ESCARAS', '2018-10-24 23:23:52', '2018-10-24 23:23:52'),
(33, 3, 0, 'cabeceira-mdf', 'CABECEIRA MDF', '2018-10-29 23:26:26', '2018-10-29 23:26:26'),
(35, 6, 0, 'poltrona-fixa-compacta-1', 'POLTRONA FIXA COMPACTA', '2018-11-03 18:17:20', '2018-11-03 18:17:20'),
(36, 6, 0, 'cama-madeira-macica', 'CAMA  MADEIRA MACIÇA', '2019-01-19 16:51:08', '2019-01-19 20:45:28'),
(37, 6, 0, 'beliche-madeira-macica', 'BELICHE MADEIRA MACIÇA', '2019-01-19 16:51:20', '2019-01-19 16:51:20'),
(38, 1, 0, 'colchao-com-latex', 'COLCHÃO COM LÁTEX', '2019-05-30 22:25:14', '2019-06-01 18:17:42'),
(39, 1, 0, 'colchao-hotelaria', 'COLCHÃO HOTELARIA', '2020-01-31 19:04:19', '2020-01-31 19:04:19'),
(41, 7, 0, 'almofadas', 'ALMOFADAS', '2020-02-26 19:31:12', '2020-02-26 19:31:12');

-- --------------------------------------------------------

--
-- Estrutura da tabela `produtos_destaques`
--

CREATE TABLE `produtos_destaques` (
  `id` int(10) UNSIGNED NOT NULL,
  `produto_id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `produtos_destaques`
--

INSERT INTO `produtos_destaques` (`id`, `produto_id`, `ordem`, `created_at`, `updated_at`) VALUES
(6, 112, 27, '2018-11-01 16:33:53', '2018-11-01 16:33:53'),
(7, 108, 26, '2018-11-01 16:34:14', '2018-11-01 16:34:14'),
(8, 107, 25, '2018-11-01 16:34:47', '2018-11-01 16:34:47'),
(9, 103, 24, '2018-11-01 16:35:06', '2018-11-01 16:35:06'),
(12, 60, 22, '2018-11-01 16:35:51', '2018-11-01 16:35:51'),
(13, 59, 21, '2018-11-01 16:36:05', '2018-11-01 16:36:05'),
(14, 58, 20, '2018-11-01 16:36:17', '2018-11-01 16:36:17'),
(15, 57, 19, '2018-11-01 16:38:24', '2018-11-01 16:38:24'),
(16, 39, 3, '2018-11-01 16:38:43', '2018-11-01 16:38:43'),
(18, 38, 17, '2018-11-01 16:40:38', '2018-11-01 16:40:38'),
(19, 42, 1, '2018-11-01 16:40:54', '2018-11-01 16:40:54'),
(22, 17, 2, '2018-11-01 16:41:42', '2018-11-01 16:41:42'),
(27, 140, 15, '2018-11-01 16:43:09', '2018-11-01 16:43:09'),
(30, 41, 14, '2018-11-01 16:43:56', '2018-11-01 16:43:56'),
(31, 47, 13, '2018-11-01 16:44:34', '2018-11-01 16:44:34'),
(32, 98, 10, '2018-11-01 16:44:55', '2018-11-01 16:44:55'),
(34, 50, 12, '2018-11-01 16:45:19', '2018-11-01 16:45:19'),
(35, 49, 11, '2018-11-01 16:45:29', '2018-11-01 16:45:29'),
(36, 25, 9, '2018-11-01 16:45:41', '2018-11-01 16:45:41'),
(42, 141, 8, '2018-11-01 16:47:16', '2018-11-01 16:47:16'),
(47, 71, 7, '2018-11-01 16:48:34', '2018-11-01 16:48:34'),
(49, 116, 6, '2018-11-01 16:49:16', '2018-11-01 16:49:16'),
(53, 151, 5, '2018-11-01 17:02:10', '2018-11-01 17:02:10'),
(54, 127, 4, '2018-11-01 17:02:51', '2018-11-01 17:02:51'),
(57, 182, 0, '2019-04-30 19:10:45', '2019-04-30 19:10:45'),
(58, 183, 0, '2019-04-30 19:19:50', '2019-04-30 19:19:50'),
(59, 184, 0, '2019-04-30 19:44:38', '2019-04-30 19:44:38'),
(60, 185, 0, '2019-04-30 19:50:57', '2019-04-30 19:50:57'),
(61, 187, 0, '2019-05-02 21:37:46', '2019-05-02 21:37:46'),
(62, 186, 0, '2019-05-02 21:41:39', '2019-05-02 21:41:39'),
(64, 189, 0, '2019-05-30 22:44:02', '2019-05-30 22:44:02'),
(65, 190, 0, '2019-05-30 22:53:45', '2019-05-30 22:53:45'),
(66, 191, 0, '2019-05-30 23:01:00', '2019-05-30 23:01:00'),
(67, 193, 0, '2019-05-30 23:18:34', '2019-05-30 23:18:34'),
(68, 192, 0, '2019-05-30 23:25:08', '2019-05-30 23:25:08'),
(69, 194, 0, '2019-09-12 20:29:04', '2019-09-12 20:29:04'),
(70, 195, 0, '2020-01-31 19:14:44', '2020-01-31 19:14:44'),
(71, 196, 0, '2020-02-26 22:06:31', '2020-02-26 22:06:31');

-- --------------------------------------------------------

--
-- Estrutura da tabela `produtos_imagens`
--

CREATE TABLE `produtos_imagens` (
  `id` int(10) UNSIGNED NOT NULL,
  `produto_id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `produtos_imagens`
--

INSERT INTO `produtos_imagens` (`id`, `produto_id`, `ordem`, `imagem`, `created_at`, `updated_at`) VALUES
(10, 8, 0, 'bamboo-anniversary-2_20180927123808INNp6aZ4Zf.png', '2018-09-27 15:38:09', '2018-09-27 15:38:09'),
(31, 4, 0, 'posture-comfort-frontal_201809281837537bcqDeknGw.png', '2018-09-28 21:37:57', '2018-09-28 21:37:57'),
(32, 27, 0, 'epeda-acordes-2_20181001122830fMgTAhDkkd.png', '2018-10-01 15:28:31', '2018-10-01 15:28:31'),
(33, 28, 0, 'flex-tenerife-2_201810011822081TZZaeIX4V.png', '2018-10-01 21:22:08', '2018-10-01 21:22:08'),
(34, 29, 0, 'flex-avila-2_20181001182636EDZ27OuL5U.png', '2018-10-01 21:26:37', '2018-10-01 21:26:37'),
(39, 31, 3, 'poltrona-1216-3_201810021304362mzx0wtpVk.jpg', '2018-10-02 16:04:37', '2018-10-02 16:04:37'),
(40, 31, 1, 'poltrona-1216-2_20181002130436cnotF2P5mq.jpg', '2018-10-02 16:04:37', '2018-10-02 16:04:37'),
(43, 32, 4, 'poltrona-1237-5_20181002181743ed9f0cFfp3.jpg', '2018-10-02 21:17:44', '2018-10-02 21:17:44'),
(44, 32, 1, 'poltrona-1237-3_201810021817443U1Zogzsuc.jpg', '2018-10-02 21:17:45', '2018-10-02 21:17:45'),
(45, 32, 2, 'poltrona-1237-4_20181002181751gUttboiK2M.jpg', '2018-10-02 21:17:53', '2018-10-02 21:17:53'),
(47, 32, 0, 'poltrona-1237-2_20181003123340kW6DJJfpIp.jpg', '2018-10-03 15:33:41', '2018-10-03 15:33:41'),
(48, 33, 3, 'sofa-1456-3_20181003142912XWDaLjw2UH.jpg', '2018-10-03 17:29:13', '2018-10-03 17:29:13'),
(49, 33, 1, 'sofa-1456-2_201810031429128xE7tIne34.jpg', '2018-10-03 17:29:13', '2018-10-03 17:29:13'),
(50, 34, 1, 'sofa-1500-2_20181003150003XzjS4xdlv9.jpg', '2018-10-03 18:00:04', '2018-10-03 18:00:04'),
(51, 34, 3, 'sofa-1500-5_20181003150004Ab1OthaMw8.jpg', '2018-10-03 18:00:05', '2018-10-03 18:00:05'),
(57, 33, 2, 'sofa-solteiro-1456_20181003183059ert2nlzxKQ.jpg', '2018-10-03 21:30:59', '2018-10-03 21:30:59'),
(61, 39, 4, 'amazon-pocket-colchao-solteiro_20181004123125lDwD7CPXUF.jpg', '2018-10-04 15:31:25', '2018-10-04 15:31:25'),
(62, 39, 2, 'amazon-pocket-conjunto158_20181004123125jjF0lINBKy.jpg', '2018-10-04 15:31:26', '2018-10-04 15:31:26'),
(63, 39, 1, 'colchao-138-158_20181004123126S75j9ImEq8.jpg', '2018-10-04 15:31:27', '2018-10-04 15:31:27'),
(64, 39, 3, 'amazon-pocket-conjunto-solteiro_20181004123127dCdOrLWyLl.jpg', '2018-10-04 15:31:27', '2018-10-04 15:31:27'),
(65, 39, 5, 'premium-amazon-tratamento-amazonfair_20181004125049eeVZaqIzd9.jpg', '2018-10-04 15:50:50', '2018-10-04 15:50:50'),
(71, 41, 2, 'sofa-1602-3_20181004183050nM616y4gpi.jpg', '2018-10-04 21:30:51', '2018-10-04 21:30:51'),
(72, 41, 1, 'sofa-1602_20181004183109Fb90SYsQBx.jpg', '2018-10-04 21:31:18', '2018-10-04 21:31:18'),
(73, 42, 0, 'cama-1430-articulada-dream-4_20181004184933BjjrwghYGb.jpg', '2018-10-04 21:49:34', '2018-10-04 21:49:34'),
(74, 42, 0, 'cama-1430-articulada-dream-3_20181004184932S3Hp8U5sZC.jpg', '2018-10-04 21:49:34', '2018-10-04 21:49:34'),
(75, 42, 0, 'cama-1430-articulada-dream-2_20181004184933egB0CIZrc4.jpg', '2018-10-04 21:49:34', '2018-10-04 21:49:34'),
(96, 58, 3, 'sleep-max-d33-casal-25cm_20181006132307SDO17LkMlh.jpg', '2018-10-06 16:23:07', '2018-10-06 16:23:07'),
(97, 58, 2, 'sleep-max-d33-casal_20181006132307r5xgEwCByL.jpg', '2018-10-06 16:23:07', '2018-10-06 16:23:07'),
(98, 58, 1, 'sleep-max-d33-25cm-solteiro_20181006132307sla3LWDoEF.jpg', '2018-10-06 16:23:07', '2018-10-06 16:23:07'),
(99, 59, 1, 'sleep-max-d45-25cm-solteiro_201810061347005HLUPU4u9Y.jpg', '2018-10-06 16:47:00', '2018-10-06 16:47:00'),
(100, 59, 3, 'sleep-max-d45-casal-25cm_20181006134700iBTtp8tUEO.jpg', '2018-10-06 16:47:00', '2018-10-06 16:47:00'),
(101, 59, 2, 'sleep-max-d45-casal_201810061347000yYoDr7doq.jpg', '2018-10-06 16:47:01', '2018-10-06 16:47:01'),
(105, 64, 0, 'theva-kapok-3_20181008132216oYNLqABicy.png', '2018-10-08 16:22:17', '2018-10-08 16:22:17'),
(106, 64, 0, 'theva-kapok-2_20181008132216hq3LrZGK8O.png', '2018-10-08 16:22:17', '2018-10-08 16:22:17'),
(108, 66, 3, 'theva-plushpillo-premium-4_20181008133844lBVf5pGiUo.jpg', '2018-10-08 16:38:44', '2018-10-08 16:38:44'),
(109, 66, 2, 'theva-plushpillo-premium-2_20181008133844UNDGVs04pT.png', '2018-10-08 16:38:46', '2018-10-08 16:38:46'),
(110, 66, 1, 'theva-plushpillo-premium-3_20181008133846Xx8JXezY0C.png', '2018-10-08 16:38:47', '2018-10-08 16:38:47'),
(111, 67, 0, 'theva-memogel-pillow-3_201810081345577QLydRGm8X.png', '2018-10-08 16:45:58', '2018-10-08 16:45:58'),
(112, 67, 0, 'theva-memogel-pillow-4_20181008134559d9UyKAozqk.jpg', '2018-10-08 16:46:00', '2018-10-08 16:46:00'),
(113, 67, 0, 'theva-memogel-pillow-2_20181008134559KroFHAHiqK.png', '2018-10-08 16:46:00', '2018-10-08 16:46:00'),
(114, 69, 0, 'barra-standup1_20181008144047i6BI4GXQmI.png', '2018-10-08 17:40:49', '2018-10-08 17:40:49'),
(115, 70, 0, 'grade-cama_20181009122231puuBU2Mwvg.jpg', '2018-10-09 15:22:32', '2018-10-09 15:22:32'),
(116, 71, 0, 'suporte-terapeutico-antirefluxo-adulto3_20181009123411xI2tQgXZq5.png', '2018-10-09 15:34:12', '2018-10-09 15:34:12'),
(117, 72, 0, 'detalhe-produtos-plush-900x900px_20181009125729vdfRNNMv1n.jpg', '2018-10-09 15:57:29', '2018-10-09 15:57:29'),
(118, 74, 0, 'suporte-terapeutico-antirrefluxo-infantil2_20181009130908Gt77AUmWdp.png', '2018-10-09 16:09:09', '2018-10-09 16:09:09'),
(119, 74, 0, 'suporte-terapeutico-antirrefluxo-infantil1_20181009130911uGLWvRvBnF.png', '2018-10-09 16:09:12', '2018-10-09 16:09:12'),
(120, 75, 1, 'gold-queen-3_20181009132132jDrqya53Cl.png', '2018-10-09 16:21:32', '2018-10-09 16:21:32'),
(122, 75, 2, 'gold-queen-2_20181009132158jvQdWri6tG.png', '2018-10-09 16:21:59', '2018-10-09 16:21:59'),
(123, 76, 2, 'basic-bambu-2_20181009133015HJOWi9NJSp.png', '2018-10-09 16:30:16', '2018-10-09 16:30:16'),
(124, 76, 1, 'basic-bambu-3_20181009133016wtM1HgZxHS.png', '2018-10-09 16:30:17', '2018-10-09 16:30:17'),
(125, 77, 0, 'basicsoft-det-01_20181009133630aKoIlBGtbl.png', '2018-10-09 16:36:31', '2018-10-09 16:36:31'),
(126, 77, 0, 'basicsoft-det-02_20181009133631dTHXNlkqSh.png', '2018-10-09 16:36:32', '2018-10-09 16:36:32'),
(127, 78, 1, 'top-pad_20181009134921Q5msPZ5BW9.png', '2018-10-09 16:49:22', '2018-10-09 16:49:22'),
(128, 78, 2, 'top-pad-3_20181009134922L6SSYhlW0C.jpg', '2018-10-09 16:49:23', '2018-10-09 16:49:23'),
(129, 79, 2, 'theva-memogel-pillow-4_20181009135940CJzvWxrazx.jpg', '2018-10-09 16:59:41', '2018-10-09 16:59:41'),
(130, 79, 1, 'topper-visco-gel-3_20181009135939fD9TdhqC3m.jpg', '2018-10-09 16:59:41', '2018-10-09 16:59:41'),
(131, 80, 0, 'nasa-alto-3_20181009140945WMiHCdukGs.jpg', '2018-10-09 17:09:46', '2018-10-09 17:09:46'),
(132, 80, 0, 'nasa-alto-2_20181009140945x4Rz3naKnf.jpg', '2018-10-09 17:09:46', '2018-10-09 17:09:46'),
(133, 81, 1, 'nasa-cervical-2_20181009141549N17C08bVi2.jpg', '2018-10-09 17:15:50', '2018-10-09 17:15:50'),
(134, 81, 2, 'nasa-cerviacal-33_20181009141549polbYjI0Il.jpg', '2018-10-09 17:15:50', '2018-10-09 17:15:50'),
(137, 83, 0, 'real-latex-natural-3_20181009143551IvXcaMs6Iy.jpg', '2018-10-09 17:35:52', '2018-10-09 17:35:52'),
(138, 83, 0, 'real-latex-natural-2_20181009143552c5o0ZdBeIf.jpg', '2018-10-09 17:35:52', '2018-10-09 17:35:52'),
(141, 86, 0, 'ce146a2671b6c816732b7a67cee669ed_20181009150441Dykm2GtTTv.jpg', '2018-10-09 18:04:42', '2018-10-09 18:04:42'),
(144, 90, 0, '90365e2716c41ac413e9f1fe29bc74ca_20181009193916K9nZbgvJDK.jpg', '2018-10-09 22:39:17', '2018-10-09 22:39:17'),
(149, 43, 0, '2680-2_20181011122946zpKyoi10Wr.jpg', '2018-10-11 15:29:47', '2018-10-11 15:29:47'),
(150, 44, 0, '2681-2_20181011123057oMiFSa6gbe.jpg', '2018-10-11 15:30:58', '2018-10-11 15:30:58'),
(151, 46, 0, '2650-2_20181011123208thN1rSCvMm.jpg', '2018-10-11 15:32:09', '2018-10-11 15:32:09'),
(152, 94, 0, '2691-2_20181011123238TAEIx8u6ue.jpg', '2018-10-11 15:32:39', '2018-10-11 15:32:39'),
(153, 95, 3, '2696-2_20181011123331ccbbsGSLro.jpg', '2018-10-11 15:33:32', '2018-10-11 15:33:32'),
(154, 95, 1, '2695-2_20181011123331oEOArB100d.jpg', '2018-10-11 15:33:32', '2018-10-11 15:33:32'),
(155, 95, 2, '2696_201810111233324ADmwytuqj.jpg', '2018-10-11 15:33:32', '2018-10-11 15:33:32'),
(156, 102, 0, 'impact_20181019183241BXi1un7zoQ.jpg', '2018-10-19 21:32:42', '2018-10-19 21:32:42'),
(157, 103, 0, 'impact_20181019190039WBN7rxP8vQ.jpg', '2018-10-19 22:00:40', '2018-10-19 22:00:40'),
(161, 107, 0, '87_20181023182324ENHlpq3A2J.png', '2018-10-23 21:23:25', '2018-10-23 21:23:25'),
(162, 108, 0, '86_20181023185839ulPCvVVxZ6.png', '2018-10-23 21:58:40', '2018-10-23 21:58:40'),
(163, 109, 0, '85_20181023200814mgQguWMK16.png', '2018-10-23 23:08:15', '2018-10-23 23:08:15'),
(164, 110, 0, '84_20181024163509O7LeQMGsBR.png', '2018-10-24 19:35:10', '2018-10-24 19:35:10'),
(165, 111, 0, '83_20181024172026Kcs2yxhIIg.png', '2018-10-24 20:20:27', '2018-10-24 20:20:27'),
(166, 112, 0, '82_20181024181124IxhCY8S9Cp.png', '2018-10-24 21:11:25', '2018-10-24 21:11:25'),
(167, 113, 0, '81_20181024183502hzHuL4Six0.png', '2018-10-24 21:35:02', '2018-10-24 21:35:02'),
(169, 115, 2, 'articulado-01-1_20181024195112eksxgWbnbZ.jpg', '2018-10-24 22:51:13', '2018-10-24 22:51:13'),
(170, 115, 1, 'articulado-01-2_20181024195252AQuH7pcOGa.jpg', '2018-10-24 22:52:53', '2018-10-24 22:52:53'),
(171, 115, 3, 'articulado-01-4_20181024195331KHkARVyGgL.jpg', '2018-10-24 22:53:32', '2018-10-24 22:53:32'),
(172, 118, 0, 'colchonete-camping-aberto_20181024201953wo5sWsmpSf.jpg', '2018-10-24 23:19:54', '2018-10-24 23:19:54'),
(173, 139, 0, 'poltrona-monza-aberta_20181030184655XxrV9yzIit.jpg', '2018-10-30 21:46:56', '2018-10-30 21:46:56'),
(174, 160, 0, 'marr_20181101163310zOWJyoxJMS.png', '2018-11-01 19:33:11', '2018-11-01 19:33:11'),
(176, 169, 2, 'mg-7157_20181114154742IoRHNgKZme.jpg', '2018-11-14 17:47:43', '2018-11-14 17:47:43'),
(177, 169, 3, 'mg-7238_20181114154742mHQrv7UnPj.jpg', '2018-11-14 17:47:43', '2018-11-14 17:47:43'),
(178, 169, 4, 'mg-7237_20181114154742RkJuvr4m2g.jpg', '2018-11-14 17:47:43', '2018-11-14 17:47:43'),
(179, 169, 1, 'mg-7076_20181114154743d2o6AtghjS.jpg', '2018-11-14 17:47:43', '2018-11-14 17:47:43'),
(180, 170, 3, 'mg-7277_20181114160730m1IBmkjB1e.jpg', '2018-11-14 18:07:30', '2018-11-14 18:07:30'),
(181, 170, 2, 'mg-7276_20181114160730O2pG2AvTKH.jpg', '2018-11-14 18:07:30', '2018-11-14 18:07:30'),
(182, 170, 1, 'mg-7331_20181114160730A9s9Utm11Y.jpg', '2018-11-14 18:07:31', '2018-11-14 18:07:31'),
(186, 172, 0, 'mg-7256_201811141644106WbvBR628g.jpg', '2018-11-14 18:44:11', '2018-11-14 18:44:11'),
(187, 173, 0, 'mg-7245_20181114165719hk2f5g9aOd.jpg', '2018-11-14 18:57:20', '2018-11-14 18:57:20'),
(190, 176, 0, 'affection-2_20181127154953LZUpMMBDNt.png', '2018-11-27 17:49:54', '2018-11-27 17:49:54'),
(194, 189, 0, 'bestplush-det-03_20190530194426BkPRXjwmea.png', '2019-05-30 22:44:28', '2019-05-30 22:44:28'),
(195, 189, 0, 'bestplush-det-02_201905301944271OqgWnxf67.png', '2019-05-30 22:44:28', '2019-05-30 22:44:28'),
(196, 190, 0, 'theplace-det-03_201905301954072U8DS7y9Pn.png', '2019-05-30 22:54:08', '2019-05-30 22:54:08'),
(197, 190, 0, 'theplace-det-02_201905301954102Rl8cJusxr.png', '2019-05-30 22:54:12', '2019-05-30 22:54:12'),
(198, 191, 0, 'renovation-det-03_20190530200217p6XV5ynwFu.png', '2019-05-30 23:02:19', '2019-05-30 23:02:19'),
(199, 191, 0, 'renovation-det-02_201905302002417rNodwdjTR.png', '2019-05-30 23:02:42', '2019-05-30 23:02:42'),
(200, 192, 0, 'renascence-det-02_20190530201059MVncrvmGfq.png', '2019-05-30 23:11:00', '2019-05-30 23:11:00'),
(201, 192, 0, 'renascence-det-03_20190530201101oU1ws4WJyO.png', '2019-05-30 23:11:03', '2019-05-30 23:11:03'),
(202, 193, 0, 'memolatex-det-02_20190530201929zA6MWJ69cV.png', '2019-05-30 23:19:30', '2019-05-30 23:19:30'),
(203, 193, 0, 'memolatex-det-03_20190530201929N8ThIdt7Rw.png', '2019-05-30 23:19:31', '2019-05-30 23:19:31'),
(204, 194, 3, 'bamboo-love-profusion-baixa-158x198_201909121749206ukvBX6VpB.png', '2019-09-12 20:49:21', '2019-09-12 20:49:21'),
(205, 194, 1, 'bamboo-love-profusion-colchao-138x188_20190912175117ii59hWMRYg.jpg', '2019-09-12 20:51:20', '2019-09-12 20:51:20'),
(206, 194, 2, 'bamboo-love-profusion-base-138x188_20190912175201FGeUhcLdWZ.jpg', '2019-09-12 20:52:05', '2019-09-12 20:52:05'),
(208, 197, 2, 'double-branca_20200226163628o2QJqmzuih.png', '2020-02-26 19:36:29', '2020-02-26 19:36:29'),
(209, 197, 4, 'almofada-double-c_20200226163630aVUH7kCR9O.png', '2020-02-26 19:36:31', '2020-02-26 19:36:31'),
(210, 197, 1, 'double-branca-jr_20200226163631taAkZ04v1d.png', '2020-02-26 19:36:32', '2020-02-26 19:36:32'),
(211, 197, 5, 'mao-viscoelastica_202002261636335Tlx15eyCQ.png', '2020-02-26 19:36:35', '2020-02-26 19:36:35'),
(212, 197, 3, 'double-support_202002261636342DcDvwJl0M.png', '2020-02-26 19:36:35', '2020-02-26 19:36:35'),
(213, 198, 1, 'bigliza_20200226164212nSnXzHbRHM.png', '2020-02-26 19:42:12', '2020-02-26 19:42:12'),
(214, 198, 2, 'big-1_202002261642173aqAuv8btk.png', '2020-02-26 19:42:18', '2020-02-26 19:42:18'),
(215, 198, 3, 'mao-viscoelastica_20200226164218I4xXBIZ95j.png', '2020-02-26 19:42:20', '2020-02-26 19:42:20'),
(216, 168, 0, 'puff-conforto-2_202002261834113R8BPTGNi2.jpg', '2020-02-26 21:34:12', '2020-02-26 21:34:12'),
(217, 168, 0, 'puff-conforto-6_20200226183558xult1rxEai.jpg', '2020-02-26 21:35:59', '2020-02-26 21:35:59'),
(218, 168, 0, 'puff-conforto-5_20200226183558z5kIIR8pm9.jpg', '2020-02-26 21:35:59', '2020-02-26 21:35:59'),
(219, 168, 0, 'puff-conforto-7_20200226183600aq4rS8YmPF.jpg', '2020-02-26 21:36:01', '2020-02-26 21:36:01'),
(220, 202, 0, 'mont-long-2_20200226184127hmmf6rejq9.jpg', '2020-02-26 21:41:28', '2020-02-26 21:41:28'),
(221, 203, 0, 'pitangueiras-2_20200226184546UTw0sbdukB.jpg', '2020-02-26 21:45:47', '2020-02-26 21:45:47'),
(222, 207, 0, 'pratik-sol-2_20200226190459ck7mNW5dat.jpg', '2020-02-26 22:05:01', '2020-02-26 22:05:01'),
(223, 79, 3, 'topper_20200226192622X5rJORQN60.jpg', '2020-02-26 22:26:22', '2020-02-26 22:26:22'),
(224, 196, 0, 'lift_20200226193249EZVWCzXQol.png', '2020-02-26 22:32:50', '2020-02-26 22:32:50'),
(225, 85, 0, 'molas-cervical-2_20200229160854JkdjwSJqD1.jpg', '2020-02-29 19:08:55', '2020-02-29 19:08:55'),
(226, 82, 0, 'natural-latex-2_20200229161205FBg26yndIh.jpg', '2020-02-29 19:12:06', '2020-02-29 19:12:06'),
(227, 209, 0, 'classic-pillow-2_20200229163733VBkuD0cMhA.jpg', '2020-02-29 19:37:34', '2020-02-29 19:37:34'),
(228, 210, 0, 'gellex-2_20200229164246bJZkiuuabB.jpg', '2020-02-29 19:42:46', '2020-02-29 19:42:46'),
(229, 214, 0, 'altura-reg-nasa_20200229170108DfpeHb6cUX.jpg', '2020-02-29 20:01:08', '2020-02-29 20:01:08'),
(230, 215, 0, 'altura-reg-nasa_20200229170733lBFuNloC3V.jpg', '2020-02-29 20:07:34', '2020-02-29 20:07:34');

-- --------------------------------------------------------

--
-- Estrutura da tabela `produtos_marcas`
--

CREATE TABLE `produtos_marcas` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `home` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `produtos_marcas`
--

INSERT INTO `produtos_marcas` (`id`, `ordem`, `slug`, `nome`, `imagem`, `home`, `created_at`, `updated_at`) VALUES
(2, 21, 'altenburg', 'Altenburg', 'alten_20180828151553hcEJCOeo7R.jpg', 1, '2018-08-28 18:15:53', '2018-08-28 18:15:53'),
(3, 8, 'castor', 'Castor', 'logo-castor-com-slogan_20180926182200vHy2eglLVu.png', 1, '2018-08-28 18:16:42', '2018-09-26 21:22:01'),
(4, 22, 'copespuma', 'Copespuma', 'cope_201808281517209ZN27kEZnY.jpg', 1, '2018-08-28 18:17:20', '2018-08-28 18:17:20'),
(6, 10, 'herval', 'HERVAL', 'herval-logotipo_201810311935457MzbJ2JrzI.jpeg', 1, '2018-08-28 22:53:06', '2018-10-31 22:35:45'),
(7, 3, 'sankonfort', 'Sankonfort', 'logo-sanko_20180903191448tw4je9lSdA.png', 1, '2018-09-03 22:14:48', '2018-09-03 22:14:48'),
(8, 6, 'simmons', 'Simmons', 'j-htdgzw_20181001120450UEcTX1RqpQ.png', 1, '2018-09-03 22:16:12', '2018-10-01 15:04:50'),
(9, 5, 'flex-do-brasil', 'Flex do Brasil', 'ssjhjpaa_20181001120421bIYjZ1rngY.png', 1, '2018-09-03 22:18:26', '2018-10-01 15:04:21'),
(10, 4, 'epeda', 'Epeda', 'dqt0wk2g_201810011204400f9mb0F1pT.png', 1, '2018-09-03 22:22:11', '2018-10-01 15:04:40'),
(11, 7, 'aireloom', 'Aireloom', 'czfecjyq_20181001120545rdD7MSMJkY.png', 1, '2018-09-03 22:29:37', '2018-10-01 15:05:45'),
(12, 20, 'dunlopillo', 'Dunlopillo', 'logo-dunlopillo_20180903194139PCDSPoOq57.png', 1, '2018-09-03 22:41:39', '2018-09-03 22:41:39'),
(13, 19, 'theva', 'Theva', 'logo-theva_201809031942586ErCMTQUxo.png', 1, '2018-09-03 22:42:59', '2018-09-03 22:42:59'),
(14, 18, 'duoflex', 'Duoflex', 'logo-duoflex_20180903194609ulJSWK8FUu.jpg', 1, '2018-09-03 22:46:11', '2018-09-03 22:46:11'),
(15, 9, 'eastman-house', 'Eastman House', 'logo-eastman-house_20180903195035SvgqHdN6jm.png', 1, '2018-09-03 22:50:35', '2018-10-25 19:51:13'),
(17, 17, 'general-injection', 'General Injection', 'logo-general-injection_20180903195526jBrphVCAdX.png', 1, '2018-09-03 22:55:26', '2018-09-03 22:55:26'),
(18, 11, 'fa', 'FA', 'logo-fa_20180903195600d1h967rzq6.png', 1, '2018-09-03 22:56:00', '2018-09-03 22:56:00'),
(20, 15, 'rincao', 'Rincão', 'logo-rincao_20180903200034jeyRlnCUhI.jpg', 1, '2018-09-03 22:59:39', '2018-09-03 23:00:34'),
(21, 13, 'daune', 'Daune', 'logo-daune_20180903200514gLaUhfjLMt.png', 1, '2018-09-03 23:05:14', '2018-09-03 23:05:14'),
(23, 2, 'mga', 'MGA', 'logo-mga_20180903201115lC70cAIzmz.jpg', 1, '2018-09-03 23:11:15', '2018-09-03 23:11:15'),
(24, 1, 'colchao-store', 'Colchão Store', 'marca-colchaostore_20180903202024CVciRyoOAE.jpg', 1, '2018-09-03 23:20:26', '2018-09-03 23:20:26'),
(25, 12, 'comfort-prime', 'Comfort Prime', 'logo-confort-prime_20180903203034veD2bEdN0e.png', 1, '2018-09-03 23:30:35', '2018-11-05 18:18:55'),
(27, 14, 'mb-moveis-1', 'MB Móveis ', 'mb-moveis_20181029181421quH0aqroLO.png', 1, '2018-10-29 21:14:21', '2018-10-29 21:14:21');

-- --------------------------------------------------------

--
-- Estrutura da tabela `produtos_medidas`
--

CREATE TABLE `produtos_medidas` (
  `id` int(10) UNSIGNED NOT NULL,
  `produto_id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `a_vista` int(11) NOT NULL,
  `oferta` int(11) NOT NULL,
  `parcelado` int(11) NOT NULL,
  `parcelas` int(11) NOT NULL,
  `peso` int(11) NOT NULL,
  `cubagem` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `produtos_medidas`
--

INSERT INTO `produtos_medidas` (`id`, `produto_id`, `ordem`, `titulo`, `a_vista`, `oferta`, `parcelado`, `parcelas`, `peso`, `cubagem`, `created_at`, `updated_at`) VALUES
(4, 3, 4, 'Colch&atilde;o Casal<br />\r\n138x188 cm', 159000, 0, 159000, 10, 70, 210, '2018-09-21 16:20:36', '2018-09-27 16:14:59'),
(5, 3, 1, 'Colch&atilde;o Solteiro<br />\r\n088x188 cm', 109000, 0, 109000, 10, 50, 150, '2018-09-25 16:02:32', '2019-05-02 21:27:19'),
(6, 3, 2, 'Colch&atilde;o Solteiro King<br />\r\n096x203 cm', 129000, 0, 129000, 10, 60, 170, '2018-09-25 16:03:24', '2019-05-02 21:27:35'),
(7, 3, 3, 'Colch&atilde;o Casal<br />\r\n128x188 cm', 156000, 0, 156000, 10, 70, 210, '2018-09-25 16:04:22', '2019-05-02 21:27:58'),
(8, 3, 5, 'Colch&atilde;o Queen<br />\r\n158x198 cm', 192000, 0, 192000, 10, 100, 280, '2018-09-25 16:05:15', '2019-05-02 21:28:23'),
(9, 3, 6, 'Colch&atilde;o King<br />\r\n193x203 cm', 230000, 0, 230000, 10, 140, 350, '2018-09-25 16:05:49', '2019-05-02 21:28:41'),
(10, 3, 7, 'Colch&atilde;o + Box Solteiro<br />\r\n088x188 cm', 139000, 0, 139000, 10, 100, 300, '2018-09-25 16:18:54', '2019-05-02 21:29:03'),
(11, 3, 8, 'Colch&atilde;o + Box Solteiro King<br />\r\n096x203 cm', 161000, 0, 161000, 10, 120, 350, '2018-09-25 16:20:00', '2019-05-02 21:29:25'),
(12, 3, 9, 'Colch&atilde;o + Box Casal<br />\r\n128x188 cm', 193000, 0, 193000, 10, 140, 420, '2018-09-25 16:20:33', '2019-05-02 21:29:47'),
(13, 3, 10, 'Colch&atilde;o + Box Casal<br />\r\n138x188 cm', 197000, 0, 197000, 10, 140, 420, '2018-09-25 16:21:09', '2019-05-02 21:30:15'),
(14, 3, 11, 'Colch&atilde;o + Box Queen<br />\r\n158x198 cm', 250000, 0, 250000, 10, 200, 560, '2018-09-25 16:22:06', '2019-05-02 21:30:40'),
(15, 3, 12, 'Colch&atilde;o + Box King<br />\r\n193x203 cm', 294000, 0, 294000, 10, 280, 700, '2018-09-25 16:22:42', '2019-05-02 21:31:05'),
(16, 4, 1, 'Colch&atilde;o Solteiro<br />\r\n078x188 cm&nbsp;', 72000, 0, 72000, 10, 40, 110, '2018-09-25 21:34:46', '2019-05-02 21:37:23'),
(17, 4, 2, 'Colch&atilde;o Solteiro<br />\r\n088x188 cm', 75000, 0, 75000, 10, 40, 110, '2018-09-25 21:37:14', '2019-05-02 21:37:36'),
(18, 4, 3, 'Colch&atilde;o VI&uacute;va<br />\r\n110x188 cm', 115000, 0, 115000, 10, 50, 160, '2018-09-25 21:38:18', '2018-09-27 16:24:40'),
(19, 4, 4, 'Colch&atilde;o Casal<br />\r\n138x188 cm', 109000, 0, 109000, 10, 65, 200, '2018-09-25 21:38:55', '2018-09-27 16:25:01'),
(20, 5, 1, 'Colch&atilde;o Solteiro<br />\r\n078x188cm', 64000, 0, 64000, 10, 40, 110, '2018-09-25 21:53:55', '2019-05-02 21:36:21'),
(21, 5, 2, 'Colch&atilde;o Solteiro<br />\r\n088x188cm', 68000, 0, 68000, 10, 40, 110, '2018-09-25 21:54:29', '2019-05-02 21:36:40'),
(22, 5, 3, 'Colch&atilde;o Vi&uacute;va<br />\r\n110x188cm', 99000, 0, 99000, 10, 50, 160, '2018-09-25 21:57:34', '2018-09-27 16:25:31'),
(23, 5, 4, 'Colch&atilde;o Casal<br />\r\n138x188cm', 99000, 0, 99000, 10, 65, 200, '2018-09-25 21:58:15', '2018-09-27 16:25:39'),
(24, 6, 1, 'Colch&atilde;o Solteiro<br />\r\n088x188 cm', 110000, 0, 110000, 10, 50, 100, '2018-09-26 19:53:04', '2019-05-02 21:31:50'),
(25, 6, 2, 'Colch&atilde;o Solteiro King<br />\r\n096x203 cm', 131000, 0, 131000, 10, 60, 120, '2018-09-26 19:53:54', '2019-05-02 21:32:11'),
(26, 6, 3, 'Colch&atilde;o Casal<br />\r\n128x188 cm', 167000, 0, 167000, 10, 70, 140, '2018-09-26 19:54:32', '2019-05-02 21:32:32'),
(27, 6, 4, 'Colch&atilde;o Casal<br />\r\n138x188 cm', 167000, 0, 167000, 10, 70, 140, '2018-09-26 19:55:19', '2019-05-02 21:32:47'),
(28, 6, 5, 'Colch&atilde;o Queen<br />\r\n158x198 cm', 196000, 0, 196000, 10, 100, 200, '2018-09-26 19:55:53', '2019-05-02 21:33:05'),
(29, 6, 6, 'Colch&atilde;o King<br />\r\n193x203 cm', 235000, 0, 235000, 10, 140, 280, '2018-09-26 19:56:30', '2019-05-02 21:33:25'),
(30, 6, 7, 'Colch&atilde;o + Box Solteiro<br />\r\n088x188 cm', 140000, 0, 140000, 10, 150, 300, '2018-09-26 19:57:32', '2019-05-02 21:33:43'),
(31, 6, 8, 'Colch&atilde;o + Box Solteiro King<br />\r\n096x203 cm', 163000, 0, 163000, 10, 170, 350, '2018-09-26 19:58:41', '2019-05-02 21:34:09'),
(32, 6, 9, 'Colch&atilde;o + Box Casal<br />\r\n128x188 cm', 204000, 0, 204000, 10, 210, 420, '2018-09-26 19:59:19', '2019-05-02 21:34:34'),
(33, 6, 10, 'Colch&atilde;o + Box Casal&nbsp;<br />\r\n138x188 cm', 205000, 0, 205000, 10, 210, 420, '2018-09-26 20:00:17', '2018-09-27 16:27:25'),
(34, 6, 11, 'Colch&atilde;o + Box Queen<br />\r\n158x198 cm', 254000, 0, 254000, 10, 280, 560, '2018-09-26 20:01:06', '2019-05-02 21:35:11'),
(35, 6, 12, 'Colch&atilde;o + Box King<br />\r\n193x203 cm', 299000, 0, 299000, 10, 350, 700, '2018-09-26 20:01:46', '2019-05-02 21:35:32'),
(36, 8, 1, 'Colch&atilde;o Solteiro<br />\r\n088x188 cm', 229000, 0, 229000, 10, 60, 170, '2018-09-27 15:50:43', '2020-02-26 20:26:53'),
(37, 7, 1, 'Colch&atilde;o Solteiro<br />\r\n088x188 cm', 169000, 0, 169000, 10, 60, 170, '2018-09-27 16:33:20', '2019-05-02 21:38:25'),
(38, 7, 2, 'Colch&atilde;o Solteiro King&nbsp;<br />\r\n096x203 cm', 195000, 0, 195000, 10, 70, 210, '2018-09-27 16:37:02', '2019-05-02 21:38:43'),
(39, 7, 3, 'Colch&atilde;o Casal<br />\r\n128x188 cm', 231000, 0, 231000, 10, 90, 270, '2018-09-27 16:41:49', '2019-05-02 21:39:02'),
(40, 7, 4, 'Colch&atilde;o Casal<br />\r\n138x188 cm', 246000, 0, 246000, 10, 90, 270, '2018-09-27 16:42:40', '2019-05-02 21:39:47'),
(41, 7, 5, 'Colch&atilde;o Queen<br />\r\n158x198 cm', 286000, 0, 286000, 10, 110, 320, '2018-09-27 16:43:32', '2019-05-02 21:39:27'),
(42, 7, 6, 'Colch&atilde;o King<br />\r\n193x203 cm', 346000, 0, 346000, 10, 140, 400, '2018-09-27 16:44:08', '2019-05-02 21:40:17'),
(43, 7, 7, 'Colch&atilde;o + Box Solteiro<br />\r\n088x188 cm', 199000, 0, 199000, 10, 110, 320, '2018-09-27 16:46:50', '2018-09-27 16:46:50'),
(44, 7, 8, 'Colch&atilde;o + Box Solteiro King<br />\r\n096x203 cm', 227000, 0, 227000, 10, 140, 400, '2018-09-27 16:47:54', '2019-05-02 21:40:52'),
(45, 7, 9, 'Colch&atilde;o + Box Casal<br />\r\n128x188 cm', 268000, 0, 268000, 10, 170, 500, '2018-09-27 16:49:50', '2019-05-02 21:41:16'),
(46, 7, 10, 'Colch&atilde;o + Box Casal<br />\r\n138x188 cm', 284000, 0, 284000, 10, 170, 500, '2018-09-27 16:50:41', '2019-05-02 21:41:41'),
(47, 7, 11, 'Colch&atilde;o + Box Queen<br />\r\n158x198 cm', 344000, 0, 344000, 10, 200, 600, '2018-09-27 16:51:27', '2019-05-02 21:42:06'),
(48, 7, 12, 'Colch&atilde;o + Box King<br />\r\n193x203 cm', 410000, 0, 410000, 10, 250, 750, '2018-09-27 16:52:14', '2019-05-02 21:42:30'),
(49, 8, 2, 'Colch&atilde;o Solteiro King<br />\r\n096x203 cm', 259000, 0, 259000, 10, 70, 210, '2018-09-27 17:00:54', '2020-02-26 20:27:13'),
(50, 8, 3, 'Colch&atilde;o Casal<br />\r\n138x188 cm', 326000, 0, 326000, 10, 90, 270, '2018-09-27 17:01:25', '2020-02-26 20:27:28'),
(51, 8, 4, 'Colch&atilde;o Queen<br />\r\n158x198 cm', 391000, 0, 391000, 10, 110, 320, '2018-09-27 17:01:54', '2020-02-26 20:27:50'),
(52, 8, 5, 'Colch&atilde;o King<br />\r\n193x203 cm', 489000, 0, 489000, 10, 140, 400, '2018-09-27 17:02:25', '2020-02-26 20:28:11'),
(53, 8, 6, 'Colch&atilde;o + Box Solteiro&nbsp;<br />\r\n088x188 cm', 274000, 0, 274000, 10, 110, 320, '2018-09-27 17:03:39', '2020-02-26 20:28:31'),
(54, 8, 7, 'Colch&atilde;o + Box Solteiro King<br />\r\n096x203 cm', 313000, 0, 313000, 10, 140, 400, '2018-09-27 17:04:37', '2020-02-26 20:28:58'),
(55, 8, 8, 'Colch&atilde;o + Box Casal<br />\r\n138x188 cm', 383000, 0, 383000, 10, 170, 500, '2018-09-27 17:05:58', '2018-09-27 17:05:58'),
(56, 8, 9, 'Colch&atilde;o + Box Queen<br />\r\n158x198 cm', 479000, 0, 479000, 10, 200, 600, '2018-09-27 17:07:15', '2020-02-26 20:29:23'),
(57, 8, 10, 'Colch&atilde;o + Box King<br />\r\n193x203 cm', 597000, 0, 597000, 10, 250, 750, '2018-09-27 17:07:58', '2020-02-26 20:35:49'),
(88, 12, 1, 'Colch&atilde;o Solteiro<br />\r\n088x188 cm', 126000, 0, 126000, 10, 50, 100, '2018-09-27 19:33:11', '2020-03-20 20:04:19'),
(89, 12, 2, 'Colch&atilde;o Solteiro King&nbsp;<br />\r\n096x203 cm', 144000, 0, 144000, 10, 60, 120, '2018-09-27 19:33:39', '2020-03-20 20:04:41'),
(90, 12, 3, 'Colch&atilde;o Casal&nbsp;<br />\r\n138x188 cm', 180000, 0, 180000, 10, 70, 140, '2018-09-27 19:34:20', '2020-03-20 20:05:17'),
(91, 12, 4, 'Colch&atilde;o Queen<br />\r\n158x198 cm', 216000, 0, 216000, 10, 100, 200, '2018-09-27 19:34:59', '2020-03-20 20:05:32'),
(92, 12, 5, 'Colch&atilde;o King<br />\r\n193x203 cm', 270000, 0, 270000, 10, 140, 280, '2018-09-27 19:35:39', '2020-03-20 20:05:50'),
(93, 14, 1, 'Colch&atilde;o Ber&ccedil;o<br />\r\n060x130x010 cm', 0, 0, 0, 2, 80, 235, '2018-09-27 20:26:18', '2020-01-28 17:36:53'),
(94, 14, 2, 'Colch&atilde;o Ber&ccedil;o<br />\r\n070x130x010 cm', 0, 0, 0, 2, 90, 273, '2018-09-27 20:27:10', '2020-01-28 17:37:18'),
(109, 15, 1, 'Colch&atilde;o Ber&ccedil;o<br />\r\n070x130x012 cm', 0, 0, 0, 2, 110, 330, '2018-09-27 21:14:20', '2020-01-28 17:36:28'),
(110, 15, 2, 'Colch&atilde;o Juvenil<br />\r\n070x150x012 cm', 0, 0, 0, 2, 126, 380, '2018-09-27 21:15:12', '2020-01-28 17:36:36'),
(111, 12, 6, 'Colch&atilde;o + Box Solteiro<br />\r\n088x188 cm', 165000, 0, 165000, 10, 150, 300, '2018-09-27 21:22:01', '2020-03-20 20:06:17'),
(112, 12, 7, 'Colch&atilde;o + Box Solteiro King<br />\r\n096x203 cm', 188000, 0, 188000, 10, 170, 350, '2018-09-27 21:22:05', '2020-03-20 20:06:44'),
(113, 12, 8, 'Colch&atilde;o + Box Casal<br />\r\n138x188 cm', 229000, 0, 229000, 10, 210, 430, '2018-09-27 21:23:58', '2020-03-20 20:07:14'),
(114, 12, 9, 'Colch&atilde;o + Box Queen<br />\r\n158x198 cm', 292000, 0, 292000, 10, 280, 560, '2018-09-27 21:25:20', '2020-03-20 20:07:44'),
(115, 12, 10, 'Colch&atilde;o + Box King<br />\r\n193x203 cm', 358000, 0, 358000, 10, 350, 700, '2018-09-27 21:26:43', '2020-03-20 20:08:09'),
(116, 13, 1, 'Colch&atilde;o Solteiro<br />\r\n088x188 cm', 148000, 0, 148000, 10, 50, 100, '2018-09-27 21:38:27', '2018-09-27 21:38:27'),
(117, 13, 2, 'Colch&atilde;o Solteiro King<br />\r\n096x203 cm', 185000, 0, 185000, 10, 60, 120, '2018-09-27 21:39:37', '2018-09-27 21:39:37'),
(118, 13, 3, 'Colch&atilde;o Casal<br />\r\n138x188 cm', 185000, 0, 185000, 10, 70, 140, '2018-09-27 21:40:51', '2018-09-27 21:40:51'),
(119, 13, 4, 'Colch&atilde;o Queen<br />\r\n158x198 cm', 219000, 0, 219000, 10, 100, 200, '2018-09-27 21:41:31', '2018-09-27 21:41:31'),
(120, 13, 5, 'Colch&atilde;o King<br />\r\n193x203 cm', 279000, 0, 279000, 10, 140, 280, '2018-09-27 21:42:00', '2018-09-27 21:42:00'),
(121, 13, 6, 'Colch&atilde;o + Box Solteiro<br />\r\n088x188 cm', 200000, 0, 200000, 10, 150, 300, '2018-09-27 21:43:03', '2018-09-27 21:43:03'),
(122, 13, 7, 'Colch&atilde;o + Box Solteiro King<br />\r\n096x203 cm', 247000, 0, 247000, 10, 170, 350, '2018-09-27 21:43:52', '2018-09-27 21:43:52'),
(123, 13, 8, 'Colch&atilde;o + Box Casal<br />\r\n138x188 cm', 249000, 0, 249000, 10, 210, 420, '2018-09-27 21:44:40', '2018-09-27 21:44:40'),
(124, 13, 9, 'Colch&atilde;o + Box Queen<br />\r\n158x198 cm', 317000, 0, 317000, 10, 280, 560, '2018-09-27 21:45:33', '2018-09-27 21:45:33'),
(125, 13, 10, 'Colch&atilde;o + Box King<br />\r\n193x203 cm', 403000, 0, 403000, 10, 350, 700, '2018-09-27 21:46:11', '2018-09-27 21:46:11'),
(129, 17, 4, 'Colch&atilde;o Queen<br />\r\n158x198 cm', 395000, 0, 395000, 10, 110, 320, '2018-09-28 15:44:33', '2018-09-28 15:44:33'),
(130, 17, 5, 'Colch&atilde;o King<br />\r\n193x203 cm', 495000, 0, 495000, 10, 140, 400, '2018-09-28 15:45:23', '2018-09-28 15:45:23'),
(134, 17, 9, 'Colch&atilde;o + Box Queen<br />\r\n158x198 cm', 493000, 0, 493000, 10, 200, 600, '2018-09-28 15:52:46', '2018-09-28 15:52:46'),
(135, 17, 10, 'Colch&atilde;o + Box King<br />\r\n193x203 cm', 619000, 0, 619000, 10, 250, 750, '2018-09-28 15:53:36', '2018-09-28 15:53:36'),
(136, 18, 1, 'Colch&atilde;o Solteiro<br />\r\n088x188 cm', 209000, 0, 209000, 10, 60, 170, '2018-09-28 15:55:43', '2018-09-28 15:55:43'),
(137, 18, 2, 'Colch&atilde;o Solteiro King<br />\r\n096x203 cm', 239000, 0, 239000, 10, 70, 210, '2018-09-28 15:56:17', '2018-09-28 15:56:17'),
(138, 18, 3, 'Colch&atilde;o Casal&nbsp;<br />\r\n138x188 cm', 295000, 0, 295000, 10, 90, 270, '2018-09-28 15:56:49', '2018-09-28 15:56:49'),
(139, 18, 4, 'Colch&atilde;o Queen<br />\r\n158x198 cm', 355000, 0, 355000, 10, 110, 320, '2018-09-28 15:57:21', '2018-09-28 15:57:21'),
(140, 18, 5, 'Colch&atilde;o King&nbsp;<br />\r\n193x203 cm', 445000, 0, 445000, 10, 140, 400, '2018-09-28 15:58:02', '2018-09-28 15:58:02'),
(141, 18, 6, 'Colch&atilde;o + Box Solteiro&nbsp;<br />\r\n088x188 cm', 261000, 0, 261000, 10, 110, 320, '2018-09-28 15:59:12', '2018-09-28 15:59:12'),
(142, 18, 7, 'Colch&atilde;o + Box Solteiro King<br />\r\n096x203 cm', 301000, 0, 301000, 10, 140, 400, '2018-09-28 15:59:57', '2018-09-28 15:59:57'),
(143, 18, 8, 'Colch&atilde;o + Box Casal<br />\r\n138x188 cm', 359000, 0, 359000, 10, 170, 500, '2018-09-28 16:01:07', '2018-09-28 16:01:07'),
(145, 18, 9, 'Colch&atilde;o + Box Queen<br />\r\n158x198 cm', 453000, 0, 453000, 10, 200, 600, '2018-09-28 16:01:59', '2018-09-28 16:01:59'),
(146, 18, 10, 'Colch&atilde;o + Box King<br />\r\n193x203 cm', 569000, 0, 569000, 10, 250, 750, '2018-09-28 16:04:05', '2018-09-28 16:04:05'),
(147, 20, 1, 'Colch&atilde;o Solteiro<br />\r\n088x188 cm', 165000, 0, 165000, 10, 60, 170, '2018-09-28 16:35:12', '2020-03-20 22:39:30'),
(148, 20, 2, 'Colch&atilde;o Solteiro King<br />\r\n096x203 cm', 188000, 0, 188000, 10, 70, 210, '2018-09-28 16:35:45', '2020-03-20 22:39:48'),
(149, 20, 3, 'Colch&atilde;o Casal&nbsp;<br />\r\n138x188 cm', 235000, 0, 235000, 10, 90, 270, '2018-09-28 16:36:52', '2020-03-20 22:40:04'),
(150, 20, 4, 'Colch&atilde;o Queen<br />\r\n158x198 cm', 282000, 0, 282000, 10, 110, 320, '2018-09-28 16:37:28', '2020-03-20 22:40:21'),
(151, 20, 5, 'Colch&atilde;o King<br />\r\n193x203 cm', 353000, 0, 353000, 10, 140, 400, '2018-09-28 16:38:05', '2020-03-20 22:40:43'),
(152, 20, 6, 'Colch&atilde;o + Box Solteiro&nbsp;<br />\r\n​​​​088x188 cm', 204000, 0, 204000, 10, 110, 320, '2018-09-28 16:39:00', '2020-03-20 22:41:16'),
(153, 20, 7, 'Colch&atilde;o + Box Solteiro King<br />\r\n096x203 cm', 232000, 0, 232000, 10, 140, 400, '2018-09-28 16:40:17', '2020-03-20 22:41:42'),
(154, 20, 8, 'Colch&atilde;o + Box Casal<br />\r\n138x188 cm', 284000, 0, 284000, 10, 170, 500, '2018-09-28 16:41:02', '2020-03-20 22:42:05'),
(155, 20, 9, 'Colch&atilde;o + Box Queen<br />\r\n158x198 cm', 358000, 0, 358000, 10, 200, 600, '2018-09-28 16:42:08', '2020-03-20 22:42:27'),
(156, 20, 10, 'Colch&atilde;o + Box King<br />\r\n193x203 cm', 441000, 0, 441000, 10, 250, 750, '2018-09-28 16:43:21', '2020-03-20 22:42:49'),
(157, 19, 1, 'Colch&atilde;o Solteiro<br />\r\n088x188 cm', 125000, 0, 125000, 10, 50, 100, '2018-09-28 16:47:18', '2020-03-20 22:35:18'),
(158, 19, 2, 'Colch&atilde;o Solteiro King<br />\r\n096x203 cm', 144000, 0, 144000, 10, 60, 120, '2018-09-28 16:48:19', '2020-03-20 22:35:37'),
(159, 19, 3, 'Colch&atilde;o Casal<br />\r\n138x188 cm', 179000, 0, 179000, 10, 70, 140, '2018-09-28 16:48:47', '2020-03-20 22:35:54'),
(160, 19, 4, 'Colch&atilde;o Queen<br />\r\n158x198 cm', 214000, 0, 214000, 10, 100, 200, '2018-09-28 16:49:52', '2020-03-20 22:36:07'),
(163, 19, 5, 'Colch&atilde;o King<br />\r\n193x203 cm', 269000, 0, 269000, 10, 140, 280, '2018-09-28 16:51:43', '2020-03-20 22:36:43'),
(164, 19, 6, 'Colch&atilde;o + Box Solteiro<br />\r\n088x188 cm', 164000, 0, 164000, 10, 150, 300, '2018-09-28 16:52:49', '2020-03-20 22:37:15'),
(165, 19, 7, 'Colch&atilde;o + Box Solteiro King<br />\r\n096x203 cm', 183000, 0, 183000, 10, 170, 350, '2018-09-28 16:53:34', '2020-03-20 22:37:36'),
(166, 19, 8, 'Colch&atilde;o + Box Casal<br />\r\n138x188 cm', 228000, 0, 228000, 10, 210, 420, '2018-09-28 16:55:10', '2020-03-20 22:37:59'),
(167, 19, 9, 'Colch&atilde;o + Box Queen<br />\r\n158x198 cm', 290000, 0, 290000, 10, 280, 560, '2018-09-28 16:56:03', '2020-03-20 22:38:21'),
(168, 19, 10, 'Colch&atilde;o + Box King<br />\r\n193x203 cm', 357000, 0, 357000, 10, 350, 700, '2018-09-28 16:56:50', '2020-03-20 22:38:43'),
(179, 22, 1, 'Colch&atilde;o Solteiro<br />\r\n088x188 cm', 116000, 0, 116000, 10, 50, 100, '2018-09-28 17:58:07', '2020-03-20 20:00:03'),
(180, 22, 2, 'Colch&atilde;o Solteiro King<br />\r\n096x203 cm', 133000, 0, 133000, 10, 60, 120, '2018-09-28 17:58:37', '2020-03-20 20:00:23'),
(181, 22, 3, 'Colch&atilde;o Casal<br />\r\n138x188 cm', 165000, 0, 165000, 10, 70, 140, '2018-09-28 17:59:02', '2020-03-20 20:00:41'),
(182, 22, 4, 'Colch&atilde;o Queen<br />\r\n158x198 cm', 198000, 0, 198000, 10, 100, 200, '2018-09-28 17:59:31', '2020-03-20 20:00:56'),
(183, 22, 5, 'Colch&atilde;o King<br />\r\n193x203 cm', 248000, 0, 248000, 10, 140, 280, '2018-09-28 18:00:04', '2020-03-20 20:01:14'),
(184, 22, 6, 'Colch&atilde;o + Box Solteiro<br />\r\n088x188 cm', 155000, 0, 155000, 10, 150, 300, '2018-09-28 18:01:25', '2020-03-20 20:01:46'),
(185, 22, 7, 'Colch&atilde;o + Box Solteiro King<br />\r\n096x203 cm', 177000, 0, 177000, 10, 170, 350, '2018-09-28 18:02:11', '2020-03-20 20:02:14'),
(186, 22, 8, 'Colch&atilde;o + Box Casal&nbsp;<br />\r\n138x188 cm', 214000, 0, 214000, 10, 210, 420, '2018-09-28 19:50:37', '2020-03-20 20:02:41'),
(187, 22, 9, 'Colch&atilde;o + Box Queen<br />\r\n158x198 cm', 274000, 0, 274000, 10, 280, 560, '2018-09-28 19:54:00', '2020-03-20 20:03:07'),
(188, 22, 10, 'Colch&atilde;o + Box King<br />\r\n193x203 cm', 336000, 0, 336000, 10, 350, 700, '2018-09-28 19:55:04', '2020-03-20 20:03:30'),
(189, 23, 1, 'Box Ba&uacute;<br />\r\n078x188 cm', 79000, 0, 79000, 10, 60, 170, '2018-09-29 16:17:58', '2018-10-04 17:20:45'),
(196, 27, 1, 'Colch&atilde;o Solteiro<br />\r\n078x188cm', 84000, 0, 84000, 10, 40, 110, '2018-10-01 15:31:47', '2018-10-04 17:00:26'),
(197, 27, 2, 'Colch&atilde;o Solteiro<br />\r\n088x188 cm', 84000, 0, 84000, 10, 40, 110, '2018-10-01 15:32:12', '2018-10-04 17:00:35'),
(198, 27, 3, 'Colch&atilde;o Solteiro King<br />\r\n096x203 cm', 96000, 0, 96000, 10, 50, 160, '2018-10-01 15:32:45', '2018-10-04 17:00:48'),
(199, 27, 4, 'Colch&atilde;o Casal<br />\r\n128x188 cm', 119000, 0, 119000, 10, 65, 200, '2018-10-01 15:33:16', '2018-10-04 17:01:01'),
(200, 27, 5, 'Colch&atilde;o Casal<br />\r\n138x188 cm', 119000, 0, 119000, 10, 65, 200, '2018-10-01 15:33:37', '2018-10-04 17:01:12'),
(201, 27, 6, 'Colch&atilde;o Queen<br />\r\n158x198 cm', 144000, 0, 144000, 10, 80, 240, '2018-10-01 15:34:09', '2018-10-04 17:01:23'),
(204, 28, 1, 'Colch&atilde;o Solteiro<br />\r\n078x188 cm', 105000, 0, 105000, 10, 50, 100, '2018-10-01 21:18:23', '2020-03-20 19:48:08'),
(205, 28, 2, 'Colch&atilde;o Solteiro<br />\r\n088x188 cm', 105000, 0, 105000, 10, 50, 100, '2018-10-01 21:18:41', '2020-03-20 19:48:40'),
(206, 28, 3, 'Colch&atilde;o Solteiro King&nbsp;<br />\r\n096x203 cm', 118000, 0, 118000, 10, 60, 120, '2018-10-01 21:19:14', '2020-03-20 19:49:06'),
(207, 28, 4, 'Colch&atilde;o Casal<br />\r\n128x188 cm', 148000, 0, 148000, 10, 70, 140, '2018-10-01 21:19:42', '2020-03-20 19:49:33'),
(208, 28, 5, 'Colch&atilde;o Casal<br />\r\n138x188 cm', 148000, 0, 148000, 10, 70, 140, '2018-10-01 21:20:12', '2020-03-20 19:49:56'),
(209, 28, 6, 'Colch&atilde;o Queen<br />\r\n158x198 cm', 177000, 0, 177000, 10, 100, 200, '2018-10-01 21:21:14', '2020-03-20 19:50:20'),
(210, 28, 7, 'Colch&atilde;o King<br />\r\n193x203 cm', 222000, 0, 222000, 10, 140, 280, '2018-10-01 21:21:51', '2020-03-20 19:50:42'),
(211, 29, 1, 'Colch&atilde;o Solteiro<br />\r\n078x188 cm', 84000, 0, 84000, 10, 40, 110, '2018-10-01 21:27:33', '2018-10-01 21:27:42'),
(212, 29, 2, 'Colch&atilde;o Solteiro<br />\r\n088x188 cm', 84000, 0, 84000, 10, 40, 110, '2018-10-01 21:28:06', '2018-10-01 21:28:06'),
(213, 29, 3, 'Colch&atilde;o Solteiro King<br />\r\n096x203 cm', 96000, 0, 96000, 10, 50, 160, '2018-10-01 21:28:38', '2018-10-01 21:28:38'),
(214, 29, 4, 'Colch&atilde;o Casal<br />\r\n128x188 cm', 119000, 0, 119000, 10, 65, 200, '2018-10-01 21:29:08', '2018-10-01 21:29:08'),
(215, 29, 5, 'Colch&atilde;o Casal<br />\r\n138x188 cm', 119000, 0, 119000, 10, 65, 200, '2018-10-01 21:29:33', '2018-10-04 17:03:03'),
(216, 29, 6, 'Colch&atilde;o Queen<br />\r\n158x198 cm', 144000, 0, 144000, 10, 80, 240, '2018-10-01 21:30:02', '2018-10-01 21:30:02'),
(217, 29, 7, 'Colch&atilde;o King<br />\r\n193x203 cm<br />\r\n&nbsp;', 179000, 0, 179000, 10, 100, 300, '2018-10-01 21:30:34', '2018-10-01 21:30:34'),
(222, 31, 1, 'Revestimento Corano', 125000, 0, 125000, 10, 50, 250, '2018-10-02 20:49:34', '2019-02-14 16:12:48'),
(223, 31, 3, 'Revestimento Tecido B', 145000, 0, 145000, 10, 50, 250, '2018-10-02 20:53:34', '2019-02-14 16:13:14'),
(224, 31, 2, 'Revestimento Tecido C', 139900, 0, 139900, 10, 50, 250, '2018-10-02 20:54:32', '2018-10-02 20:55:17'),
(225, 32, 1, 'Revestimento Corano', 125000, 0, 125000, 10, 50, 250, '2018-10-02 21:15:03', '2019-02-14 16:11:47'),
(226, 32, 3, 'Revestimento Tecido C', 139900, 0, 139900, 2, 50, 250, '2018-10-02 21:15:35', '2019-02-14 16:12:24'),
(227, 32, 3, 'Revestimento Tecido B', 145000, 0, 145000, 10, 50, 250, '2018-10-02 21:16:18', '2019-02-14 16:12:07'),
(228, 33, 4, 'CASAL CORINO', 269000, 0, 269000, 10, 80, 600, '2018-10-03 17:30:39', '2018-11-01 15:51:25'),
(229, 33, 1, 'SOLTEIRO CORINO', 209000, 0, 209000, 10, 80, 500, '2018-10-03 17:40:18', '2018-11-01 15:51:02'),
(230, 33, 5, 'CASAL TECIDO C', 299000, 0, 299000, 10, 80, 600, '2018-10-03 17:41:13', '2018-11-01 15:51:34'),
(231, 33, 6, 'CASAL TECIDO B', 309000, 0, 309000, 10, 80, 600, '2018-10-03 17:41:44', '2018-11-01 15:51:44'),
(232, 33, 2, 'SOLTEIRO TECIDO C', 229000, 0, 229000, 10, 80, 500, '2018-10-03 17:42:22', '2018-11-01 15:51:10'),
(233, 33, 3, 'SOLTEIRO TECIDO B', 239000, 0, 239000, 10, 80, 500, '2018-10-03 17:43:09', '2018-11-01 15:51:18'),
(234, 34, 0, 'CASAL', 399000, 0, 399000, 10, 160, 480, '2018-10-03 17:58:41', '2018-10-03 17:58:41'),
(236, 35, 2, 'Colch&atilde;o Casal<br />\r\n138x188 cm', 0, 0, 0, 10, 78, 234, '2018-10-03 19:12:04', '2019-02-12 14:00:37'),
(247, 38, 1, 'Colch&atilde;o Solteiro King&nbsp;<br />\r\n096x203 cm', 0, 0, 0, 10, 72, 216, '2018-10-03 21:19:07', '2019-02-12 13:47:08'),
(248, 38, 2, 'Colch&atilde;o Casal<br />\r\n138x188 cm', 0, 0, 0, 10, 96, 288, '2018-10-03 21:19:40', '2019-02-12 13:47:18'),
(249, 38, 3, 'Colch&atilde;o Queen<br />\r\n158x198 cm', 0, 0, 0, 10, 115, 450, '2018-10-03 21:20:26', '2019-02-12 13:47:27'),
(250, 38, 4, 'Colch&atilde;o King<br />\r\n193x203 cm', 0, 0, 0, 10, 145, 550, '2018-10-03 21:21:09', '2019-02-12 13:47:38'),
(251, 39, 1, 'Colch&atilde;o Solteiro<br />\r\n078x188 cm', 0, 0, 0, 10, 60, 170, '2018-10-04 16:16:42', '2020-01-28 17:31:49'),
(252, 39, 2, 'Colch&atilde;o Solteiro<br />\r\n088x188 cm', 0, 0, 0, 10, 60, 170, '2018-10-04 16:17:28', '2020-01-28 17:31:58'),
(253, 39, 4, 'Colch&atilde;o Casal<br />\r\n128x188 cm', 0, 0, 0, 10, 90, 270, '2018-10-04 16:17:54', '2020-01-28 17:32:12'),
(254, 39, 3, 'Colch&atilde;o Solteiro King<br />\r\n096x203 cm', 0, 0, 0, 10, 70, 210, '2018-10-04 16:18:13', '2020-01-28 17:32:05'),
(255, 39, 5, 'Colch&atilde;o Casal<br />\r\n138x188 cm', 0, 0, 0, 10, 90, 270, '2018-10-04 16:18:39', '2020-01-28 17:32:18'),
(256, 39, 6, 'Colch&atilde;o Queen<br />\r\n158x198 cm', 0, 0, 0, 10, 110, 320, '2018-10-04 16:19:12', '2020-01-28 17:32:25'),
(257, 39, 7, 'Colch&atilde;o King&nbsp;<br />\r\n193x203 cm', 0, 0, 0, 10, 140, 400, '2018-10-04 16:19:40', '2020-01-28 17:32:32'),
(258, 39, 8, 'Colch&atilde;o + Box Solteiro<br />\r\n078x188 cm', 0, 0, 0, 10, 110, 320, '2018-10-04 16:20:39', '2020-01-28 17:32:41'),
(259, 39, 9, 'Colch&atilde;o + Box Solteiro<br />\r\n088x188 cm', 0, 0, 0, 10, 110, 320, '2018-10-04 16:21:16', '2020-01-28 17:32:48'),
(260, 39, 10, 'Colch&atilde;o + Box Solteiro King<br />\r\n096x203 cm', 0, 0, 0, 10, 140, 400, '2018-10-04 16:25:20', '2020-01-28 17:32:55'),
(261, 39, 11, 'Colch&atilde;o + Box Casal<br />\r\n128x188 cm', 0, 0, 0, 10, 170, 500, '2018-10-04 16:26:24', '2020-01-28 17:33:04'),
(262, 39, 12, 'Colch&atilde;o + Box Casal<br />\r\n138x188 cm', 0, 0, 0, 10, 170, 500, '2018-10-04 16:27:07', '2020-01-28 17:33:12'),
(263, 39, 13, 'Colch&atilde;o + Box Queen<br />\r\n158x198 cm', 0, 0, 0, 10, 200, 600, '2018-10-04 16:27:53', '2020-01-28 17:33:19'),
(264, 39, 14, 'Colch&atilde;o + Box King<br />\r\n193x203 cm', 0, 0, 0, 10, 250, 750, '2018-10-04 16:28:29', '2020-01-28 17:33:29'),
(279, 23, 2, 'Box Ba&uacute;<br />\r\n088x188 cm', 79000, 0, 79000, 10, 60, 170, '2018-10-04 17:23:44', '2018-10-04 17:24:18'),
(280, 23, 3, 'Box Ba&uacute;<br />\r\n096x203 cm', 85000, 0, 85000, 10, 70, 210, '2018-10-04 17:24:09', '2018-10-04 17:24:09'),
(281, 24, 1, 'Box Ba&uacute;<br />\r\n128x188 cm', 99000, 0, 99000, 10, 90, 270, '2018-10-04 17:27:00', '2018-12-01 20:26:18'),
(282, 24, 2, 'Box Ba&uacute;<br />\r\n138x188 cm', 99000, 0, 99000, 10, 90, 270, '2018-10-04 17:27:30', '2018-12-01 20:26:32'),
(283, 25, 0, 'Box Ba&uacute;<br />\r\n158x188 cm', 159000, 0, 159000, 10, 110, 320, '2018-10-04 17:30:05', '2018-10-04 17:30:05'),
(284, 26, 0, 'Box Ba&uacute;<br />\r\n193x203 cm', 169000, 0, 169000, 10, 140, 400, '2018-10-04 17:34:14', '2018-10-04 17:34:14'),
(285, 41, 0, 'CASAL TECIDO B', 429000, 0, 429000, 10, 101, 500, '2018-10-04 21:33:19', '2018-10-04 21:33:32'),
(286, 42, 1, 'Colch&atilde;o Solteiro King + Box Ajust&aacute;vel Sem Massageador<br />\r\n100x200 cm', 0, 0, 0, 10, 150, 425, '2018-10-04 21:53:49', '2019-08-21 15:01:58'),
(287, 42, 2, 'Colch&atilde;o Solteiro King + Box Ajust&aacute;vel Com Massageador<br />\r\n100x200 cm<br />\r\n&nbsp;', 690000, 0, 690000, 10, 150, 425, '2018-10-04 21:54:58', '2018-12-04 14:05:27'),
(288, 43, 1, 'Casal 138 - Tecido B', 76500, 0, 76500, 10, 12, 63, '2018-10-04 22:13:24', '2018-10-10 20:04:09'),
(289, 43, 2, 'Queen 158 - Tecido B', 81000, 0, 81000, 10, 14, 72, '2018-10-04 22:14:00', '2018-10-10 20:04:19'),
(290, 43, 3, 'King 193 - Tecido B', 95900, 0, 95900, 10, 17, 88, '2018-10-04 22:14:39', '2018-10-10 20:04:29'),
(291, 44, 1, 'Casal 138 - Tecido B', 76500, 0, 76500, 10, 12, 63, '2018-10-04 22:49:56', '2018-10-10 20:03:11'),
(292, 44, 2, 'Queen 158 - Tecido B', 81000, 0, 81000, 10, 14, 72, '2018-10-04 22:51:50', '2018-10-10 20:03:21'),
(293, 44, 3, 'King 193 - Tecido B', 95900, 0, 95900, 10, 17, 88, '2018-10-04 22:52:34', '2018-10-10 20:03:38'),
(294, 46, 1, 'Casal 138 - Tecido B', 74800, 0, 74800, 10, 12, 63, '2018-10-04 23:01:38', '2018-10-10 20:02:17'),
(295, 46, 2, 'Queen 158 - Tecido B', 79300, 0, 79300, 10, 14, 72, '2018-10-04 23:02:11', '2018-10-10 20:02:31'),
(296, 46, 3, 'King 193 - Tecido B', 93800, 0, 93800, 10, 17, 88, '2018-10-04 23:02:45', '2018-10-10 20:02:44'),
(297, 46, 0, 'Solteiro 88 - Tecido B', 57800, 0, 57800, 10, 8, 41, '2018-10-04 23:03:16', '2018-10-10 20:02:05'),
(299, 47, 2, 'Solteiro<br />\r\n088x188 cm', 127000, 0, 127000, 10, 80, 180, '2018-10-05 15:27:29', '2018-10-19 15:38:55'),
(300, 47, 3, 'Solteiro King<br />\r\n096x203 cm', 139000, 0, 139000, 10, 80, 215, '2018-10-05 15:29:02', '2018-10-19 15:39:07'),
(301, 48, 1, 'Solteiro<br />\r\n078x188 cm', 64000, 0, 64000, 10, 50, 158, '2018-10-05 15:46:00', '2018-10-05 15:46:07'),
(302, 48, 2, 'Solteiro<br />\r\n088x188 cm', 66000, 0, 66000, 10, 50, 178, '2018-10-05 15:46:47', '2018-10-05 15:46:47'),
(303, 48, 3, 'Solteiro King<br />\r\n096x203 cm', 69000, 0, 69000, 10, 50, 210, '2018-10-05 15:47:13', '2018-10-05 15:47:13'),
(304, 49, 1, 'Solteiro<br />\r\n078x188 cm', 70000, 0, 70000, 10, 80, 160, '2018-10-05 15:59:24', '2018-10-05 15:59:24'),
(305, 49, 2, 'Solteiro<br />\r\n088x188 cm', 74000, 0, 74000, 10, 80, 180, '2018-10-05 15:59:59', '2018-10-05 15:59:59'),
(306, 49, 3, 'Solteiro King<br />\r\n096x203 cm', 85000, 0, 85000, 10, 80, 215, '2018-10-05 16:01:11', '2018-10-05 16:01:11'),
(324, 50, 4, 'Bicama Molejo Vers&aacute;til + Colch&atilde;o Atenas<br />\r\nSolteiro 078x188 cm', 148000, 0, 148000, 10, 90, 240, '2018-10-05 17:28:32', '2018-10-25 22:34:18'),
(325, 50, 5, 'Bicama Molejo Vers&aacute;til + Colch&atilde;o Atenas<br />\r\nSolteiro 088x188 cm', 156000, 0, 156000, 10, 90, 240, '2018-10-05 17:30:04', '2018-10-25 22:34:49'),
(326, 50, 6, 'Bicama Molejo Vers&aacute;til + Colch&atilde;o Atenas<br />\r\nSolteiro King 096x203 cm', 175000, 0, 175000, 10, 120, 340, '2018-10-05 17:30:39', '2018-10-25 22:35:08'),
(327, 52, 1, 'Solteiro<br />\r\n078x188 cm', 66000, 0, 66000, 10, 50, 100, '2018-10-05 22:16:31', '2018-10-05 22:16:31'),
(328, 52, 2, 'Solteiro<br />\r\n088x188 cm', 68000, 0, 68000, 10, 50, 100, '2018-10-05 22:17:17', '2018-10-05 22:17:17'),
(329, 52, 3, 'Solteiro King<br />\r\n096x203 cm', 70000, 0, 70000, 10, 60, 120, '2018-10-05 22:17:37', '2018-10-05 22:17:37'),
(330, 52, 4, 'Casal<br />\r\n138x188 cm', 110000, 0, 110000, 10, 70, 140, '2018-10-05 22:18:01', '2018-10-05 22:18:01'),
(331, 52, 5, 'Queen<br />\r\n158x198 cm', 135000, 0, 135000, 10, 100, 200, '2018-10-05 22:18:27', '2018-10-05 22:18:27'),
(332, 52, 6, 'King&nbsp;<br />\r\n193x203 cm', 140000, 0, 140000, 10, 140, 280, '2018-10-05 22:19:00', '2018-10-05 22:19:00'),
(333, 53, 1, 'Solteiro<br />\r\n078x188 cm', 66000, 0, 66000, 10, 50, 100, '2018-10-05 22:25:28', '2018-10-05 22:25:28'),
(334, 53, 2, 'Solteiro<br />\r\n088x188 cm', 68000, 0, 68000, 10, 50, 100, '2018-10-05 22:25:47', '2018-10-05 22:25:47'),
(335, 53, 3, 'Solteiro King<br />\r\n096x203 cm', 70000, 0, 70000, 10, 60, 120, '2018-10-05 22:26:12', '2018-10-05 22:26:12'),
(336, 53, 4, 'Casal<br />\r\n138x188 cm', 110000, 0, 110000, 10, 70, 140, '2018-10-05 22:26:41', '2018-10-05 22:26:41'),
(337, 53, 5, 'Queen<br />\r\n158x198 cm', 135000, 0, 135000, 10, 100, 200, '2018-10-05 22:27:37', '2018-10-05 22:27:37'),
(338, 53, 6, 'King&nbsp;<br />\r\n193x203 cm', 140000, 0, 140000, 10, 140, 280, '2018-10-05 22:28:09', '2018-10-05 22:28:09'),
(354, 55, 1, 'Colch&atilde;o Solteiro<br />\r\n078x188cm', 0, 0, 0, 6, 33, 100, '2018-10-06 15:42:56', '2020-01-28 17:24:07'),
(355, 55, 2, 'Colch&atilde;o Solteiro<br />\r\n088x188 cm', 0, 0, 0, 6, 33, 100, '2018-10-06 15:43:26', '2020-01-28 17:24:14'),
(356, 56, 1, 'Colch&atilde;o Solteiro<br />\r\n078x188 cm&nbsp;', 0, 0, 0, 6, 33, 100, '2018-10-06 15:49:27', '2020-01-28 17:25:13'),
(357, 56, 2, 'Colch&atilde;o Solteiro<br />\r\n088x188 cm', 0, 0, 0, 6, 33, 100, '2018-10-06 15:49:46', '2020-01-28 17:25:19'),
(358, 57, 1, 'Colch&atilde;o Solteiro<br />\r\n078x188x018 cm', 0, 0, 0, 10, 33, 100, '2018-10-06 16:19:40', '2020-01-28 17:24:33'),
(359, 57, 2, 'Colch&atilde;o Solteiro<br />\r\n088x188x018 cm', 0, 0, 0, 10, 33, 100, '2018-10-06 16:20:11', '2020-01-28 17:24:39'),
(360, 57, 3, 'Colch&atilde;o Solteiro<br />\r\n078x188x025 cm', 0, 0, 0, 10, 33, 100, '2018-10-06 16:20:49', '2020-01-28 17:24:46'),
(361, 57, 4, 'Colch&atilde;o Solteiro<br />\r\n088x188x025 cm', 0, 0, 0, 10, 33, 100, '2018-10-06 16:21:44', '2020-01-28 17:24:53'),
(362, 58, 1, 'Colch&atilde;o Solteiro<br />\r\n078x188x018 cm', 0, 0, 0, 10, 33, 100, '2018-10-06 16:24:13', '2020-01-28 17:22:21'),
(363, 58, 2, 'Colch&atilde;o Solteiro<br />\r\n088x188x018 cm', 0, 0, 0, 10, 33, 100, '2018-10-06 16:24:59', '2020-01-28 17:22:27'),
(364, 58, 3, 'Colch&atilde;o Solteiro King&nbsp;<br />\r\n096x203x018 cm', 0, 0, 0, 10, 33, 100, '2018-10-06 16:26:21', '2020-01-28 17:22:34'),
(365, 58, 4, 'Colch&atilde;o Casal<br />\r\n128x188x018 cm', 0, 0, 0, 10, 33, 100, '2018-10-06 16:26:53', '2020-01-28 17:22:42'),
(366, 58, 5, 'Colch&atilde;o Casal<br />\r\n138x188x018 cm', 0, 0, 0, 10, 33, 100, '2018-10-06 16:27:19', '2020-01-28 17:22:48'),
(367, 58, 6, 'Colch&atilde;o Queen<br />\r\n158x198x018 cm', 0, 0, 0, 10, 33, 100, '2018-10-06 16:27:46', '2020-01-28 17:22:58'),
(368, 58, 7, 'Colch&atilde;o Solteiro<br />\r\n078x188x025 cm', 0, 0, 0, 10, 40, 110, '2018-10-06 16:28:39', '2020-01-28 17:23:06'),
(369, 58, 8, 'Colch&atilde;o Solteiro<br />\r\n088x188x025 cm', 0, 0, 0, 10, 40, 110, '2018-10-06 16:29:02', '2020-01-28 17:23:14'),
(370, 58, 9, 'Colch&atilde;o Solteiro King&nbsp;<br />\r\n096x203x025 cm', 0, 0, 0, 10, 50, 160, '2018-10-06 16:29:31', '2020-01-28 17:23:22'),
(371, 58, 10, 'Colch&atilde;o Casal<br />\r\n128x188x025 cm', 0, 0, 0, 10, 65, 200, '2018-10-06 16:30:16', '2020-01-28 17:23:30'),
(372, 58, 11, 'Colch&atilde;o Casal<br />\r\n138x188x025 cm', 0, 0, 0, 10, 65, 200, '2018-10-06 16:30:48', '2020-01-28 17:23:38'),
(373, 58, 12, 'Colch&atilde;o Queen<br />\r\n158x198x025 cm', 0, 0, 0, 10, 80, 240, '2018-10-06 16:31:15', '2020-01-28 17:23:46'),
(374, 59, 1, 'Colch&atilde;o Solteiro<br />\r\n078x188x018 cm', 0, 0, 0, 10, 33, 100, '2018-10-06 16:47:50', '2020-01-28 17:20:26'),
(375, 59, 2, 'Colch&atilde;o Solteiro<br />\r\n088x188x018 cm', 0, 0, 0, 10, 33, 100, '2018-10-06 16:48:16', '2020-01-28 17:20:33'),
(376, 59, 3, 'Colch&atilde;o Solteiro King&nbsp;<br />\r\n096x203x018 cm', 0, 0, 0, 10, 42, 125, '2018-10-06 16:49:02', '2020-01-28 17:20:44'),
(377, 59, 4, 'Colch&atilde;o Casal<br />\r\n128x188x018 cm', 0, 0, 0, 10, 52, 155, '2018-10-06 16:49:35', '2020-01-28 17:20:55'),
(378, 59, 5, 'Colch&atilde;o Casal<br />\r\n138x188x018 cm', 0, 0, 0, 10, 52, 155, '2018-10-06 16:50:00', '2020-01-28 17:21:03'),
(379, 59, 6, 'Colch&atilde;o Queen<br />\r\n158x198x018 cm', 0, 0, 0, 10, 62, 190, '2018-10-06 16:50:27', '2020-01-28 17:21:11'),
(380, 59, 7, 'Colch&atilde;o Solteiro<br />\r\n078x188x025 cm', 0, 0, 0, 10, 40, 110, '2018-10-06 16:51:08', '2020-01-28 17:21:18'),
(381, 59, 8, 'Colch&atilde;o Solteiro<br />\r\n088x188x025 cm', 0, 0, 0, 10, 40, 110, '2018-10-06 16:51:42', '2020-01-28 17:21:27'),
(382, 59, 9, 'Colch&atilde;o Solteiro King&nbsp;<br />\r\n096x203x025 cm', 0, 0, 0, 10, 50, 160, '2018-10-06 16:52:08', '2020-01-28 17:21:37'),
(383, 59, 10, 'Colch&atilde;o Casal<br />\r\n128x188x025 cm', 0, 0, 0, 10, 65, 200, '2018-10-06 16:53:15', '2020-01-28 17:21:45'),
(384, 59, 11, 'Colch&atilde;o Casal<br />\r\n138x188x025 cm', 0, 0, 0, 10, 65, 200, '2018-10-06 16:53:38', '2020-01-28 17:21:53'),
(385, 59, 12, 'Colch&atilde;o Queen<br />\r\n158x198x025 cm', 0, 0, 0, 10, 80, 240, '2018-10-06 16:54:07', '2020-01-28 17:22:00'),
(386, 60, 1, 'Colch&atilde;o Solteiro<br />\r\n078x188x022 cm', 0, 0, 0, 6, 33, 100, '2018-10-06 17:02:20', '2020-01-28 17:16:51'),
(387, 60, 2, 'Colch&atilde;o Solteiro<br />\r\n088x188x022 cm', 0, 0, 0, 6, 33, 100, '2018-10-06 17:02:42', '2020-01-28 17:19:30'),
(388, 60, 3, 'Colch&atilde;o Solteiro King&nbsp;<br />\r\n096x203x022 cm', 0, 0, 0, 6, 42, 125, '2018-10-06 17:03:11', '2020-01-28 17:19:38'),
(389, 60, 4, 'Colch&atilde;o Casal<br />\r\n128x188x022 cm', 0, 0, 0, 6, 52, 155, '2018-10-06 17:03:47', '2020-01-28 17:19:50'),
(390, 60, 5, 'Colch&atilde;o Casal<br />\r\n138x188x022 cm', 0, 0, 0, 10, 52, 155, '2018-10-06 17:04:19', '2020-01-28 17:19:57'),
(391, 60, 6, 'Colch&atilde;o Queen<br />\r\n158x198x022 cm', 0, 0, 0, 10, 62, 190, '2018-10-06 17:04:53', '2020-01-28 17:20:05'),
(399, 63, 0, '50x70 cm', 27900, 0, 27900, 2, 10, 30, '2018-10-08 16:27:11', '2018-10-08 16:27:11'),
(400, 64, 0, '50x70 cm', 27900, 0, 27900, 2, 10, 30, '2018-10-08 16:27:32', '2018-10-08 16:27:32'),
(402, 66, 0, '50x70 cm', 13900, 0, 13900, 2, 10, 30, '2018-10-08 16:39:52', '2018-10-08 16:39:52'),
(403, 67, 0, '50x70 cm', 24900, 0, 24900, 2, 10, 30, '2018-10-08 16:46:19', '2018-10-08 16:46:19'),
(404, 68, 0, '50x70 cm', 9900, 0, 9900, 2, 10, 30, '2018-10-08 16:50:47', '2018-10-08 16:50:47'),
(405, 69, 0, '51x61 cm', 24900, 0, 24900, 2, 10, 30, '2018-10-08 17:41:16', '2020-02-26 19:04:52'),
(406, 70, 0, '60x94 cm', 34900, 0, 34900, 2, 10, 30, '2018-10-09 15:24:05', '2020-02-26 19:04:26'),
(407, 71, 0, '70x82 cm', 24000, 0, 24000, 3, 10, 30, '2018-10-09 15:36:17', '2020-02-26 19:07:28'),
(408, 72, 0, '45x70 cm', 24900, 0, 24900, 2, 10, 30, '2018-10-09 15:57:03', '2018-10-09 15:57:03'),
(409, 73, 0, '26x52 cm', 14900, 0, 14900, 2, 10, 30, '2018-10-09 16:03:14', '2018-10-09 16:03:14'),
(410, 74, 0, '60x80 cm', 21000, 18900, 21000, 3, 10, 30, '2018-10-09 16:10:08', '2018-11-12 21:30:17'),
(411, 75, 0, '50x70 cm', 38900, 0, 38900, 2, 10, 30, '2018-10-09 16:25:15', '2018-10-09 16:25:15'),
(412, 76, 0, '50x70 cm', 32900, 0, 32900, 2, 10, 30, '2018-10-09 16:29:26', '2019-08-29 19:02:21'),
(413, 77, 0, '50x70 cm', 32900, 0, 32900, 2, 10, 30, '2018-10-09 16:35:25', '2019-08-29 19:01:41'),
(414, 78, 1, '088x188x03 cm', 89900, 0, 89900, 6, 10, 30, '2018-10-09 16:46:09', '2018-10-09 16:46:09'),
(415, 78, 2, '138x188x03 cm', 119900, 0, 119900, 10, 10, 30, '2018-10-09 16:46:42', '2018-10-09 16:46:42'),
(416, 78, 3, '158x198x03 cm', 159900, 0, 159900, 10, 10, 30, '2018-10-09 16:47:23', '2018-10-09 16:47:23'),
(417, 78, 4, '193x203x03 cm', 189900, 0, 189900, 10, 10, 30, '2018-10-09 16:48:05', '2018-10-09 16:48:05'),
(419, 79, 2, '096x203x04 cm', 31900, 0, 31900, 2, 10, 30, '2018-10-09 16:57:24', '2020-02-26 22:18:08'),
(420, 79, 3, '138x188x04 cm', 38900, 0, 38900, 2, 10, 30, '2018-10-09 16:57:46', '2020-02-26 22:18:30'),
(421, 79, 4, '158x198x04 cm', 48900, 0, 48900, 2, 10, 30, '2018-10-09 16:58:10', '2020-02-26 22:18:43'),
(422, 80, 0, '50x70 cm', 12900, 0, 12900, 2, 10, 30, '2018-10-09 17:11:17', '2018-10-09 17:11:17'),
(423, 81, 0, '50x70 cm', 12900, 0, 12900, 2, 10, 30, '2018-10-09 17:15:11', '2018-10-09 17:15:11'),
(424, 82, 0, '50x70 cm', 11900, 0, 11900, 2, 10, 30, '2018-10-09 17:25:08', '2018-10-09 17:25:08'),
(425, 83, 0, '50x70 cm', 17900, 16900, 17900, 2, 10, 30, '2018-10-09 17:35:08', '2019-05-03 18:14:37'),
(427, 85, 0, '50x70 cm', 17900, 0, 17900, 2, 10, 30, '2018-10-09 17:45:51', '2019-05-03 18:11:41'),
(428, 86, 0, '50x70 cm', 13900, 0, 13900, 2, 10, 30, '2018-10-09 18:05:58', '2019-05-03 18:13:48'),
(430, 88, 0, '50x70 cm', 8900, 0, 8900, 2, 10, 30, '2018-10-09 22:17:20', '2020-02-29 19:49:04'),
(432, 90, 0, '50x70 cm', 8900, 0, 8900, 2, 10, 30, '2018-10-09 22:40:02', '2018-10-09 22:40:02'),
(433, 91, 0, '38x38 cm', 12900, 0, 12900, 2, 10, 30, '2018-10-09 22:47:32', '2018-10-09 22:47:32'),
(434, 92, 0, '50x70 cm', 5900, 0, 5900, 2, 10, 30, '2018-10-10 15:43:20', '2018-10-10 15:43:20'),
(435, 93, 0, '50x70 cm', 24900, 0, 24900, 2, 10, 30, '2018-10-10 15:48:57', '2019-01-28 16:12:07'),
(436, 94, 1, 'Casal 138 - Tecido B', 169900, 0, 169900, 10, 22, 164, '2018-10-10 16:51:37', '2018-10-10 19:59:26'),
(437, 94, 2, 'Queen 158 - Tecido B', 181000, 0, 181000, 10, 24, 192, '2018-10-10 16:51:58', '2018-10-10 19:59:55'),
(438, 94, 3, 'King 193 - Tecido B', 204600, 0, 204600, 10, 29, 226, '2018-10-10 16:52:29', '2018-10-10 20:00:18'),
(439, 95, 1, 'Casal 138 - Tecido B', 106400, 0, 106400, 10, 12, 21, '2018-10-10 17:03:57', '2018-10-10 17:03:57'),
(440, 95, 2, 'Queen 158 - Tecido B', 114100, 0, 114100, 10, 14, 24, '2018-10-10 17:04:22', '2018-10-10 17:04:22'),
(441, 95, 3, 'King 193 - Tecido B', 127600, 0, 127600, 10, 17, 29, '2018-10-10 17:04:47', '2018-10-10 17:04:47'),
(442, 95, 4, 'Cabeceira 138 + Cama Estofada MH 2696&nbsp;', 253400, 0, 253400, 10, 69, 400, '2018-10-10 18:02:32', '2018-10-10 18:02:32'),
(443, 95, 5, 'Cabeceira 158 + Cama Estofada MH 2696', 270500, 0, 270500, 10, 75, 271, '2018-10-10 19:52:26', '2018-10-10 19:52:26'),
(444, 95, 6, 'Cabeceira 193 + Cama Estofada MH 2696', 296200, 0, 296200, 10, 84, 319, '2018-10-10 19:53:57', '2018-10-10 19:53:57'),
(445, 96, 1, '078x188 cm', 25000, 0, 25000, 2, 40, 110, '2018-10-10 20:49:56', '2018-10-10 20:49:56'),
(446, 96, 2, '088x188 cm', 28000, 0, 28000, 2, 40, 110, '2018-10-10 20:50:31', '2018-10-10 20:50:31'),
(447, 96, 3, '096x203 cm', 30000, 0, 30000, 2, 50, 160, '2018-10-10 20:50:54', '2018-10-10 20:50:54'),
(448, 97, 1, 'Solteiro + Colch&atilde;o Olympia&nbsp;<br />\r\n078x188 cm', 170000, 0, 170000, 10, 90, 240, '2018-10-10 21:19:31', '2018-10-10 21:19:31'),
(449, 97, 2, 'Solteiro + Colch&atilde;o Olympia&nbsp;<br />\r\n088x188 cm', 182000, 0, 182000, 10, 90, 240, '2018-10-10 21:19:56', '2018-10-10 21:19:56'),
(450, 97, 3, 'Solteiro King + Colch&atilde;o Olympia<br />\r\n096x203 cm', 204000, 0, 204000, 10, 120, 340, '2018-10-10 21:21:19', '2018-10-10 21:21:19'),
(451, 98, 1, '128x188 cm', 34000, 0, 34000, 3, 65, 200, '2018-10-16 21:34:32', '2018-10-16 21:35:25'),
(452, 98, 2, '138x188 cm', 35000, 0, 35000, 3, 65, 200, '2018-10-16 21:35:09', '2018-10-16 21:35:09'),
(453, 99, 1, '158x198 cm', 54000, 0, 54000, 5, 80, 240, '2018-10-16 21:39:28', '2018-10-16 21:39:28'),
(454, 99, 2, '193x203 cm', 60000, 0, 60000, 6, 100, 300, '2018-10-16 21:39:56', '2018-10-16 21:39:56'),
(455, 100, 1, '078x188 cm', 97000, 0, 97000, 10, 80, 160, '2018-10-19 15:40:54', '2018-10-19 15:40:54'),
(456, 100, 2, '088x188 cm', 97000, 0, 97000, 10, 80, 180, '2018-10-19 15:41:19', '2018-10-19 15:41:19'),
(457, 100, 3, '096x203 cm', 105000, 0, 105000, 10, 80, 215, '2018-10-19 15:41:45', '2018-10-19 15:41:45'),
(458, 102, 2, 'Colch&atilde;o Solteiro<br />\r\n078x188cm', 94000, 0, 94000, 10, 60, 170, '2018-10-19 21:20:20', '2018-10-19 22:24:48'),
(459, 102, 3, 'Colch&atilde;o Solteiro<br />\r\n088x188 cm', 94000, 0, 94000, 10, 60, 170, '2018-10-19 21:20:42', '2018-10-19 22:25:00'),
(460, 102, 4, 'Colch&atilde;o Solteiro King<br />\r\n096x203 cm', 119000, 0, 119000, 10, 70, 210, '2018-10-19 21:21:08', '2018-10-19 22:25:14'),
(461, 102, 5, 'Colch&atilde;o Casal<br />\r\n128x188 cm', 139000, 0, 139000, 10, 90, 270, '2018-10-19 21:21:37', '2018-10-19 22:25:27'),
(462, 102, 6, 'Colch&atilde;o Casal&nbsp;<br />\r\n138x188 cm', 139000, 0, 139000, 10, 90, 270, '2018-10-19 21:21:58', '2018-10-19 22:25:42'),
(463, 102, 7, 'Colch&atilde;o Queen<br />\r\n158x198 cm', 166000, 0, 166000, 10, 110, 320, '2018-10-19 21:22:30', '2018-10-19 22:25:57'),
(465, 102, 8, 'Colch&atilde;o King&nbsp;<br />\r\n193x203 cm', 205000, 0, 205000, 10, 140, 400, '2018-10-19 21:23:02', '2018-10-19 22:26:12'),
(472, 103, 1, 'Colch&atilde;o Solteiro<br />\r\n078x188cm', 75000, 0, 75000, 7, 40, 110, '2018-10-19 21:57:14', '2018-10-19 22:21:59'),
(473, 103, 2, 'Colch&atilde;o Solteiro<br />\r\n088x188 cm', 75000, 0, 75000, 7, 40, 110, '2018-10-19 21:57:37', '2018-10-19 22:22:15'),
(474, 103, 3, 'Colch&atilde;o Solteiro King&nbsp;<br />\r\n096x203 cm', 88000, 0, 88000, 8, 50, 160, '2018-10-19 21:58:14', '2018-10-19 22:22:46'),
(475, 103, 4, 'Colch&atilde;o Casal<br />\r\n128x188 cm', 109000, 0, 109000, 10, 65, 200, '2018-10-19 21:58:44', '2018-10-19 22:23:04'),
(476, 103, 5, 'Colch&atilde;o Casal&nbsp;<br />\r\n138x188 cm', 109000, 0, 109000, 10, 65, 200, '2018-10-19 21:59:02', '2018-10-19 22:23:25'),
(477, 103, 6, 'Colch&atilde;o Queen<br />\r\n158x198 cm', 134000, 0, 134000, 10, 80, 240, '2018-10-19 21:59:29', '2018-10-19 22:23:48'),
(505, 107, 1, 'Colch&atilde;o Solteiro<br />\r\n078x188x014 cm', 36000, 0, 36000, 4, 80, 110, '2018-10-23 21:24:59', '2018-10-23 21:25:19'),
(506, 107, 2, 'Colch&atilde;o Solteiro<br />\r\n088x188x014 cm', 39000, 0, 39000, 4, 80, 125, '2018-10-23 21:25:54', '2018-10-23 21:25:54'),
(508, 107, 3, 'Colch&atilde;o Casal<br />\r\n128x188x014 cm', 59000, 0, 59000, 6, 80, 145, '2018-10-23 21:26:42', '2018-10-23 21:26:42'),
(509, 107, 4, 'Colch&atilde;o Casal<br />\r\n138x188x014 cm', 63000, 0, 63000, 6, 80, 180, '2018-10-23 21:27:25', '2018-10-23 21:27:25'),
(510, 107, 6, 'Colch&atilde;o Solteiro<br />\r\n078x188x017 cm', 42000, 0, 42000, 4, 80, 110, '2018-10-23 21:28:00', '2018-10-23 21:28:00'),
(511, 107, 7, 'Colch&atilde;o Solteiro<br />\r\n088x188x017 cm', 47000, 0, 47000, 4, 80, 125, '2018-10-23 21:28:30', '2018-10-23 21:28:30'),
(512, 107, 8, 'Colch&atilde;o Casal<br />\r\n128x188x017 cm', 68000, 0, 68000, 6, 80, 145, '2018-10-23 21:29:11', '2018-10-23 21:29:11'),
(513, 107, 9, 'Colch&atilde;o Casal<br />\r\n138x188x017 cm', 73000, 0, 73000, 7, 80, 180, '2018-10-23 21:29:45', '2018-10-23 21:29:45'),
(514, 107, 5, 'Colch&atilde;o Queen<br />\r\n158x198x014 cm', 75000, 0, 75000, 7, 80, 200, '2018-10-23 21:30:21', '2018-10-23 21:30:21'),
(515, 107, 10, 'Colch&atilde;o Queen<br />\r\n158x198x017 cm', 88000, 0, 88000, 8, 80, 200, '2018-10-23 21:30:51', '2018-10-23 21:30:51'),
(516, 108, 1, 'Colch&atilde;o Solteiro<br />\r\n078x188x014 cm', 39000, 0, 39000, 3, 80, 110, '2018-10-23 22:00:06', '2018-10-23 22:00:06'),
(517, 108, 2, 'Colch&atilde;o Solteiro<br />\r\n088x188x014 cm', 44000, 0, 44000, 4, 80, 125, '2018-10-23 22:00:31', '2018-10-23 22:00:31'),
(518, 108, 3, 'Colch&atilde;o Casal<br />\r\n128x188x014 cm', 64000, 0, 64000, 6, 80, 160, '2018-10-23 22:01:16', '2018-10-23 22:01:16'),
(519, 108, 4, 'Colch&atilde;o Casal<br />\r\n138x188x014 cm', 69000, 0, 69000, 6, 80, 180, '2018-10-23 22:01:50', '2018-10-23 22:01:50'),
(520, 108, 5, 'Colch&atilde;o Queen<br />\r\n158x198x014 cm', 82000, 0, 82000, 8, 80, 200, '2018-10-23 22:02:46', '2018-10-23 22:02:46'),
(521, 108, 6, 'Colch&atilde;o Solteiro<br />\r\n078x188x017 cm', 46000, 0, 46000, 4, 80, 110, '2018-10-23 22:05:01', '2018-10-23 22:05:01'),
(522, 108, 7, 'Colch&atilde;o Solteiro<br />\r\n088x188x017 cm', 52000, 0, 52000, 5, 80, 125, '2018-10-23 22:15:00', '2018-10-23 22:15:00'),
(523, 108, 8, 'Colch&atilde;o Casal<br />\r\n128x188x017 cm', 75000, 0, 75000, 7, 80, 160, '2018-10-23 22:15:40', '2018-10-23 22:15:40'),
(524, 108, 9, 'Colch&atilde;o Casal<br />\r\n138x188x017 cm', 79000, 0, 79000, 7, 80, 180, '2018-10-23 22:16:26', '2018-10-23 22:16:26'),
(525, 108, 10, 'Colch&atilde;o Queen<br />\r\n158x198x017 cm', 98000, 0, 98000, 10, 80, 200, '2018-10-23 22:16:59', '2018-10-23 22:16:59'),
(526, 109, 1, 'Colch&atilde;o Solteiro<br />\r\n078x188x012 cm', 28000, 0, 28000, 2, 28, 50, '2018-10-23 23:08:59', '2018-10-23 23:08:59'),
(527, 109, 2, 'Colch&atilde;o Solteiro<br />\r\n088x188x012 cm', 31000, 0, 31000, 3, 28, 50, '2018-10-23 23:09:26', '2018-10-23 23:09:26'),
(528, 110, 1, 'Colch&atilde;o Solteiro<br />\r\n078x188x020 cm', 57000, 0, 57000, 5, 33, 100, '2018-10-24 19:36:57', '2018-10-24 19:36:57'),
(529, 110, 2, 'Colch&atilde;o Solteiro<br />\r\n088x188x020 cm', 64000, 0, 64000, 6, 33, 100, '2018-10-24 19:37:21', '2018-10-24 19:37:21'),
(530, 110, 3, 'Colch&atilde;o Casal<br />\r\n128x188x020 cm', 90000, 0, 90000, 9, 52, 155, '2018-10-24 19:37:56', '2018-10-24 19:37:56'),
(531, 110, 4, 'Colch&atilde;o Casal<br />\r\n138x188x020 cm', 97000, 0, 97000, 10, 52, 155, '2018-10-24 19:38:38', '2018-10-24 19:38:38'),
(532, 110, 5, 'Colch&atilde;o Queen<br />\r\n158x198x020 cm', 118000, 0, 118000, 10, 62, 190, '2018-10-24 19:39:14', '2018-10-24 19:39:14'),
(533, 110, 6, 'Colch&atilde;o Solteiro<br />\r\n078x188x025 cm', 69000, 0, 69000, 6, 40, 110, '2018-10-24 19:39:48', '2018-10-24 19:39:48'),
(534, 110, 7, 'Colch&atilde;o Solteiro<br />\r\n088x188x025 cm', 78000, 0, 78000, 7, 40, 110, '2018-10-24 19:40:15', '2018-10-24 19:40:15'),
(535, 110, 8, 'Colch&atilde;o Casal<br />\r\n128x188x025 cm', 112000, 0, 112000, 10, 65, 200, '2018-10-24 19:40:37', '2018-10-24 19:40:37'),
(536, 110, 9, 'Colch&atilde;o Casal<br />\r\n138x188x025 cm', 119000, 0, 119000, 10, 65, 200, '2018-10-24 19:41:02', '2018-10-24 19:41:02'),
(537, 110, 10, 'Colch&atilde;o Queen<br />\r\n158x198x025 cm', 145000, 0, 145000, 10, 80, 240, '2018-10-24 19:41:32', '2018-10-24 19:41:32'),
(538, 111, 1, 'Colch&atilde;o Solteiro<br />\r\n078x188x020 cm', 70000, 0, 70000, 7, 33, 100, '2018-10-24 20:22:39', '2018-10-24 20:22:39'),
(539, 111, 2, 'Colch&atilde;o Solteiro<br />\r\n088x188x020 cm', 79000, 0, 79000, 10, 33, 100, '2018-10-24 20:23:19', '2018-10-24 20:23:19'),
(540, 111, 3, 'Colch&atilde;o Casal<br />\r\n128x188x020 cm', 112000, 0, 112000, 10, 52, 155, '2018-10-24 20:24:06', '2018-10-24 20:24:06'),
(541, 111, 4, 'Colch&atilde;o Casal<br />\r\n138x188x020 cm', 120000, 0, 120000, 10, 52, 155, '2018-10-24 20:24:39', '2018-10-24 20:24:39'),
(542, 111, 5, 'Colch&atilde;o Queen<br />\r\n158x198x020 cm', 146000, 0, 146000, 10, 62, 190, '2018-10-24 20:25:04', '2018-10-24 20:25:04'),
(543, 112, 1, 'Colch&atilde;o Solteiro<br />\r\n078x188x018 cm', 52000, 0, 52000, 5, 33, 100, '2018-10-24 21:13:26', '2018-10-24 21:13:26'),
(544, 112, 2, 'Colch&atilde;o Solteiro<br />\r\n088x188x018 cm', 52000, 0, 52000, 5, 33, 100, '2018-10-24 21:14:09', '2018-10-24 21:14:09'),
(545, 112, 3, 'Colch&atilde;o Solteiro King&nbsp;<br />\r\n096x203x018 cm', 69000, 0, 69000, 6, 42, 125, '2018-10-24 21:14:42', '2018-10-24 21:14:42'),
(546, 112, 4, 'Colch&atilde;o Casal<br />\r\n128x188x018 cm', 89000, 0, 89000, 8, 52, 155, '2018-10-24 21:15:17', '2018-10-24 21:15:17'),
(547, 112, 5, 'Colch&atilde;o Casal<br />\r\n138x188x018 cm', 89000, 0, 89000, 8, 52, 155, '2018-10-24 21:16:08', '2018-10-24 21:16:08'),
(548, 112, 6, 'Colch&atilde;o Queen<br />\r\n158x198x018 cm', 99000, 0, 99000, 9, 62, 190, '2018-10-24 21:16:59', '2018-10-24 21:16:59'),
(549, 112, 7, 'Colch&atilde;o Solteiro<br />\r\n078x188x025 cm', 69000, 0, 69000, 6, 40, 110, '2018-10-24 21:17:50', '2018-10-24 21:17:50'),
(550, 112, 8, 'Colch&atilde;o Solteiro<br />\r\n088x188x025 cm', 69000, 0, 69000, 6, 40, 110, '2018-10-24 21:18:48', '2018-10-24 21:18:48'),
(551, 112, 9, 'Colch&atilde;o Solteiro King&nbsp;<br />\r\n096x203x025 cm', 89000, 0, 89000, 8, 50, 160, '2018-10-24 21:19:32', '2018-10-24 21:19:32'),
(552, 112, 10, 'Colch&atilde;o Vi&uacute;va<br />\r\n110x188x025 cm', 110000, 0, 110000, 10, 50, 160, '2018-10-24 21:20:04', '2018-10-24 21:20:04'),
(553, 112, 11, 'Colch&atilde;o Casal<br />\r\n128x188x025 cm', 110000, 0, 110000, 10, 65, 200, '2018-10-24 21:20:34', '2018-10-24 21:20:34'),
(554, 112, 12, 'Colch&atilde;o Casal<br />\r\n138x188x025 cm', 110000, 0, 110000, 10, 65, 200, '2018-10-24 21:20:59', '2018-10-24 21:20:59'),
(555, 112, 13, 'Colch&atilde;o Queen<br />\r\n158x198x025 cm', 129000, 0, 129000, 10, 80, 240, '2018-10-24 21:21:40', '2018-10-24 21:21:40'),
(556, 113, 1, 'Colch&atilde;o Solteiro<br />\r\n078x188x022 cm', 64000, 0, 64000, 6, 33, 100, '2018-10-24 21:35:59', '2018-10-24 21:35:59'),
(557, 113, 2, 'Colch&atilde;o Solteiro<br />\r\n088x188x022 cm', 72000, 0, 72000, 7, 33, 100, '2018-10-24 21:36:37', '2018-10-24 21:36:37'),
(558, 113, 3, 'Colch&atilde;o Solteiro King&nbsp;<br />\r\n096x203x022 cm', 84000, 0, 84000, 8, 42, 125, '2018-10-24 21:37:08', '2018-10-24 21:37:08'),
(560, 113, 5, 'Colch&atilde;o Casal<br />\r\n138x188x022 cm', 110000, 0, 110000, 10, 52, 155, '2018-10-24 21:38:27', '2018-10-24 21:38:27'),
(561, 113, 4, 'Colch&atilde;o Casal<br />\r\n128x188x022 cm', 103000, 0, 103000, 10, 52, 155, '2018-10-24 21:39:26', '2018-10-24 21:39:26'),
(562, 113, 6, 'Colch&atilde;o Queen<br />\r\n158x198x022 cm', 135000, 0, 135000, 10, 62, 190, '2018-10-24 21:40:24', '2018-10-24 21:42:14'),
(563, 113, 7, 'Colch&atilde;o King<br />\r\n193x203x022 cm', 168000, 0, 168000, 10, 80, 235, '2018-10-24 21:46:46', '2018-10-24 21:46:58'),
(570, 115, 1, 'Solteiro<br />\r\n65x80 cm', 48000, 0, 48000, 4, 80, 200, '2018-10-24 23:05:37', '2018-10-24 23:06:43'),
(571, 115, 2, 'Casal<br />\r\n120x80 cm', 88000, 0, 88000, 8, 100, 200, '2018-10-24 23:06:36', '2018-11-09 20:36:35'),
(572, 116, 1, 'Solteiro&nbsp;<br />\r\n63x63 cm', 27000, 24900, 27000, 3, 80, 200, '2018-10-24 23:07:57', '2019-02-14 21:03:31');
INSERT INTO `produtos_medidas` (`id`, `produto_id`, `ordem`, `titulo`, `a_vista`, `oferta`, `parcelado`, `parcelas`, `peso`, `cubagem`, `created_at`, `updated_at`) VALUES
(573, 116, 2, 'Casal<br />\r\n120x63 cm', 54000, 49900, 54000, 5, 100, 200, '2018-10-24 23:08:19', '2019-02-14 21:03:43'),
(574, 117, 1, '65x1,00x1,5 cm', 5900, 0, 5900, 2, 10, 15, '2018-10-24 23:14:23', '2018-10-24 23:14:23'),
(575, 117, 2, '65x1,35x3,0 cm', 7900, 0, 7900, 2, 10, 15, '2018-10-24 23:14:54', '2018-10-24 23:14:54'),
(576, 118, 0, '65x180x3,0 cm', 5900, 0, 5900, 2, 10, 15, '2018-10-24 23:20:36', '2018-10-24 23:20:48'),
(577, 119, 1, 'Solteiro<br />\r\n85x185 cm', 5900, 0, 5900, 2, 15, 25, '2018-10-24 23:27:48', '2018-10-24 23:27:48'),
(578, 119, 2, 'Casal<br />\r\n135x185 cm', 8900, 0, 8900, 2, 15, 25, '2018-10-24 23:28:10', '2018-10-24 23:28:10'),
(579, 120, 0, '60x92 cm', 11900, 0, 11900, 2, 15, 35, '2018-10-24 23:33:21', '2018-10-24 23:33:21'),
(583, 123, 1, 'Colch&atilde;o Solteiro<br />\r\n088x188 cm', 164000, 0, 164000, 10, 60, 170, '2018-10-26 19:39:54', '2019-05-02 21:11:54'),
(584, 123, 2, 'Colch&atilde;o Solteiro King&nbsp;<br />\r\n096x203 cm', 189000, 0, 189000, 10, 70, 210, '2018-10-26 19:40:13', '2019-05-02 21:12:13'),
(585, 123, 3, 'Colch&atilde;o Casal<br />\r\n128x188 cm', 224000, 0, 224000, 10, 90, 270, '2018-10-26 19:40:46', '2019-05-02 21:12:34'),
(586, 123, 4, 'Colch&atilde;o Casal&nbsp;<br />\r\n138x188 cm', 239000, 0, 239000, 10, 90, 270, '2018-10-26 19:41:07', '2019-05-02 21:12:57'),
(587, 123, 5, 'Colch&atilde;o Queen<br />\r\n158x198 cm', 277000, 0, 277000, 10, 110, 320, '2018-10-26 19:41:31', '2019-05-02 21:13:15'),
(588, 123, 6, 'Colch&atilde;o King&nbsp;<br />\r\n193x203 cm', 336000, 0, 336000, 10, 140, 400, '2018-10-26 19:41:52', '2019-05-02 21:13:36'),
(589, 123, 7, 'Colch&atilde;o + Box Solteiro&nbsp;<br />\r\n088x188 cm', 194000, 0, 194000, 10, 110, 320, '2018-10-26 19:44:07', '2019-05-02 21:16:36'),
(590, 123, 8, 'Colch&atilde;o + Box Solteiro King<br />\r\n096x 203 cm', 221000, 0, 221000, 10, 140, 400, '2018-10-26 19:44:53', '2018-10-26 19:44:53'),
(591, 123, 9, 'Colch&atilde;o + Box Casal<br />\r\n128x188 cm', 261000, 0, 261000, 10, 170, 500, '2018-10-26 19:45:31', '2019-05-02 21:18:20'),
(592, 123, 10, 'Colch&atilde;o + Box Casal&nbsp;<br />\r\n138x188 cm', 277000, 0, 277000, 10, 170, 500, '2018-10-26 19:46:09', '2019-05-02 21:18:57'),
(593, 123, 11, 'Colch&atilde;o + Box Queen<br />\r\n158x198 cm', 335000, 0, 335000, 10, 200, 600, '2018-10-26 19:46:47', '2019-05-02 21:22:53'),
(594, 123, 12, 'Colch&atilde;o + Box King<br />\r\n193x203 cm', 400000, 0, 400000, 10, 250, 750, '2018-10-26 19:47:35', '2019-05-02 21:23:18'),
(595, 124, 0, '35x55 cm', 8900, 0, 8900, 2, 10, 15, '2018-10-26 23:21:15', '2018-10-26 23:22:28'),
(596, 125, 1, 'Solteiro 88 - Corino', 51000, 0, 51000, 5, 12, 21, '2018-10-29 21:25:47', '2018-10-29 21:25:47'),
(597, 125, 2, 'Casal 138 - Corino', 72000, 0, 72000, 5, 12, 21, '2018-10-29 21:26:40', '2018-10-29 21:26:40'),
(598, 125, 3, 'Queen 158 - Corino', 76000, 0, 76000, 5, 14, 24, '2018-10-29 21:27:29', '2018-10-29 21:27:29'),
(599, 125, 4, 'King 193 - Corino', 79000, 0, 79000, 5, 17, 29, '2018-10-29 21:27:59', '2018-10-29 21:27:59'),
(600, 126, 1, 'Solteiro 88 - Corino', 46000, 0, 46000, 5, 12, 21, '2018-10-29 21:32:37', '2018-10-29 21:32:37'),
(601, 126, 2, 'Casal 138 - Corino', 65000, 0, 65000, 5, 12, 21, '2018-10-29 21:33:18', '2018-10-29 21:33:18'),
(602, 126, 3, 'Queen 158 - Corino', 69000, 0, 69000, 5, 14, 24, '2018-10-29 21:33:53', '2018-10-29 21:33:53'),
(603, 126, 4, 'King 193 - Corino', 73000, 0, 73000, 5, 17, 29, '2018-10-29 21:34:14', '2018-10-29 21:34:14'),
(604, 127, 1, 'Solteiro 88 - Corino', 33000, 0, 33000, 5, 12, 21, '2018-10-29 21:38:27', '2018-10-29 21:38:27'),
(605, 127, 2, 'Casal 138 - Corino', 43000, 0, 43000, 5, 12, 21, '2018-10-29 21:38:47', '2018-10-29 21:38:47'),
(606, 127, 3, 'Queen 158 - Corino', 47000, 0, 47000, 5, 14, 24, '2018-10-29 21:39:13', '2018-10-29 21:39:13'),
(607, 127, 4, 'King 193 - Corino', 52000, 0, 52000, 5, 17, 29, '2018-10-29 21:39:31', '2018-10-29 21:39:31'),
(608, 128, 1, 'Solteiro 88 - Corino', 49000, 0, 49000, 5, 12, 21, '2018-10-29 21:45:34', '2018-10-29 21:45:34'),
(609, 128, 2, 'Casal 138 - Corino', 69000, 0, 69000, 5, 12, 21, '2018-10-29 21:46:02', '2018-10-29 21:46:02'),
(610, 128, 3, 'Queen 158 - Corino', 73000, 0, 73000, 5, 14, 24, '2018-10-29 21:46:26', '2018-10-29 21:46:26'),
(611, 128, 4, 'King 193 - Corino', 76000, 0, 76000, 5, 17, 29, '2018-10-29 21:46:47', '2018-10-29 21:46:47'),
(612, 129, 1, 'Solteiro 88 - Corino', 49000, 0, 49000, 5, 12, 21, '2018-10-29 21:49:55', '2018-10-29 21:49:55'),
(613, 129, 2, 'Casal 138 - Corino', 69000, 0, 69000, 5, 12, 21, '2018-10-29 21:50:06', '2018-10-29 21:50:06'),
(614, 129, 3, 'Queen 158 - Corino', 72000, 0, 72000, 5, 14, 24, '2018-10-29 21:50:30', '2018-10-29 21:50:30'),
(615, 129, 4, 'King 193 - Corino', 77000, 0, 77000, 5, 17, 29, '2018-10-29 21:50:55', '2018-10-29 21:50:55'),
(616, 130, 1, 'Solteiro 88 - Corino', 48000, 0, 48000, 5, 12, 21, '2018-10-29 21:53:39', '2018-10-29 21:53:39'),
(617, 130, 2, 'Casal 138 - Corino', 55000, 0, 55000, 5, 12, 21, '2018-10-29 21:53:52', '2018-10-29 21:53:52'),
(618, 130, 3, 'Queen 158 - Corino', 63000, 0, 63000, 5, 14, 24, '2018-10-29 21:54:08', '2018-10-29 21:54:08'),
(619, 130, 4, 'King 193 - Corino', 70000, 0, 70000, 5, 17, 29, '2018-10-29 21:54:24', '2018-10-29 21:54:24'),
(620, 131, 1, 'Solteiro 88 - Corino', 35000, 0, 35000, 5, 12, 21, '2018-10-29 21:58:12', '2018-10-29 21:58:12'),
(621, 131, 2, 'Casal 138 - Corino', 41000, 0, 41000, 5, 12, 21, '2018-10-29 21:58:27', '2018-10-29 21:58:27'),
(622, 131, 3, 'Queen 158 - Corino', 49000, 0, 49000, 5, 14, 24, '2018-10-29 21:58:42', '2018-10-29 21:58:42'),
(623, 131, 4, 'King 193 - Corino', 53000, 0, 53000, 5, 17, 29, '2018-10-29 21:59:31', '2018-10-29 21:59:31'),
(624, 132, 1, 'Solteiro 88 - Corino', 49000, 0, 49000, 5, 12, 21, '2018-10-29 22:02:39', '2018-10-29 22:02:39'),
(625, 132, 2, 'Casal 138 - Corino', 69000, 0, 69000, 5, 12, 21, '2018-10-29 22:03:05', '2018-10-29 22:03:05'),
(626, 132, 3, 'Queen 158 - Corino', 73000, 0, 73000, 5, 14, 24, '2018-10-29 22:03:20', '2018-10-29 22:03:20'),
(627, 132, 4, 'King 193 - Corino', 76000, 0, 76000, 5, 17, 29, '2018-10-29 22:03:34', '2018-10-29 22:03:34'),
(628, 133, 1, 'Solteiro 88 - Corino', 49000, 0, 49000, 5, 12, 21, '2018-10-29 22:06:37', '2018-10-29 22:06:37'),
(629, 133, 2, 'Casal 138 - Corino', 69000, 0, 69000, 5, 12, 21, '2018-10-29 22:06:52', '2018-10-29 22:06:52'),
(630, 133, 3, 'Queen 158 - Corino', 73000, 0, 73000, 5, 14, 24, '2018-10-29 22:07:12', '2018-10-29 22:07:12'),
(631, 133, 4, 'King 193 - Corino', 76000, 0, 76000, 5, 17, 29, '2018-10-29 22:07:28', '2018-10-29 22:07:28'),
(632, 134, 1, 'Solteiro 88 - Corino', 49000, 0, 49000, 5, 12, 21, '2018-10-29 22:12:59', '2018-10-29 22:12:59'),
(633, 134, 2, 'Casal 138 - Corino', 69000, 0, 69000, 5, 12, 21, '2018-10-29 22:13:13', '2018-10-29 22:13:13'),
(634, 134, 3, 'Queen 158 - Corino', 73000, 0, 73000, 5, 14, 24, '2018-10-29 22:13:28', '2018-10-29 22:13:28'),
(635, 134, 4, 'King 193 - Corino', 76000, 0, 76000, 5, 17, 29, '2018-10-29 22:13:45', '2018-10-29 22:13:45'),
(636, 135, 1, 'Solteiro 88 - Corino', 44000, 0, 44000, 5, 12, 21, '2018-10-29 23:20:02', '2018-10-29 23:20:02'),
(637, 135, 2, 'Casal 138 - Corino', 51000, 0, 51000, 5, 12, 21, '2018-10-29 23:20:39', '2018-10-29 23:20:39'),
(638, 135, 3, 'Queen 158 - Corino', 55000, 0, 55000, 5, 14, 24, '2018-10-29 23:21:04', '2018-10-29 23:21:04'),
(639, 135, 4, 'King 193 - Corino', 63000, 0, 63000, 5, 17, 29, '2018-10-29 23:21:19', '2018-10-29 23:21:19'),
(640, 136, 1, 'Solteiro 88 - Corino', 43000, 0, 43000, 5, 12, 21, '2018-10-29 23:23:40', '2018-10-29 23:23:40'),
(641, 136, 2, 'Casal 138 - Corino', 51000, 0, 51000, 5, 12, 21, '2018-10-29 23:23:55', '2018-10-29 23:23:55'),
(642, 136, 3, 'Queen 158 - Corino', 55000, 0, 55000, 5, 14, 24, '2018-10-29 23:24:15', '2018-10-29 23:24:15'),
(643, 136, 4, 'King 193 - Corino', 65000, 0, 65000, 5, 17, 29, '2018-10-29 23:24:32', '2018-10-29 23:24:32'),
(644, 137, 0, 'Revestimento Corano', 399000, 0, 399000, 10, 50, 250, '2018-10-30 21:03:59', '2019-02-14 16:10:38'),
(645, 139, 0, 'Revestimento Suede', 249000, 0, 249000, 10, 50, 250, '2018-10-30 21:45:20', '2018-10-30 21:57:43'),
(646, 140, 0, 'Revestimento Corano', 499000, 0, 499000, 10, 50, 250, '2018-10-30 22:05:58', '2019-02-14 16:10:06'),
(647, 138, 0, 'Revestimento Corano', 299000, 0, 299000, 10, 50, 250, '2018-10-30 23:18:12', '2019-02-14 16:09:47'),
(648, 141, 1, '088x188x02 cm', 34900, 0, 34900, 3, 10, 30, '2018-10-30 23:30:57', '2019-05-03 18:09:22'),
(649, 141, 2, '138x188x02 cm', 51900, 0, 51900, 4, 10, 30, '2018-10-30 23:31:16', '2019-05-03 18:09:40'),
(650, 141, 3, '158x198x02 cm', 61900, 0, 61900, 5, 10, 30, '2018-10-30 23:31:43', '2019-05-03 18:10:00'),
(651, 141, 4, '193x203x02 cm', 79000, 76900, 79000, 7, 10, 30, '2018-10-30 23:32:21', '2019-05-03 18:10:33'),
(652, 142, 1, 'Solteiro 88 - Corino', 33000, 0, 33000, 5, 12, 21, '2018-10-31 19:49:31', '2018-10-31 19:49:31'),
(653, 142, 2, 'Casal 138 - Corino', 47000, 0, 47000, 5, 12, 21, '2018-10-31 19:49:51', '2018-10-31 19:49:51'),
(654, 142, 3, 'Queen 158 - Corino', 52000, 0, 52000, 5, 14, 24, '2018-10-31 19:50:13', '2018-10-31 19:50:13'),
(655, 142, 4, 'King 193 - Corino', 60000, 0, 60000, 5, 17, 29, '2018-10-31 19:54:03', '2018-10-31 19:54:03'),
(656, 143, 1, 'Solteiro 88 - Corino', 49000, 0, 49000, 5, 12, 21, '2018-10-31 19:56:48', '2018-10-31 19:56:48'),
(657, 143, 2, 'Casal 138 - Corino', 69000, 0, 69000, 5, 12, 21, '2018-10-31 19:57:09', '2018-10-31 19:57:09'),
(658, 143, 3, 'Queen 158 - Corino', 72000, 0, 72000, 5, 14, 24, '2018-10-31 19:57:29', '2018-10-31 19:57:29'),
(659, 143, 4, 'King 193 - Corino', 77000, 0, 77000, 5, 17, 29, '2018-10-31 19:57:54', '2018-10-31 19:57:54'),
(660, 144, 1, 'Solteiro 88 - Corino', 49000, 0, 49000, 5, 12, 21, '2018-10-31 20:00:42', '2018-10-31 20:00:42'),
(661, 144, 2, 'Casal 138 - Corino', 60000, 0, 60000, 5, 12, 21, '2018-10-31 20:01:03', '2018-10-31 20:01:03'),
(662, 144, 3, 'Queen 158 - Corino', 65000, 0, 65000, 5, 14, 24, '2018-10-31 20:01:22', '2018-10-31 20:01:22'),
(663, 144, 4, 'King 193 - Corino', 69000, 0, 69000, 5, 17, 29, '2018-10-31 20:01:41', '2018-10-31 20:01:41'),
(664, 145, 1, 'Solteiro 88 - Corino', 35000, 0, 35000, 5, 12, 21, '2018-10-31 20:04:27', '2018-10-31 20:04:27'),
(665, 145, 2, 'Casal 138 - Corino', 41000, 0, 41000, 5, 12, 21, '2018-10-31 20:04:40', '2018-10-31 20:04:40'),
(666, 145, 3, 'Queen 158 - Corino', 49000, 0, 49000, 5, 14, 24, '2018-10-31 20:04:51', '2018-10-31 20:04:51'),
(667, 145, 4, 'King 193 - Corino', 53000, 0, 53000, 5, 17, 29, '2018-10-31 20:05:09', '2018-10-31 20:05:09'),
(668, 146, 1, 'Solteiro 88 - Corino', 35000, 0, 35000, 5, 12, 21, '2018-10-31 20:07:38', '2018-10-31 20:07:38'),
(669, 146, 2, 'Casal 138 - Corino', 41000, 0, 41000, 5, 12, 21, '2018-10-31 20:07:49', '2018-10-31 20:07:49'),
(670, 146, 3, 'Queen 158 - Corino', 49000, 0, 49000, 5, 14, 24, '2018-10-31 20:08:03', '2018-10-31 20:08:03'),
(671, 146, 4, 'King 193 - Corino', 53000, 0, 53000, 5, 17, 29, '2018-10-31 20:08:27', '2018-10-31 20:08:27'),
(672, 147, 1, 'Solteiro 88 - Corino', 49000, 0, 49000, 5, 12, 21, '2018-10-31 20:11:34', '2018-10-31 20:11:34'),
(673, 147, 2, 'Casal 138 - Corino', 60000, 0, 60000, 5, 12, 21, '2018-10-31 20:11:49', '2018-10-31 20:11:49'),
(674, 147, 3, 'Queen 158 - Corino', 65000, 0, 65000, 5, 14, 24, '2018-10-31 20:12:12', '2018-10-31 20:12:12'),
(675, 147, 4, 'King 193 - Corino', 69000, 0, 69000, 5, 17, 29, '2018-10-31 20:12:30', '2018-10-31 20:12:30'),
(676, 148, 1, 'Solteiro 88 - Corino', 48000, 0, 48000, 5, 12, 21, '2018-10-31 20:19:29', '2018-10-31 20:19:29'),
(677, 148, 2, 'Casal 138 - Corino', 55000, 0, 55000, 5, 12, 21, '2018-10-31 20:19:52', '2018-10-31 20:19:52'),
(678, 148, 3, 'Queen 158 - Corino', 62000, 0, 62000, 5, 14, 24, '2018-10-31 20:20:09', '2018-10-31 20:20:09'),
(679, 148, 4, 'King 193 - Corino', 70000, 0, 70000, 5, 17, 29, '2018-10-31 20:20:35', '2018-10-31 20:20:35'),
(680, 149, 1, 'Solteiro 88 - Suede', 41000, 0, 41000, 5, 12, 21, '2018-10-31 20:23:55', '2018-10-31 20:24:27'),
(682, 149, 2, 'Casal 138 - Suede', 50000, 0, 50000, 5, 12, 21, '2018-10-31 20:24:16', '2018-10-31 20:24:52'),
(683, 149, 3, 'Queen 158 - Suede', 57000, 0, 57000, 5, 14, 24, '2018-10-31 20:25:29', '2018-10-31 20:25:29'),
(684, 149, 4, 'King 193 - Suede', 64000, 0, 64000, 5, 17, 29, '2018-10-31 20:25:51', '2018-10-31 20:25:51'),
(685, 150, 1, 'Solteiro 88 - Corino', 49000, 0, 49000, 5, 12, 21, '2018-10-31 20:31:11', '2018-10-31 20:31:11'),
(686, 150, 2, 'Casal 138 - Corino', 69000, 0, 69000, 5, 12, 21, '2018-10-31 20:31:30', '2018-10-31 20:31:30'),
(687, 150, 3, 'Queen 158 - Corino', 72000, 0, 72000, 5, 14, 24, '2018-10-31 20:31:45', '2018-10-31 20:31:45'),
(688, 150, 4, 'King 193 - Corino', 77000, 0, 77000, 5, 17, 29, '2018-10-31 20:32:00', '2018-10-31 20:32:00'),
(689, 151, 1, 'Solteiro 88&nbsp;', 45000, 0, 45000, 5, 12, 21, '2018-10-31 20:38:59', '2018-10-31 20:38:59'),
(690, 151, 2, 'Casal 138&nbsp;', 66000, 0, 66000, 5, 12, 21, '2018-10-31 20:39:13', '2018-10-31 20:39:13'),
(691, 151, 3, 'Queen 158&nbsp;', 73000, 0, 73000, 5, 14, 24, '2018-10-31 20:39:33', '2018-10-31 20:39:33'),
(692, 151, 4, 'King 193&nbsp;', 83000, 0, 83000, 5, 17, 29, '2018-10-31 20:39:56', '2018-10-31 20:40:02'),
(693, 152, 1, 'Solteiro 88&nbsp;', 45000, 0, 45000, 5, 12, 21, '2018-10-31 20:48:00', '2018-10-31 20:48:00'),
(694, 152, 2, 'Casal 138&nbsp;', 66000, 0, 66000, 5, 12, 21, '2018-10-31 20:48:11', '2018-10-31 20:48:11'),
(695, 152, 3, 'Queen 158&nbsp;', 73000, 0, 73000, 5, 14, 24, '2018-10-31 20:48:26', '2018-10-31 20:48:26'),
(696, 152, 4, 'King 193&nbsp;', 83000, 0, 83000, 5, 17, 29, '2018-10-31 20:48:41', '2018-10-31 20:48:41'),
(697, 154, 1, 'Solteiro 88&nbsp;', 45000, 0, 45000, 5, 12, 21, '2018-10-31 20:49:00', '2018-10-31 20:49:00'),
(698, 154, 2, 'Casal 138&nbsp;', 66000, 0, 66000, 5, 12, 21, '2018-10-31 20:49:14', '2018-10-31 20:49:14'),
(699, 154, 3, 'Queen 158&nbsp;', 73000, 0, 73000, 5, 14, 24, '2018-10-31 20:49:27', '2018-10-31 20:49:27'),
(700, 154, 4, 'King 193&nbsp;', 83000, 0, 83000, 5, 17, 29, '2018-10-31 20:49:42', '2018-10-31 20:49:42'),
(701, 156, 1, 'Solteiro 88&nbsp;', 45000, 0, 45000, 5, 12, 21, '2018-10-31 20:52:46', '2018-10-31 20:52:46'),
(702, 156, 2, 'Casal 138&nbsp;', 66000, 0, 66000, 5, 12, 21, '2018-10-31 20:53:02', '2018-10-31 20:53:02'),
(703, 156, 3, 'Queen 158&nbsp;', 73000, 0, 73000, 5, 14, 24, '2018-10-31 20:53:14', '2018-10-31 20:53:14'),
(704, 156, 4, 'King 193&nbsp;', 83000, 0, 83000, 5, 17, 29, '2018-10-31 20:53:29', '2018-10-31 20:53:29'),
(705, 157, 1, 'Solteiro 88&nbsp;', 45000, 0, 45000, 5, 12, 21, '2018-10-31 20:57:15', '2018-10-31 20:57:15'),
(706, 157, 2, 'Casal 138&nbsp;', 66000, 0, 66000, 5, 12, 21, '2018-10-31 20:57:28', '2018-10-31 20:57:28'),
(707, 157, 3, 'Queen 158&nbsp;', 73000, 0, 73000, 5, 14, 24, '2018-10-31 20:57:42', '2018-10-31 20:57:42'),
(708, 157, 4, 'King 193&nbsp;', 83000, 0, 83000, 5, 17, 29, '2018-10-31 20:57:51', '2018-10-31 20:57:51'),
(709, 158, 1, 'Solteiro 88&nbsp;', 45000, 0, 45000, 5, 12, 21, '2018-10-31 21:00:59', '2018-10-31 21:00:59'),
(710, 158, 2, 'Casal 138&nbsp;', 66000, 0, 66000, 5, 12, 21, '2018-10-31 21:01:14', '2018-10-31 21:01:14'),
(711, 158, 3, 'Queen 158&nbsp;', 73000, 0, 73000, 5, 14, 24, '2018-10-31 21:01:29', '2018-10-31 21:01:29'),
(712, 158, 4, 'King 193&nbsp;', 83000, 0, 83000, 5, 17, 29, '2018-10-31 21:01:48', '2018-10-31 21:01:48'),
(713, 159, 1, 'Solteiro 88&nbsp;', 45000, 0, 45000, 5, 12, 21, '2018-10-31 21:04:02', '2018-10-31 21:04:02'),
(714, 159, 2, 'Casal 138&nbsp;', 66000, 0, 66000, 5, 12, 21, '2018-10-31 21:04:21', '2018-10-31 21:04:21'),
(715, 159, 3, 'Queen 158&nbsp;', 73000, 0, 73000, 5, 14, 24, '2018-10-31 21:04:33', '2018-10-31 21:04:33'),
(716, 159, 4, 'King 193&nbsp;', 83000, 0, 83000, 5, 17, 29, '2018-10-31 21:04:49', '2018-10-31 21:04:49'),
(717, 153, 1, 'Solteiro 88&nbsp;', 45000, 0, 45000, 5, 12, 21, '2018-10-31 21:07:36', '2018-10-31 21:07:36'),
(718, 153, 2, 'Casal 138&nbsp;', 66000, 0, 66000, 5, 12, 21, '2018-10-31 21:07:51', '2018-10-31 21:07:51'),
(719, 153, 3, 'Queen 158&nbsp;', 73000, 0, 73000, 5, 14, 24, '2018-10-31 21:08:22', '2018-10-31 21:08:22'),
(720, 153, 0, 'King 193&nbsp;', 83000, 0, 83000, 5, 17, 29, '2018-10-31 21:08:35', '2018-10-31 21:08:35'),
(721, 122, 1, 'Colch&atilde;o Solteiro<br />\r\n088x188 cm', 0, 0, 0, 10, 60, 170, '2018-11-01 19:17:32', '2019-02-12 14:11:14'),
(722, 122, 2, 'Colch&atilde;o Solteiro King<br />\r\n096x203 cm', 0, 0, 0, 10, 70, 210, '2018-11-01 19:18:19', '2019-02-12 13:58:43'),
(723, 122, 3, 'Colch&atilde;o Casal&nbsp;<br />\r\n138x188 cm', 0, 0, 0, 10, 90, 270, '2018-11-01 19:18:54', '2019-02-12 13:58:55'),
(724, 122, 4, 'Colch&atilde;o Queen<br />\r\n158x198 cm', 0, 0, 0, 10, 110, 320, '2018-11-01 19:19:25', '2019-02-12 13:59:08'),
(725, 122, 5, 'Colch&atilde;o King&nbsp;<br />\r\n193x203 cm', 0, 0, 0, 10, 140, 400, '2018-11-01 19:19:48', '2019-02-12 13:59:18'),
(727, 160, 1, 'Colch&atilde;o Solteiro<br />\r\n088x188 cm', 123000, 0, 123000, 10, 60, 170, '2018-11-01 19:35:19', '2020-03-20 19:51:54'),
(728, 160, 2, 'Colch&atilde;o Solteiro King&nbsp;<br />\r\n096x203 cm', 141000, 0, 141000, 10, 70, 210, '2018-11-01 19:35:43', '2020-03-20 19:52:30'),
(729, 160, 3, 'Colch&atilde;o Casal<br />\r\n128x188 cm', 176000, 0, 176000, 10, 90, 270, '2018-11-01 19:36:09', '2020-03-20 19:52:51'),
(730, 160, 4, 'Colch&atilde;o Casal<br />\r\n138x188 cm', 176000, 0, 176000, 10, 90, 270, '2018-11-01 19:36:28', '2020-03-20 19:53:10'),
(731, 160, 5, 'Colch&atilde;o Queen<br />\r\n158x198 cm', 211000, 0, 211000, 10, 110, 320, '2018-11-01 19:36:48', '2020-03-20 19:53:40'),
(732, 160, 6, 'Colch&atilde;o King&nbsp;<br />\r\n193x203 cm', 264000, 0, 264000, 10, 140, 400, '2018-11-01 19:37:13', '2020-03-20 19:53:59'),
(734, 160, 7, 'Colch&atilde;o + Box Solteiro&nbsp;<br />\r\n088x188 cm', 162000, 0, 162000, 10, 110, 320, '2018-11-01 19:38:47', '2020-03-20 19:55:55'),
(735, 160, 8, 'Colch&atilde;o + Box Solteiro King<br />\r\n096x203 cm', 185000, 0, 185000, 10, 140, 400, '2018-11-01 19:39:25', '2020-03-20 19:56:23'),
(736, 160, 9, 'Colch&atilde;o + Box Casal<br />\r\n128x188 cm', 225000, 0, 225000, 10, 170, 500, '2018-11-01 19:40:07', '2020-03-20 19:56:53'),
(737, 160, 10, 'Colch&atilde;o + Box Casal&nbsp;<br />\r\n138x188 cm', 225000, 0, 225000, 10, 170, 500, '2018-11-01 19:40:36', '2020-03-20 19:57:18'),
(738, 160, 11, 'Colch&atilde;o + Box Queen<br />\r\n158x198 cm', 287000, 0, 287000, 10, 200, 600, '2018-11-01 19:41:25', '2020-03-20 19:57:44'),
(739, 160, 12, 'Colch&atilde;o + Box King<br />\r\n193x203 cm', 352000, 0, 352000, 10, 250, 750, '2018-11-01 19:42:02', '2020-03-20 19:58:09'),
(743, 164, 0, 'Solteiro Tecido 9023-E', 79000, 0, 79000, 10, 50, 250, '2018-11-03 18:22:07', '2018-11-03 18:22:07'),
(744, 165, 0, 'Solteiro Tecido 9005-E', 99000, 0, 99000, 10, 50, 250, '2018-11-03 18:22:31', '2018-11-03 18:22:31'),
(745, 166, 0, 'Solteiro Tecido 1012-A', 72000, 0, 72000, 10, 50, 250, '2018-11-03 18:23:39', '2018-11-03 18:23:39'),
(746, 167, 1, 'Colch&atilde;o Solteiro<br />\r\n078x188 cm&nbsp;', 52000, 49000, 49000, 10, 33, 100, '2018-11-05 18:24:11', '2019-05-17 19:37:52'),
(747, 167, 2, 'Colch&atilde;o Solteiro<br />\r\n088x188 cm', 52000, 49000, 49000, 10, 33, 100, '2018-11-05 18:24:31', '2019-05-17 19:38:07'),
(748, 167, 3, 'Colch&atilde;o + Box Solteiro<br />\r\n078x188 cm', 74000, 69000, 69000, 10, 83, 250, '2018-11-05 18:25:18', '2019-05-17 19:36:48'),
(749, 167, 4, 'Colch&atilde;o + Box Solteiro&nbsp;<br />\r\n088x188 cm', 74000, 69000, 69000, 10, 83, 250, '2018-11-05 18:25:46', '2019-05-17 19:37:06'),
(750, 168, 1, 'Solteiro&nbsp;<br />\r\n60x60 cm', 19900, 0, 19900, 2, 80, 200, '2018-11-09 20:32:47', '2018-11-09 20:32:47'),
(751, 168, 2, 'Casal<br />\r\n128x60 cm', 39900, 0, 39900, 3, 80, 200, '2018-11-09 20:33:32', '2018-11-09 20:38:30'),
(752, 169, 1, 'Perfil Baixo<br />\r\n40x60x11 cm', 0, 0, 0, 2, 10, 30, '2018-11-14 17:53:00', '2019-02-01 21:28:34'),
(753, 169, 2, 'Perfil M&eacute;dio<br />\r\n40x60x14 cm', 0, 0, 0, 3, 10, 30, '2018-11-14 17:53:31', '2019-02-01 21:28:42'),
(754, 169, 3, 'Perfil Alto<br />\r\n40x60x18 cm', 0, 0, 0, 3, 10, 30, '2018-11-14 17:54:11', '2019-02-01 21:28:50'),
(755, 170, 0, '50x70 cm', 0, 0, 0, 3, 10, 30, '2018-11-14 18:05:52', '2019-02-01 21:28:17'),
(757, 172, 0, '50x70 cm', 0, 0, 0, 3, 10, 30, '2018-11-14 18:44:41', '2019-02-01 21:27:34'),
(759, 173, 0, '50x70 cm', 0, 0, 0, 2, 10, 30, '2018-11-14 18:57:45', '2019-02-01 21:27:15'),
(763, 176, 1, 'Colch&atilde;o Solteiro<br />\r\n088x188 cm', 99000, 0, 99000, 10, 50, 100, '2018-11-27 17:54:51', '2018-11-27 17:54:51'),
(764, 176, 2, 'Colch&atilde;o Casal<br />\r\n138x188 cm', 149000, 0, 149000, 10, 70, 140, '2018-11-27 17:55:20', '2018-11-27 17:55:20'),
(765, 176, 3, 'Colch&atilde;o Queen<br />\r\n158x198 cm', 179000, 0, 179000, 10, 100, 200, '2018-11-27 17:56:08', '2018-11-27 17:56:08'),
(766, 176, 4, 'Colch&atilde;o King&nbsp;<br />\r\n193x203 cm', 219000, 0, 219000, 10, 140, 280, '2018-11-27 17:56:41', '2018-11-27 17:56:41'),
(767, 176, 5, 'Colch&atilde;o + Box Solteiro<br />\r\n088x188 cm', 137000, 0, 137000, 10, 150, 300, '2018-11-27 17:57:28', '2018-11-27 17:57:28'),
(768, 176, 6, 'Colch&atilde;o + Box Casal&nbsp;<br />\r\n138x188 cm', 197000, 0, 197000, 10, 210, 430, '2018-11-27 17:58:11', '2018-11-27 17:58:11'),
(769, 176, 7, 'Colch&atilde;o + Box Queen<br />\r\n158x198 cm', 255000, 0, 255000, 10, 280, 560, '2018-11-27 17:59:22', '2018-11-27 17:59:22'),
(770, 176, 8, 'Colch&atilde;o + Box King<br />\r\n193x203 cm', 299000, 0, 299000, 10, 350, 700, '2018-11-27 18:00:02', '2018-11-27 18:00:02'),
(777, 178, 1, 'Colch&atilde;o Solteiro<br />\r\n088x188 cm', 0, 0, 0, 10, 60, 170, '2018-11-30 20:32:17', '2019-02-12 14:03:28'),
(778, 178, 2, 'Colch&atilde;o Solteiro King&nbsp;<br />\r\n096x203 cm', 0, 0, 0, 10, 70, 210, '2018-11-30 20:32:34', '2019-02-12 13:48:27'),
(779, 178, 3, 'Colch&atilde;o Casal&nbsp;<br />\r\n138x188 cm', 0, 0, 0, 10, 90, 270, '2018-11-30 20:33:56', '2019-02-12 13:48:36'),
(780, 178, 4, 'Colch&atilde;o Queen<br />\r\n158x198 cm', 0, 0, 0, 10, 110, 320, '2018-11-30 20:34:59', '2019-02-12 13:48:45'),
(781, 178, 5, 'Colch&atilde;o King&nbsp;<br />\r\n193x203 cm', 0, 0, 0, 10, 140, 400, '2018-11-30 20:35:20', '2019-02-12 13:48:56'),
(782, 178, 6, 'Colch&atilde;o + Box Solteiro&nbsp;<br />\r\n088x188 cm', 0, 0, 0, 10, 110, 320, '2018-11-30 20:35:43', '2019-02-12 13:49:08'),
(783, 178, 7, 'Colch&atilde;o + Box Solteiro King<br />\r\n096x 203 cm', 0, 0, 0, 10, 140, 400, '2018-11-30 20:36:03', '2019-02-12 13:49:18'),
(784, 178, 8, 'Colch&atilde;o + Box Casal&nbsp;<br />\r\n138x188 cm', 0, 0, 0, 10, 170, 500, '2018-11-30 20:36:28', '2019-02-12 13:49:28'),
(785, 178, 9, 'Colch&atilde;o + Box Queen<br />\r\n158x198 cm', 0, 0, 0, 10, 200, 600, '2018-11-30 20:36:58', '2019-02-12 13:49:39'),
(786, 178, 10, 'Colch&atilde;o + Box King<br />\r\n193x203 cm', 0, 0, 0, 10, 250, 750, '2018-11-30 20:37:20', '2019-02-12 13:49:49'),
(787, 179, 1, 'Colch&atilde;o Solteiro<br />\r\n088x188 cm', 0, 0, 0, 10, 60, 170, '2018-11-30 20:45:23', '2019-02-12 14:03:49'),
(788, 179, 2, 'Colch&atilde;o Solteiro King&nbsp;<br />\r\n096x203 cm', 0, 0, 0, 10, 70, 210, '2018-11-30 20:45:45', '2019-02-12 13:56:27'),
(789, 179, 3, 'Colch&atilde;o Casal&nbsp;<br />\r\n138x188 cm', 0, 0, 0, 10, 90, 270, '2018-11-30 20:46:14', '2019-02-12 13:56:39'),
(790, 179, 4, 'Colch&atilde;o Queen<br />\r\n158x198 cm', 0, 0, 0, 10, 110, 320, '2018-11-30 20:46:43', '2019-02-12 13:56:51'),
(791, 179, 5, 'Colch&atilde;o King<br />\r\n193x203 cm', 0, 0, 0, 10, 140, 400, '2018-11-30 20:47:05', '2019-02-12 13:57:04'),
(792, 179, 6, 'Colch&atilde;o + Box Solteiro&nbsp;<br />\r\n088x188 cm', 0, 0, 0, 10, 110, 320, '2018-11-30 20:47:41', '2019-02-12 13:57:16'),
(793, 179, 7, 'Colch&atilde;o + Box Solteiro King<br />\r\n096x203 cm', 0, 0, 0, 10, 140, 400, '2018-11-30 20:48:25', '2019-02-12 13:57:28'),
(795, 179, 8, 'Colch&atilde;o + Box Casal&nbsp;<br />\r\n138x188 cm', 0, 0, 0, 10, 170, 500, '2018-11-30 20:48:54', '2019-02-12 13:57:41'),
(796, 179, 9, 'Colch&atilde;o + Box Queen<br />\r\n158x198 cm', 0, 0, 0, 10, 200, 600, '2018-11-30 20:49:17', '2019-02-12 13:57:54'),
(797, 179, 10, 'Colch&atilde;o + Box King<br />\r\n193x203 cm', 0, 0, 0, 10, 250, 750, '2018-11-30 20:50:04', '2019-02-12 13:58:08'),
(798, 180, 1, 'Solteiro 088x188 cm', 69000, 0, 69000, 5, 100, 280, '2019-01-19 17:01:46', '2019-01-19 17:01:46'),
(799, 180, 2, 'Vi&uacute;va 110x188 cm', 89000, 0, 89000, 5, 100, 300, '2019-01-19 17:02:18', '2019-01-19 17:02:18'),
(800, 180, 3, 'Casal 138x188 cm', 89000, 0, 89000, 10, 100, 320, '2019-01-19 17:03:11', '2019-01-19 17:03:11'),
(801, 180, 4, 'Queen 158x198 cm', 109000, 0, 109000, 5, 100, 340, '2019-01-19 17:03:39', '2019-01-19 17:03:39'),
(802, 181, 1, 'Solteiro 078x188 cm', 139000, 0, 139000, 5, 150, 500, '2019-01-19 17:09:40', '2019-01-19 17:09:40'),
(803, 181, 2, 'Solteiro 088x188 cm', 149000, 0, 149000, 5, 150, 600, '2019-01-19 17:09:58', '2019-01-19 17:10:05'),
(804, 182, 1, 'Colch&atilde;o Casal<br />\r\n138x188 cm', 259000, 0, 259000, 10, 90, 270, '2019-04-30 19:12:14', '2019-04-30 19:13:06'),
(805, 182, 2, 'Colch&atilde;o Queen<br />\r\n158x198 cm', 309000, 0, 309000, 10, 110, 320, '2019-04-30 19:12:49', '2019-04-30 19:12:49'),
(806, 182, 3, 'Colch&atilde;o King<br />\r\n193x203 cm', 379000, 0, 379000, 10, 140, 400, '2019-04-30 19:13:40', '2019-04-30 19:13:40'),
(807, 183, 1, 'Colch&atilde;o Casal&nbsp;<br />\r\n138x188 cm', 285000, 0, 285000, 10, 90, 270, '2019-04-30 19:20:16', '2019-04-30 19:20:16'),
(808, 183, 2, 'Colch&atilde;o Queen<br />\r\n158x198 cm', 335000, 0, 335000, 10, 110, 320, '2019-04-30 19:20:50', '2019-04-30 19:20:50'),
(809, 183, 3, 'Colch&atilde;o King<br />\r\n193x203 cm', 415000, 0, 415000, 10, 140, 400, '2019-04-30 19:21:17', '2019-04-30 19:21:17'),
(810, 184, 1, 'Colch&atilde;o Casal&nbsp;<br />\r\n138x188 cm', 295000, 0, 295000, 10, 90, 270, '2019-04-30 19:45:16', '2019-04-30 19:45:16'),
(811, 184, 2, 'Colch&atilde;o Queen<br />\r\n158x198 cm', 345000, 0, 345000, 10, 110, 320, '2019-04-30 19:45:36', '2019-04-30 19:45:36'),
(812, 184, 3, 'Colch&atilde;o King&nbsp;<br />\r\n193x203 cm', 425000, 0, 425000, 10, 140, 400, '2019-04-30 19:45:57', '2019-04-30 19:45:57'),
(813, 185, 1, 'Colch&atilde;o Casal<br />\r\n138x188 cm', 255000, 0, 255000, 10, 90, 270, '2019-04-30 19:51:38', '2019-06-10 20:05:07'),
(814, 185, 2, 'Colch&atilde;o Queen<br />\r\n158x198 cm', 295000, 0, 295000, 10, 110, 320, '2019-04-30 19:51:58', '2019-04-30 19:51:58'),
(815, 185, 3, 'Colch&atilde;o King&nbsp;<br />\r\n193x203 cm', 365000, 0, 365000, 10, 140, 400, '2019-04-30 19:52:15', '2019-04-30 19:52:15'),
(816, 186, 1, 'Colch&atilde;o Solteiro<br />\r\n088x188 cm', 0, 0, 0, 10, 50, 100, '2019-05-02 21:29:33', '2020-03-07 00:13:00'),
(817, 186, 2, 'Colch&atilde;o Solteiro King&nbsp;<br />\r\n096x203 cm', 0, 0, 0, 10, 60, 120, '2019-05-02 21:29:51', '2020-03-07 00:13:08'),
(818, 186, 3, 'Colch&atilde;o Casal&nbsp;<br />\r\n138x188 cm', 0, 0, 0, 10, 70, 140, '2019-05-02 21:32:43', '2020-03-07 00:13:17'),
(819, 186, 4, 'Colch&atilde;o Queen<br />\r\n158x198 cm', 0, 0, 0, 10, 100, 200, '2019-05-02 21:33:23', '2020-03-07 00:13:25'),
(820, 186, 5, 'Colch&atilde;o King&nbsp;<br />\r\n193x203 cm', 0, 0, 0, 10, 140, 280, '2019-05-02 21:33:55', '2020-03-07 00:13:34'),
(821, 187, 1, 'Colch&atilde;o Solteiro<br />\r\n088x188 cm', 0, 0, 0, 10, 50, 100, '2019-05-02 21:38:52', '2020-03-07 00:11:48'),
(822, 187, 2, 'Colch&atilde;o Solteiro King&nbsp;<br />\r\n096x203 cm', 0, 0, 0, 10, 60, 120, '2019-05-02 21:39:12', '2020-03-07 00:11:58'),
(823, 187, 3, 'Colch&atilde;o Casal&nbsp;<br />\r\n138x188 cm', 0, 0, 0, 10, 70, 140, '2019-05-02 21:39:31', '2020-03-07 00:12:10'),
(824, 187, 4, 'Colch&atilde;o Queen<br />\r\n158x198 cm', 0, 0, 0, 10, 100, 200, '2019-05-02 21:40:51', '2020-03-07 00:12:19'),
(825, 187, 5, 'Colch&atilde;o King&nbsp;<br />\r\n193x203 cm', 0, 0, 0, 10, 140, 280, '2019-05-02 21:41:13', '2020-03-07 00:12:30'),
(826, 24, 3, 'Box Ba&uacute;<br />\r\n138x188 cm Bipartido (2 pe&ccedil;as de 069x188 cm)', 159000, 0, 159000, 10, 110, 320, '2019-05-16 19:06:46', '2019-05-16 19:08:13'),
(827, 189, 1, '088x188 cm', 0, 0, 0, 2, 60, 170, '2019-05-30 22:46:00', '2019-05-30 22:46:00'),
(828, 189, 2, '097x203 cm&nbsp;', 0, 0, 0, 2, 70, 210, '2019-05-30 22:46:28', '2019-05-30 22:56:45'),
(829, 189, 3, '120x200 cm', 0, 0, 0, 2, 90, 270, '2019-05-30 22:46:55', '2019-05-30 22:46:55'),
(830, 189, 4, '138x188 cm', 0, 0, 0, 2, 90, 270, '2019-05-30 22:47:26', '2019-05-30 22:47:38'),
(831, 189, 5, '158x198 cm', 0, 0, 0, 2, 110, 320, '2019-05-30 22:47:58', '2019-05-30 22:47:58'),
(832, 189, 6, '178x198 cm', 0, 0, 0, 2, 120, 360, '2019-05-30 22:48:16', '2019-05-30 22:48:16'),
(833, 189, 7, '193x203 cm', 0, 0, 0, 2, 140, 400, '2019-05-30 22:48:32', '2019-05-30 22:48:32'),
(834, 189, 8, '210x210 cm', 0, 0, 0, 2, 140, 400, '2019-05-30 22:48:52', '2019-05-30 22:48:52'),
(835, 190, 1, '088x188 cm', 0, 0, 0, 2, 60, 170, '2019-05-30 22:54:27', '2019-05-30 22:54:27'),
(836, 190, 2, '097x203 cm&nbsp;', 0, 0, 0, 2, 70, 210, '2019-05-30 22:54:38', '2019-05-30 22:57:05'),
(837, 190, 3, '120x200 cm', 0, 0, 0, 2, 90, 270, '2019-05-30 22:54:56', '2019-05-30 22:54:56'),
(838, 190, 4, '138x188 cm', 0, 0, 0, 2, 90, 270, '2019-05-30 22:55:12', '2019-05-30 22:55:12'),
(839, 190, 5, '158x198 cm', 0, 0, 0, 2, 110, 320, '2019-05-30 22:55:23', '2019-05-30 22:55:23'),
(840, 190, 6, '178x198 cm', 0, 0, 0, 2, 120, 360, '2019-05-30 22:55:38', '2019-05-30 22:55:38'),
(841, 190, 7, '193x203 cm', 0, 0, 0, 2, 140, 400, '2019-05-30 22:55:55', '2019-05-30 22:55:55'),
(842, 190, 8, '210x210 cm', 0, 0, 0, 2, 140, 400, '2019-05-30 22:56:09', '2019-05-30 22:56:09'),
(843, 191, 1, '088x188 cm', 0, 0, 0, 2, 60, 170, '2019-05-30 23:03:04', '2019-05-30 23:03:04'),
(844, 191, 2, '097x203 cm&nbsp;', 0, 0, 0, 2, 70, 210, '2019-05-30 23:03:15', '2019-05-30 23:03:15'),
(845, 191, 3, '120x200 cm', 0, 0, 0, 2, 90, 270, '2019-05-30 23:03:27', '2019-05-30 23:03:27'),
(846, 191, 4, '138x188 cm', 0, 0, 0, 2, 90, 270, '2019-05-30 23:03:44', '2019-05-30 23:03:44'),
(847, 191, 5, '158x198 cm', 0, 0, 0, 2, 110, 320, '2019-05-30 23:03:55', '2019-05-30 23:03:55'),
(848, 191, 6, '178x198 cm', 0, 0, 0, 2, 120, 360, '2019-05-30 23:04:07', '2019-05-30 23:04:07'),
(849, 191, 7, '193x203 cm', 0, 0, 0, 2, 140, 400, '2019-05-30 23:04:19', '2019-05-30 23:04:19'),
(850, 191, 8, '210x210 cm', 0, 0, 0, 2, 140, 400, '2019-05-30 23:04:31', '2019-05-30 23:04:31'),
(851, 192, 1, '088x188 cm', 0, 0, 0, 2, 60, 170, '2019-05-30 23:11:21', '2019-05-30 23:11:21'),
(852, 192, 2, '097x203 cm&nbsp;', 0, 0, 0, 2, 70, 210, '2019-05-30 23:11:31', '2019-05-30 23:11:31'),
(853, 192, 3, '120x200 cm', 0, 0, 0, 2, 90, 270, '2019-05-30 23:11:41', '2019-05-30 23:11:41'),
(854, 192, 4, '138x188 cm', 0, 0, 0, 2, 90, 270, '2019-05-30 23:12:05', '2019-05-30 23:12:05'),
(855, 192, 5, '158x198 cm', 0, 0, 0, 2, 110, 320, '2019-05-30 23:12:19', '2019-05-30 23:12:19'),
(856, 192, 6, '178x198 cm', 0, 0, 0, 2, 120, 360, '2019-05-30 23:12:45', '2019-05-30 23:12:45'),
(857, 192, 7, '193x203 cm', 0, 0, 0, 2, 140, 400, '2019-05-30 23:12:59', '2019-05-30 23:12:59'),
(858, 192, 8, '210x210 cm', 0, 0, 0, 2, 140, 400, '2019-05-30 23:13:13', '2019-05-30 23:13:13'),
(859, 193, 1, '088x188 cm', 0, 0, 0, 2, 50, 100, '2019-05-30 23:19:44', '2019-05-30 23:19:44'),
(860, 193, 2, '097x203 cm&nbsp;', 0, 0, 0, 2, 60, 120, '2019-05-30 23:19:53', '2019-05-30 23:19:53'),
(861, 193, 3, '120x200 cm', 0, 0, 0, 2, 70, 140, '2019-05-30 23:20:02', '2019-05-30 23:20:02'),
(862, 193, 4, '138x188 cm', 0, 0, 0, 2, 70, 140, '2019-05-30 23:20:12', '2019-05-30 23:20:12'),
(863, 193, 5, '158x198 cm', 0, 0, 0, 2, 100, 200, '2019-05-30 23:20:27', '2019-05-30 23:20:27'),
(864, 193, 6, '178x198 cm', 0, 0, 0, 2, 110, 220, '2019-05-30 23:20:36', '2019-05-30 23:20:36'),
(865, 193, 7, '193x203 cm', 0, 0, 0, 2, 140, 280, '2019-05-30 23:20:47', '2019-05-30 23:20:47'),
(866, 193, 8, '210x210 cm', 0, 0, 0, 2, 140, 280, '2019-05-30 23:21:02', '2019-05-30 23:21:02'),
(867, 194, 1, 'Colch&atilde;o Solteiro<br />\r\n088x188 cm', 204000, 0, 204000, 10, 60, 170, '2019-09-12 20:45:17', '2020-02-26 20:36:25'),
(868, 194, 2, 'Colch&atilde;o Solteiro King<br />\r\n096x203 cm', 234000, 0, 234000, 10, 70, 210, '2019-09-12 20:45:49', '2020-02-26 20:36:40'),
(869, 194, 3, 'Colch&atilde;o Vi&uacute;va<br />\r\n120x203 cm', 291000, 0, 291000, 10, 90, 270, '2019-09-12 20:46:27', '2020-02-26 20:36:58'),
(870, 194, 4, 'Colch&atilde;o Casal<br />\r\n138x188 cm', 291000, 0, 291000, 10, 90, 270, '2019-09-12 20:46:57', '2020-02-26 20:37:10'),
(871, 194, 5, 'Colch&atilde;o Queen<br />\r\n158x198 cm', 349000, 0, 349000, 10, 110, 320, '2019-09-12 20:47:36', '2020-02-26 20:37:25'),
(872, 194, 6, 'Colch&atilde;o Queen Full<br />\r\n178x198 cm', 393000, 0, 393000, 10, 120, 360, '2019-09-12 20:48:07', '2020-02-26 20:37:41'),
(873, 194, 7, 'Colch&atilde;o King<br />\r\n193x203 cm', 437000, 0, 437000, 10, 140, 400, '2019-09-12 20:48:30', '2020-02-26 20:37:55'),
(874, 194, 8, 'Colch&atilde;o + Box Solteiro&nbsp;<br />\r\n088x188 cm', 249000, 0, 249000, 10, 110, 320, '2019-09-12 20:55:44', '2020-02-26 20:38:09'),
(875, 194, 9, 'Colch&atilde;o + Box Solteiro King<br />\r\n096x203 cm', 288000, 0, 288000, 10, 140, 400, '2019-09-12 20:56:49', '2020-02-26 20:38:25'),
(876, 194, 10, 'Colch&atilde;o + Box Vi&uacute;va<br />\r\n120x203 cm', 348000, 0, 348000, 10, 170, 500, '2019-09-12 20:57:39', '2020-02-26 20:38:37'),
(877, 194, 11, 'Colch&atilde;o + Box Casal<br />\r\n138x188 cm', 348000, 0, 348000, 10, 170, 500, '2019-09-12 20:58:17', '2020-02-26 20:38:50'),
(878, 194, 12, 'Colch&atilde;o + Box Queen<br />\r\n158x198 cm', 437000, 0, 437000, 10, 200, 600, '2019-09-12 20:58:53', '2020-02-26 20:39:03'),
(879, 194, 13, 'Colch&atilde;o + Box Queen Full<br />\r\n178x198 cm', 491000, 0, 491000, 10, 225, 680, '2019-09-12 20:59:25', '2020-02-26 20:39:17'),
(880, 194, 14, 'Colch&atilde;o + Box King<br />\r\n193x203 cm', 545000, 0, 545000, 10, 250, 750, '2019-09-12 21:00:00', '2020-02-26 20:39:32'),
(881, 195, 1, 'Colch&atilde;o Casal<br />\r\n138x188x025 cm', 179000, 0, 179000, 10, 65, 200, '2020-01-31 19:15:51', '2020-01-31 19:15:51'),
(882, 195, 2, 'Colch&atilde;o Queen<br />\r\n158x198x025 cm', 199000, 0, 199000, 10, 80, 240, '2020-01-31 19:16:15', '2020-01-31 19:16:15'),
(884, 196, 0, '64x62 cm', 44900, 0, 44900, 3, 10, 30, '2020-02-26 19:29:02', '2020-02-26 19:29:02'),
(885, 197, 0, '10x20x40 cm', 9900, 0, 9900, 2, 10, 30, '2020-02-26 19:37:49', '2020-02-26 19:37:49'),
(886, 198, 0, '13x40x48 cm', 15900, 0, 15900, 2, 10, 30, '2020-02-26 19:43:23', '2020-02-26 19:43:23'),
(887, 199, 1, 'Colch&atilde;o Solteiro<br />\r\n088x188 cm', 123000, 0, 123000, 10, 40, 100, '2020-02-26 20:56:19', '2020-02-26 20:56:46'),
(888, 199, 2, 'Colch&atilde;o Solteiro King<br />\r\n096x203 cm', 130000, 0, 130000, 10, 50, 160, '2020-02-26 20:57:20', '2020-02-26 20:57:20'),
(889, 199, 3, 'Colch&atilde;o Casal<br />\r\n128x188 cm', 169000, 0, 169000, 10, 65, 200, '2020-02-26 20:57:55', '2020-02-26 20:57:55'),
(890, 199, 4, 'Colch&atilde;o Casal<br />\r\n138x188 cm', 169000, 0, 169000, 10, 65, 200, '2020-02-26 20:58:15', '2020-02-26 20:58:15'),
(891, 199, 5, 'Colch&atilde;o Queen<br />\r\n158x198 cm', 195000, 0, 195000, 10, 80, 240, '2020-02-26 20:58:45', '2020-02-26 20:58:45'),
(892, 200, 1, 'Colch&atilde;o Solteiro<br />\r\n088x188 cm', 204000, 0, 204000, 10, 60, 170, '2020-02-26 21:15:37', '2020-02-26 21:15:37'),
(893, 200, 2, 'Colch&atilde;o Solteiro King&nbsp;<br />\r\n096x203 cm', 234000, 0, 234000, 10, 70, 210, '2020-02-26 21:16:02', '2020-02-26 21:16:02'),
(894, 200, 3, 'Colch&atilde;o Casal<br />\r\n138x188 cm', 291000, 0, 291000, 10, 90, 270, '2020-02-26 21:16:25', '2020-02-26 21:16:25'),
(895, 200, 4, 'Colch&atilde;o Queen<br />\r\n158x198 cm', 349000, 0, 349000, 10, 110, 320, '2020-02-26 21:16:45', '2020-02-26 21:16:45'),
(896, 200, 5, 'Colch&atilde;o Queen Full<br />\r\n178x198 cm', 393000, 0, 393000, 10, 120, 360, '2020-02-26 21:17:08', '2020-02-26 21:17:08'),
(897, 200, 6, 'Colch&atilde;o King&nbsp;<br />\r\n193x203 cm', 437000, 0, 437000, 10, 140, 400, '2020-02-26 21:17:43', '2020-02-26 21:17:43'),
(898, 200, 7, 'Colch&atilde;o + Box Solteiro&nbsp;<br />\r\n088x188 cm', 249000, 0, 249000, 10, 110, 320, '2020-02-26 21:18:10', '2020-02-26 21:18:10'),
(899, 200, 8, 'Colch&atilde;o + Box Solteiro King<br />\r\n096x 203 cm', 288000, 0, 288000, 10, 140, 400, '2020-02-26 21:18:30', '2020-02-26 21:18:30'),
(900, 200, 9, 'Colch&atilde;o + Box Casal<br />\r\n138x188 cm', 348000, 0, 348000, 10, 170, 500, '2020-02-26 21:19:04', '2020-02-26 21:19:04'),
(901, 200, 10, 'Colch&atilde;o + Box Queen<br />\r\n158x198 cm', 437000, 0, 437000, 10, 200, 600, '2020-02-26 21:19:28', '2020-02-26 21:19:34'),
(902, 200, 11, 'Colch&atilde;o + Box Queen Full<br />\r\n178x198 cm', 491000, 0, 491000, 10, 225, 680, '2020-02-26 21:19:57', '2020-02-26 21:19:57'),
(903, 200, 12, 'Colch&atilde;o + Box King<br />\r\n193x203 cm', 545000, 0, 545000, 10, 250, 750, '2020-02-26 21:20:23', '2020-02-26 21:20:23'),
(904, 201, 1, 'Colch&atilde;o Solteiro<br />\r\n088x188 cm', 229000, 0, 229000, 10, 60, 170, '2020-02-26 21:22:42', '2020-02-26 21:22:42'),
(905, 201, 2, 'Colch&atilde;o Solteiro King&nbsp;<br />\r\n096x203 cm', 259000, 0, 259000, 10, 70, 210, '2020-02-26 21:23:03', '2020-02-26 21:23:03'),
(906, 201, 3, 'Colch&atilde;o Casal<br />\r\n138x188 cm', 325000, 0, 325000, 10, 90, 270, '2020-02-26 21:23:34', '2020-02-26 21:23:34'),
(907, 201, 4, 'Colch&atilde;o Queen<br />\r\n158x198 cm', 391000, 0, 391000, 10, 110, 320, '2020-02-26 21:24:04', '2020-02-26 21:24:04'),
(908, 201, 5, 'Colch&atilde;o Queen Full<br />\r\n178x198 cm', 439000, 0, 439000, 10, 120, 360, '2020-02-26 21:24:29', '2020-02-26 21:24:29'),
(909, 201, 6, 'Colch&atilde;o King&nbsp;<br />\r\n193x203 cm', 489000, 0, 489000, 10, 140, 400, '2020-02-26 21:25:07', '2020-02-26 21:25:07'),
(910, 201, 7, 'Colch&atilde;o + Box Solteiro&nbsp;<br />\r\n088x188 cm', 274000, 0, 274000, 10, 110, 320, '2020-02-26 21:25:32', '2020-02-26 21:25:32'),
(911, 201, 8, 'Colch&atilde;o + Box Solteiro King<br />\r\n096x 203 cm', 313000, 0, 313000, 10, 140, 400, '2020-02-26 21:26:01', '2020-02-26 21:26:01'),
(912, 201, 9, 'Colch&atilde;o + Box Casal<br />\r\n138x188 cm', 383000, 0, 383000, 10, 170, 500, '2020-02-26 21:26:25', '2020-02-26 21:26:25'),
(913, 201, 10, 'Colch&atilde;o + Box Queen<br />\r\n158x198 cm', 479000, 0, 479000, 10, 200, 600, '2020-02-26 21:26:47', '2020-02-26 21:26:47'),
(914, 201, 11, 'Colch&atilde;o + Box Queen Full<br />\r\n178x198 cm', 537000, 0, 537000, 10, 225, 680, '2020-02-26 21:27:12', '2020-02-26 21:27:12'),
(915, 201, 12, 'Colch&atilde;o + Box King<br />\r\n193x203 cm', 597000, 0, 597000, 10, 250, 750, '2020-02-26 21:27:36', '2020-02-26 21:27:36'),
(916, 202, 1, 'Solteiro<br />\r\n60x190 cm', 2900, 0, 2900, 2, 10, 30, '2020-02-26 21:42:07', '2020-02-26 21:42:07'),
(917, 202, 2, 'Casal&nbsp;<br />\r\n130x190 cm', 3900, 0, 3900, 2, 10, 30, '2020-02-26 21:42:34', '2020-02-26 21:42:34'),
(918, 203, 0, '58x178x4,0 cm', 10900, 0, 10900, 2, 10, 30, '2020-02-26 21:46:48', '2020-02-26 21:46:48'),
(919, 204, 0, '078x188 cm', 4900, 0, 4900, 2, 10, 30, '2020-02-26 21:50:10', '2020-02-26 21:50:10'),
(920, 205, 0, '066x094x3,0 cm', 4900, 0, 4900, 2, 10, 30, '2020-02-26 21:55:09', '2020-02-26 21:55:09'),
(921, 206, 0, '045x085x3,0 cm', 2900, 0, 2900, 2, 10, 30, '2020-02-26 22:00:34', '2020-02-26 22:00:34'),
(922, 207, 0, '046x055x10 cm', 7900, 0, 7900, 2, 10, 30, '2020-02-26 22:05:53', '2020-02-26 22:05:53'),
(923, 208, 0, '45x65 cm', 6900, 0, 6900, 2, 10, 30, '2020-02-29 19:28:27', '2020-02-29 19:28:27'),
(924, 209, 0, '50x70 cm', 5900, 0, 5900, 2, 10, 30, '2020-02-29 19:37:56', '2020-02-29 19:37:56'),
(925, 210, 0, '50x70 cm', 15900, 0, 15900, 2, 10, 30, '2020-02-29 19:43:07', '2020-02-29 19:43:07'),
(926, 211, 0, '45x65 cm', 6500, 0, 6500, 2, 10, 30, '2020-02-29 19:48:28', '2020-02-29 19:48:28'),
(927, 212, 0, '50x70 cm', 7900, 0, 7900, 2, 10, 30, '2020-02-29 19:54:16', '2020-02-29 19:54:16'),
(928, 213, 0, '45x65 cm', 5900, 0, 5900, 2, 10, 30, '2020-02-29 19:57:13', '2020-02-29 19:57:13'),
(929, 214, 0, '50x70 cm', 16900, 0, 16900, 2, 10, 30, '2020-02-29 20:01:28', '2020-02-29 20:01:28'),
(930, 215, 0, '50x70 cm', 29900, 0, 29900, 3, 10, 30, '2020-02-29 20:07:55', '2020-02-29 20:07:55');

-- --------------------------------------------------------

--
-- Estrutura da tabela `produtos_promocoes`
--

CREATE TABLE `produtos_promocoes` (
  `id` int(10) UNSIGNED NOT NULL,
  `produto_id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `produtos_promocoes`
--

INSERT INTO `produtos_promocoes` (`id`, `produto_id`, `ordem`, `created_at`, `updated_at`) VALUES
(1, 4, 0, '2018-11-01 16:32:06', '2018-11-01 16:32:06'),
(2, 7, 0, '2018-11-01 16:32:40', '2018-11-01 16:32:40'),
(3, 3, 0, '2018-11-01 16:33:03', '2018-11-01 16:33:03'),
(5, 112, 0, '2018-11-01 16:33:53', '2018-11-01 16:33:53'),
(6, 108, 0, '2018-11-01 16:34:14', '2018-11-01 16:34:14'),
(7, 107, 0, '2018-11-01 16:34:47', '2018-11-01 16:34:47'),
(8, 103, 0, '2018-11-01 16:35:06', '2018-11-01 16:35:06'),
(11, 38, 0, '2018-11-01 16:40:38', '2018-11-01 16:40:38'),
(12, 42, 0, '2018-11-01 16:40:55', '2018-11-01 16:40:55'),
(14, 17, 0, '2018-11-01 16:41:42', '2018-11-01 16:41:42'),
(15, 13, 0, '2018-11-01 16:41:58', '2018-11-01 16:41:58'),
(17, 8, 0, '2018-11-01 16:42:44', '2018-11-01 16:42:44'),
(18, 140, 0, '2018-11-01 16:43:09', '2018-11-01 16:43:09'),
(19, 138, 0, '2018-11-01 16:43:26', '2018-11-01 16:43:26'),
(20, 34, 0, '2018-11-01 16:43:47', '2018-11-01 16:43:47'),
(21, 41, 0, '2018-11-01 16:43:56', '2018-11-01 16:43:56'),
(22, 32, 0, '2018-11-01 16:44:08', '2018-11-01 16:44:08'),
(24, 50, 0, '2018-11-01 16:45:19', '2018-11-01 16:45:19'),
(26, 83, 0, '2018-11-01 16:46:26', '2018-11-01 16:46:26'),
(27, 81, 0, '2018-11-01 16:46:38', '2018-11-01 16:46:38'),
(28, 141, 0, '2018-11-01 16:47:16', '2018-11-01 16:47:16'),
(29, 70, 0, '2018-11-01 16:47:47', '2018-11-01 16:47:47'),
(30, 69, 0, '2018-11-01 16:47:58', '2018-11-01 16:47:58'),
(31, 63, 0, '2018-11-01 16:48:20', '2018-11-01 16:48:20'),
(32, 71, 0, '2018-11-01 16:48:35', '2018-11-01 16:48:35'),
(33, 92, 0, '2018-11-01 16:48:58', '2018-11-01 16:48:58'),
(34, 116, 0, '2018-11-01 16:49:16', '2018-11-01 16:49:16'),
(35, 115, 0, '2018-11-01 16:49:25', '2018-11-01 16:49:25'),
(36, 118, 0, '2018-11-01 16:49:37', '2018-11-01 16:49:37'),
(37, 117, 0, '2018-11-01 16:49:45', '2018-11-01 16:49:45'),
(38, 167, 0, '2018-11-05 18:26:11', '2018-11-05 18:26:11'),
(39, 196, 0, '2020-02-26 22:06:31', '2020-02-26 22:06:31');

-- --------------------------------------------------------

--
-- Estrutura da tabela `produtos_propriedades`
--

CREATE TABLE `produtos_propriedades` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `produtos_propriedades`
--

INSERT INTO `produtos_propriedades` (`id`, `ordem`, `titulo`, `imagem`, `created_at`, `updated_at`) VALUES
(1, 1, 'ANTI-FUNGO', 'anti_20180917122044cWJzeBI09V.png', '2018-09-17 15:18:42', '2018-09-17 15:20:44'),
(2, 2, 'ANTI-BACTÉRIA', 'bac_20180917122036M8a0JxTd6M.png', '2018-09-17 15:18:56', '2018-09-17 15:20:36'),
(3, 3, 'ANTI-ACÁRO', 'acaro_20180917122017q6E9aFLAG5.png', '2018-09-17 15:19:14', '2018-09-17 15:20:17'),
(4, 4, 'NO TURN', 'no-turn_20180917122023NvxVjRM1V5.png', '2018-09-17 15:19:28', '2018-09-17 15:20:23');

-- --------------------------------------------------------

--
-- Estrutura da tabela `produtos_relacionados`
--

CREATE TABLE `produtos_relacionados` (
  `id` int(10) UNSIGNED NOT NULL,
  `produto_id` int(10) UNSIGNED NOT NULL,
  `relacionado_id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `produtos_relacionados`
--

INSERT INTO `produtos_relacionados` (`id`, `produto_id`, `relacionado_id`, `ordem`, `created_at`, `updated_at`) VALUES
(1, 49, 107, 0, '2018-11-05 22:57:14', '2018-11-05 22:57:14'),
(2, 49, 108, 0, '2018-11-05 22:58:10', '2018-11-05 22:58:10'),
(3, 49, 27, 0, '2018-11-05 23:00:51', '2018-11-05 23:00:51'),
(5, 49, 115, 0, '2018-11-05 23:02:53', '2018-11-05 23:02:53');

-- --------------------------------------------------------

--
-- Estrutura da tabela `produtos_secoes`
--

CREATE TABLE `produtos_secoes` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `produto_id` int(10) UNSIGNED DEFAULT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `produtos_secoes`
--

INSERT INTO `produtos_secoes` (`id`, `slug`, `titulo`, `produto_id`, `imagem`, `created_at`, `updated_at`) VALUES
(1, 'colchoes', 'Colchões', NULL, 'velvet_20181017204704556hRKJfyQ.jpeg', NULL, '2018-10-17 23:47:05'),
(2, 'bases', 'Bases', NULL, '', NULL, NULL),
(3, 'cabeceiras', 'Cabeceiras', NULL, '', NULL, NULL),
(4, 'sofa-cama-poltronas', 'Sofá-cama & Poltronas', NULL, '', NULL, NULL),
(5, 'bicama-dobraveis', 'Bicama & Dobráveis', NULL, '', NULL, NULL),
(6, 'moveis', 'Móveis', NULL, '', NULL, NULL),
(7, 'travesseiros-acessorios', 'Travesseiros & Acessórios', NULL, '', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `produtos_subcategorias`
--

CREATE TABLE `produtos_subcategorias` (
  `id` int(10) UNSIGNED NOT NULL,
  `secao_id` int(10) UNSIGNED NOT NULL,
  `categoria_id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `produto_propriedade`
--

CREATE TABLE `produto_propriedade` (
  `id` int(10) UNSIGNED NOT NULL,
  `produto_id` int(10) UNSIGNED NOT NULL,
  `propriedade_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `produto_propriedade`
--

INSERT INTO `produto_propriedade` (`id`, `produto_id`, `propriedade_id`, `created_at`, `updated_at`) VALUES
(2, 3, 2, NULL, NULL),
(3, 3, 3, NULL, NULL),
(4, 3, 4, NULL, NULL),
(6, 4, 2, NULL, NULL),
(7, 4, 3, NULL, NULL),
(8, 4, 4, NULL, NULL),
(10, 5, 2, NULL, NULL),
(11, 5, 3, NULL, NULL),
(12, 5, 4, NULL, NULL),
(14, 6, 2, NULL, NULL),
(15, 6, 3, NULL, NULL),
(16, 6, 4, NULL, NULL),
(18, 7, 2, NULL, NULL),
(19, 7, 3, NULL, NULL),
(20, 7, 4, NULL, NULL),
(21, 8, 1, NULL, NULL),
(22, 8, 2, NULL, NULL),
(23, 8, 3, NULL, NULL),
(24, 8, 4, NULL, NULL),
(37, 12, 1, NULL, NULL),
(38, 12, 2, NULL, NULL),
(39, 12, 3, NULL, NULL),
(40, 12, 4, NULL, NULL),
(41, 13, 1, NULL, NULL),
(42, 13, 2, NULL, NULL),
(43, 13, 3, NULL, NULL),
(44, 13, 4, NULL, NULL),
(46, 14, 2, NULL, NULL),
(47, 14, 3, NULL, NULL),
(49, 15, 2, NULL, NULL),
(50, 15, 3, NULL, NULL),
(55, 17, 1, NULL, NULL),
(56, 17, 2, NULL, NULL),
(57, 17, 3, NULL, NULL),
(58, 17, 4, NULL, NULL),
(59, 18, 1, NULL, NULL),
(60, 18, 2, NULL, NULL),
(61, 18, 3, NULL, NULL),
(62, 18, 4, NULL, NULL),
(63, 19, 1, NULL, NULL),
(64, 19, 2, NULL, NULL),
(65, 19, 3, NULL, NULL),
(66, 19, 4, NULL, NULL),
(67, 20, 1, NULL, NULL),
(68, 20, 2, NULL, NULL),
(69, 20, 3, NULL, NULL),
(70, 20, 4, NULL, NULL),
(75, 22, 1, NULL, NULL),
(76, 22, 2, NULL, NULL),
(77, 22, 3, NULL, NULL),
(78, 22, 4, NULL, NULL),
(79, 27, 1, NULL, NULL),
(80, 27, 2, NULL, NULL),
(81, 27, 3, NULL, NULL),
(82, 27, 4, NULL, NULL),
(88, 28, 1, NULL, NULL),
(89, 28, 2, NULL, NULL),
(90, 28, 3, NULL, NULL),
(91, 28, 4, NULL, NULL),
(92, 29, 1, NULL, NULL),
(93, 29, 2, NULL, NULL),
(94, 29, 3, NULL, NULL),
(95, 29, 4, NULL, NULL),
(101, 35, 2, NULL, NULL),
(102, 35, 3, NULL, NULL),
(103, 35, 4, NULL, NULL),
(112, 38, 1, NULL, NULL),
(113, 38, 2, NULL, NULL),
(114, 38, 3, NULL, NULL),
(115, 38, 4, NULL, NULL),
(116, 39, 1, NULL, NULL),
(117, 39, 2, NULL, NULL),
(118, 39, 3, NULL, NULL),
(119, 39, 4, NULL, NULL),
(124, 42, 1, NULL, NULL),
(125, 42, 2, NULL, NULL),
(126, 42, 3, NULL, NULL),
(127, 42, 4, NULL, NULL),
(136, 55, 1, NULL, NULL),
(137, 55, 2, NULL, NULL),
(138, 55, 3, NULL, NULL),
(139, 56, 1, NULL, NULL),
(140, 56, 2, NULL, NULL),
(141, 56, 3, NULL, NULL),
(142, 57, 1, NULL, NULL),
(143, 57, 2, NULL, NULL),
(144, 57, 3, NULL, NULL),
(145, 58, 1, NULL, NULL),
(146, 58, 2, NULL, NULL),
(147, 58, 3, NULL, NULL),
(148, 59, 1, NULL, NULL),
(149, 59, 2, NULL, NULL),
(150, 59, 3, NULL, NULL),
(151, 60, 1, NULL, NULL),
(152, 60, 2, NULL, NULL),
(153, 60, 3, NULL, NULL),
(162, 63, 3, NULL, NULL),
(163, 64, 1, NULL, NULL),
(164, 64, 2, NULL, NULL),
(165, 64, 3, NULL, NULL),
(171, 66, 3, NULL, NULL),
(172, 67, 1, NULL, NULL),
(173, 67, 2, NULL, NULL),
(174, 67, 3, NULL, NULL),
(175, 68, 1, NULL, NULL),
(176, 68, 2, NULL, NULL),
(177, 68, 3, NULL, NULL),
(178, 72, 3, NULL, NULL),
(179, 73, 3, NULL, NULL),
(182, 75, 3, NULL, NULL),
(183, 76, 2, NULL, NULL),
(184, 77, 3, NULL, NULL),
(185, 78, 2, NULL, NULL),
(186, 80, 1, NULL, NULL),
(187, 80, 2, NULL, NULL),
(188, 80, 3, NULL, NULL),
(189, 81, 1, NULL, NULL),
(190, 81, 2, NULL, NULL),
(191, 81, 3, NULL, NULL),
(192, 82, 1, NULL, NULL),
(193, 82, 2, NULL, NULL),
(194, 82, 3, NULL, NULL),
(195, 83, 1, NULL, NULL),
(196, 83, 2, NULL, NULL),
(197, 83, 3, NULL, NULL),
(201, 85, 3, NULL, NULL),
(202, 86, 1, NULL, NULL),
(203, 86, 2, NULL, NULL),
(204, 86, 3, NULL, NULL),
(208, 88, 1, NULL, NULL),
(209, 88, 2, NULL, NULL),
(210, 88, 3, NULL, NULL),
(214, 90, 1, NULL, NULL),
(215, 90, 2, NULL, NULL),
(216, 90, 3, NULL, NULL),
(217, 92, 1, NULL, NULL),
(218, 92, 2, NULL, NULL),
(219, 92, 3, NULL, NULL),
(220, 93, 1, NULL, NULL),
(221, 93, 2, NULL, NULL),
(222, 93, 3, NULL, NULL),
(223, 63, 2, NULL, NULL),
(227, 102, 2, NULL, NULL),
(228, 102, 3, NULL, NULL),
(229, 102, 4, NULL, NULL),
(230, 103, 2, NULL, NULL),
(231, 103, 3, NULL, NULL),
(232, 103, 4, NULL, NULL),
(242, 107, 2, NULL, NULL),
(243, 107, 3, NULL, NULL),
(244, 108, 2, NULL, NULL),
(245, 108, 3, NULL, NULL),
(246, 109, 2, NULL, NULL),
(247, 109, 3, NULL, NULL),
(248, 110, 2, NULL, NULL),
(249, 110, 3, NULL, NULL),
(250, 111, 2, NULL, NULL),
(251, 111, 3, NULL, NULL),
(252, 112, 1, NULL, NULL),
(253, 112, 2, NULL, NULL),
(254, 112, 3, NULL, NULL),
(255, 113, 1, NULL, NULL),
(256, 113, 2, NULL, NULL),
(257, 113, 3, NULL, NULL),
(260, 122, 2, NULL, NULL),
(261, 122, 3, NULL, NULL),
(262, 122, 4, NULL, NULL),
(263, 123, 2, NULL, NULL),
(264, 123, 3, NULL, NULL),
(265, 123, 4, NULL, NULL),
(266, 160, 1, NULL, NULL),
(267, 160, 2, NULL, NULL),
(268, 160, 3, NULL, NULL),
(269, 160, 4, NULL, NULL),
(270, 167, 4, NULL, NULL),
(275, 176, 1, NULL, NULL),
(276, 176, 2, NULL, NULL),
(277, 176, 3, NULL, NULL),
(278, 176, 4, NULL, NULL),
(283, 178, 1, NULL, NULL),
(284, 178, 2, NULL, NULL),
(285, 178, 3, NULL, NULL),
(286, 178, 4, NULL, NULL),
(287, 179, 1, NULL, NULL),
(288, 179, 2, NULL, NULL),
(289, 179, 3, NULL, NULL),
(290, 179, 4, NULL, NULL),
(291, 182, 1, NULL, NULL),
(292, 182, 2, NULL, NULL),
(293, 182, 3, NULL, NULL),
(294, 182, 4, NULL, NULL),
(295, 183, 1, NULL, NULL),
(296, 183, 2, NULL, NULL),
(297, 183, 3, NULL, NULL),
(298, 183, 4, NULL, NULL),
(299, 184, 1, NULL, NULL),
(300, 184, 2, NULL, NULL),
(301, 184, 3, NULL, NULL),
(302, 184, 4, NULL, NULL),
(303, 185, 1, NULL, NULL),
(304, 185, 2, NULL, NULL),
(305, 185, 3, NULL, NULL),
(306, 185, 4, NULL, NULL),
(307, 186, 1, NULL, NULL),
(308, 186, 2, NULL, NULL),
(309, 186, 3, NULL, NULL),
(310, 186, 4, NULL, NULL),
(311, 187, 1, NULL, NULL),
(312, 187, 2, NULL, NULL),
(313, 187, 3, NULL, NULL),
(314, 187, 4, NULL, NULL),
(319, 189, 1, NULL, NULL),
(320, 189, 2, NULL, NULL),
(321, 189, 3, NULL, NULL),
(322, 189, 4, NULL, NULL),
(323, 190, 1, NULL, NULL),
(324, 190, 2, NULL, NULL),
(325, 190, 3, NULL, NULL),
(326, 190, 4, NULL, NULL),
(327, 191, 1, NULL, NULL),
(328, 191, 2, NULL, NULL),
(329, 191, 3, NULL, NULL),
(330, 191, 4, NULL, NULL),
(331, 192, 1, NULL, NULL),
(332, 192, 2, NULL, NULL),
(333, 192, 3, NULL, NULL),
(334, 193, 1, NULL, NULL),
(335, 193, 2, NULL, NULL),
(336, 193, 3, NULL, NULL),
(337, 193, 4, NULL, NULL),
(338, 194, 1, NULL, NULL),
(339, 194, 2, NULL, NULL),
(340, 194, 3, NULL, NULL),
(341, 194, 4, NULL, NULL),
(342, 195, 1, NULL, NULL),
(343, 195, 2, NULL, NULL),
(344, 195, 3, NULL, NULL),
(345, 199, 1, NULL, NULL),
(346, 199, 2, NULL, NULL),
(347, 199, 3, NULL, NULL),
(348, 199, 4, NULL, NULL),
(349, 200, 1, NULL, NULL),
(350, 200, 2, NULL, NULL),
(351, 200, 3, NULL, NULL),
(352, 200, 4, NULL, NULL),
(353, 201, 1, NULL, NULL),
(354, 201, 2, NULL, NULL),
(355, 201, 3, NULL, NULL),
(356, 201, 4, NULL, NULL),
(357, 208, 3, NULL, NULL),
(358, 209, 3, NULL, NULL),
(359, 210, 1, NULL, NULL),
(360, 210, 2, NULL, NULL),
(361, 210, 3, NULL, NULL),
(362, 211, 1, NULL, NULL),
(363, 211, 2, NULL, NULL),
(364, 211, 3, NULL, NULL),
(365, 212, 1, NULL, NULL),
(366, 212, 2, NULL, NULL),
(367, 212, 3, NULL, NULL),
(368, 213, 1, NULL, NULL),
(369, 213, 2, NULL, NULL),
(370, 213, 3, NULL, NULL),
(371, 214, 1, NULL, NULL),
(372, 214, 2, NULL, NULL),
(373, 214, 3, NULL, NULL),
(374, 215, 1, NULL, NULL),
(375, 215, 2, NULL, NULL),
(376, 215, 3, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'trupe', 'contato@trupe.net', '$2y$10$k72gM.hkzhkqPmJKwF22jONpm6hwWEKLUnFDy1ghZ2jt8XKwB.KIS', 'JOon0pgQzwywOgYSFMwvmAk8w1IjnOvt7Pi46UGkPJcYfxKH8xJyIYsk2AAx', NULL, '2018-11-01 15:10:39'),
(2, 'colchaostore', 'colchaostore@colchaostore.com.br', '$2y$10$QxAC3ST4Jdko1fwiNshcmulMY4iC/v77aNnbrQbMZO/JIRZbX0EKy', 'MmQO4YuWVwzyVtqUmCIBAyg0fHpJDjsy8u2na3rsUDK0c9lO9MYFidyLitzN', '2018-08-27 23:22:46', '2018-11-01 16:18:45'),
(3, 'mariana', 'mariana@jabaquaracolchoes.com.br', '$2y$10$NZ8rR7Zdshl4d122haxq7eukUNH8VUsLrvSlIFZe2Um5ieevOcBT6', 'j8x4GtcEJBouoFUKwgkczTci9GuWrRBiSNZAKVfZt8VhEbYWaNe6UlWhFgvv', '2018-09-25 15:33:02', '2018-09-25 23:00:15');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `configuracoes`
--
ALTER TABLE `configuracoes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contato`
--
ALTER TABLE `contato`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contatos_recebidos`
--
ALTER TABLE `contatos_recebidos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `curriculos`
--
ALTER TABLE `curriculos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `frete`
--
ALTER TABLE `frete`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `newsletter`
--
ALTER TABLE `newsletter`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `newsletter_email_unique` (`email`);

--
-- Indexes for table `orcamentos`
--
ALTER TABLE `orcamentos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `paginas`
--
ALTER TABLE `paginas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `paginas_imagens`
--
ALTER TABLE `paginas_imagens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `paginas_imagens_pagina_id_foreign` (`pagina_id`);

--
-- Indexes for table `produtos`
--
ALTER TABLE `produtos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `produtos_marca_id_foreign` (`marca_id`),
  ADD KEY `produtos_categoria_id_foreign` (`categoria_id`),
  ADD KEY `produtos_subcategoria_id_foreign` (`subcategoria_id`);

--
-- Indexes for table `produtos_categorias`
--
ALTER TABLE `produtos_categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `produtos_destaques`
--
ALTER TABLE `produtos_destaques`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `produtos_destaques_produto_id_unique` (`produto_id`);

--
-- Indexes for table `produtos_imagens`
--
ALTER TABLE `produtos_imagens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `produtos_imagens_produto_id_foreign` (`produto_id`);

--
-- Indexes for table `produtos_marcas`
--
ALTER TABLE `produtos_marcas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `produtos_medidas`
--
ALTER TABLE `produtos_medidas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `produtos_medidas_produto_id_foreign` (`produto_id`);

--
-- Indexes for table `produtos_promocoes`
--
ALTER TABLE `produtos_promocoes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `produtos_promocoes_produto_id_unique` (`produto_id`);

--
-- Indexes for table `produtos_propriedades`
--
ALTER TABLE `produtos_propriedades`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `produtos_relacionados`
--
ALTER TABLE `produtos_relacionados`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `produtos_relacionados_produto_id_relacionado_id_unique` (`produto_id`,`relacionado_id`),
  ADD KEY `produtos_relacionados_relacionado_id_foreign` (`relacionado_id`);

--
-- Indexes for table `produtos_secoes`
--
ALTER TABLE `produtos_secoes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `produtos_secoes_slug_unique` (`slug`),
  ADD KEY `produtos_secoes_produto_id_foreign` (`produto_id`);

--
-- Indexes for table `produtos_subcategorias`
--
ALTER TABLE `produtos_subcategorias`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `produto_propriedade`
--
ALTER TABLE `produto_propriedade`
  ADD PRIMARY KEY (`id`),
  ADD KEY `produto_propriedade_produto_id_foreign` (`produto_id`),
  ADD KEY `produto_propriedade_propriedade_id_foreign` (`propriedade_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `configuracoes`
--
ALTER TABLE `configuracoes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `contato`
--
ALTER TABLE `contato`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `contatos_recebidos`
--
ALTER TABLE `contatos_recebidos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `curriculos`
--
ALTER TABLE `curriculos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `frete`
--
ALTER TABLE `frete`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `newsletter`
--
ALTER TABLE `newsletter`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `orcamentos`
--
ALTER TABLE `orcamentos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `paginas`
--
ALTER TABLE `paginas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `paginas_imagens`
--
ALTER TABLE `paginas_imagens`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `produtos`
--
ALTER TABLE `produtos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=216;

--
-- AUTO_INCREMENT for table `produtos_categorias`
--
ALTER TABLE `produtos_categorias`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `produtos_destaques`
--
ALTER TABLE `produtos_destaques`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;

--
-- AUTO_INCREMENT for table `produtos_imagens`
--
ALTER TABLE `produtos_imagens`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=231;

--
-- AUTO_INCREMENT for table `produtos_marcas`
--
ALTER TABLE `produtos_marcas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `produtos_medidas`
--
ALTER TABLE `produtos_medidas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=931;

--
-- AUTO_INCREMENT for table `produtos_promocoes`
--
ALTER TABLE `produtos_promocoes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `produtos_propriedades`
--
ALTER TABLE `produtos_propriedades`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `produtos_relacionados`
--
ALTER TABLE `produtos_relacionados`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `produtos_secoes`
--
ALTER TABLE `produtos_secoes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `produtos_subcategorias`
--
ALTER TABLE `produtos_subcategorias`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `produto_propriedade`
--
ALTER TABLE `produto_propriedade`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=377;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `paginas_imagens`
--
ALTER TABLE `paginas_imagens`
  ADD CONSTRAINT `paginas_imagens_pagina_id_foreign` FOREIGN KEY (`pagina_id`) REFERENCES `paginas` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `produtos`
--
ALTER TABLE `produtos`
  ADD CONSTRAINT `produtos_categoria_id_foreign` FOREIGN KEY (`categoria_id`) REFERENCES `produtos_categorias` (`id`),
  ADD CONSTRAINT `produtos_marca_id_foreign` FOREIGN KEY (`marca_id`) REFERENCES `produtos_marcas` (`id`),
  ADD CONSTRAINT `produtos_subcategoria_id_foreign` FOREIGN KEY (`subcategoria_id`) REFERENCES `produtos_subcategorias` (`id`) ON DELETE SET NULL;

--
-- Limitadores para a tabela `produtos_destaques`
--
ALTER TABLE `produtos_destaques`
  ADD CONSTRAINT `produtos_destaques_produto_id_foreign` FOREIGN KEY (`produto_id`) REFERENCES `produtos` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `produtos_imagens`
--
ALTER TABLE `produtos_imagens`
  ADD CONSTRAINT `produtos_imagens_produto_id_foreign` FOREIGN KEY (`produto_id`) REFERENCES `produtos` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `produtos_medidas`
--
ALTER TABLE `produtos_medidas`
  ADD CONSTRAINT `produtos_medidas_produto_id_foreign` FOREIGN KEY (`produto_id`) REFERENCES `produtos` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `produtos_promocoes`
--
ALTER TABLE `produtos_promocoes`
  ADD CONSTRAINT `produtos_promocoes_produto_id_foreign` FOREIGN KEY (`produto_id`) REFERENCES `produtos` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `produtos_relacionados`
--
ALTER TABLE `produtos_relacionados`
  ADD CONSTRAINT `produtos_relacionados_produto_id_foreign` FOREIGN KEY (`produto_id`) REFERENCES `produtos` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `produtos_relacionados_relacionado_id_foreign` FOREIGN KEY (`relacionado_id`) REFERENCES `produtos` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `produtos_secoes`
--
ALTER TABLE `produtos_secoes`
  ADD CONSTRAINT `produtos_secoes_produto_id_foreign` FOREIGN KEY (`produto_id`) REFERENCES `produtos` (`id`) ON DELETE SET NULL;

--
-- Limitadores para a tabela `produto_propriedade`
--
ALTER TABLE `produto_propriedade`
  ADD CONSTRAINT `produto_propriedade_produto_id_foreign` FOREIGN KEY (`produto_id`) REFERENCES `produtos` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `produto_propriedade_propriedade_id_foreign` FOREIGN KEY (`propriedade_id`) REFERENCES `produtos_propriedades` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
