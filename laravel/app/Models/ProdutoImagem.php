<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class ProdutoImagem extends Model
{
    protected $table = 'produtos_imagens';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeProduto($query, $id)
    {
        return $query->where('produto_id', $id);
    }

    public static function uploadImagem($campo = 'imagem')
    {
        return CropImage::make($campo, [
            [
                'width'  => 125,
                'height' => 110,
                'color'  => '#fff',
                'path'   => 'assets/img/produtos/imagens/thumbs/'
            ],
            [
                'width'  => 530,
                'height' => 450,
                'color'  => '#fff',
                'path'   => 'assets/img/produtos/imagens/'
            ],
            [
                'width'  => 1060,
                'height' => 900,
                'color'  => '#fff',
                'path'   => 'assets/img/produtos/imagens/zoom/'
            ]
        ]);
    }
}
