<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('frontend.common.template', function($view) {
            $view->with('config', \App\Models\Configuracoes::first());
            $view->with('contato', \App\Models\Contato::first());
            $view->with('secoes', \App\Models\ProdutoSecao::with('categorias', 'produto')->get());
            $view->with('paginas', \App\Models\Pagina::all());
            $view->with('marcas', \App\Models\ProdutoMarca::ordenados()->get());
        });

        view()->composer('painel.common.nav', function($view) {
            $view->with('secoes', \App\Models\ProdutoSecao::all());
            $view->with('contatosNaoLidos', \App\Models\ContatoRecebido::naoLidos()->count());
            $view->with('curriculosNaoLidos', \App\Models\Curriculo::naoLidos()->count());
            $view->with('orcamentosNaoLidos', \App\Models\Orcamento::naoLidos()->count());
            $view->with('countNewsletter', \App\Models\Newsletter::count());
        });

        $prefixos = ['g_', ''];
        shuffle($prefixos);
        view()->share('contato_prefixos', $prefixos);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
