<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
        $router->model('produtos', 'App\Models\Produto');
		$router->model('medidas', 'App\Models\ProdutoMedida');
		$router->model('relacionados', 'App\Models\ProdutoRelacionado');
		$router->model('imagens_produtos', 'App\Models\ProdutoImagem');
		$router->model('categorias', 'App\Models\ProdutoCategoria');
		$router->model('subcategorias', 'App\Models\ProdutoSubcategoria');
        $router->model('secoes', 'App\Models\ProdutoSecao');
		$router->model('marcas', 'App\Models\ProdutoMarca');
		$router->model('propriedades', 'App\Models\ProdutoPropriedade');
		$router->model('destaques', 'App\Models\ProdutoDestaque');
		$router->model('promocoes', 'App\Models\ProdutoPromocao');
		$router->model('banners', 'App\Models\Banner');
        $router->model('paginas', 'App\Models\Pagina');
        $router->model('paginas_imagens', 'App\Models\PaginaImagem');
        $router->model('frete', 'App\Models\Frete');
        $router->model('curriculos', 'App\Models\Curriculo');
        $router->model('newsletter', 'App\Models\Newsletter');
        $router->model('orcamentos', 'App\Models\Orcamento');
		$router->model('configuracoes', 'App\Models\Configuracoes');
        $router->model('recebidos', 'App\Models\ContatoRecebido');
        $router->model('contato', 'App\Models\Contato');
        $router->model('usuarios', 'App\Models\User');

        parent::boot($router);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
