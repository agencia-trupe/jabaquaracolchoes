<?php

if (! function_exists('dinheiro')) {
    function dinheiro($valor, $fallback = 'CONSULTE')
    {
        if (! $valor || $valor <= 0) return $fallback;

        return 'R$ ' . number_format($valor / 100, 2, ',', '.');
    }
}
