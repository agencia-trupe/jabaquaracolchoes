<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProdutosRelacionadosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'relacionado_id' => 'required'
        ];
    }

    public function attributes()
    {
        return ['relacionado_id' => 'produto relacionado'];
    }
}
