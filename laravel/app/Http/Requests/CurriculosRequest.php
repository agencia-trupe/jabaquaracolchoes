<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CurriculosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nome'             => 'required',
            'email'            => 'required|email',
            'cargo_pretendido' => 'required',
            'arquivo'          => 'required|max:4000|mimes:doc,docx,pdf'
        ];
    }

    public function messages()
    {
        return [
            'nome.required'     => 'Preencha seu nome',
            'email.required'    => 'Preencha seu e-mail',
            'email.email'       => 'Insira um e-mail válido',
            'cargo_pretendido.required' => 'Preencha o cargo pretendido',
            'arquivo.required'  => 'Insira um arquivo',
            'arquivo.mimes'     => 'O arquivo deve ser do tipo: DOC, DOCX ou PDF',
            'arquivo.max'       => 'O arquivo deve ter menos de 4MB'
        ];
    }
}
