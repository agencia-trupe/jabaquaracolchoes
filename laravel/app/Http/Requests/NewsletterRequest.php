<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class NewsletterRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nome'  => 'required',
            'email' => 'required|email|unique:newsletter,email'
        ];
    }

    public function messages()
    {
        return [
            'nome.required'  => 'preencha seu nome',
            'email.required' => 'preencha seu e-mail',
            'email.email'    => 'insira um e-mail válido',
            'email.unique'   => 'este e-mail já está cadastrado'
        ];
    }
}
