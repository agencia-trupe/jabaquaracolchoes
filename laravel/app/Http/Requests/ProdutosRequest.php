<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProdutosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'lojas' => 'required|array',
            'categoria_id' => 'required',
            'marca_id' => 'required',
            'titulo' => 'required',
            'capa' => 'required|image',
            'legenda' => 'required',
            'descricao' => 'required',
        ];

        if ($this->method() != 'POST') {
            $rules['capa'] = 'image';
        }

        return $rules;
    }

    public function attributes()
    {
        return [
            'categoria_id' => 'categoria',
            'marca_id' => 'marca'
        ];
    }
}
