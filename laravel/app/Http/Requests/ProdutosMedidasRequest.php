<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProdutosMedidasRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'titulo'    => 'required',
            'a_vista'   => 'required_with:oferta',
            'parcelas'  => 'integer|required_with:parcelado',
            'peso'      => 'required|integer|min:0',
            'cubagem'   => 'required|integer|min:0',
        ];
    }
}
