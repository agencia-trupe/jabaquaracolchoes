<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class OrcamentosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nome'  => 'required',
            'email' => 'required|email',
            'cep'   => 'required|regex:/^\d{5}-\d{3}$/',
        ];
    }

    public function messages()
    {
        return [
            'nome.required'  => 'Preencha seu nome',
            'email.required' => 'Preencha seu e-mail',
            'email.email'    => 'Insira um e-mail válido',
            'cep.required'   => 'Preencha seu CEP',
            'cep.regex'      => 'Insira um CEP válido',
        ];
    }
}
