<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ContatoRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'email|required',
            'telefone' => 'required',
            'whatsapp' => 'required',
            'endereco' => 'required',
            'google_maps' => 'required',
            'horario_de_atendimento' => 'required',
            'cnpj' => 'required',

            'g_email' => 'email|required',
            'g_telefone' => 'required',
            'g_whatsapp' => 'required',
            'g_endereco' => 'required',
            'g_google_maps' => 'required',
            'g_horario_de_atendimento' => 'required',
            'g_cnpj' => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'email'                    => '[jabaquara] e-mail',
            'telefone'                 => '[jabaquara] telefone',
            'whatsapp'                 => '[jabaquara] whatsapp',
            'endereco'                 => '[jabaquara] endereço',
            'google_maps'              => '[jabaquara] código google maps',
            'horario_de_atendimento'   => '[jabaquara] horário de atendimento',
            'cnpj'                     => '[jabaquara] cnpj',

            'g_email'                  => '[gravi] e-mail',
            'g_telefone'               => '[gravi] telefone',
            'g_whatsapp'               => '[gravi] whatsapp',
            'g_endereco'               => '[gravi] endereço',
            'g_google_maps'            => '[gravi] código google maps',
            'g_horario_de_atendimento' => '[gravi] horário de atendimento',
            'g_cnpj'                   => '[gravi] cnpj',
        ];
    }
}
