<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Models\Produto;
use App\Models\ProdutoSecao;

class ProdutosSecoesController extends Controller
{
    public function edit(ProdutoSecao $secao)
    {
        $produtos = Produto::secao($secao)->latest()->lists('titulo', 'id');

        return view('painel.produtos.secoes.edit', compact('secao', 'produtos'));
    }

    public function update(Request $request, ProdutoSecao $secao)
    {
        $this->validate(request(), [
            'imagem' => 'image'
        ]);

        try {

            $input = request()->except('back_to');

            if (isset($input['imagem'])) {
                $input['imagem'] = ProdutoSecao::upload_imagem();
            }
            if ($input['produto_id'] == '') {
                $input['produto_id'] = null;
            }

            $secao->update($input);

            return redirect()->route('painel.produtos.{secoes}.destaque.edit', [$secao, 'back_to' => $request->back_to])->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }
}
