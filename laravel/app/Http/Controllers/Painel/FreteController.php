<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Models\Frete;

class FreteController extends Controller
{
    public function index()
    {
        $registros = Frete::all();

        return view('painel.frete.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.frete.create');
    }

    protected function validateAllFieldsRequired()
    {
        return $this->validate(request(), array_map(function() {
            return 'required';
        }, request()->except('_token')));
    }

    public function store()
    {
        $this->validateAllFieldsRequired();

        try {

            $input = request()->all();

            Frete::create($input);

            return redirect()->route('painel.frete.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Frete $registro)
    {
        return view('painel.frete.edit', compact('registro'));
    }

    public function update(Frete $registro)
    {
        $this->validateAllFieldsRequired();

        try {

            $input = request()->all();

            $registro->update($input);

            return redirect()->route('painel.frete.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Frete $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.frete.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }
}
