<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\ProdutoPromocao;

class ProdutosPromocoesController extends Controller
{
    public function index()
    {
        $registros = ProdutoPromocao::with('produto')->ordenados()->get();

        return view('painel.produtos.promocoes.index', compact('registros'));
    }

    public function destroy(ProdutoPromocao $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.produtos.promocoes.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }
}
