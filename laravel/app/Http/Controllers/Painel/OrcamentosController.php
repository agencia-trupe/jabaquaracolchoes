<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Orcamento;

class OrcamentosController extends Controller
{
    public function index()
    {
        $orcamentos = Orcamento::orderBy('created_at', 'DESC');

        if ($loja = request('loja')) {
            $orcamentos = $orcamentos->whereLoja($loja);
        }

        $orcamentos = $orcamentos->paginate(15);

        return view('painel.orcamentos.index', compact('orcamentos'));
    }

    public function show(Orcamento $orcamento)
    {
        $orcamento->update(['lido' => 1]);

        return view('painel.orcamentos.show', compact('orcamento'));
    }

    public function destroy(Orcamento $orcamento)
    {
        try {

            $orcamento->delete();
            return back()->with('success', 'Orçamento excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir orçamento: '.$e->getMessage()]);

        }
    }

    public function toggle(Orcamento $orcamento, Request $request)
    {
        try {

            $orcamento->update([
                'lido' => !$orcamento->lido
            ]);

            return back()->with('success', 'Orçamento alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar orçamento: '.$e->getMessage()]);

        }
    }
}
