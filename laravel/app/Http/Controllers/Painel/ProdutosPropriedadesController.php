<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ProdutosPropriedadesRequest;
use App\Http\Controllers\Controller;

use App\Models\ProdutoPropriedade;

class ProdutosPropriedadesController extends Controller
{
    public function index()
    {
        $registros = ProdutoPropriedade::ordenados()->get();

        return view('painel.produtos.propriedades.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.produtos.propriedades.create');
    }

    public function store(ProdutosPropriedadesRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = ProdutoPropriedade::upload_imagem();

            ProdutoPropriedade::create($input);

            return redirect()->route('painel.produtos.propriedades.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(ProdutoPropriedade $registro)
    {
        return view('painel.produtos.propriedades.edit', compact('registro'));
    }

    public function update(ProdutosPropriedadesRequest $request, ProdutoPropriedade $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = ProdutoPropriedade::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.produtos.propriedades.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(ProdutoPropriedade $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.produtos.propriedades.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
