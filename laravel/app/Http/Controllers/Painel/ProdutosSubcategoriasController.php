<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ProdutosCategoriasRequest;
use App\Http\Controllers\Controller;

use App\Models\ProdutoSecao;
use App\Models\ProdutoCategoria;
use App\Models\ProdutoSubcategoria;

class ProdutosSubcategoriasController extends Controller
{
    public function index(ProdutoSecao $secao, ProdutoCategoria $categoria)
    {
        $registros = ProdutoSubcategoria::where('secao_id', $secao->id)
            ->where('categoria_id', $categoria->id)
            ->ordenados()->get();

        return view('painel.produtos.subcategorias.index', compact('registros', 'secao', 'categoria'));
    }

    public function create(ProdutoSecao $secao, ProdutoCategoria $categoria)
    {
        return view('painel.produtos.subcategorias.create', compact('secao', 'categoria'));
    }

    public function store(ProdutoSecao $secao, ProdutoCategoria $categoria, ProdutosCategoriasRequest $request)
    {
        try {

            $input = $request->except('back_to');
            $input['secao_id'] = $secao->id;
            $input['categoria_id'] = $categoria->id;

            ProdutoSubcategoria::create($input);

            return redirect()->route('painel.produtos.{secoes}.categorias.subcategorias.index', [$secao, $categoria, 'back_to' => $request->back_to])->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(ProdutoSecao $secao, ProdutoCategoria $categoria, ProdutoSubcategoria $registro)
    {
        return view('painel.produtos.subcategorias.edit', compact('secao', 'categoria', 'registro'));
    }

    public function update(ProdutoSecao $secao, ProdutoCategoria $categoria, ProdutoSubcategoria $registro, ProdutosCategoriasRequest $request)
    {
        try {

            $input = $request->except('back_to');

            $registro->update($input);

            return redirect()->route('painel.produtos.{secoes}.categorias.subcategorias.index', [$secao, $categoria, 'back_to' => $request->back_to])->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Request $request, ProdutoSecao $secao, ProdutoCategoria $categoria, ProdutoSubcategoria $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.produtos.{secoes}.categorias.subcategorias.index', [$secao, $categoria, 'back_to' => $request->back_to])->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
