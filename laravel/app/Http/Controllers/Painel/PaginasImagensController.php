<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ImagensRequest;
use App\Http\Controllers\Controller;

use App\Models\Pagina;
use App\Models\PaginaImagem;

class PaginasImagensController extends Controller
{
    public function index(Pagina $pagina)
    {
        $registros = $pagina->imagens;

        return view('painel.paginas.imagens.index', compact('pagina', 'registros'));
    }

    public function create(Pagina $pagina)
    {
        return view('painel.paginas.imagens.create', compact('pagina'));
    }

    public function store(Pagina $pagina, ImagensRequest $request)
    {
        try {

            $input = $request->all();

            if (request()->file('imagem')) {
                $input['imagem'] = PaginaImagem::upload_imagem();
            }

            $pagina->imagens()->create($input);

            return redirect()->route('painel.paginas.imagens.index', $pagina)->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Pagina $pagina, PaginaImagem $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.paginas.imagens.index', $pagina)->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
