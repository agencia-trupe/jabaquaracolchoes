<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Models\Curriculo;

class CurriculosController extends Controller
{
    public function index()
    {
        $curriculos = Curriculo::latest()->paginate(15);

        return view('painel.curriculos.index', compact('curriculos'));
    }

    public function show(Curriculo $curriculo)
    {
        $curriculo->marcaComoLido();

        $arquivo = public_path('curriculos/'.$curriculo->arquivo);

        return response()->download($arquivo);
    }

    public function destroy(Curriculo $curriculo)
    {
        try {

            $curriculo->delete();
            return redirect()->route('painel.curriculos.index')->with('success', 'Currículo excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir currículo: '.$e->getMessage()]);

        }
    }
}
