<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests\ProdutosMedidasRequest;

use App\Models\Produto;
use App\Models\ProdutoMedida;

class ProdutosMedidasController extends Controller
{
    public function index(Produto $produto)
    {
        $registros = $produto->medidas;

        return view('painel.produtos.medidas.index', compact('produto', 'registros'));
    }

    public function create(Produto $produto)
    {
        $secaoId = $produto->secaoModel->id;
        $medidas = ProdutoMedida::whereHas('produto', function($query) use ($secaoId) { $query->where('secao_id', $secaoId); })
            ->groupBy('titulo')
            ->orderBy('titulo', 'ASC')
            ->lists('titulo', 'titulo')
            ->map(function($medida) {
                return strip_tags($medida);
            });

        return view('painel.produtos.medidas.create', compact('produto', 'medidas'));
    }

    public function store(Produto $produto, ProdutosMedidasRequest $request)
    {
        try {

            $input = $request->except('medidas', 'back_to');

            $produto->medidas()->create($input);

            return redirect()->route('painel.produtos.medidas.index', [$produto, 'back_to' => $request->back_to])->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Produto $produto, ProdutoMedida $registro)
    {
        $secaoId = $produto->secaoModel->id;
        $medidas = ProdutoMedida::whereHas('produto', function($query) use ($secaoId) { $query->where('secao_id', $secaoId); })
            ->groupBy('titulo')
            ->orderBy('titulo', 'ASC')
            ->lists('titulo', 'titulo')
            ->map(function($medida) {
                return strip_tags($medida);
            });

        return view('painel.produtos.medidas.edit', compact('produto', 'registro', 'medidas'));
    }

    public function update(Produto $produto, ProdutoMedida $registro, ProdutosMedidasRequest $request)
    {
        try {

            $input = $request->except('medidas', 'back_to');

            $registro->update($input);

            return redirect()->route('painel.produtos.medidas.index', [$produto, 'back_to' => $request->back_to])->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Request $request, Produto $produto, ProdutoMedida $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.produtos.medidas.index', [$produto, 'back_to' => $request->back_to])->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }
}
