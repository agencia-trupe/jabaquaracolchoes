<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ProdutosMarcasRequest;
use App\Http\Controllers\Controller;

use App\Models\ProdutoMarca;

class ProdutosMarcasController extends Controller
{
    public function index()
    {
        $registros = ProdutoMarca::ordenados()->get();

        return view('painel.produtos.marcas.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.produtos.marcas.create');
    }

    public function store(ProdutosMarcasRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = ProdutoMarca::upload_imagem();

            ProdutoMarca::create($input);

            return redirect()->route('painel.produtos.marcas.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(ProdutoMarca $registro)
    {
        return view('painel.produtos.marcas.edit', compact('registro'));
    }

    public function update(ProdutosMarcasRequest $request, ProdutoMarca $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = ProdutoMarca::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.produtos.marcas.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(ProdutoMarca $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.produtos.marcas.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
