<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\ProdutoPromocao;
use App\Models\ProdutoMarca;
use App\Models\Produto;
use App\Models\ProdutoSecao;
use App\Models\ProdutoCategoria;
use App\Models\ProdutoSubcategoria;

use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

class ProdutosController extends Controller
{
    private function paginate($items, $perPage = 24, $page = null, $options = [])
    {
        $options = [
            'path'  => request()->url(),
            'query' => [
                'marca'    => request('marca'),
                'min'      => request('min'),
                'max'      => request('max'),
                'conforto' => request('conforto'),
                'ordem'    => request('ordem')
            ]
        ];
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }

    public function secao(ProdutoSecao $secao)
    {
        $produtos = Produto::ativos()->secao($secao)
            ->with('medidas', 'promocao');

        if (request('marca')) {
            $produtos = $produtos->where('marca_id', request('marca'));
        }

        $produtos = $produtos->ordenados()->get();

        if (request('min')) {
            $produtos = $produtos->filter(function($produto) {
                return $produto->menorValor() && $produto->menorValor() >= (request('min') * 100);
            });
        }
        if (request('max')) {
            $produtos = $produtos->filter(function($produto) {
                return $produto->menorValor() && $produto->menorValor() <= (request('max') * 100);
            });
        }

        if (request('conforto')) {
            $produtos = $produtos->where('conforto', (int) request('conforto'));
        }

        if ($ordem = request('ordem')) {
            if (strtolower($ordem) == 'asc') {
                $produtos = $produtos->sortBy(function($produto) {
                    return $produto->menorValor();
                });
            } elseif (strtolower($ordem) == 'desc') {
                $produtos = $produtos->sortByDesc(function($produto) {
                    return $produto->menorValor();
                });
            }
        }

        $produtos = $this->paginate($produtos);

        $categorias = ProdutoCategoria::where('secao_id', $secao->id)->with('subcategorias')->ordenados()->get();
        $marcas = ProdutoMarca::ordenados()->get();

        return view('frontend.produtos.lista', compact('secao', 'produtos', 'categorias', 'marcas'));
    }

    public function categoria(ProdutoSecao $secao, ProdutoCategoria $categoria)
    {
        if ($categoria->secao_id != $secao->id) {
            return abort('404');
        }

        $produtos = Produto::ativos()->secao($secao)
            ->where('categoria_id', $categoria->id)
            ->with('medidas', 'promocao');

        if (request('marca')) {
            $produtos = $produtos->where('marca_id', request('marca'));
        }

        $produtos = $produtos->ordenados()->get();

        if (request('min')) {
            $produtos = $produtos->filter(function($produto) {
                return $produto->menorValor() && $produto->menorValor() >= (request('min') * 100);
            });
        }
        if (request('max')) {
            $produtos = $produtos->filter(function($produto) {
                return $produto->menorValor() && $produto->menorValor() <= (request('max') * 100);
            });
        }

        if (request('conforto')) {
            $produtos = $produtos->where('conforto', (int) request('conforto'));
        }

        if ($ordem = request('ordem')) {
            if (strtolower($ordem) == 'asc') {
                $produtos = $produtos->sortBy(function($produto) {
                    return $produto->menorValor();
                });
            } elseif (strtolower($ordem) == 'desc') {
                $produtos = $produtos->sortByDesc(function($produto) {
                    return $produto->menorValor();
                });
            }
        }

        $produtos = $this->paginate($produtos);
        $categorias = ProdutoCategoria::where('secao_id', $secao->id)->with('subcategorias')->ordenados()->get();
        $marcas = ProdutoMarca::ordenados()->get();

        return view('frontend.produtos.lista', compact('secao', 'categoria', 'produtos', 'categorias', 'marcas'));
    }

    public function subcategoria(ProdutoSecao $secao, ProdutoCategoria $categoria, ProdutoSubcategoria $subcategoria)
    {
        if ($subcategoria->secao_id != $secao->id || $subcategoria->categoria_id != $categoria->id) {
            return abort('404');
        }

        $produtos = Produto::ativos()->secao($secao)
            ->where('categoria_id', $categoria->id)
            ->where('subcategoria_id', $subcategoria->id)
            ->with('medidas', 'promocao');

        if (request('marca')) {
            $produtos = $produtos->where('marca_id', request('marca'));
        }

        $produtos = $produtos->ordenados()->get();

        if (request('min')) {
            $produtos = $produtos->filter(function($produto) {
                return $produto->menorValor() && $produto->menorValor() >= (request('min') * 100);
            });
        }
        if (request('max')) {
            $produtos = $produtos->filter(function($produto) {
                return $produto->menorValor() && $produto->menorValor() <= (request('max') * 100);
            });
        }

        if (request('conforto')) {
            $produtos = $produtos->where('conforto', (int) request('conforto'));
        }

        if ($ordem = request('ordem')) {
            if (strtolower($ordem) == 'asc') {
                $produtos = $produtos->sortBy(function($produto) {
                    return $produto->menorValor();
                });
            } elseif (strtolower($ordem) == 'desc') {
                $produtos = $produtos->sortByDesc(function($produto) {
                    return $produto->menorValor();
                });
            }
        }

        $produtos = $this->paginate($produtos);
        $categorias = ProdutoCategoria::where('secao_id', $secao->id)->with('subcategorias')->ordenados()->get();
        $marcas = ProdutoMarca::ordenados()->get();

        return view('frontend.produtos.lista', compact('secao', 'categoria', 'subcategoria', 'produtos', 'categorias', 'marcas'));
    }

    public function marcas(ProdutoMarca $marca)
    {
        if (! $marca->exists) {
            $produtos = Produto::with('medidas', 'promocao')
                ->ativos()
                ->latest()->paginate(24);
            $marca = null;
        } else {
            $produtos = Produto::with('medidas', 'promocao')
                ->ativos()
                ->where('marca_id', $marca->id)
                ->latest()->paginate(24);
        }

        $marcas = ProdutoMarca::ordenados()->get();

        return view('frontend.produtos.marcas', compact('produtos', 'marcas', 'marca'));
    }

    public function busca()
    {
        $termo = request('termo');

        if ($termo) {
            $produtos = Produto::with('medidas', 'promocao')
                ->ativos()
                ->latest()->busca($termo)->get();

            return view('frontend.produtos.busca', compact('termo', 'produtos'));
        }

        return redirect()->route('home');
    }

    public function promocoes()
    {
        $promocoes = ProdutoPromocao::with('produto', 'produto.medidas', 'produto.promocao')
            ->whereHas('produto', function ($query) {
                $query->where('ativo', '=', 1);
            })->ordenados()->get();

        return view('frontend.produtos.promocoes', compact('promocoes'));
    }

    public function detalhe(Produto $produto)
    {
        if (! $produto->ativo) abort('404');

        return view('frontend.produtos.detalhe', compact('produto'));
    }
}
