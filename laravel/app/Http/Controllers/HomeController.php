<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use App\Http\Requests\NewsletterRequest;

use App\Models\Banner;
use App\Models\ProdutoDestaque;
use App\Models\ProdutoMarca;
use App\Models\Newsletter;

class HomeController extends Controller
{
    public function index()
    {
        $banners   = Banner::where('ativo', true)->ordenados()->get();
        $marcas    = ProdutoMarca::where('home', true)->ordenados()->get();
        $destaques = ProdutoDestaque::with('produto', 'produto.medidas', 'produto.promocao')
            ->ordenados()->paginate(4);

        return view('frontend.home', compact('banners', 'destaques', 'marcas'));
    }

    public function newsletter(NewsletterRequest $request)
    {
        Newsletter::create($request->all());

        return response()->json([
            'message' => 'cadastro efetuado com sucesso'
        ]);
    }
}
