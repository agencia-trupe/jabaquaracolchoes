<?php

namespace App\Http\Controllers;

use App\Http\Requests\CurriculosRequest;

use App\Models\Curriculo;
use App\Models\Contato;

use Illuminate\Support\Facades\Mail;
use App\Helpers\Tools;

class TrabalheConoscoController extends Controller
{
    public function index()
    {
        return view('frontend.trabalhe-conosco');
    }

    public function post(CurriculosRequest $request, Curriculo $curriculo)
    {
        $data = $request->all();
        $data['arquivo'] = Tools::fileUpload('arquivo', 'curriculos/');

        $curriculo->create($data);
        $this->sendMail($data);

        return back()->with('success', true);
    }

    private function sendMail($data)
    {
        if (! $email = Contato::first()->email) {
           return false;
        }

        Mail::send('emails.curriculo', $data, function($m) use ($email, $data)
        {
            $m->to($email, config('app.name'))
               ->subject('[CURRÍCULO] '.config('app.name'))
               ->replyTo($data['email'], $data['nome']);
        });
    }
}
