<?php

Route::group(['middleware' => ['web']], function () {
    // Páginas Institucionais
    Route::get('/', 'HomeController@index')->name('home');
    Route::post('newsletter', 'HomeController@newsletter')->name('newsletter');
    Route::get('pagina/{paginas}', 'PaginasController@index')->name('paginas');
    Route::get('lojas', 'LojaController@index')->name('loja');
    Route::get('trabalhe-conosco', 'TrabalheConoscoController@index')->name('trabalhe-conosco');
    Route::post('trabalhe-conosco', 'TrabalheConoscoController@post')->name('trabalhe-conosco.post');
    Route::get('fale-conosco', 'ContatoController@index')->name('fale-conosco');
    Route::post('fale-conosco', 'ContatoController@post')->name('fale-conosco.post');

    // Produtos
    Route::get('produtos/busca', 'ProdutosController@busca')->name('produtos.busca');
    Route::get('produtos/promocoes', 'ProdutosController@promocoes')->name('produtos.promocoes');
    Route::get('produtos/marcas/{marcas?}', 'ProdutosController@marcas')->name('produtos.marcas');
    Route::get('produtos/detalhe/{produtos}', 'ProdutosController@detalhe')->name('produtos.detalhe');
    Route::get('produtos/{secoes}', 'ProdutosController@secao')->name('produtos.secao');
    Route::get('produtos/{secoes}/{categorias}', 'ProdutosController@categoria')->name('produtos.categoria');
    Route::get('produtos/{secoes}/{categorias}/{subcategorias}', 'ProdutosController@subcategoria')->name('produtos.subcategoria');

    Route::group(['prefix' => 'orcamento'], function() {
        Route::get('/', 'OrcamentoController@index')->name('orcamento');
        Route::post('store', 'OrcamentoController@store')->name('orcamento.store');
        Route::post('update', 'OrcamentoController@update')->name('orcamento.update');
        Route::delete('destroy', 'OrcamentoController@destroy')->name('orcamento.destroy');
        Route::post('frete', 'OrcamentoController@frete')->name('orcamento.frete');
        Route::post('post', 'OrcamentoController@post')->name('orcamento.post');
    });


    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function() {
        Route::get('/', 'PainelController@index')->name('painel');

        /* GENERATED ROUTES */
        Route::resource('produtos.relacionados', 'ProdutosRelacionadosController', ['except' => ['edit', 'update']]);
        Route::resource('produtos.medidas', 'ProdutosMedidasController');
		Route::get('produtos/{produtos}/imagens/clear', [
			'as'   => 'painel.produtos.imagens.clear',
			'uses' => 'ProdutosImagensController@clear'
        ]);
        Route::resource('produtos.imagens', 'ProdutosImagensController', ['parameters' => ['imagens' => 'imagens_produtos']]);
        Route::resource('produtos/marcas', 'ProdutosMarcasController');
        Route::resource('produtos/propriedades', 'ProdutosPropriedadesController');
        Route::resource('produtos/destaques', 'ProdutosDestaquesController', [
            'only' => ['index', 'destroy']
        ]);
        Route::resource('produtos/promocoes', 'ProdutosPromocoesController', [
            'only' => ['index', 'destroy']
        ]);
        Route::get('produtos/{secoes}/destaque', 'ProdutosSecoesController@edit')->name('painel.produtos.{secoes}.destaque.edit');
        Route::match(['PUT', 'PATCH'], 'produtos/{secoes}/destaque', 'ProdutosSecoesController@update')->name('painel.produtos.{secoes}.destaque.update');
        Route::resource('produtos/{secoes}/categorias', 'ProdutosCategoriasController');
        Route::resource('produtos/{secoes}/categorias.subcategorias', 'ProdutosSubcategoriasController');
        Route::resource('produtos/{secoes}', 'ProdutosController', [
            'only' => ['index', 'create', 'store']
        ]);
        Route::get('produtos/{secoes}/{produtos}/edit', 'ProdutosController@edit')->name('painel.produtos.{secoes}.edit');
        Route::match(['PUT', 'PATCH'], 'produtos/{secoes}/{produtos}', 'ProdutosController@update')->name('painel.produtos.{secoes}.update');
        Route::delete('produtos/{secoes}/{produtos}', 'ProdutosController@destroy')->name('painel.produtos.{secoes}.destroy');

		Route::resource('banners', 'BannersController');
        Route::resource('paginas', 'PaginasController', [
            'only' => ['index', 'edit', 'update']
        ]);
        Route::resource('paginas.imagens', 'PaginasImagensController', [
            'except' => ['edit', 'update'],
            'parameters' => ['imagens' => 'paginas_imagens']
        ]);
        Route::resource('frete', 'FreteController');
        Route::resource('curriculos', 'CurriculosController', [
            'only' => ['index', 'show', 'destroy']
        ]);
        Route::resource('newsletter', 'NewsletterController', [
            'only' => ['index', 'destroy']
        ]);
        Route::get('newsletter/exportar', 'NewsletterController@exportar')
            ->name('painel.newsletter.exportar');

        Route::get('orcamentos/{orcamentos}/toggle', [
            'as' => 'painel.orcamentos.toggle',
            'uses' => 'OrcamentosController@toggle'
            ]);
        Route::resource('orcamentos', 'OrcamentosController');

        Route::get('contato/recebidos/{recebidos}/toggle', [
            'as' => 'painel.contato.recebidos.toggle',
            'uses' => 'ContatosRecebidosController@toggle'
            ]);
        Route::resource('contato/recebidos', 'ContatosRecebidosController');
        Route::resource('contato', 'ContatoController');
        Route::resource('configuracoes', 'ConfiguracoesController', [
            'only' => ['index', 'update']
        ]);
        Route::resource('usuarios', 'UsuariosController');

        Route::post('ckeditor-upload', 'PainelController@imageUpload');
        Route::post('order', 'PainelController@order');
        Route::get('fetch-subcategorias/{id}', function($id) {
            $categoria = \App\Models\ProdutoCategoria::findOrFail($id);
            return $categoria->subcategorias->lists('titulo', 'id');
        });
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

        Route::get('generator', 'GeneratorController@index')->name('generator.index');
        Route::post('generator', 'GeneratorController@submit')->name('generator.submit');
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function() {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });
});
