export default function Banners() {
    $('.banners-cycle').cycle({
        slides: '> .banner-slide',
        pager: '> .banners-cycle-pager',
        pagerTemplate: '<a href="#">{{slideNum}}</a>'
    });
};
