export default function FreteOrcamento() {
    $('.mask-cep').inputmask({
        mask: '99999-999',
        jitMasking: true
    });

    $('.input-frete').on('input', function(event) {
        var cep = $(this).val().replace('-', '');

        if (cep.length != 8 || !/^\d{8}/.test(cep)) {
            return;
        }

        var _this = $(this);

        _this.attr('disabled', true);
        $('.frete-resultado').remove();
        $('input[name=cep]').val('');

        $.post(_this.data('href'), {
            cep: _this.val()
        }).done(function (data) {
            if ('erro' in data) {
                _this.val('');
                return alert(data.erro);
            }

            $('.cotacao-frete').append(`
                <p class="frete-resultado">Frete: ${data.frete}</p>
            `);
            $('.orcamento table tfoot').append(`
                <tr class="frete-resultado">
                    <td colspan="4">Frete:</td>
                    <td>${data.frete}</td>
                </tr>
                <tr class="frete-resultado">
                    <td colspan="4">Total:</td>
                    <td>${data.total}</td>
                </tr>
            `);
            $('input[name=cep]').val(_this.val());
        }).always(function() {
            _this.attr('disabled', false);
        });
    });
};
