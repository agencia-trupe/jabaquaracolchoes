export default function ModalBusca() {
    $('.btn-busca').click(function(event) {
        event.preventDefault();

        $('.modal-busca').fadeIn(function() {
            $('.modal-busca input[type=text]').focus();
        });

        $('body').css('overflow', 'hidden');
    });

    $('.modal-busca').click(function(event) {
        event.preventDefault();

        $('.modal-busca').fadeOut();
        $('body').css('overflow', 'auto');
    });

    $('.modal-busca form').click(function(event) {
        event.stopPropagation();
    });
};
