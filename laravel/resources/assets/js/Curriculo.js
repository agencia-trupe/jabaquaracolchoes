export default function Curriculo() {
    var textoPadrao = $('.input-arquivo span').text();

    $('.input-arquivo input').change(function(event) {
        if (event.target.files[0]) {
            $('.input-arquivo span').text(event.target.files[0].name)
                .addClass('ativo');
        } else {
            $('.input-arquivo span').text(textoPadrao).removeClass('ativo');
        }
    });
};
