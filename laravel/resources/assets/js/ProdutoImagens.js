export default function ProdutoImagens() {
    var triggerZoom = function() {
        var zoomUrl = $('.imagem-zoom').data('zoom');
        $('.imagem-zoom').zoom({
            url: zoomUrl,
            target: '#image-zoom',
            onZoomIn: function() { $('#image-zoom').show(); },
            onZoomOut: function() { $('#image-zoom').hide(); }
        });
    };

    $('.imagens-nav-next').click(function(event) {
        event.preventDefault();

        var nextThumb = $('.imagens-thumbs a.active').next();

        if (nextThumb.length) {
            nextThumb.trigger('click');
        } else {
            $('.imagens-thumbs a').first().trigger('click');
        }
    });

    $('.imagens-nav-prev').click(function(event) {
        event.preventDefault();

        var prevThumb = $('.imagens-thumbs a.active').prev();

        if (prevThumb.length) {
            prevThumb.trigger('click');
        } else {
            $('.imagens-thumbs a').last().trigger('click');
        }
    });

    $('.imagens-thumbs a').click(function(event) {
        event.preventDefault();

        if ($(this).hasClass('active')) return;

        $('.imagens-thumbs a').removeClass('active');
        $(this).addClass('active');

        $('.imagem-zoom img').attr('src', $(this).attr('href'))
        $('.imagem-zoom')
            .data('zoom', $(this).data('zoom'))
            .trigger('zoom.destroy');

        triggerZoom();
    });

    triggerZoom();
};
