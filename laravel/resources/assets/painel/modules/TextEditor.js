const config = {
    padrao: {
        toolbar: [['FontSize'],['Bold', 'Italic'], ['TextColor']],
        extraPlugins: 'font,colorbutton,colordialog'
    },

    clean: {
        toolbar: [],
        removePlugins: 'toolbar,elementspath',
    },

    cleanBr: {
        toolbar: [],
        removePlugins: 'toolbar,elementspath',
        enterMode: CKEDITOR.ENTER_BR
    },

    medida: {
        toolbar: [],
        removePlugins: 'toolbar,elementspath',
        enterMode: CKEDITOR.ENTER_BR,
        height: 80
    },

    paginas: {
        toolbar: [
            ['Format'],
            ['Bold', 'Italic'],
            ['Link', 'Unlink'],
            ['TextColor']
        ],
        format_tags: 'h2;h3;p',
        height: 450,
        extraPlugins: 'colorbutton,colordialog',
        colorButton_enableMore: true
    }
};

export default function TextEditor() {
    CKEDITOR.config.language = 'pt-br';
    CKEDITOR.config.uiColor = '#dce4ec';
    CKEDITOR.config.contentsCss = [
        $('base').attr('href') + '/assets/ckeditor.css',
        CKEDITOR.config.contentsCss
    ];
    CKEDITOR.config.removePlugins = 'elementspath';
    CKEDITOR.config.resize_enabled = false;
    CKEDITOR.plugins.addExternal('injectimage', $('base').attr('href') + '/assets/injectimage/plugin.js');
    CKEDITOR.config.allowedContent = true;
    CKEDITOR.config.extraPlugins = 'injectimage';

    $('.ckeditor').each(function (i, obj) {
        CKEDITOR.replace(obj.id, config[obj.dataset.editor]);
    });
};
