export default function Mascaras() {
    $('.mascara-dinheiro').maskMoney({
        prefix: 'R$ ',
        decimal: ',',
        thousands: '.'
    });

    $('.mascara-dinheiro').each(function() {
        if (! $(this).val()) return;

        if ($(this).val() <= 0) {
            $(this).val('');
        } else {
            $(this).maskMoney('mask', $(this).val());
        }
    });
};
