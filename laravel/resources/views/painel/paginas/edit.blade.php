@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Páginas /</small> {{ $registro->titulo }}</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.paginas.update', $registro],
        'method' => 'patch',
        'files'  => true])
    !!}

        @include('painel.common.flash')

        <div class="form-group">
            {!! Form::label('texto', 'Texto') !!}
            {!! Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'paginas']) !!}
        </div>

        {!! Form::submit('Alterar', ['class' => 'btn btn-success']) !!}

        <a href="{{ route('painel.paginas.index') }}" class="btn btn-default btn-voltar">Voltar</a>

    {!! Form::close() !!}

@endsection
