@extends('painel.common.template')

@section('content')

    <legend>
        <h2>
            <small>Página / {{ $pagina->titulo }} / Imagens /</small>
            Adicionar Imagem
        </h2>
    </legend>

    {!! Form::open([
        'route' => ['painel.paginas.imagens.store', $pagina],
        'files' => true
    ]) !!}

        @include('painel.common.flash')

        <div class="well form-group">
            {!! Form::label('imagem', 'Imagem') !!}
            {!! Form::file('imagem', ['class' => 'form-control']) !!}
        </div>

        {!! Form::submit('Inserir', ['class' => 'btn btn-success']) !!}

        <a href="{{ route('painel.paginas.imagens.index', $pagina) }}" class="btn btn-default btn-voltar">Voltar</a>

    {!! Form::close() !!}

@endsection
