@include('painel.common.flash')

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/banners/'.$registro->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto_1', 'Texto 1') !!}
    {!! Form::text('texto_1', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto_2', 'Texto 2') !!}
    {!! Form::text('texto_2', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto_3', 'Texto 3') !!}
    {!! Form::text('texto_3', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('a_partir_de', 'A partir de') !!}
    {!! Form::text('a_partir_de', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('link', 'Link (incluir http://)') !!}
    {!! Form::text('link', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('ativo', 'Ativo') !!}<br>
    <input type="hidden" name="ativo" value="0">
    @if(!isset($registro))
        <input type="checkbox" name="ativo" value="1" checked>
    @else
        <input type="checkbox" name="ativo" value="1" @if($registro->ativo) checked @endif>
    @endif
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.banners.index') }}" class="btn btn-default btn-voltar">Voltar</a>
