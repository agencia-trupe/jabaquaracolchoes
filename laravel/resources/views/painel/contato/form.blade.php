@include('painel.common.flash')

<div class="row">
    <div class="col-md-6">
        <h3 class="alert alert-info" style="margin: 0 0 1em">Jabaquara</h3>

        <div class="form-group">
            {!! Form::label('email', 'E-mail') !!}
            {!! Form::email('email', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('telefone', 'Telefones (separados por vírgula)') !!}
            {!! Form::text('telefone', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('whatsapp', 'WhatsApp') !!}
            {!! Form::text('whatsapp', null, ['class' => 'form-control']) !!}
        </div>

        <hr>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    {!! Form::label('endereco', 'Endereço') !!}
                    {!! Form::textarea('endereco', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('google_maps', 'Código GoogleMaps') !!}
                    {!! Form::text('google_maps', null, ['class' => 'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('horario_de_atendimento', 'Horário de Atendimento') !!}
                    {!! Form::textarea('horario_de_atendimento', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
                </div>
            </div>
        </div>

        <hr>

        <div class="form-group">
            {!! Form::label('cnpj', 'CNPJ') !!}
            {!! Form::text('cnpj', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="col-md-6">
        <h3 class="alert alert-warning" style="margin: 0 0 1em">Gravi</h3>

        <div class="form-group">
            {!! Form::label('g_email', 'E-mail') !!}
            {!! Form::email('g_email', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('g_telefone', 'Telefones (separados por vírgula)') !!}
            {!! Form::text('g_telefone', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('g_whatsapp', 'WhatsApp') !!}
            {!! Form::text('g_whatsapp', null, ['class' => 'form-control']) !!}
        </div>

        <hr>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    {!! Form::label('g_endereco', 'Endereço') !!}
                    {!! Form::textarea('g_endereco', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('g_google_maps', 'Código GoogleMaps') !!}
                    {!! Form::text('g_google_maps', null, ['class' => 'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('g_horario_de_atendimento', 'Horário de Atendimento') !!}
                    {!! Form::textarea('g_horario_de_atendimento', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
                </div>
            </div>
        </div>

        <hr>

        <div class="form-group">
            {!! Form::label('g_cnpj', 'CNPJ') !!}
            {!! Form::text('g_cnpj', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<hr>

<div class="well">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('facebook', 'Facebook') !!}
                {!! Form::text('facebook', null, ['class' => 'form-control']) !!}
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('instagram', 'Instagram') !!}
                {!! Form::text('instagram', null, ['class' => 'form-control']) !!}
            </div>
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
