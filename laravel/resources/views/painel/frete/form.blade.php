@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('estado', 'Estado') !!}
    {!! Form::text('estado', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('cidade', 'Cidade') !!}
    {!! Form::text('cidade', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('de0_a_20', '0 a 20Kg') !!}
    {!! Form::text('de0_a_20', null, ['class' => 'form-control num-frete']) !!}
</div>

<div class="form-group">
    {!! Form::label('de20_a_50', '20 a 50Kg') !!}
    {!! Form::text('de20_a_50', null, ['class' => 'form-control num-frete']) !!}
</div>

<div class="form-group">
    {!! Form::label('de50_a_100', '50 a 100Kg') !!}
    {!! Form::text('de50_a_100', null, ['class' => 'form-control num-frete']) !!}
</div>

<div class="form-group">
    {!! Form::label('de100_a_150', '100 a 150Kg') !!}
    {!! Form::text('de100_a_150', null, ['class' => 'form-control num-frete']) !!}
</div>

<div class="form-group">
    {!! Form::label('de150_a_200', '150 a 200Kg') !!}
    {!! Form::text('de150_a_200', null, ['class' => 'form-control num-frete']) !!}
</div>

<div class="form-group">
    {!! Form::label('mais_de_200', 'Acima de 200Kg') !!}
    {!! Form::text('mais_de_200', null, ['class' => 'form-control num-frete']) !!}
</div>

<div class="form-group">
    {!! Form::label('ad', '% AD Valorem') !!}
    {!! Form::text('ad', null, ['class' => 'form-control num-frete']) !!}
</div>

<div class="form-group">
    {!! Form::label('pedagio', 'Pedágio') !!}
    {!! Form::text('pedagio', null, ['class' => 'form-control num-frete']) !!}
</div>

<div class="form-group">
    {!! Form::label('taxas_diversas', '% TX Diversas') !!}
    {!! Form::text('taxas_diversas', null, ['class' => 'form-control num-frete']) !!}
</div>

<div class="form-group">
    {!! Form::label('gris', '% GRIS') !!}
    {!! Form::text('gris', null, ['class' => 'form-control num-frete']) !!}
</div>

<div class="form-group">
    {!! Form::label('cep_de', 'CEP de') !!}
    {!! Form::text('cep_de', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('cep_ate', 'CEP até') !!}
    {!! Form::text('cep_ate', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('valor_isento', 'Valor para frete grátis') !!}
    {!! Form::text('valor_isento', null, ['class' => 'form-control num-frete']) !!}
</div>

<div class="form-group">
    {!! Form::label('coleta', 'Taxa de coleta') !!}
    {!! Form::text('coleta', null, ['class' => 'form-control num-frete']) !!}
</div>

<div class="form-group">
    {!! Form::label('divisao', 'Fator de divisão') !!}
    {!! Form::text('divisao', null, ['class' => 'form-control num-frete']) !!}
</div>

<div class="form-group">
    {!! Form::label('icms', 'ICMS') !!}
    {!! Form::select('icms', ['sim' => 'Sim', 'nao' => 'Não'], null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.frete.index') }}" class="btn btn-default btn-voltar">Voltar</a>
