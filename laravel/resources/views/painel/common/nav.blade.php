<ul class="nav navbar-nav">
	<li @if(Tools::routeIs('painel.banners*')) class="active" @endif>
		<a href="{{ route('painel.banners.index') }}">Banners</a>
	</li>
    <li class="dropdown @if(Tools::routeIs('painel.produtos*')) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Produtos
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            @foreach($secoes as $secao)
            <li @if(Route::current()->hasParameter('secoes') && Route::current()->parameter('secoes')->id == $secao->id) class="active" @endif>
                <a href="{{ route('painel.produtos.{secoes}.index', $secao) }}">{{ $secao->titulo }}</a>
            </li>
            @endforeach
            <li class="divider"></li>
            <li @if(Tools::routeIs('painel.produtos.marcas*')) class="active" @endif>
                <a href="{{ route('painel.produtos.marcas.index') }}">Marcas</a>
            </li>
            <li @if(Tools::routeIs('painel.produtos.propriedades*')) class="active" @endif>
                <a href="{{ route('painel.produtos.propriedades.index') }}">Propriedades</a>
            </li>
            <li class="divider"></li>
            <li @if(Tools::routeIs('painel.produtos.destaques*')) class="active" @endif>
                <a href="{{ route('painel.produtos.destaques.index') }}">Destaques</a>
            </li>
            <li @if(Tools::routeIs('painel.produtos.promocoes*')) class="active" @endif>
                <a href="{{ route('painel.produtos.promocoes.index') }}">Promoções</a>
            </li>
        </ul>
    </li>
	<li @if(Tools::routeIs('painel.paginas*')) class="active" @endif>
		<a href="{{ route('painel.paginas.index') }}">Páginas</a>
    </li>
    <li @if(Tools::routeIs('painel.frete*')) class="active" @endif>
		<a href="{{ route('painel.frete.index') }}">Frete</a>
    </li>
    <li class="dropdown @if(Tools::routeIs('painel.contato*')) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Contato
            @if($contatosNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
            @endif
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.contato.index')) class="active" @endif>
                <a href="{{ route('painel.contato.index') }}">Informações de Contato</a>
            </li>
            <li @if(Tools::routeIs('painel.contato.recebidos*')) class="active" @endif>
                <a href="{{ route('painel.contato.recebidos.index') }}">
                    Contatos Recebidos
                    @if($contatosNaoLidos >= 1)
                    <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
                    @endif
                </a>
            </li>
        </ul>
    </li>
    <li @if(Tools::routeIs('painel.curriculos*')) class="active" @endif>
        <a href="{{ route('painel.curriculos.index') }}">
            Currículos
            @if($curriculosNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $curriculosNaoLidos }}</span>
            @endif
        </a>
    </li>
    <li @if(Tools::routeIs('painel.newsletter*')) class="active" @endif>
        <a href="{{ route('painel.newsletter.index') }}">
            Newsletter
            @if($countNewsletter)
            <span class="label label-primary" style="background:rgba(255,255,255,.2);margin-left:3px">{{ $countNewsletter }}</span>
            @endif
        </a>
    </li>
    <li @if(Tools::routeIs('painel.orcamentos*')) class="active" @endif>
        <a href="{{ route('painel.orcamentos.index') }}">
            Orçamentos
            @if($orcamentosNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $orcamentosNaoLidos }}</span>
            @endif
        </a>
    </li>
</ul>
