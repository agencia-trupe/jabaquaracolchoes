@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Produtos / {{ $produto->secaoModel->titulo }} / {{ $produto->titulo }} /</small> Editar Medida</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.produtos.medidas.update', $produto, $registro],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.produtos.medidas.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
