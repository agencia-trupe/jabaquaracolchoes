@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Produtos / {{ $produto->secaoModel->titulo }} / {{ $produto->titulo }} /</small> Adicionar Medida</h2>
    </legend>

    {!! Form::open(['route' => ['painel.produtos.medidas.store', $produto], 'files' => true]) !!}

        @include('painel.produtos.medidas.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
