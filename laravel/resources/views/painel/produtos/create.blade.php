@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Produtos / {{ $secao->titulo }} /</small> Adicionar Produto</h2>
    </legend>

    {!! Form::open(['route' => ['painel.produtos.{secoes}.store', $secao], 'files' => true]) !!}

        @include('painel.produtos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
