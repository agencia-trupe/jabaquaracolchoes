@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            <small>Produtos /</small> {{ $secao->titulo }}
            <div class="btn-group pull-right">
                <a href="{{ route('painel.produtos.{secoes}.destaque.edit', [$secao, 'back_to' => Request::fullUrl()]) }}" class="btn btn-sm btn-warning"><div class="glyphicon glyphicon-star" style="margin-right:10px;"></div>Editar Destaque</a>
                <a href="{{ route('painel.produtos.{secoes}.categorias.index', [$secao, 'back_to' => Request::fullUrl()]) }}" class="btn btn-sm btn-info"><div class="glyphicon glyphicon-th-list" style="margin-right:10px;"></div>Editar Categorias</a>
                <a href="{{ route('painel.produtos.{secoes}.create', [$secao, 'back_to' => Request::fullUrl()]) }}" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Produto</a>
            </div>
        </h2>
    </legend>

    <div class="well">
        <div class="row">
            <form action="" method="GET">
                <div class="col-md-2">
                    {!! Form::select('ativo', ['true' => 'Ativos', 'false' => 'Inativos'], null, ['class' => 'form-control', 'placeholder' => 'Todos produtos']) !!}
                </div>
                <div class="col-md-2">
                    {!! Form::select('loja', Tools::lojas(), null, ['class' => 'form-control', 'placeholder' => 'Loja']) !!}
                </div>
                <div class="col-md-2">
                    {!! Form::select('marca', $marcas, null, ['class' => 'form-control', 'placeholder' => 'Marca']) !!}
                </div>
                <div class="col-md-2">
                    {!! Form::select('categoria', $categorias, null, ['class' => 'form-control js-categoria', 'placeholder' => 'Categoria']) !!}
                </div>
                <div class="col-md-2">
                    <select name="subcategoria" class="form-control js-subcategoria" @if(request('subcategoria')) data-selected="{{ request('subcategoria') }}" @endif @if(!request('subcategoria')) disabled @endif>
                        <option value="">Selecione</option>
                    </select>
                </div>
                <div class="col-md-2">
                    <button class="btn btn-block btn-success">
                        <span class="glyphicon glyphicon-search" style="margin-right:10px"></span>
                        Filtrar
                    </button>
                </div>
            </form>
        </div>
    </div>

    @if(!count($registros))
    <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="produtos">
        <thead>
            <tr>
                @if(! request('marca') && ! request('categoria') && ! request('subcategoria'))
                <th>Ordenar</th>
                @endif
                <th>Ativo</th>
                <th>Lojas</th>
                <th>Categoria</th>
                <th>Subcategoria</th>
                <th>Marca</th>
                <th>Título</th>
                <th>Medidas</th>
                <th>Imagens</th>
                <th>Relacionados</th>
                <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)
            <tr class="tr-row" id="{{ $registro->id }}">
                @if(! request('marca') && ! request('categoria') && ! request('subcategoria'))
                <td>
                    <a href="#" class="btn btn-info btn-sm btn-move">
                        <span class="glyphicon glyphicon-move"></span>
                    </a>
                </td>
                @endif
                <td style="text-align:center">
                    <span class="glyphicon {{ $registro->ativo ? 'text-success glyphicon-ok' : 'text-danger glyphicon-remove' }}"></span>
                </td>
                <td>
                    @foreach(explode(',', $registro->lojas) as $loja)
                    <span style="display:block">{{ Tools::lojas()[$loja] }}</span>
                    @endforeach
                </td>
                <td>{{ $registro->categoria->titulo }}</td>
                <td>{{ $registro->subcategoria ? $registro->subcategoria->titulo : '-' }}</td>
                <td>{{ $registro->marca->nome }}</td>
                <td>{{ $registro->titulo }}</td>
                <td>
                    <a href="{{ route('painel.produtos.medidas.index', [$registro, 'back_to' => Request::fullUrl()]) }}" class="btn btn-sm btn-info">
                        <span class="glyphicon glyphicon-fullscreen" style="margin-right:10px"></span>
                        Gerenciar
                    </a>
                </td>
                <td>
                    <a href="{{ route('painel.produtos.imagens.index', [$registro, 'back_to' => Request::fullUrl()]) }}" class="btn btn-sm btn-info">
                        <span class="glyphicon glyphicon-picture" style="margin-right:10px"></span>
                        Gerenciar
                    </a>
                </td>
                <td>
                    <a href="{{ route('painel.produtos.relacionados.index', [$registro, 'back_to' => Request::fullUrl()]) }}" class="btn btn-sm btn-info">
                        <span class="glyphicon glyphicon-th" style="margin-right:10px"></span>
                        Gerenciar
                    </a>
                </td>
                <td class="crud-actions" style="width:95px">
                    {!! Form::open([
                        'route'  => ['painel.produtos.{secoes}.destroy', $secao, $registro],
                        'method' => 'delete'
                    ]) !!}

                    <input type="hidden" name="back_to" value="{{ Request::fullUrl() }}">

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.produtos.{secoes}.edit', [$secao, $registro, 'back_to' => Request::fullUrl()]) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-pencil"></span>
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove"></span></button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif

@endsection
