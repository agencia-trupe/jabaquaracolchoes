@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Produtos / {{ $secao->titulo }} /</small> Editar Produto</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.produtos.{secoes}.update', $secao, $registro],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.produtos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
