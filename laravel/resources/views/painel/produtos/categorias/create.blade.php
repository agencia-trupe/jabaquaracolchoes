@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Produtos / {{ $secao->titulo }} / Categorias /</small> Adicionar Categoria</h2>
    </legend>

    {!! Form::open(['route' => ['painel.produtos.{secoes}.categorias.store', $secao], 'files' => true]) !!}

        @include('painel.produtos.categorias.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
