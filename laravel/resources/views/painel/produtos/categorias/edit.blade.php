@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Produtos / {{ $secao->titulo }} / Categorias /</small> Editar Categoria</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.produtos.{secoes}.categorias.update', $secao, $registro],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.produtos.categorias.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
