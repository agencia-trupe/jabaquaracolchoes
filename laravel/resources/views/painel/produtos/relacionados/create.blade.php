@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Produtos / {{ $produto->secaoModel->titulo }} / {{ $produto->titulo }} /</small> Adicionar Produto Relacionado</h2>
    </legend>

    {!! Form::open(['route' => ['painel.produtos.relacionados.store', $produto], 'files' => true]) !!}

        @include('painel.produtos.relacionados.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
