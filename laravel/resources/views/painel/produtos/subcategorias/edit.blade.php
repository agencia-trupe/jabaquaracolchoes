@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Produtos / {{ $secao->titulo }} / {{ $categoria->titulo }} / Subcategorias /</small> Editar Subcategoria</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.produtos.{secoes}.categorias.subcategorias.update', $secao, $categoria, $registro],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.produtos.subcategorias.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
