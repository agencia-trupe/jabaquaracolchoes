@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Produtos / {{ $secao->titulo }} / {{ $categoria->titulo }} / Subcategorias /</small> Adicionar Subcategoria</h2>
    </legend>

    {!! Form::open(['route' => ['painel.produtos.{secoes}.categorias.subcategorias.store', $secao, $categoria], 'files' => true]) !!}

        @include('painel.produtos.subcategorias.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
