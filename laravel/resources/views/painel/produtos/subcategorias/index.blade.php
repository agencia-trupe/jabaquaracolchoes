@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <a href="{{ route('painel.produtos.{secoes}.categorias.index', [$secao, 'back_to' => request('back_to')]) }}" class="btn btn-sm btn-default">&larr; Voltar para {{ $secao->titulo }} / Categorias</a>

    <legend>
        <h2>
            <small>Produtos / {{ $secao->titulo }} / {{ $categoria->titulo }} /</small>
            Subcategorias
            <a href="{{ route('painel.produtos.{secoes}.categorias.subcategorias.create', [$secao, $categoria, 'back_to' => request('back_to')]) }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Subcategoria</a>
        </h2>
    </legend>


    @if(!count($registros))
    <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="produtos_subcategorias">
        <thead>
            <tr>
                <th>Ordenar</th>
                <th>Título</th>
                <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)
            <tr class="tr-row" id="{{ $registro->id }}">
                <td>
                    <a href="#" class="btn btn-info btn-sm btn-move">
                        <span class="glyphicon glyphicon-move"></span>
                    </a>
                </td>
                <td>{{ $registro->titulo }}</td>
                <td class="crud-actions">
                    {!! Form::open([
                        'route'  => ['painel.produtos.{secoes}.categorias.subcategorias.destroy', $secao, $categoria, $registro],
                        'method' => 'delete'
                    ]) !!}

                    <input type="hidden" name="back_to" value="{{ request('back_to') }}">

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.produtos.{secoes}.categorias.subcategorias.edit', [$secao, $categoria, $registro, 'back_to' => request('back_to')]) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif

@endsection
