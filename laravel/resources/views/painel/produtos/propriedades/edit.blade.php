@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Produtos / Propriedades /</small> Editar Propriedade</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.produtos.propriedades.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.produtos.propriedades.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
