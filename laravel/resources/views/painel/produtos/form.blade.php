@include('painel.common.flash')

<input type="hidden" name="back_to" value="{{ request('back_to') }}">

<div class="well">
    <strong>Lojas</strong>
    <hr style="border-color:#ddd;margin:.5em 0 1em">

    @foreach(Tools::lojas() as $loja => $label)
    <div class="checkbox" style="margin:.5em 0 0">
        <label>
            <input type="checkbox" name="lojas[]" value="{{ $loja }}" @if(isset($registro) && str_contains($registro->lojas, $loja) || (count(old('lojas')) && in_array($loja, old('lojas')))) checked @endif>
            <span style="font-weight:bold">{{ $label }}</span>
        </label>
    </div>
    @endforeach
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('categoria_id', 'Categoria') !!}
            {!! Form::select('categoria_id', $categorias, null, ['class' => 'form-control js-categoria', 'placeholder' => 'Selecione']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('subcategoria_id', 'Subcategoria (opcional)') !!}
            <select name="subcategoria_id" class="form-control js-subcategoria" @if(isset($registro) && $registro->subcategoria) data-selected="{{ $registro->subcategoria->id }}" @endif @if(!isset($registro)) disabled @endif>
                <option value="">Selecione</option>
            </select>
        </div>
    </div>
</div>

<div class="form-group">
    {!! Form::label('marca_id', 'Marca') !!}
    {!! Form::select('marca_id', $marcas, null, ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('capa', 'Capa') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/produtos/capa/'.$registro->capa) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('capa', ['class' => 'form-control']) !!}
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('material', 'Material') !!}
            {!! Form::text('material', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('linha', 'Linha') !!}
            {!! Form::text('linha', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('legenda', 'Legenda') !!}
            {!! Form::text('legenda', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('codigo', 'Código') !!}
            {!! Form::text('codigo', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="form-group">
    {!! Form::label('conforto', 'Conforto') !!}
    {!! Form::select('conforto', \App\Models\Produto::TIPOS_CONFORTO, null, ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
</div>

<div class="form-group">
    {!! Form::label('resumo', 'Resumo') !!}
    {!! Form::textarea('resumo', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
</div>

<div class="form-group">
    {!! Form::label('descricao', 'Descrição') !!}
    {!! Form::textarea('descricao', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<div class="form-group">
    {!! Form::label('dados_complementares', 'Dados complementares') !!}
    {!! Form::textarea('dados_complementares', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<div class="well">
    {!! Form::label('propriedades', 'Propriedades') !!}
    <div class="row">
        @foreach($propriedades as $propriedade)
        <div class="col-md-3">
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="propriedades[]" value="{{ $propriedade->id }}" @if(isset($registro) && $registro->propriedades->contains($propriedade->id)) checked @endif @if(old('propriedades') && in_array($propriedade->id, old('propriedades'))) checked @endif>
                    {{ $propriedade->titulo }}
                </label>
            </div>
        </div>
        @endforeach
    </div>
</div>

<div class="form-group">
    {!! Form::label('ativo', 'Produto ativo (em exibição no site)') !!}<br>
    <input type="hidden" name="ativo" value="0">
    @if(!isset($registro))
        <input type="checkbox" name="ativo" value="1">
    @else
        <input type="checkbox" name="ativo" value="1" @if($registro->ativo) checked @endif>
    @endif
</div>

<hr>

<div class="form-group">
    {!! Form::label('destaque', 'Marcar como destaque') !!}<br>
    <input type="hidden" name="destaque" value="0">
    @if(!isset($registro))
        <input type="checkbox" name="destaque" value="1">
    @else
        <input type="checkbox" name="destaque" value="1" @if($registro->destaque) checked @endif>
    @endif
</div>

<div class="form-group">
    {!! Form::label('promocao', 'Marcar como promoção') !!}<br>
    <input type="hidden" name="promocao" value="0">
    @if(!isset($registro))
        <input type="checkbox" name="promocao" value="1">
    @else
        <input type="checkbox" name="promocao" value="1" @if($registro->promocao) checked @endif>
    @endif
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ request('back_to') ?: route('painel.produtos.{secoes}.index', $secao) }}" class="btn btn-default btn-voltar">Voltar</a>
