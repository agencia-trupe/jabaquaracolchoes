@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Produtos / {{ $secao->titulo }} /</small> Editar Destaque</h2>
    </legend>

    {!! Form::model($secao, [
        'route'  => ['painel.produtos.{secoes}.destaque.update', $secao],
        'method' => 'patch',
        'files'  => true])
    !!}

        @include('painel.common.flash')

        <input type="hidden" name="back_to" value="{{ request('back_to') }}">

        <div class="form-group">
            {!! Form::label('produto_id', 'Produto') !!}
            {!! Form::select('produto_id', $produtos, null, ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
        </div>

        <div class="well form-group">
            {!! Form::label('imagem', 'Imagem') !!}
        @if($secao->imagem)
            <img src="{{ url('assets/img/produtos/destaque-secao/'.$secao->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
        @endif
            {!! Form::file('imagem', ['class' => 'form-control']) !!}
        </div>

        {!! Form::submit('Alterar', ['class' => 'btn btn-success']) !!}

        <a href="{{ request('back_to') ?: route('painel.produtos.{secoes}.index', $secao) }}" class="btn btn-default btn-voltar">Voltar</a>

    {!! Form::close() !!}

@endsection
