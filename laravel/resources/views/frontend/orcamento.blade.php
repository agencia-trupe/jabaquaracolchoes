@extends('frontend.common.template')

@section('breadcrumb') Orçamento @endsection

@section('content')

    <div class="orcamento center">
        <h1>
            Meu Orçamento
            @if(count(session('orcamento', [])) && count($medidas))
                <a href="{{ route('home') }}" class="btn-escolher-mais">
                    Escolher mais produtos
                </a>
            @endif
        </h1>

        @if(count(session('orcamento', [])) && count($medidas))
            <div class="left">
                <div class="table-wrapper">
                    <table>
                        <thead>
                            <th colspan="2">Produto</th>
                            <th>Preço unitário</th>
                            <th>Qtde.</th>
                            <th>Subtotal</th>
                        </thead>
                        <tbody>
                            @foreach($medidas as $medida)
                            <tr>
                                <td>
                                    <a href="{{ route('produtos.detalhe', $medida->produto) }}">
                                        <img src="{{ asset('assets/img/produtos/capa/'.$medida->produto->capa) }}" alt="">
                                    </a>
                                </td>
                                <td>
                                    {{ $medida->produto->titulo }} -
                                    {{ strip_tags($medida->titulo) }}
                                    <form action="{{ route('orcamento.destroy') }}" method="POST">
                                        {!! csrf_field() !!}
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="medida" value="{{ $medida->id }}">
                                        <input type="submit" value="X">
                                    </form>
                                </td>
                                <td>{{ dinheiro($medida->valorParaCalculo()) }}</td>
                                <td>
                                    <form action="{{ route('orcamento.update') }}" method="POST">
                                        {!! csrf_field() !!}
                                        <input type="hidden" name="medida" value="{{ $medida->id }}">
                                        <input type="text" name="quantidade" value="{{ $medida->quantidade }}" pattern="[0-9]+" maxlength="3" required>
                                    </form>
                                </td>
                                <td>{{ dinheiro($medida->valorParaCalculo() * $medida->quantidade) }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="4">Total orçado:</td>
                                <td>{{ dinheiro($valorTotal) }}</td>
                            </tr>
                        </tfoot>
                    </table>
                </div>

                <div class="cotacao-frete">
                    <p>
                        Cotação de frete
                        <small>(apenas simulação, o valor pode variar para mais ou para menos):</small>
                        <input type="text" name="frete" class="input-frete mask-cep" placeholder="CEP" data-href="{{ route('orcamento.frete') }}">
                    </p>
                </div>
            </div>

            <form action="{{ route('orcamento.post') }}" method="POST" class="form-orcamento">
                {!! csrf_field() !!}
                <p>DADOS PARA RECEBER O ORÇAMENTO</p>
                <input type="text" name="nome" placeholder="nome" value="{{ old('nome') }}" required>
                <input type="email" name="email" placeholder="e-mail" value="{{ old('email') }}" required>
                <input type="text" name="telefone" placeholder="telefone" value="{{ old('telefone') }}">
                <input type="text" name="cep" placeholder="CEP" class="mask-cep" value="{{ old('cep') }}" required>
                <textarea name="mensagem" placeholder="mensagem">{{ old('mensagem') }}</textarea>
                <input type="submit" value="ENVIAR PEDIDO">
                @if($errors->any())
                <div class="erros">
                    @foreach($errors->all() as $error)
                    {!! $error !!}<br>
                    @endforeach
                </div>
                @endif
            </form>
        @else
            @if(session('success'))
                <p class="enviado">Orçamento enviado com sucesso!</p>
            @else
                <p class="produtos-nenhum">Nenhum produto adicionado ao pedido de orçamento.</p>
            @endif
        @endif
    </div>

@endsection
