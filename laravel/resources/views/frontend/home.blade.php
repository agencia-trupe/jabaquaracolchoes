@extends('frontend.common.template')

@section('content')

    <div class="marcas center">
        <div class="wrapper marcas-carousel">
            @foreach($marcas as $marca)
            <a href="{{ route('produtos.marcas', $marca) }}" class="marca">
                <img src="{{ asset('assets/img/produtos/marcas/'.$marca->imagem) }}" alt="">
            </a>
            @endforeach
        </div>
    </div>

    <div class="banners center">
        <div class="banners-cycle">
            @foreach($banners as $banner)
            <a @if($banner->link) href="{{ $banner->link }}" @endif class="banner-slide" style="background-image:url({{ asset('assets/img/banners/'.$banner->imagem) }})">
                @if($banner->texto_1 || $banner->texto_2 || $banner->texto_3 || $banner->a_partir_de)
                <div class="textos">
                    @if($banner->texto_1 || $banner->texto_2 || $banner->texto_3)
                    <div class="col">
                        @if($banner->texto_1)
                        <span class="texto-1">{{ $banner->texto_1 }}</span>
                        @endif
                        @if($banner->texto_2)
                        <span class="texto-2">{{ $banner->texto_2 }}</span>
                        @endif
                        @if($banner->texto_3)
                        <span class="texto-3">{{ $banner->texto_3 }}</span>
                        @endif
                    </div>
                    @endif
                    @if($banner->a_partir_de)
                    <div class="col">
                        <span class="a-partir">
                            a partir de
                            <strong>{{ $banner->a_partir_de }}</strong>
                        </span>
                    </div>
                    @endif
                    @if($banner->link)
                    <div class="col">
                        <div class="seta-link"></div>
                    </div>
                    @endif
                </div>
                @endif
            </a>
            @endforeach

            <div class="banners-cycle-pager"></div>
        </div>
    </div>

    <div class="destaques-home center">
        <h3><span>NOSSOS DESTAQUES</span></h3>
        <div class="produtos-list produtos-list-relacionados">
            @include('frontend.produtos._thumbs', [
                'produtos' => $destaques->map(function($destaque) {
                    return $destaque->produto;
                })
            ])
        </div>

        @if(count($destaques) && $destaques->hasMorePages())
        {!! $destaques->render() !!}
        <div class="load-next-wrapper">
            <div class="load-next">CARREGAR MAIS PRODUTOS</div>
        </div>
        @endif
    </div>

    <div class="newsletter center">
        <p>Cadastre-se para receber a nossa newsletter:</p>
        <form action="{{ route('newsletter') }}" method="POST" id="form-newsletter">
            <input type="text" name="newsletter_nome" placeholder="nome" required>
            <input type="email" name="newsletter_email" placeholder="e-mail" required>
            <input type="submit" value="ENVIAR">
            <div class="response"></div>
        </form>
    </div>

@endsection
