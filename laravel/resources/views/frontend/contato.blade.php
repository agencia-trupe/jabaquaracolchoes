@extends('frontend.common.template')

@section('breadcrumb') Fale conosco @endsection

@section('content')

    <div class="pagina-contato center">
        <div class="textos">
            <h3>FALE CONOSCO</h3>

            @foreach($contato_prefixos as $prefixo)
            <div class="col">
                <p class="telefone">
                    @foreach(explode(',', $contato->{$prefixo.'telefone'}) as $telefone)
                    <span>{{ trim($telefone) }}</span>
                    @endforeach
                </p>
                <a href="mailto:{{ $contato->{$prefixo.'email'} }}" class="email">
                    {{ $contato->{$prefixo.'email'} }}
                </a>
            </div>
            @endforeach
        </div>
        <form action="{{ route('fale-conosco.post') }}" method="POST">
            @if(session('success'))
                <p class="enviado">Mensagem enviada com sucesso!</p>
            @else
                {!! csrf_field() !!}
                <p>ENVIE SUA MENSAGEM</p>
                <input type="text" name="nome" placeholder="nome" value="{{ old('nome') }}" required>
                <input type="email" name="email" placeholder="e-mail" value="{{ old('email') }}" required>
                <input type="text" name="telefone" placeholder="telefone" value="{{ old('telefone') }}">
                <textarea name="mensagem" placeholder="mensagem" required>{{ old('mensagem') }}</textarea>
                <input type="submit" value="ENVIAR MENSAGEM">
                @if($errors->any())
                <div class="erros">
                    @foreach($errors->all() as $error)
                    {!! $error !!}<br>
                    @endforeach
                </div>
                @endif
            @endif
        </form>
    </div>

@endsection
