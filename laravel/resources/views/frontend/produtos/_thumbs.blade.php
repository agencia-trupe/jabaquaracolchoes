@foreach($produtos as $produto)
    <a href="{{ route('produtos.detalhe', $produto) }}" class="produto-thumb">
        @if($produto->material)
        <div class="material">
            <span>{{ $produto->material }}</span>
        </div>
        @endif
        @if($produto->promocao)
        <div class="promocao">PROMOÇÃO</div>
        @endif
        <div class="imagem">
            <img src="{{ asset('assets/img/produtos/capa/'.$produto->capa) }}" alt="">
            <div class="overlay"></div>
        </div>
        @if($produto->marca)
        <div class="marca">
            <span>{{ $produto->marca->nome }}</span>
        </div>
        @endif
        <div class="textos">
            @if($produto->linha)
            <p class="linha">{{ $produto->linha }}</p>
            @endif
            <p class="titulo">{{ $produto->titulo }}</p>
            <p class="legenda">{{ $produto->legenda }}</p>
            <p class="valor">
                a partir de:
                <strong>{{ dinheiro($produto->menorValor()) }}</strong>
            </p>
            <div class="mais-detalhes">
                <span>+ detalhes</span>
            </div>
        </div>
    </a>
@endforeach
