@extends('frontend.common.template')

@section('breadcrumb')

    @if(!isset($categoria))
        {{ $secao->titulo }}
    @else
        @if(!isset($subcategoria))
            <a href="{{ route('produtos.secao', $secao) }}">{{ $secao->titulo }}</a>
            <span>></span>
            {{ $categoria->titulo }}
        @else
            <a href="{{ route('produtos.secao', $secao) }}">{{ $secao->titulo }}</a>
            <span>></span>
            <a href="{{ route('produtos.categoria', [$secao, $categoria]) }}">{{ $categoria->titulo }}</a>
            <span>></span>
            {{ $subcategoria->titulo }}
        @endif
    @endif

@endsection

@section('content')

    <div class="produtos-sidebar center">
        <div class="produtos-filters">
            <div class="filters-desktop">
                <div class="filters-header">
                    Filtros de busca
                </div>
                <div class="filter-links">
                    <div class="filter-title">ORDENAR</div>
                    <select name="filtro_ordenar" class="filtro-select">
                        <option value="{{ str_replace('/?', '?', request()->fullUrlWithQuery(['ordem' => null])) }}">Selecione</option>
                        <option value="{{ str_replace('/?', '?', request()->fullUrlWithQuery(['ordem' => 'asc'])) }}" @if(request('ordem') == 'asc') selected @endif>Menor valor</option>
                        <option value="{{ str_replace('/?', '?', request()->fullUrlWithQuery(['ordem' => 'desc'])) }}" @if(request('ordem') == 'desc') selected @endif>Maior valor</option>
                    </select>
                </div>
                <div class="filter-links">
                    <div class="filter-title">CATEGORIA</div>
                    <a href="{{ route('produtos.secao', array_merge([$secao], $_GET)) }}" @if(!isset($categoria)) class="active" @endif>Todas as categorias</a>
                    @foreach($categorias as $cat)
                    <a href="{{ route('produtos.categoria', array_merge([$secao, $cat], request()->query())) }}" @if(isset($categoria) && $categoria->id == $cat->id) class="active" @endif>{{ $cat->titulo }}</a>
                    @if(count($cat->subcategorias))
                    <div class="filter-sub">
                        @foreach($cat->subcategorias as $sub)
                        <a href="{{ route('produtos.subcategoria', array_merge([$secao, $cat, $sub], request()->query())) }}" @if(isset($subcategoria) && $subcategoria->id == $sub->id) class="active" @endif>&raquo; {{ $sub->titulo }}</a>
                        @endforeach
                    </div>
                    @endif
                    @endforeach
                </div>
                <div class="filter-links">
                    <div class="filter-title">MARCA</div>
                    <a href="{{ str_replace('/?', '?', request()->fullUrlWithQuery(['marca' => null])) }}" @if(!request('marca')) class="active" @endif>Todas as marcas</a>
                    @foreach($marcas as $m)
                    <a href="{{ str_replace('/?', '?',request()->fullUrlWithQuery(['marca' => $m->id])) }}" @if(request('marca') == $m->id) class="active" @endif>
                        {{ $m->nome }}
                    </a>
                    @endforeach
                </div>
                <div class="filter-links">
                    <div class="filter-title">FAIXA DE PREÇO</div>
                    <form action="{{ request()->fullUrl() }}" method="GET" class="filter-form">
                        <input type="hidden" name="marca" value="{{ request('marca') }}">
                        <label>
                            A PARTIR DE
                            <input type="number" name="min" value="{{ request('min') }}" placeholder="R$">
                        </label>
                        <label>
                            MÁXIMO
                            <input type="number" name="max" value="{{ request('max') }}" placeholder="R$">
                        </label>
                        <input type="submit" value="Filtrar">
                    </form>
                </div>
                <div class="filter-links">
                    <div class="filter-title">CONFORTO</div>
                    <a href="{{ str_replace('/?', '?', request()->fullUrlWithQuery(['conforto' => null])) }}" @if(!request('conforto')) class="active" @endif>Todos</a>
                    @foreach(\App\Models\Produto::TIPOS_CONFORTO as $id => $titulo)
                    <a href="{{ str_replace('/?', '?',request()->fullUrlWithQuery(['conforto' => $id])) }}" @if(request('conforto') == $id) class="active" @endif>
                        {{ $titulo }}
                    </a>
                    @endforeach
                </div>
            </div>
            <div class="filters-mobile">
                <select name="filtro_ordenar" class="filtro-select">
                    <option value="{{ str_replace('/?', '?', request()->fullUrlWithQuery(['ordem' => null])) }}">Ordenar</option>
                    <option value="{{ str_replace('/?', '?', request()->fullUrlWithQuery(['ordem' => 'asc'])) }}" @if(request('ordem') == 'asc') selected @endif>Menor valor</option>
                    <option value="{{ str_replace('/?', '?', request()->fullUrlWithQuery(['ordem' => 'desc'])) }}" @if(request('ordem') == 'desc') selected @endif>Maior valor</option>
                </select>
                <select name="filtro_marcas" class="filtro-select">
                    <option value="{{ route('produtos.secao', array_merge([$secao], $_GET)) }}">Todas as categorias</option>
                    @foreach($categorias as $cat)
                    <option value="{{ route('produtos.categoria', array_merge([$secao, $cat], request()->query())) }}" @if(isset($categoria) && $categoria->id == $cat->id) selected @endif>{{ $cat->titulo }}</option>
                        @foreach($cat->subcategorias as $sub)
                        <option value="{{ route('produtos.subcategoria', array_merge([$secao, $cat, $sub], request()->query())) }}" @if(isset($subcategoria) && $subcategoria->id == $sub->id) selected @endif>&raquo; {{ $sub->titulo }}</option>
                        @endforeach
                    @endforeach
                </select>
                <select name="filtro_marcas" class="filtro-select">
                    <option value="{{ str_replace('/?', '?', request()->fullUrlWithQuery(['marca' => null])) }}">Todas as marcas</option>
                    @foreach($marcas as $m)
                    <option value="{{ str_replace('/?', '?', request()->fullUrlWithQuery(['marca' => $m->id])) }}" @if(request('marca') == $m->id) selected @endif>{{ $m->nome }}</option>
                    @endforeach
                </select>
                <form action="{{ request()->fullUrl() }}" class="filter-form" method="GET">
                    <input type="hidden" name="marca" value="{{ request('marca') }}">
                    <label>
                        A PARTIR DE
                        <input type="number" name="min" value="{{ request('min') }}" placeholder="R$">
                    </label>
                    <label>
                        MÁXIMO
                        <input type="number" name="max" value="{{ request('max') }}" placeholder="R$">
                    </label>
                    <input type="submit" value="Filtrar">
                </form>
                <select name="filtro_conforto" class="filtro-select" style="margin:4px 0 0">
                    <option value="{{ str_replace('/?', '?', request()->fullUrlWithQuery(['conforto' => null])) }}">Todos os tipos de conforto</option>
                    @foreach(\App\Models\Produto::TIPOS_CONFORTO as $id => $titulo)
                    <option value="{{ str_replace('/?', '?', request()->fullUrlWithQuery(['conforto' => $id])) }}" @if(request('conforto') == $id) selected @endif>{{ $titulo }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="produtos-list produtos-list-side">
            @if(count($produtos))
                @include('frontend.produtos._thumbs', compact('produtos'))
            @else
                <div class="produtos-nenhum">Nenhum produto encontrado.</div>
            @endif
        </div>

        @if(count($produtos) && $produtos->hasMorePages())
        {!! $produtos->links() !!}
        <div class="load-next-wrapper">
            <div class="load-next">CARREGAR MAIS PRODUTOS</div>
        </div>
        @endif
    </div>

@endsection
