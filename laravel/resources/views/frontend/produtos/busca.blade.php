@extends('frontend.common.template')

@section('breadcrumb') Resultados da Busca: {{ $termo }} @endsection

@section('content')

    <div class="center">
        @if(!count($produtos))
            <div class="produtos-nenhum">Nenhum produto encontrado.</div>
        @else
            <div class="produtos-list produtos-list-fullwidth">
                @include('frontend.produtos._thumbs', compact('produtos'))
            </div>
        @endif
    </div>

@endsection
