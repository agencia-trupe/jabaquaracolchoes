@extends('frontend.common.template')

@section('breadcrumb') Trabalhe conosco @endsection

@section('content')

    <div class="pagina-contato center">
        <div class="textos">
            <h3>TRABALHE CONOSCO</h3>
            <p>Envie seu currículo e faça parte da equipe Jabaquara Colchões.</p>
        </div>
        <form action="{{ route('trabalhe-conosco.post') }}" method="POST" enctype="multipart/form-data">
            @if(session('success'))
                <p class="enviado">Currículo enviado com sucesso!</p>
            @else
                {!! csrf_field() !!}
                <p>ENVIE SEU CURRÍCULO</p>
                <input type="text" name="nome" placeholder="nome" value="{{ old('nome') }}" required>
                <input type="email" name="email" placeholder="e-mail" value="{{ old('email') }}" required>
                <input type="text" name="telefone" placeholder="telefone" value="{{ old('telefone') }}">
                <input type="text" name="cargo_pretendido" placeholder="cargo pretendido" value="{{ old('cargo_pretendido') }}" required>
                <label class="input-arquivo">
                    <span>ANEXAR CURRÍCULO</span>
                    <input type="file" name="arquivo" required>
                </label>
                <input type="submit" value="ENVIAR CURRÍCULO">
                @if($errors->any())
                <div class="erros">
                    @foreach($errors->all() as $error)
                    {!! $error !!}<br>
                    @endforeach
                </div>
                @endif
            @endif
        </form>
    </div>

@endsection
