    <header>
        <div class="center">
            <div class="container">
                <a href="{{ route('home') }}" class="logo">{{ config('app.name') }}</a>

                <div class="conteudo">
                    <div class="informacoes">
                        <div class="telefone">
                            @foreach(explode(',', $contato->{$contato_prefixos[0].'telefone'}) as $telefone)
                            <span>{{ trim($telefone) }}</span>
                            @endforeach
                        </div>
                        <div class="social">
                            @if($contato->instagram)
                            <a href="{{ $contato->instagram }}" class="instagram" target="_blank">instagram</a>
                            @endif
                            @if($contato->facebook)
                            <a href="{{ $contato->facebook }}" class="facebook" target="_blank">facebook</a>
                            @endif
                        </div>
                    </div>
                    <div class="acoes">
                        <p class="slogan">DESDE 1970 CUIDANDO DOS SEUS SONHOS!</p>

                        <form action="{{ route('produtos.busca') }}" class="busca" method="GET">
                            <input type="text" name="termo" placeholder="buscar..." required>
                            <button type="submit" value="buscar"></button>
                        </form>

                        <div class="botoes">
                            <a href="{{ route('orcamento') }}" class="btn-carrinho">
                                <span>{{ count(session('orcamento', [])) }}</span>
                            </a>
                            <button id="mobile-toggle" type="button" role="button">
                                <span class="lines"></span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="secoes">
                @foreach($secoes as $secao)
                <div class="secoes-btn">
                    <a href="{{ route('produtos.secao', $secao) }}" @if(Route::current() && Route::current()->hasParameter('secoes') && is_object(Route::current()->parameter('secoes')) && Route::current()->parameter('secoes')->id == $secao->id) class="active" @endif>{{ $secao->titulo }}</a>
                    @if(count($secao->categorias))
                    <div class="secoes-dropdown">
                        <div class="categorias">
                            @foreach($secao->categorias as $cat)
                            <a href="{{ route('produtos.categoria', [$secao, $cat]) }}">{{ $cat->titulo }}</a>
                            @endforeach
                        </div>
                        @if($secao->produto)
                        <a href="{{ route('produtos.detalhe', $secao->produto) }}" class="produto-destaque">
                            <img src="{{ asset('assets/img/produtos/destaque-secao/'.$secao->imagem) }}" alt="">
                            <div class="textos">
                                <p class="linha">{{ $secao->produto->linha }}</p>
                                <p class="titulo">{{ $secao->produto->titulo }}</p>
                                <p class="legenda">{{ $secao->produto->legenda }}</p>
                                <p class="valor">
                                    a partir de:
                                    <strong>{{ dinheiro($secao->produto->menorValor()) }}</strong>
                                </p>
                            </div>
                        </a>
                        @endif
                    </div>
                    @endif
                </div>
                @endforeach

                <div class="secoes-btn">
                    <a href="{{ route('produtos.promocoes') }}" @if(Tools::routeIs('produtos.promocoes')) class="active" @endif>Promoções</a>
                </div>
                <div class="secoes-btn">
                    <a href="{{ route('produtos.marcas') }}" @if(Tools::routeIs('produtos.marcas')) class="active" @endif>Todas as Marcas</a>
                    @if(count($marcas))
                    <div class="secoes-dropdown">
                        <div class="marcas-dropdown">
                            @foreach($marcas as $marca)
                            <a href="{{ route('produtos.marcas', $marca) }}">{{ $marca->nome }}</a>
                            @endforeach
                        </div>
                    </div>
                    @endif
                </div>
            </div>

            <div id="nav-mobile">
                @foreach($secoes as $secao)
                <div>
                    <a href="{{ route('produtos.secao', $secao) }}">{{ $secao->titulo }}</a>
                    @if(count($secao->categorias))
                    <span class="mobile-cat-toggle"></span>
                    <div class="mobile-categorias">
                        @foreach($secao->categorias as $cat)
                        <a href="{{ route('produtos.categoria', [$secao, $cat]) }}">{{ $cat->titulo }}</a>
                        @endforeach
                    </div>
                    @endif
                </div>
                @endforeach
                <div>
                    <a href="{{ route('produtos.promocoes') }}">Promoções</a>
                </div>
                <div>
                    <a href="{{ route('produtos.marcas') }}">Todas as Marcas</a>
                    @if(count($marcas))
                    <span class="mobile-cat-toggle"></span>
                    <div class="mobile-categorias">
                        @foreach($marcas as $marca)
                        <a href="{{ route('produtos.marcas', $marca) }}">{{ $marca->nome }}</a>
                        @endforeach
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </header>

    @if(!Tools::routeIs('home') && !isset($errorPage))
        <div class="center">
            <div class="breadcrumb">
                <a href="{{ route('home') }}">Home</a>
                <span>></span>
                @yield('breadcrumb')
            </div>
        </div>
    @endif
