    <div class="center">
        <a href="#" class="btn-topo">TOPO</a>
    </div>
    <footer>
        <div class="center">
            <div class="links">
                <img src="{{ asset('assets/img/layout/marca-jabaquaracolchoes-rodape.png') }}" alt="">
                <div class="links-wrapper">
                    <div class="col">
                        <a href="{{ route('produtos.secao', 'colchoes') }}">Colchões</a>
                        <a href="{{ route('produtos.secao', 'bases') }}">Bases</a>
                        <a href="{{ route('produtos.secao', 'cabeceiras') }}">Cabeceiras</a>
                        <a href="{{ route('produtos.secao', 'sofa-cama-poltronas') }}">Sofá-cama & Poltronas</a>
                        <a href="{{ route('produtos.secao', 'bicama-dobraveis') }}">Bicama & Dobráveis</a>
                        <a href="{{ route('produtos.secao', 'moveis') }}">Móveis</a>
                        <a href="{{ route('produtos.secao', 'travesseiros-acessorios') }}">Travesseiros & Acessórios</a>
                        <a href="{{ route('produtos.promocoes') }}">Promoções</a>
                    </div>
                    <div class="col">
                        @foreach($paginas as $pagina)
                        <a href="{{ route('paginas', $pagina) }}">{{ $pagina->titulo }}</a>
                        @endforeach
                        <a href="{{ route('loja') }}">Lojas | Localização - mapa</a>
                        <a href="{{ route('trabalhe-conosco') }}">Trabalhe Conosco</a>
                        <a href="{{ route('fale-conosco') }}">Fale Conosco</a>

                        <div class="social">
                            @if($contato->instagram)
                            <a href="{{ $contato->instagram }}" class="instagram" target="_blank">instagram</a>
                            @endif
                            @if($contato->facebook)
                            <a href="{{ $contato->facebook }}" class="facebook" target="_blank">facebook</a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            @foreach($contato_prefixos as $prefixo)
            <div class="contato">
                <p class="telefone">
                    @foreach(explode(',', $contato->{$prefixo.'telefone'}) as $telefone)
                    <span>{{ trim($telefone) }}</span>
                    @endforeach
                </p>
                <p class="whatsapp">
                    Atendimento via whatsapp
                    <span>{{ $contato->{$prefixo.'whatsapp'} }}</span>
                </p>
                <p class="endereco">
                    {!! $contato->{$prefixo.'endereco'} !!}
                </p>
                <a href="mailto:{{ $contato->{$prefixo.'email'} }}" class="email">
                    {{ $contato->{$prefixo.'email'} }}
                </a>
                <p class="atendimento">
                    Horário de atendimento:<br>
                    {!! $contato->{$prefixo.'horario_de_atendimento'} !!}
                </p>
                <p class="grupo">
                    Empresa do grupo Jabaquara Colchões Ltda. |
                    CNPJ {{ $contato->{$prefixo.'cnpj'} }}
                </p>
            </div>
            @endforeach
        </div>
        <div class="center copyright">
            <p>
                © {{ date('Y') }} {{ config('app.name') }} - Todos os direitos reservados.
                <span>|</span>
                <a href="http://www.trupe.net" target="_blank">Criação de Sites</a>:
                <a href="http://www.trupe.net" target="_blank">Trupe Agência Criativa</a>
            </p>
        </div>
    </footer>
