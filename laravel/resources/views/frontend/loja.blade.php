@extends('frontend.common.template')

@section('breadcrumb') Lojas | Localização - mapa @endsection

@section('content')

    <div class="lojas center">
        <h1>LOJAS | LOCALIZAÇÃO - MAPA</h1>

        <div class="col-wrapper">

        @foreach($contato_prefixos as $prefixo)
            <div class="col">
                <p class="endereco">
                    {!! $contato->{$prefixo.'endereco'} !!}
                </p>
                <a href="mailto:{{ $contato->{$prefixo.'email'} }}" class="email">
                    {{ $contato->{$prefixo.'email'} }}
                </a>
                <p class="atendimento">
                    Horário de atendimento:<br>
                    {!! $contato->{$prefixo.'horario_de_atendimento'} !!}
                </p>
                <div class="mapa">{!! $contato->{$prefixo.'google_maps'} !!}</div>
            </div>
        @endforeach
        </div>
    </div>

@endsection
