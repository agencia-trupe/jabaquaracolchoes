@extends('frontend.common.template')

@section('breadcrumb') {{ $pagina->titulo }} @endsection

@section('content')

    <div class="pagina center">
        <div class="texto">
            <h1>{{ $pagina->titulo }}</h1>
            {!! $pagina->texto !!}
        </div>
        <div class="imagens">
            @foreach($pagina->imagens as $imagem)
            <img src="{{ asset('assets/img/paginas/imagens/'.$imagem->imagem) }}" alt="">
            @endforeach
        </div>
    </div>

@endsection
