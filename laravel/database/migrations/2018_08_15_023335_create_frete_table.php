<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFreteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('frete', function (Blueprint $table) {
            $table->increments('id');
            $table->string('estado');
            $table->string('cidade');
            $table->string('de0_a_20');
            $table->string('de20_a_50');
            $table->string('de50_a_100');
            $table->string('de100_a_150');
            $table->string('de150_a_200');
            $table->string('mais_de_200');
            $table->string('ad');
            $table->string('pedagio');
            $table->string('taxas_diversas');
            $table->string('gris');
            $table->string('cep_de');
            $table->string('cep_ate');
            $table->string('valor_isento');
            $table->string('coleta');
            $table->string('divisao');
            $table->string('icms');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('frete');
    }
}
