<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProdutosSubcategoriasTable extends Migration
{
    public function up()
    {
        Schema::create('produtos_subcategorias', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('secao_id')->unsigned();
            $table->integer('categoria_id')->unsigned();
            $table->integer('ordem')->default(0);
            $table->string('slug');
            $table->string('titulo');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('produtos_subcategorias');
    }
}
