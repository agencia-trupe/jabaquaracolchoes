<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAtivoToProdutosTable extends Migration
{
    public function up()
    {
        Schema::table('produtos', function($table) {
            $table->boolean('ativo')->default(1)->after('marca_id');
        });
    }

    public function down()
    {
        Schema::table('produtos', function($table) {
            $table->dropColumn('ativo');
        });
    }
}
