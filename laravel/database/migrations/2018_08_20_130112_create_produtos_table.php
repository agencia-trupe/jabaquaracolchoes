<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProdutosTable extends Migration
{
    public function up()
    {
        Schema::create('produtos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('secao_id')->unsigned();
            $table->integer('categoria_id')->unsigned();
            $table->integer('subcategoria_id')->unsigned()->nullable();
            $table->integer('marca_id')->unsigned();
            $table->string('slug');
            $table->string('titulo');
            $table->string('capa');
            $table->string('material');
            $table->string('linha');
            $table->string('legenda');
            $table->string('codigo');
            $table->text('resumo');
            $table->text('descricao');
            $table->text('dados_complementares');
            $table->timestamps();
            $table->foreign('marca_id')->references('id')->on('produtos_marcas');
            $table->foreign('categoria_id')->references('id')->on('produtos_categorias');
            $table->foreign('subcategoria_id')->references('id')->on('produtos_subcategorias')->onDelete('set null');
        });

        Schema::create('produtos_imagens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('produto_id')->unsigned();
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->timestamps();
            $table->foreign('produto_id')->references('id')->on('produtos')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('produtos_imagens');
        Schema::drop('produtos');
    }
}
