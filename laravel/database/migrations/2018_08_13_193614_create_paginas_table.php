<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaginasTable extends Migration
{
    public function up()
    {
        Schema::create('paginas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo');
            $table->string('slug');
            $table->text('texto');
            $table->timestamps();
        });

        Schema::create('paginas_imagens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pagina_id')->unsigned();
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->timestamps();

            $table->foreign('pagina_id')
                ->references('id')->on('paginas')
                ->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('paginas_imagens');
        Schema::drop('paginas');
    }
}
