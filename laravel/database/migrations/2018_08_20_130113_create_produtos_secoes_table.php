<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProdutosSecoesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produtos_secoes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug')->unique();
            $table->string('titulo');
            $table->integer('produto_id')->unsigned()->nullable();
            $table->string('imagem');
            $table->foreign('produto_id')->references('id')->on('produtos')->onDelete('set null');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('produtos_secoes');
    }
}
