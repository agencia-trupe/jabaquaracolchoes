<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProdutosMarcasTable extends Migration
{
    public function up()
    {
        Schema::create('produtos_marcas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('slug');
            $table->string('nome');
            $table->string('imagem');
            $table->boolean('home');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('produtos_marcas');
    }
}
