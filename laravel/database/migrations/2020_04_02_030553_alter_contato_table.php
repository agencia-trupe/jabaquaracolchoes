<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterContatoTable extends Migration
{
    public function up()
    {
        Schema::table('contato', function(Blueprint $table) {
            $table->string('cnpj')->after('horario_de_atendimento');

            $table->string('g_email');
            $table->string('g_telefone');
            $table->string('g_whatsapp');
            $table->text('g_endereco');
            $table->text('g_google_maps');
            $table->text('g_horario_de_atendimento');
            $table->string('g_cnpj');

            $table->dropColumn('imagem');
        });
    }

    public function down()
    {
        Schema::table('contato', function(Blueprint $table) {
            $table->dropColumn('cnpj');

            $table->dropColumn('g_email');
            $table->dropColumn('g_telefone');
            $table->dropColumn('g_whatsapp');
            $table->dropColumn('g_endereco');
            $table->dropColumn('g_google_maps');
            $table->dropColumn('g_horario_de_atendimento');
            $table->dropColumn('g_cnpj');

            $table->string('imagem')->after('instagram');
        });
    }
}
