<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProdutosTable extends Migration
{
    public function up()
    {
        Schema::table('produtos', function(Blueprint $table) {
            $table
                ->string('lojas')
                ->after('marca_id')
                ->default('jabaquara,gravi');
        });
    }

    public function down()
    {
        Schema::table('produtos', function(Blueprint $table) {
            $table->dropColumn('lojas');
        });
    }
}
