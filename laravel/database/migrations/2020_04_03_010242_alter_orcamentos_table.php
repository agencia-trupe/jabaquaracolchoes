<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterOrcamentosTable extends Migration
{
    public function up()
    {
        Schema::table('orcamentos', function(Blueprint $table) {
            $table->string('loja')->after('lido');
            $table->boolean('tie')->default(false)->after('loja');
        });
    }

    public function down()
    {
        Schema::table('orcamentos', function(Blueprint $table) {
            $table->dropColumn('loja');
            $table->dropColumn('tie');
        });
    }
}
