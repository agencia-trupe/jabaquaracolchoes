<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBannersTable extends Migration
{
    public function up()
    {
        Schema::create('banners', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->string('texto_1');
            $table->string('texto_2');
            $table->string('texto_3');
            $table->string('a_partir_de');
            $table->string('link');
            $table->boolean('ativo');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('banners');
    }
}
